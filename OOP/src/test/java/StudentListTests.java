import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentListTests {

    @Test
    void createStudentList_NoStudents() {
        StudentList stList = new StudentList();
        Student[] result = stList.toArray();
        assertEquals(0, result.length);
    }

    @Test
    void createStudentList_StudentsNull() {
        Student[] students = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new StudentList(students));
        String expectedMessage = "Students array should not be null";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    void createStudentList_OneStudent() {
        Student s1 = new Student(1234567, "Pedro");
        Student[] students = {s1};
        Student[] expected = {s1};
        StudentList stList = new StudentList(students);
        Student[] result = stList.toArray();
        assertArrayEquals(result, expected);
        assertNotSame(result, students);
    }

    @Test
    void createStudentList_SomeStudents() {
        Student s1 = new Student(1234567, "Pedro");
        Student s2 = new Student(7654321, "Joana");
        Student s3 = new Student(5406398, "Maria");
        Student[] students = {s1, s2, s3};
        Student[] expected = {s1, s2, s3};
        StudentList stList = new StudentList(students);
        Student[] result = stList.toArray();
        assertArrayEquals(result, expected);
        assertNotSame(result, students);
    }

    @Test
    void sortByNumberAsc_ListIncorrectlyOrdered() {
        Student s1 = new Student(1000000, "Pedro");
        Student s2 = new Student(1000001, "Joana");
        Student s3 = new Student(1000002, "Maria");
        Student[] students = {s3, s2, s1};
        Student[] expected = {s1, s2, s3};
        StudentList stList = new StudentList(students);
        stList.sortByNumberAsc();
        Student[] result = stList.toArray();
        assertArrayEquals(expected, result); // check students is sorted
    }

    @Test
    void sortByNumberAsc_ListAlreadyOrdered() {
        Student s1 = new Student(1000000, "Pedro");
        Student s2 = new Student(1000001, "Joana");
        Student s3 = new Student(1000002, "Maria");
        Student[] students = {s1, s2, s3};
        Student[] expected = {s1, s2, s3};
        StudentList stList = new StudentList(students);
        stList.sortByNumberAsc();
        Student[] result = stList.toArray();
        assertArrayEquals(expected, result); // check students is sorted
        assertNotSame(students, result);
    }

    @Test
    void sortByGradeDesc_ListIncorrectlyOrdered() {
        Student s1 = new Student(1000000, "Pedro");
        s1.doEvaluation(20);
        Student s2 = new Student(1000001, "Joana");
        s2.doEvaluation(11);
        Student s3 = new Student(1000002, "Maria");
        s3.doEvaluation(3);
        Student[] students = {s3, s2, s1};
        Student[] expected = {s1, s2, s3};
        StudentList stList = new StudentList(students);
        stList.sortByGradeDesc();
        Student[] result = stList.toArray();
        assertArrayEquals(expected, result); // check students is sorted
    }

    @Test
    void sortByGradeDesc_ListAlreadyOrdered() {
        Student s1 = new Student(1000000, "Pedro");
        s1.doEvaluation(20);
        Student s2 = new Student(1000001, "Joana");
        s2.doEvaluation(11);
        Student s3 = new Student(1000002, "Maria");
        s3.doEvaluation(3);
        Student[] students = {s1, s2, s3};
        Student[] expected = {s1, s2, s3};
        StudentList stList = new StudentList(students);
        stList.sortByGradeDesc();
        Student[] result = stList.toArray(); // return a copy of students list
        assertArrayEquals(expected, result); // check students is sorted
        assertNotSame(students, result); // check if result output is not the same as students input
    }

    @Test
    void addNewStudent() {
        Student s1 = new Student(1000000, "Pedro");
        Student s2 = new Student(1000001, "Joana");
        Student s3 = new Student(1000002, "Maria");
        Student[] students = {s1, s2};
        Student[] expected = {s1, s2, s3};
        StudentList stList = new StudentList(students);
        boolean result = stList.add(s3);
        Student[] content = stList.toArray();
        assertTrue(result);
        assertArrayEquals(expected, content);
    }

    @Test
    void addSameStudent() {
        Student s1 = new Student(1000000, "Pedro");
        Student[] students = {s1};
        Student[] expected = {s1};
        StudentList stList = new StudentList(students);
        boolean result = stList.add(s1);
        Student[] content = stList.toArray();
        assertFalse(result);
        assertArrayEquals(expected, content);
        assertNotSame(students, content);
    }

    @Test
    void addStudentWithSameNumber() {
        Student s1 = new Student(1000000, "Pedro");
        Student s2 = new Student(1000000, "Maria");
        Student[] students = {s1};
        Student[] expected = {s1};
        StudentList stList = new StudentList(students);
        boolean result = stList.add(s2);
        Student[] content = stList.toArray();
        assertFalse(result);
        assertArrayEquals(expected, content);
        assertNotSame(students, content);
    }

    @Test
    void addNull() {
        Student s1 = new Student(1000000, "Pedro");
        Student[] students = {s1};
        Student[] expected = {s1};
        StudentList stList = new StudentList(students);
        boolean result = stList.add(null);
        Student[] content = stList.toArray();
        assertFalse(result);
        assertArrayEquals(expected, content);
        assertNotSame(students, content);
    }

    @Test
    void removeStudentInTheMiddle() {
        Student s1 = new Student(1234567, "Pedro");
        Student s2 = new Student(7654321, "Joana");
        Student s3 = new Student(5406398, "Maria");
        Student[] students = {s1, s2, s3};
        Student[] expected = {s1, s3};
        StudentList stList = new StudentList(students);
        boolean result = stList.remove(s2);
        Student[] content = stList.toArray();
        assertTrue(result);
        assertArrayEquals(expected, content);
    }

    @Test
    void removeStudentAtTheBeginning() {
        Student s1 = new Student(1234567, "Pedro");
        Student s2 = new Student(7654321, "Joana");
        Student s3 = new Student(5406398, "Maria");
        Student[] students = {s1, s2, s3};
        Student[] expected = {s2, s3};
        StudentList stList = new StudentList(students);
        boolean result = stList.remove(s1);
        Student[] content = stList.toArray();
        assertTrue(result);
        assertArrayEquals(expected, content);
    }

    @Test
    void removeStudentAtTheEnd() {
        Student s1 = new Student(1234567, "Pedro");
        Student s2 = new Student(7654321, "Joana");
        Student s3 = new Student(5406398, "Maria");
        Student[] students = {s1, s2, s3};
        Student[] expected = {s1, s2};
        StudentList stList = new StudentList(students);
        boolean result = stList.remove(s3);
        Student[] content = stList.toArray();
        assertTrue(result);
        assertArrayEquals(expected, content);
    }

    @Test
    void removeSameStudentTwice() {
        Student s1 = new Student(1234567, "Pedro");
        Student s2 = new Student(7654321, "Joana");
        Student[] students = {s1, s2};
        Student[] expected = {s1};
        StudentList stList = new StudentList(students);
        boolean result1 = stList.remove(s2);
        boolean result2 = stList.remove(s2);
        Student[] content = stList.toArray();
        assertTrue(result1);
        assertFalse(result2);
        assertArrayEquals(expected, content);
    }
}