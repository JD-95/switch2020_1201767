import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RetanguloTestes {

    @Test
    void obterPerimetro() {
        int largura = 1;
        int comprimento = 2;
        Retangulo retanguloA = new Retangulo(largura, comprimento);
        double esperado = 6;
        double resultado = retanguloA.obterPerimetro();
        assertEquals(esperado, resultado);
    }

    @Test
    void obterPerimetro_ExcecaoLargura() {
        int comprimento = 1;
        int largura = -2;
        Exception excecaoLargura = assertThrows(IllegalArgumentException.class, () -> new Retangulo(comprimento, largura));

        String mensagemEsperada = "Medida largura não pode ser negativa ou igual a zero";
        String mensagemResultante = excecaoLargura.getMessage();
        assertEquals(mensagemResultante, mensagemEsperada);
    }

    @Test
    void obterPerimetro_ErroII() {
        int comprimento = 1;
        int largura = -2;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Retangulo(comprimento, largura));

        String expectedMessage = "Medida largura não pode ser negativa ou igual a zero";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void obterArea() {
        Retangulo retanguloA = new Retangulo(1, 2);
        double esperado = 2;
        double resultado = retanguloA.obterArea();
        assertEquals(esperado, resultado);
    }

    @Test
    void obterDiagonal() {
    }
}