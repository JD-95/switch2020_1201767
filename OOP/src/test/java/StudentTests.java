import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTests {

    @Test
    void createStudent_Valid() {
        int number = 1000000;
        String name = "Paulo";
        Student s1 = new Student(number, name);
        assertNotNull(s1);
        assertEquals(number, s1.getNumber());
        assertEquals(name, s1.getName());
    }

    @Test
    void createStudent_NameInvalid_ShorterThanFiveChars() {
        int number = 1000000;
        String name = "Rui";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid name : must contain at least 5 chars";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NameInvalid_WithLeftSpaces() {
        int number = 1000000;
        String name = "  Rui";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid name : must contain at least 5 chars";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NameInvalid_WithRightSpaces() {
        int number = 1000000;
        String name = "Rui  ";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid name : must contain at least 5 chars";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NameInvalid_FullOfSpaces() {
        int number = 1000000;
        String name = "     ";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid name : must contain at least 5 chars";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NameInvalid_Empty() {
        int number = 1000000;
        String name = "";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid name : must contain at least 5 chars";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }


    @Test
    void createStudent_NameInvalid_Null() {
        int number = 1000000;
        String name = null;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid name : must contain at least 5 chars";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NumberInvalid_MoreThanSevenDigits() {
        int number = 10000000;
        String name = "Andre";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid number: must be a positive 7 digit number";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NumberInvalid_LessThanSevenDigits() {
        int number = 999999;
        String name = "Andre";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid number: must be a positive 7 digit number";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void createStudent_NumberInvalid_SevenDigitsButNegative() {
        int number = -1234567;
        String name = "Andre";
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Student(number, name));
        String expectedMessage = "invalid number: must be a positive 7 digit number";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void doEvaluation_GradeInvalid_MoreThanTwenty() {
        Student s1 = new Student(1234567, "Paulo");
        int grade = 21;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> s1.doEvaluation(grade));
        String expectedMessage = "invalid grade : must range between 0 and 20";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void doEvaluation_GradeInvalid_Negative() {
        Student s1 = new Student(1234567, "Paulo");
        int grade = -1;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> s1.doEvaluation(grade));
        String expectedMessage = "invalid grade : must range between 0 and 20";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void doEvaluation_StudentAlreadyEvaluated() {
        Student s1 = new Student(1234567, "Paulo");
        s1.doEvaluation(18);
        int newGrade = 15;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> s1.doEvaluation(newGrade));
        String expectedMessage = "student already evaluated";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }

    @Test
    void doEvaluation_StudentAlreadyEvaluated_WithSameGrade() {
        Student s1 = new Student(1234567, "Paulo");
        s1.doEvaluation(18);
        int newGrade = 18;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> s1.doEvaluation(newGrade));
        String expectedMessage = "student already evaluated";
        String actualMessage = exception.getMessage();
        assertEquals(actualMessage, expectedMessage);
    }


    @Test
    void doEvaluation_Valid_Zero() {
        int grade = 0;
        Student s1 = new Student(1234567, "Paulo");
        s1.doEvaluation(grade);
        int result = s1.getGrade();
        assertEquals(grade, result);
    }

    @Test
    void doEvaluation_Valid_Twenty() {
        int grade = 20;
        Student s1 = new Student(1234567, "Paulo");
        s1.doEvaluation(grade);
        int result = s1.getGrade();
        assertEquals(grade, result);
    }

    @Test
    void equalsTrue() {
        Student s1 = new Student(1000000, "Pedro");
        Student s2 = new Student(1000000, "Joana");
        boolean result = s1.equals(s2);
        assertTrue(result);
    }

    @Test
    void equalsTrueToItself() {
        Student s1 = new Student(1000000, "Pedro");
        boolean result = s1.equals(s1);
        assertTrue(result);
    }

    @Test
    void equalsFalseDueToNull() {
        Student s1 = new Student(1000000, "Pedro");
        boolean result = s1.equals(null);
        assertFalse(result);
    }

    @Test
    void equalsFalseDueToDifferentType() {
        Student s1 = new Student(1000000, "Pedro");
        boolean result = s1.equals( new String ("1000000"));
        assertFalse(result);
    }

    @Test
    void equalsFalseDueToDifferentNumbers() {
        Student s1 = new Student(1000000, "Pedro");
        Student s2 = new Student(1000001, "Joana");
        boolean result = s1.equals(s2);
        assertFalse(result);
    }



}