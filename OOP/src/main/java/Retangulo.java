public class Retangulo {
    private double comprimento = -1;
    private double largura = -1;

    public Retangulo(double comprimento, double largura) {
        if (!medidaValido(comprimento)) {
            throw new IllegalArgumentException("Medida comprimento não pode ser negativa ou igual a zero");
        }
        if (!medidaValido(largura)) {
            throw new IllegalArgumentException("Medida largura não pode ser negativa ou igual a zero");
        }
        this.comprimento = comprimento;
        this.largura = largura;
    }

    private boolean medidaValido(double medida) {
        return medida > 0;
    }

    public double obterPerimetro() {
        return (2 * comprimento) + (2 * largura);
    }

    public double obterArea() {
        return comprimento * largura;
    }

    public double obterDiagonal() {
        return Math.sqrt((comprimento * comprimento) + (largura * largura));
    }
}