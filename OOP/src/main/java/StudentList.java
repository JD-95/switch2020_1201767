import java.util.ArrayList;
import java.util.Comparator;

public class StudentList {
    // Atributes
    private ArrayList<Student> students;

    // Constructor
    public StudentList() {
        this.students = new ArrayList();
    }

    public StudentList(Student[] students) {
        if (students == null) {
            throw new IllegalArgumentException("Students array should not be null");
        }
        this.students = new ArrayList<>();
        for (Student student : students) {
            this.add(student);
        }
    }


    // Operations
    public void sortByNumberAsc() {
        this.students.sort(new SortByNumber());
    }

    public void sortByGradeDesc() {
        this.students.sort(new SortByGradeDesc());
    }

    public Student[] toArray() {
        Student[] array = new Student[this.students.size()];
        return this.students.toArray(array);
    }

    public boolean add(Student student) {
        if (student == null || this.students.contains(student)) {
            return false;
        }
        this.students.add(student);
        return true;
    }



    public boolean remove(Student student) {
        if (student == null || !this.students.contains(student)) {
            return false;
        }
        this.students.remove(student);
        return true;
    }



    private class SortByNumber implements Comparator<Student> {
        public int compare(Student st1, Student st2) {
            return st1.compareToByNumber(st2);
        }
    }

    private class SortByGradeDesc implements Comparator<Student> {
        public int compare(Student st1, Student st2) {
            return st1.compareToByGrade(st2) * (-1);
        }
    }

    /*
    private boolean exists(Student student) {
        boolean result = false;
        for (int index = 0; index < students.length; index++) {
            if (student.equals(students[index])) {
                result = true;
            }
        }
        return result;
    }

    private int getIndexOf(Student student) {
        for (int index = 0; index < students.length; index++) {
            if (student.equals(students[index])) {
                return index;
            }
        }

        return -1;
    }

    private Student[] join(Student[] students1, Student[] students2) {

        Student[] result = new Student[students1.length + students2.length];

        for (int index = 0; index < students1.length; index++) {
            result[index] = students1[index];
        }
        for (int index = 0; index < students2.length; index++) {
            result[index + students1.length] = students2[index];
        }

        return result;
    }


    public void sortByNumberAsc() {
        Student tempStudent = null;

        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = (index1 + 1); index2 < this.students.length; index2++) {
                // swap elements if student numbers are not in ascending order
                if (this.students[index1].compareToByNumber(this.students[index2]) > 0) {
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }
            }
        }
    }


    public void sortByGradeDesc() {
        Student tempStudent = null;

        for (int index1 = 0; index1 < this.students.length; index1++) {
            for (int index2 = (index1 + 1); index2 < this.students.length; index2++) {
                // swap elements if student numbers are not in ascending order
                if (this.students[index1].compareToByGrade(this.students[index2]) < 0) {
                    tempStudent = this.students[index1];
                    this.students[index1] = this.students[index2];
                    this.students[index2] = tempStudent;
                }
            }
        }
    }

    private Student[] copyStudentsFromArray(Student[] students, int size) {
        return this.copyStudentsFromArray(students, 0, size);
    }

    private Student[] copyStudentsFromArray(Student[] students, int startIndex, int size) {
        Student[] copyArray = new Student[size];
        int index2 = startIndex;

        for (int index = 0; index < size && index < students.length; index++) {
            copyArray[index] = students[index2];
            index2++;
        }
        return copyArray;
    }

     */

}
