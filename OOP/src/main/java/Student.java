public class Student {
    // Attributes
    private int number;
    private String name;
    private int grade;

    // Constructor
    // Only allow create a new student with at least number and name
    public Student(int number, String name) {
        if (!isNumberValid(number)) {
            throw new IllegalArgumentException("invalid number: must be a positive 7 digit number");
        }
        if (!isNameValid(name)) {
            throw new IllegalArgumentException("invalid name : must contain at least 5 chars");
        }

        this.number = number;
        this.name = name;
        this.grade = -1;

    }


    // Operations


    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    /**
     * Set grade for already created student.
     * @param grade Grade with no decimals.
     * @throws java.lang.IllegalArgumentException if grade below 0 and above 20 OR student already evaluated.
     */
    public void doEvaluation(int grade) {
        if (!isGradeValid(grade)) {
            throw new IllegalArgumentException("invalid grade : must range between 0 and 20");
        }
        if (isEvaluated()) {
            throw new IllegalArgumentException("student already evaluated");

        }
        this.grade = grade;
    }

    private boolean isNameValid(String name) {
        if (name == null) {
            return false;
        }
        name = name.trim();
        if (name.length() < 5) {
            return false;
        }
        return true;
    }

    private boolean isNumberValid(int number) {
        return number >= 1000000 && number <= 9999999;
    }

    private boolean isGradeValid(int grade) {
        return grade >= 0 && grade <= 20;
    }

    private boolean isEvaluated() {
        return this.grade >= 0;
    }

    public int compareToByNumber(Student other) {
        if (this.number < other.number) {
            return -1; // lesser than other
        }
        if (this.number > other.number) {
            return 1; // greater than other
        }
        return 0; // both have the same value
    }

    public int compareToByGrade(Student other) {
        if (this.grade < other.grade) {
            return -1; // lesser than other
        }
        if (this.grade > other.grade) {
            return 1; // greater than other
        }
        return 0; // both have the same value
    }

    @Override
    public boolean equals(Object other) {
        // verify if object corresponds to itself
        if (this == other) {
            return true;
        }
        // verify if object is null or not Student type
        if (other == null || Student.class != other.getClass()) {
            return false;
        }
        // verify if number is the same
        Student student = (Student) other;
        return this.number == student.number;
    }

    // methods sort students by number and grade moved to StudentList class
   /*
    public static boolean sortStudentsByNumberAsc(Student[] students) {
        if (students == null) {
            return false;
        }

        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++) {
            for (int index2 = (index1 + 1); index2 < students.length; index2++) {
                // swap elements if student numbers are not in ascending order
                if (students[index1].compareToByNumber(students[index2]) > 0) {
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    }

    public static boolean sortStudentsByGradeDesc(Student[] students) {
        if (students == null) {
            return false;
        }

        Student tempStudent = null;

        for (int index1 = 0; index1 < students.length; index1++) {
            for (int index2 = (index1 + 1); index2 < students.length; index2++) {
                // swap elements if student grades are not in descending order
                if (students[index1].compareToByGrade(students[index2]) < 0) {
                    tempStudent = students[index1];
                    students[index1] = students[index2];
                    students[index2] = tempStudent;
                }
            }
        }
        return true;
    } */
}

