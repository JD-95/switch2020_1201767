package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio2Test {

    @Test
    void get$FinalTest1 () {
        int nrosas = 1;
        int ntulipas = 1;
        double $rosa = 1;
        double $tulipa = 1;
        double expected = 2;

        double result = Exercicio2.get$Final(nrosas, ntulipas, $rosa, $tulipa);

        assertEquals(expected, result, 0.1);
    }

    @Test
    void get$FinalTest2 () {
        int nrosas = 1;
        int ntulipas = 1;
        double $rosa = 0.6;
        double $tulipa = 0.4;
        double expected = 1;

        double result = Exercicio2.get$Final(nrosas, ntulipas, $rosa, $tulipa);

        assertEquals(expected, result, 0.1);
    }

    @Test
    void get$FinalTest3 () {
        int nrosas = 1;
        int ntulipas = 1;
        double $rosa = 1;
        double $tulipa = 0;
        double expected = 1;

        double result = Exercicio2.get$Final(nrosas, ntulipas, $rosa, $tulipa);

        assertEquals(expected, result, 0.1);
    }
}