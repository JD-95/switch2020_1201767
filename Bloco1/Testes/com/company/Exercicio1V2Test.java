package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio1V2Test {

    @Test
    void percentagemDeRaparigasTest1() {
        int rapazes = 10;
        int raparigas = 10;
        double expected = 50;

        double result = Exercicio1V2.percentagemDeRaparigas(raparigas, rapazes);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void percentagemDeRaparigasTest2() {
        int rapazes = 60;
        int raparigas = 40;
        double expected = 40;

        double result = Exercicio1V2.percentagemDeRaparigas(raparigas, rapazes);

        assertEquals(expected, result, 0.001);
    }


    @Test
    void percentagemDeRaparigasTest3() {
        int rapazes = 0;
        int raparigas = 100;
        double expected = 100;

        double result = Exercicio1V2.percentagemDeRaparigas(raparigas, rapazes);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void percentagemDeRapazesTest1() {
        int rapazes = 10;
        int raparigas = 10;
        double expected = 50;

        double result = Exercicio1V2.percentagemDeRapazes(raparigas, rapazes);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void percentagemDeRapazesTest2() {
        int rapazes = 35;
        int raparigas = 65;
        double expected = 35;

        double result = Exercicio1V2.percentagemDeRapazes(raparigas, rapazes);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void percentagemDeRapazesTest3() {
        int rapazes = 100;
        int raparigas = 0;
        double expected = 100;

        double result = Exercicio1V2.percentagemDeRapazes(raparigas, rapazes);

        assertEquals(expected, result, 0.001);
    }
}