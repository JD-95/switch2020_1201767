package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio9Test {

    @Test
    void getPerimetroRectanguloEmMetrosTest1() {
        double ladoA = 1;
        double ladoB = 1;
        double expected = 4;

        double result = Exercicio9.getPerimetroRectanguloEmMetros(ladoA, ladoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getPerimetroRectanguloEmMetrosTest2() {
        double ladoA = 0;
        double ladoB = 1;
        double expected = 2;

        double result = Exercicio9.getPerimetroRectanguloEmMetros(ladoA, ladoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getPerimetroRectanguloEmMetrosTest3() {
        double ladoA = 1;
        double ladoB = 0;
        double expected = 2;

        double result = Exercicio9.getPerimetroRectanguloEmMetros(ladoA, ladoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getPerimetroRectanguloEmMetrosTest4() {
        double ladoA = 1;
        double ladoB = 0.5;
        double expected = 3;

        double result = Exercicio9.getPerimetroRectanguloEmMetros(ladoA, ladoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getPerimetroRectanguloEmMetrosTest5() {
        double ladoA = 0.5;
        double ladoB = 1;
        double expected = 3;

        double result = Exercicio9.getPerimetroRectanguloEmMetros(ladoA, ladoB);

        assertEquals(expected, result, 0.01);
    }
}