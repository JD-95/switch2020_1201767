package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio6Test {

    @Test
    void getAlturaPredioTest1() {
        double alturaPessoa = 2;
        double sombraPessoa = 4;
        double sombraPredio = 40;
        double expected = 20;

        double result = Exercicio6.getAlturaPredio(sombraPredio, alturaPessoa, sombraPessoa);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAlturaPredioTest2() {
        double alturaPessoa = 1;
        double sombraPessoa = 1;
        double sombraPredio = 1;
        double expected = 1;

        double result = Exercicio6.getAlturaPredio(sombraPredio, alturaPessoa, sombraPessoa);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAlturaPredioTest3() {
        double alturaPessoa = 5;
        double sombraPessoa = 1;
        double sombraPredio = 1;
        double expected = 5;

        double result = Exercicio6.getAlturaPredio(sombraPredio, alturaPessoa, sombraPessoa);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAlturaPredioTest4() {
        double alturaPessoa = 1;
        double sombraPessoa = 5;
        double sombraPredio = 1;
        double expected = 0.2;

        double result = Exercicio6.getAlturaPredio(sombraPredio, alturaPessoa, sombraPessoa);

        assertEquals(expected, result, 0.01);
    }

}