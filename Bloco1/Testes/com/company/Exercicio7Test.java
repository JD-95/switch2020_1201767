package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio7Test {

    @Test
    void getDistanciaPercorridaEmKmTest1() {
        int horas = 0;
        int minutos = 0;
        int segundos = 1;
        final double VELOCIDADE = 2.90;
        double expected = 0.0029;

        double result = Exercicio7.getDistanciaPercorridaEmKm(horas, minutos, segundos, VELOCIDADE);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaPercorridaEmKmTest2() {
        int horas = 1;
        int minutos = 0;
        int segundos = 0;
        final double VELOCIDADE = 2.90;
        double expected = 10.44;

        double result = Exercicio7.getDistanciaPercorridaEmKm(horas, minutos, segundos, VELOCIDADE);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaPercorridaEmKmTest3() {
        int horas = 0;
        int minutos = 1;
        int segundos = 0;
        final double VELOCIDADE = 2.90;
        double expected = 0.174;

        double result = Exercicio7.getDistanciaPercorridaEmKm(horas, minutos, segundos, VELOCIDADE);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaPercorridaEmKmTest4() {
        int horas = 0;
        int minutos = 0;
        int segundos = 1;
        final double VELOCIDADE = 1000;
        double expected = 1;

        double result = Exercicio7.getDistanciaPercorridaEmKm(horas, minutos, segundos, VELOCIDADE);

        assertEquals(expected, result, 0.001);
    }
}