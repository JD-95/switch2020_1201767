package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio13Test {

    @Test
    void getMinutosDesdeAs0HTest1() {
        int minutos = 1;
        int horas = 0;
        int expected = 1;

        int result = Exercicio13.getMinutosDesdeAs0H(minutos, horas);

        assertEquals(expected, result, 0.1);
    }

    @Test
    void getMinutosDesdeAs0HTest2() {
        int minutos = 0;
        int horas = 1;
        int expected = 60;

        int result = Exercicio13.getMinutosDesdeAs0H(minutos, horas);

        assertEquals(expected, result, 0.1);
    }

    @Test
    void getMinutosDesdeAs0HTest3() {
        int minutos = 59;
        int horas = 23;
        int expected = 1439;

        int result = Exercicio13.getMinutosDesdeAs0H(minutos, horas);

        assertEquals(expected, result, 0.1);
    }
}