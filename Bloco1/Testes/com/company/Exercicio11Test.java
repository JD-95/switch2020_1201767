package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio11Test {

    @Test
    void getResultadoDaExpressãoTest1() {
        double x = 0;
        double expected = 1;

        double result = Exercicio11.getResultadoDaExpressão(x);

        assertEquals(expected, result,0.01);
    }

    @Test
    void getResultadoDaExpressãoTest2() {
        double x = -1;
        double expected = 5;

        double result = Exercicio11.getResultadoDaExpressão(x);

        assertEquals(expected, result,0.01);
    }

    @Test
    void getResultadoDaExpressãoTest3() {
        double x = 1;
        double expected = -1;

        double result = Exercicio11.getResultadoDaExpressão(x);

        assertEquals(expected, result,0.01);
    }
}