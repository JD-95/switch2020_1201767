package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio3Test {

    @Test
    void obterVolumeCilindroTeste1() {
        double raio = 1;
        double altura = 1;
        double expected = 3141.59;

        double result = Exercicio3.obterVolumeCilindro(raio, altura);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void obterVolumeCilindroTeste2() {
        double raio = 0.5;
        double altura = 1;
        double expected = 785.40;

        double result = Exercicio3.obterVolumeCilindro(raio, altura);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void obterVolumeCilindroTeste3() {
        double raio = 500;
        double altura = 7.6;
        double expected = 5969026041.82;

        double result = Exercicio3.obterVolumeCilindro(raio, altura);

        assertEquals(expected, result, 0.01);
    }

}