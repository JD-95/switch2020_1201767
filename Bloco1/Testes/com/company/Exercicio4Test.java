package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio4Test {

    @Test
    void getDistanciaAoRelampagoTest1() {
        double tempo = 1;
        final int VELOCIDADOSOM = 340;
        double expected = 340;

        double result = Exercicio4.getDistanciaAoRelampago(tempo, VELOCIDADOSOM);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getDistanciaAoRelampagoTest2() {
        double tempo = 0.5;
        final int VELOCIDADOSOM = 340;
        double expected = 170;

        double result = Exercicio4.getDistanciaAoRelampago(tempo, VELOCIDADOSOM);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getDistanciaAoRelampagoTest3() {
        double tempo = 7.3;
        final int VELOCIDADOSOM = 340;
        double expected = 2482;

        double result = Exercicio4.getDistanciaAoRelampago(tempo, VELOCIDADOSOM);

        assertEquals(expected, result, 0.01);
    }
}