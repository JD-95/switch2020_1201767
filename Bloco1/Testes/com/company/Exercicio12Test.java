package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio12Test {

    @Test
    void getTemperaturaCelsiusToFahrenheitTest1() {
        double temperaturaCelsius = 0;
        double expected = 32;

        double result = Exercicio12.getTemperaturaCelsiusToFahrenheit(temperaturaCelsius);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getTemperaturaCelsiusToFahrenheitTest2() {
        double temperaturaCelsius = 1;
        double expected = 33.8;

        double result = Exercicio12.getTemperaturaCelsiusToFahrenheit(temperaturaCelsius);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getTemperaturaCelsiusToFahrenheitTest3() {
        double temperaturaCelsius = -1;
        double expected = 30.2;

        double result = Exercicio12.getTemperaturaCelsiusToFahrenheit(temperaturaCelsius);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getTemperaturaCelsiusToFahrenheitTest4() {
        double temperaturaCelsius = -17.78;
        double expected = 0;

        double result = Exercicio12.getTemperaturaCelsiusToFahrenheit(temperaturaCelsius);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getTemperaturaCelsiusToFahrenheitTest5() {
        double temperaturaCelsius = 20;
        double expected = 68;

        double result = Exercicio12.getTemperaturaCelsiusToFahrenheit(temperaturaCelsius);

        assertEquals(expected, result, 0.01);
    }
}