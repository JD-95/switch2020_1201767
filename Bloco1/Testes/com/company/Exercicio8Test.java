package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio8Test {

    @Test
    void getDistanciaDosOperariosTest1() {
        double caboOperario1 = 1;
        double caboOperario2 = 1;
        double angulo = 1;
        double expected = 0.017;

        double result = Exercicio8.getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaDosOperariosTest2() {
        double caboOperario1 = 1;
        double caboOperario2 = 1;
        double angulo = 178;
        double expected = 1.999;

        double result = Exercicio8.getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaDosOperariosTest3() {
        double caboOperario1 = 40;
        double caboOperario2 = 60;
        double angulo = 60;
        double expected = 52.915;

        double result = Exercicio8.getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaDosOperariosTest4() {
        double caboOperario1 = 1;
        double caboOperario2 = 1;
        double angulo = 90;
        double expected = 1.414;

        double result = Exercicio8.getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaDosOperariosTest5() {
        double caboOperario1 = 1;
        double caboOperario2 = 2;
        double angulo = 90;
        double expected = 2.236;

        double result = Exercicio8.getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        assertEquals(expected, result, 0.001);
    }

    @Test
    void getDistanciaDosOperariosTest6() {
        double caboOperario1 = 2;
        double caboOperario2 = 1;
        double angulo = 90;
        double expected = 2.236;

        double result = Exercicio8.getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        assertEquals(expected, result, 0.001);
    }
}