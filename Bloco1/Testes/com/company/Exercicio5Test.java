package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio5Test {

    @Test
    void getAlturaDoPredioTest1() {
        final double ACELERACAO = 9.8;
        final int VELOCIDADEINICIAL = 0;
        double tempo = 1;
        double expected = 4.9;

        double result = Exercicio5.getAlturaDoPredio(VELOCIDADEINICIAL, ACELERACAO, tempo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAlturaDoPredioTest2() {
        final double ACELERACAO = 9.8;
        final int VELOCIDADEINICIAL = 0;
        double tempo = 2;
        double expected = 19.6;

        double result = Exercicio5.getAlturaDoPredio(VELOCIDADEINICIAL, ACELERACAO, tempo);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getAlturaDoPredioTest3() {
        final double ACELERACAO = 9.8;
        final int VELOCIDADEINICIAL = 0;
        double tempo = 0.5;
        double expected = 1.23;

        double result = Exercicio5.getAlturaDoPredio(VELOCIDADEINICIAL, ACELERACAO, tempo);

        assertEquals(expected, result, 0.01);
    }
}