package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio10Test {

    @Test
    void getHipotenusaTrianguloTest1() {
        double catetoA = 1;
        double catetoB = 1;
        double expected = 1.41 ;

        double result = Exercicio10.getHipotenusaTriangulo(catetoA, catetoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getHipotenusaTrianguloTest2() {
        double catetoA = 0.5;
        double catetoB = 1;
        double expected = 1.11 ;

        double result = Exercicio10.getHipotenusaTriangulo(catetoA, catetoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getHipotenusaTrianguloTest3() {
        double catetoA = 1;
        double catetoB = 0.5;
        double expected = 1.11 ;

        double result = Exercicio10.getHipotenusaTriangulo(catetoA, catetoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getHipotenusaTrianguloTest4() {
        double catetoA = 1;
        double catetoB = 0;
        double expected = 1 ;

        double result = Exercicio10.getHipotenusaTriangulo(catetoA, catetoB);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getHipotenusaTrianguloTest5() {
        double catetoA = 0;
        double catetoB = 1;
        double expected = 1 ;

        double result = Exercicio10.getHipotenusaTriangulo(catetoA, catetoB);

        assertEquals(expected, result, 0.01);
    }
}