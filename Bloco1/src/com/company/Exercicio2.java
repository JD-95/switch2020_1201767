package com.company;

import java.util.Scanner;

public class Exercicio2 {
    public static void main(String[] args) {
        // estrutura de dados
        int nrosas, ntulipas;
        double $rosa, $tulipa, $final;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print ("Indique o número de rosas:");
        nrosas = ler.nextInt();
        System.out.print ("Indique o número de tulipas:");
        ntulipas = ler.nextInt();
        System.out.print ("Indique o preço da rosa:");
        $rosa = ler.nextDouble();
        System.out.print ("Indique o preço da tulipa:");
        $tulipa = ler.nextDouble();

        // processamento
        $final = get$Final(nrosas, ntulipas, $rosa, $tulipa);

        // saída de dados
        System.out.print ("O preço do seu ramo é de " + $final + " €.");
    }

    public static double get$Final(int nrosas, int ntulipas, double $rosa, double $tulipa) {
        return (nrosas * $rosa) + (ntulipas * $tulipa);
    }
}
