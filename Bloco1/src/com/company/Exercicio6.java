package com.company;

import java.util.Scanner;

public class Exercicio6 {
    public static void main(String[] args) {
        // estrutura de dados
        double sombraPredio, alturaPessoa, sombraPessoa, alturaPredio ;

        // leitura de dados
        Scanner ler = new Scanner(System.in)  ;
        System.out.print ("Introduza a sua altura (em metros):") ;
        alturaPessoa = ler.nextDouble();
        System.out.print ("Introduza a sua sombra projetada no solo (em metros):") ;
        sombraPessoa = ler.nextDouble();
        System.out.print ("Introduza a sombra do prédio projetada no solo (em metros):") ;
        sombraPredio = ler.nextDouble();

        // processamento
        alturaPredio = getAlturaPredio(sombraPredio, alturaPessoa, sombraPessoa);

        // saida de dados
        System.out.print ("A altura do prédio é de " + String.format ("%.2f", alturaPredio) + " metros.") ;
    }

    public static double getAlturaPredio(double sombraPredio, double alturaPessoa, double sombraPessoa) {
        return (sombraPredio * alturaPessoa) / sombraPessoa;
    }
}
