package com.company;

import java.util.Scanner;

public class Exercicio5 {
    public static void main(String[] args) {
        // estrutura de dados
        double tempo, alturaPredio;
        final double ACELERACAO = 9.8;
        final int VELOCIDADEINICIAL = 0;

        // leitura de dados
        Scanner ler = new Scanner (System.in);
        System.out.print("Introduza o tempo que a pedra demorou a chegar ao solo (em segundos):");
        tempo = ler.nextDouble();

        // processamento
        alturaPredio = getAlturaDoPredio(VELOCIDADEINICIAL, ACELERACAO, tempo);

        // saida dos dados
        System.out.print ("A altura do prédio é de " + String.format("%.2f", alturaPredio) + " metros.")    ;
    }

    public static double getAlturaDoPredio(int VELOCIDADEINICIAL, double ACELERACAO, double tempo) {
        return (VELOCIDADEINICIAL * tempo) + ((ACELERACAO * (tempo * tempo)) / 2);
    }
}
