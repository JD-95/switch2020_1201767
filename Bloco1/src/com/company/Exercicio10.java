package com.company;

import java.util.Scanner;

public class Exercicio10 {
    public static void main(String[] args) {
        // estrutura de dados
        double catetoA, catetoB, hipotenusa;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o cateto A do triângulo (em metros):");
        catetoA = ler.nextDouble();
        System.out.print("Introduza o cateto B do triângulo (em metros):");
        catetoB = ler.nextDouble();

        // processamento
        hipotenusa = getHipotenusaTriangulo(catetoA, catetoB);

        // saida de dados
        System.out.print("O valor da hipotenusa do triângulo é de " + String.format("%.2f", hipotenusa) + " metros.");
    }

    public static double getHipotenusaTriangulo(double catetoA, double catetoB) {
        return Math.sqrt((Math.pow(catetoA, 2)) + (Math.pow(catetoB, 2)));
    }
}