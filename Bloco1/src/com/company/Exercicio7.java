package com.company;

import java.util.Scanner;

public class Exercicio7 {
    public static void main(String[] args) {
        // estrutura de dados
        int horas, minutos, segundos;
        double distanciaPercorrida;
        final double VELOCIDADE = 2.90 ; //metros por segundo e assumindo que a velocidade do Manel foi constante durante toda a prova

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza quantas horas percorreu:");
        horas = ler.nextInt();
        System.out.println("Introduza quantos minutos percorreu:");
        minutos = ler.nextInt();
        System.out.println("Introduza quantos segundos percorreu:");
        segundos = ler.nextInt();

        // processamento
        distanciaPercorrida = getDistanciaPercorridaEmKm (horas, minutos, segundos, VELOCIDADE);

        // saida de dados
        System.out.println("A distância que percorreu foi de " + String.format ("%.3f", distanciaPercorrida) + " km.");
    }

    // processamento isolado
    public static double getDistanciaPercorridaEmKm(int horas, int minutos, int segundos, double VELOCIDADE) {
        return (VELOCIDADE * ((horas * 3600) + (minutos * 60) + segundos)) / 1000;
    }
}
