package com.company;

import java.util.Scanner;

public class Exercicio13 {
    public static void main(String[] args) {
        // estrutura de dados
        int minutos, horas, minutosDesdeAs0H;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a hora:");
        horas = ler.nextInt();
        System.out.print("Introduza o minuto:");
        minutos = ler.nextInt();

        // processamento
        minutosDesdeAs0H = getMinutosDesdeAs0H(minutos, horas);

        // saida de dados
        System.out.print("Desde as 0H passaram " + minutosDesdeAs0H + " minutos.");
    }

    public static int getMinutosDesdeAs0H(int minutos, int horas) {
        return (horas * 60) + minutos;
    }
}
