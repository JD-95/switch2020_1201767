package com.company;

import java.util.Scanner;

public class Exercicio8 {
    public static void main(String[] args) {
        // estrutura de dados
        double caboOperario1, caboOperario2, angulo, distanciaDosOperarios;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o tamanho do cabo de aço que segura o operário 1 (em metros): ");
        caboOperario1 = ler.nextDouble();
        System.out.println("Introduza o tamanho do cabo de aço que segura o operário 2 (em metros): ");
        caboOperario2 = ler.nextDouble();
        System.out.println("Introduza o ângulo que os cabos fazem entre si (em graus): ");
        angulo = ler.nextDouble();

        // processamento
        distanciaDosOperarios = getDistanciaDosOperarios(caboOperario1, caboOperario2, angulo);

        // saida de dados
        System.out.print("A sua distancia entre os operários é de " + String.format ("%.2f", distanciaDosOperarios) + " metros.");
    }

    // processamento isolado
    public static double getDistanciaDosOperarios(double caboOperario1, double caboOperario2, double angulo) {
        // return (caboOperario1 + caboOperario2) - (Math.sqrt(2 * caboOperario1 * caboOperario2 * Math.cos(Math.toRadians(angulo))));
        return (Math.sqrt((Math.pow(caboOperario1, 2))+(Math.pow(caboOperario2,2))-(2*caboOperario1*caboOperario2*Math.cos(Math.toRadians(angulo)))));
    }
}
