package com.company;

import java.util.Scanner;

public class Exercicio11 {
    public static void main(String[] args) {
        // estrutura de dados
        double x, resultado;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o valor de x: ");
        x = ler.nextDouble();

        // processamento
        resultado = getResultadoDaExpressão(x);

        // saída de dados
        System.out.print("O valor da expressão é de " + resultado + ".");
    }

    public static double getResultadoDaExpressão(double x) {
        return Math.pow(x, 2) - (3 * x) + 1;
    }
}
