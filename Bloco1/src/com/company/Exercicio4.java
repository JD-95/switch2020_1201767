package com.company;

import java.util.Scanner;

public class Exercicio4 {
    public static void main(String[] args) {
        // estrutura de dados
        double tempo, distancia;
        final int VELOCIDADOSOM = 340; // metros por segundo

        // leitura de dados
        Scanner ler = new Scanner (System.in);
        System.out.print ("Introduza o tempo decorrido entre o relâmpago e o trovão (em segundos):");
        tempo = ler.nextDouble();


        // processamento
        distancia = getDistanciaAoRelampago(tempo, VELOCIDADOSOM);

        // saida de dados
        System.out.print ("A sua distância ao relâmpago é de " + String.format("%.2f", distancia) + ( " metros.")) ;
    }

    public static double getDistanciaAoRelampago(double temp, int VELOCIDADESOM) {
        return VELOCIDADESOM * temp;
    }

}
