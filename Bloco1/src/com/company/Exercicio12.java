package com.company;

import java.util.Scanner;

public class Exercicio12 {
    public static void main(String[] args) {
        // estrutura de dados
        double temperaturaCelsius, temperaturaFahrenheit;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o valor da temperatura em graus Celsius: ");
        temperaturaCelsius = ler.nextDouble();

        // processamento
        temperaturaFahrenheit = getTemperaturaCelsiusToFahrenheit(temperaturaCelsius);

        // saida de dados
        System.out.print("A temperatura de " + temperaturaCelsius + " graus Celsius corresponde a " + temperaturaFahrenheit + " graus Fahrenheit.");
    }

    public static double getTemperaturaCelsiusToFahrenheit(double temperaturaCelsius) {
        return 32 + ( 9/5.0 * temperaturaCelsius);
    }
}
