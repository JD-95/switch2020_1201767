package com.company;

import java.util.Scanner;

public class Exercicio3 {
    public static void main(String[] args) {
        // estrutura de dados
        double raio, altura, volume;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor do raio da base do cilindro (em metros):");
        raio = ler.nextInt();
        System.out.println("Introduza o valor da altura do cilindro (em metros):");
        altura = ler.nextInt();

        // processamento
        volume = obterVolumeCilindro(raio, altura);

        // saída de dados
        System.out.println("O volume deste sólido é de " + String.format("%.2f", volume) + " litros.");
    }

    public static double obterVolumeCilindro(double raio, double altura) {
        return Math.PI * raio * raio * altura * 1000;
    }
}
