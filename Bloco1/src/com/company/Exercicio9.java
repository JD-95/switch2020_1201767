package com.company;

import java.util.Scanner;

public class Exercicio9 {
    public static void main(String[] args) {
        // estrutura de dados
        double ladoA, ladoB, perimetroRectangulo;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o comprimento do lado A (em metros):");
        ladoA = ler.nextDouble();
        System.out.print("Introduza o comprimento do lado B (em metros):");
        ladoB = ler.nextDouble();

        // processamento
        perimetroRectangulo = getPerimetroRectanguloEmMetros(ladoA, ladoB);

        // saida de dados
        System.out.print("O valor do perímetro do rectângulo é de " + String.format("%.2f", perimetroRectangulo) + " metros.");
    }

    public static double getPerimetroRectanguloEmMetros(double ladoA, double ladoB) {
        return (2 * ladoA) + (2 * ladoB);
    }
}
