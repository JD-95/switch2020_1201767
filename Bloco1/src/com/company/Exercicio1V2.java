package com.company;

public class Exercicio1V2 {


    public static double percentagemDeRaparigas(int raparigas, int rapazes) {
        double total, percentagem;

        total = raparigas + rapazes;
        percentagem = (raparigas / total) * 100;

        return percentagem;
    }

    public static double percentagemDeRapazes(int raparigas, int rapazes) {
        double total, percentagem;

        total = raparigas + rapazes;
        percentagem = (rapazes / total) * 100;

        return percentagem;
    }
}