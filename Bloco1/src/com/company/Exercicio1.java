package com.company;

import java.util.Scanner;

public class Exercicio1 {

    public static void main (String[] args) {
        // estrutura de dados
        int rapazes, raparigas;
        double total, percentagemDeRapazes, percentagemDeRaparigas ;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de rapazes:");
        rapazes = ler.nextInt();
        System.out.println("Introduza o número de raparigas:");
        raparigas = ler.nextInt();

        // processamento
        total = rapazes + raparigas;
        percentagemDeRapazes = getPercentage(rapazes, total);
        percentagemDeRaparigas = getPercentage(raparigas, total);

        // saída de dados
        System.out.println("Percentagem de rapazes: " + String.format ("%.2f", percentagemDeRapazes) + " %.");
        System.out.println("Percentagem de raparigas: " + String.format ("%.2f", percentagemDeRaparigas) + " %.");
    }

    public static double getPercentage(int partial, double total) {
        return (partial / total) * 100;
    }
}
