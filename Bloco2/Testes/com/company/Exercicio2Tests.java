package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Exercicio2Tests {
    @Test
    void exercicio2V2Test1() {
        String expected = "nao tem 3 digitos";
        String result = Bloco2.exercicio2V2(99);
        assertEquals(expected, result);
    }

    @Test
    void exercicio2V2Test2() {
        String expected = "nao tem 3 digitos";
        String result = Bloco2.exercicio2V2(1000);
        assertEquals(expected, result);
    }

    @Test
    void exercicio2V2Test3() {
        String expected = 1 + " " + 0 + " " + 0;
        String result = Bloco2.exercicio2V2(100);
        assertEquals(expected, result);
    }

    @Test
    void exercicio2V2Test4() {
        String expected = 5 + " " + 6 + " " + 7;
        String result = Bloco2.exercicio2V2(567);
        assertEquals(expected, result);
    }
}