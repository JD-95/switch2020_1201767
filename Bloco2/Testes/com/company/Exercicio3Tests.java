package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio3Tests {

    @Test
    void obterDistanciaEntre2PontosTest1() {
        double expected = 0;
        double result = Bloco2.obterDistanciaEntre2Pontos(0, 0, 0, 0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void obterDistanciaEntre2PontosTest2() {
        double expected = 1;
        double result = Bloco2.obterDistanciaEntre2Pontos(1, 0, 0, 0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void obterDistanciaEntre2PontosTest3() {
        double expected = 2;
        double result = Bloco2.obterDistanciaEntre2Pontos(0, 2, 0, 0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void obterDistanciaEntre2PontosTest4() {
        double expected = 3;
        double result = Bloco2.obterDistanciaEntre2Pontos(0, 0, 3, 0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void obterDistanciaEntre2PontosTest5() {
        double expected = 4;
        double result = Bloco2.obterDistanciaEntre2Pontos(0, 0, 0, 4);
        assertEquals(expected, result, 0.01);
    }
}