package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio4Tests {

    @Test
    void exercicio4V2Test1() {
        double expected = 0;
        double result = Bloco2.obterFuncaoDeX(0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio4V2Test2() {
        double expected = -1;
        double result = Bloco2.obterFuncaoDeX(1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio4V2Test3() {
        double expected = -1;
        double result = Bloco2.obterFuncaoDeX(-1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio4V2Test4() {
        double expected = -0.75;
        double result = Bloco2.obterFuncaoDeX(0.5);
        assertEquals(expected, result, 0.01);
    }
}