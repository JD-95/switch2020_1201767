package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio12Tests {

    @Test
    void ensureNotificacaoProtecaoAmbienteIsInvalid() {
        String expected = "Demasiado otimista, não há nível de poluição negativo.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(-1);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteIsNoneTest1() {
        String expected = "O nível de poluição é aceitável.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteIsNoneTest2() {
        String expected = "O nível de poluição é aceitável.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0.3);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteAffectsGroup1Test1() {
        String expected = "Intimidar indústrias do 1º GRUPO a SUSPENDEREM a sua atividade.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0.31);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteAffectsGroup1Test2() {
        String expected = "Intimidar indústrias do 1º GRUPO a SUSPENDEREM a sua atividade.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0.4);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteAffectsGroup1And2Test1() {
        String expected = "Intimidar indústrias do 1º e 2º GRUPO a SUSPENDEREM a sua atividade.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0.41);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteAffectsGroup1And2Test2() {
        String expected = "Intimidar indústrias do 1º e 2º GRUPO a SUSPENDEREM a sua atividade.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0.50);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteAffectsAllGroupsTest1() {
        String expected = "Intimidar indústrias do 1º, 2º e 3º GRUPO a PARALISAREM a sua atividade.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(0.51);
        assertEquals(expected, result);
    }

    @Test
    void ensureNotificacaoProtecaoAmbienteAffectsAllGroupsTest2() {
        String expected = "Intimidar indústrias do 1º, 2º e 3º GRUPO a PARALISAREM a sua atividade.";
        String result = Bloco2.obterNotificacaoProtecaoAmbiente(1);
        assertEquals(expected, result);
    }
}