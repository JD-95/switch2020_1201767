package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio18Tests {

    @Test
    void ensureFimProcessamentoIsNoneTest1() {
        String expected = "Formato de dados inválido";
        String result = Bloco2.obterFimProcessamento(24,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIsNoneTest2() {
        String expected = "Formato de dados inválido";
        String result = Bloco2.obterFimProcessamento(0,60,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIsNoneTest3() {
        String expected = "Formato de dados inválido";
        String result = Bloco2.obterFimProcessamento(0,0,60,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs1H0Min0Sec() {
        String expected = "O processamento termina às 1:0:0";
        String result = Bloco2.obterFimProcessamento(1,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs23H0Min0Sec() {
        String expected = "O processamento termina às 23:0:0";
        String result = Bloco2.obterFimProcessamento(23,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H1Min0Sec() {
        String expected = "O processamento termina às 0:1:0";
        String result = Bloco2.obterFimProcessamento(0,1,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H59Min0Sec() {
        String expected = "O processamento termina às 0:59:0";
        String result = Bloco2.obterFimProcessamento(0,59,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H0Min1Sec() {
        String expected = "O processamento termina às 0:0:1";
        String result = Bloco2.obterFimProcessamento(0,0,1,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H0Min59Sec() {
        String expected = "O processamento termina às 0:0:59";
        String result = Bloco2.obterFimProcessamento(0,0,59,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H0Min1SecProcessamento() {
        String expected = "O processamento termina às 0:0:1";
        String result = Bloco2.obterFimProcessamento(0,0,0,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H0Min59SecProcessamento() {
        String expected = "O processamento termina às 0:0:59";
        String result = Bloco2.obterFimProcessamento(0,0,0,59);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H1Min0SecProcessamento() {
        String expected = "O processamento termina às 0:1:0";
        String result = Bloco2.obterFimProcessamento(0,0,0,60);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H59Min59SecProcessamento() {
        String expected = "O processamento termina às 0:59:59";
        String result = Bloco2.obterFimProcessamento(0,0,0,3599);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs23H23Min59SecProcessamento() {
        String expected = "O processamento termina às 23:59:59";
        String result = Bloco2.obterFimProcessamento(0,0,0,86399);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H0Min0SecProcessamentoDiaSeguinte() {
        String expected = "O processamento termina às 0:0:0 do dia seguinte";
        String result = Bloco2.obterFimProcessamento(0,0,0,86400);
        assertEquals(expected, result);
    }

    @Test
    void ensureFimProcessamentoIs0H0Min1SecProcessamentoDiaSeguinte() {
        String expected = "O processamento termina às 0:0:1 do dia seguinte";
        String result = Bloco2.obterFimProcessamento(0,0,0,86401);
        assertEquals(expected, result);
    }
}