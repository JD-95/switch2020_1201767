package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio13Tests {

    @Test
    void ensureHorasJardinagemIsNegativeTest1() {
        double expected = -1;
        double result = Bloco2.obterHorasJardinagem(-1,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureHorasJardinagemIsNegativeTest2() {
        double expected = -1;
        double result = Bloco2.obterHorasJardinagem(0,-1,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureHorasJardinagemIsNegativeTest3() {
        double expected = -1;
        double result = Bloco2.obterHorasJardinagem(0,0,-1);
        assertEquals(expected, result);
    }

    @Test
    void ensureHorasJardinagemIs0Dot08() {
        double expected = 0.08;
        double result = Bloco2.obterHorasJardinagem(1,0,0);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureHorasJardinagemIs0Dot17() {
        double expected = 0.17;
        double result = Bloco2.obterHorasJardinagem(0,1,0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureHorasJardinagemIs0Dot11() {
        double expected = 0.11;
        double result = Bloco2.obterHorasJardinagem(0,0,1);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureHorasJardinagemIs1() {
        double expected = 1;
        double result = Bloco2.obterHorasJardinagem(2,3,3);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureHorasJardinagemIs0Dot36() {
        double expected = 0.36;
        double result = Bloco2.obterHorasJardinagem(1,1,1);
        assertEquals(expected, result,0.01);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    @Test
    void ensureCustoJardinagemIsNegativeTest1() {
        double expected = -1;
        double result = Bloco2.obterCustoJardinagem(-1,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureCustoJardinagemIsNegativeTest2() {
        double expected = -1;
        double result = Bloco2.obterCustoJardinagem(0,-1,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureCustoJardinagemIsNegativeTest3() {
        double expected = -1;
        double result = Bloco2.obterCustoJardinagem(0,0,-1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCustoJardinagemIs10Dot83() {
        double expected = 10.83;
        double result = Bloco2.obterCustoJardinagem(1,0,0);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureCustoJardinagemIs21Dot66() {
        double expected = 21.66;
        double result = Bloco2.obterCustoJardinagem(0,1,0);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureCustoJardinagemIs135() {
        double expected = 135;
        double result = Bloco2.obterCustoJardinagem(2,3,3);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureCustoJardinagemIs16Dot11() {
        double expected = 16.11;
        double result = Bloco2.obterCustoJardinagem(0,0,1);
        assertEquals(expected, result,0.01);
    }

    @Test
    void ensureCustoJardinagemIs48Dot61() {
        double expected = 48.61;
        double result = Bloco2.obterCustoJardinagem(1,1,1);
        assertEquals(expected, result,0.01);
    }
}

