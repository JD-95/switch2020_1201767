package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio6Tests {

    @Test
    void obterHorasTest1() {
        double expected = 0;
        double result = Bloco2.obterHoras(0);
        assertEquals(expected, result);
    }

    @Test
    void obterHorasTest2() {
        double expected = 0;
        double result = Bloco2.obterHoras(3599);
        assertEquals(expected, result);
    }

    @Test
    void obterHorasTest3() {
        double expected = 1;
        double result = Bloco2.obterHoras(3600);
        assertEquals(expected, result);
    }

    @Test
    void obterHorasTest4() {
        double expected = 24;
        double result = Bloco2.obterHoras(86400);
        assertEquals(expected, result);
    }

    @Test
    void obterHorasTest5() {
        double expected = 23;
        double result = Bloco2.obterHoras(86399);
        assertEquals(expected, result);
    }

//----------------------------------------------------------------------------------------------------------------------

    @Test
    void obterMinutosTest1() {
        double expected = 0;
        double result = Bloco2.obterMinutos(0);
        assertEquals(expected, result);
    }

    @Test
    void obterMinutosTest2() {
        double expected = 0;
        double result = Bloco2.obterMinutos(59);
        assertEquals(expected, result);
    }

    @Test
    void obterMinutosTest3() {
        double expected = 1;
        double result = Bloco2.obterMinutos(60);
        assertEquals(expected, result);
    }

    @Test
    void obterMinutosTest4() {
        double expected = 59;
        double result = Bloco2.obterMinutos(3599);
        assertEquals(expected, result);
    }

    @Test
    void obterMinutosTest5() {
        double expected = 0;
        double result = Bloco2.obterMinutos(3600);
        assertEquals(expected, result);
    }

//----------------------------------------------------------------------------------------------------------------------

    @Test
    void obterSegundosTest1() {
        double expected = 0;
        double result = Bloco2.obterSegundos(0);
        assertEquals(expected, result);
    }

    @Test
    void obterSegundosTest2() {
        double expected = 59;
        double result = Bloco2.obterSegundos(59);
        assertEquals(expected, result);
    }

    @Test
    void obterSegundosTest3() {
        double expected = 0;
        double result = Bloco2.obterSegundos(60);
        assertEquals(expected, result);
    }

    @Test
    void obterSegundosTest4() {
        double expected = 30;
        double result = Bloco2.obterSegundos(90);
        assertEquals(expected, result);
    }

    //----------------------------------------------------------------------------------------------------------------------
    @Test
    void obterSaudacaoTest1() {
        String expected = "invalido";
        String result = Bloco2.obterSaudacao(-1);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest2() {
        String expected = "boa noite";
        String result = Bloco2.obterSaudacao(0);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest3() {
        String expected = "boa noite";
        String result = Bloco2.obterSaudacao(21599);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest4() {
        String expected = "bom dia";
        String result = Bloco2.obterSaudacao(21600);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest5() {
        String expected = "bom dia";
        String result = Bloco2.obterSaudacao(43200);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest6() {
        String expected = "boa tarde";
        String result = Bloco2.obterSaudacao(43201);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest7() {
        String expected = "boa tarde";
        String result = Bloco2.obterSaudacao(72000);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest8() {
        String expected = "boa noite";
        String result = Bloco2.obterSaudacao(72001);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest9() {
        String expected = "boa noite";
        String result = Bloco2.obterSaudacao(86399);
        assertEquals(expected, result);
    }

    @Test
    void obterSaudacaoTest10() {
        String expected = "invalido";
        String result = Bloco2.obterSaudacao(86400);
        assertEquals(expected, result);
    }
}