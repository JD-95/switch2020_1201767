package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParOuImparTest {
    @Test
    void parOuImpar() {
        String expected = "par";
        String result = Bloco2.parOuImpar(0);
        assertEquals(expected, result);
    }

    @Test
    void parOuImpar2() {
        String expected = "impar";
        String result = Bloco2.parOuImpar(1);
        assertEquals(expected, result);
    }
}
