package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio20Tests {

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest1() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("segunda", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest2() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("terça", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest3() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("quarta", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest4() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("quinta", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest5() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("sexta", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest6() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("sabádo", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest7() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("domingo", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest8() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("feriado", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoDiaSemanaTest9() {
        String expected = "Formato de dia da semana inválido.";
        String result = Bloco2.obterPrecoAluguer("Elementar", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoTipoKitTest1() {
        String expected = "Formato de kit introduzido inválido";
        String result = Bloco2.obterPrecoAluguer("Segunda", "elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoTipoKitTest2() {
        String expected = "Formato de kit introduzido inválido";
        String result = Bloco2.obterPrecoAluguer("Segunda", "semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoTipoKitTest3() {
        String expected = "Formato de kit introduzido inválido";
        String result = Bloco2.obterPrecoAluguer("Segunda", "semicompleto", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoTipoKitTest4() {
        String expected = "Formato de kit introduzido inválido";
        String result = Bloco2.obterPrecoAluguer("Segunda", "completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerInvalidoTipoKitTest5() {
        String expected = "Formato de kit introduzido inválido";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Segunda", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSegundaXElementar() {
        String expected = "O valor do aluguer é de 30 euros.";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerTercaXElementar() {
        String expected = "O valor do aluguer é de 30 euros.";
        String result = Bloco2.obterPrecoAluguer("Terça", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerQuartaXElementar() {
        String expected = "O valor do aluguer é de 30 euros.";
        String result = Bloco2.obterPrecoAluguer("Quarta", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerQuintaXElementar() {
        String expected = "O valor do aluguer é de 30 euros.";
        String result = Bloco2.obterPrecoAluguer("Quinta", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSextaXElementar() {
        String expected = "O valor do aluguer é de 30 euros.";
        String result = Bloco2.obterPrecoAluguer("Sexta", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSabadoXElementar() {
        String expected = "O valor do aluguer é de 40 euros.";
        String result = Bloco2.obterPrecoAluguer("Sábado", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerDomingoXElementar() {
        String expected = "O valor do aluguer é de 40 euros.";
        String result = Bloco2.obterPrecoAluguer("Domingo", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerFeriadoXElementar() {
        String expected = "O valor do aluguer é de 40 euros.";
        String result = Bloco2.obterPrecoAluguer("Feriado", "Elementar", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSegundaXSemiCompleto() {
        String expected = "O valor do aluguer é de 50 euros.";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerTercaXSemiCompleto() {
        String expected = "O valor do aluguer é de 50 euros.";
        String result = Bloco2.obterPrecoAluguer("Terça", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerQuartaXSemiCompleto() {
        String expected = "O valor do aluguer é de 50 euros.";
        String result = Bloco2.obterPrecoAluguer("Quarta", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerQuintaXSemiCompleto() {
        String expected = "O valor do aluguer é de 50 euros.";
        String result = Bloco2.obterPrecoAluguer("Quinta", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSextaXSemiCompleto() {
        String expected = "O valor do aluguer é de 50 euros.";
        String result = Bloco2.obterPrecoAluguer("Sexta", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSabadoXSemiCompleto() {
        String expected = "O valor do aluguer é de 70 euros.";
        String result = Bloco2.obterPrecoAluguer("Sábado", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerDomingoXSemiCompleto() {
        String expected = "O valor do aluguer é de 70 euros.";
        String result = Bloco2.obterPrecoAluguer("Domingo", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerFeriadoXSemiCompleto() {
        String expected = "O valor do aluguer é de 70 euros.";
        String result = Bloco2.obterPrecoAluguer("Feriado", "Semi-completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSegundaXCompleto() {
        String expected = "O valor do aluguer é de 100 euros.";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerTercaXCompleto() {
        String expected = "O valor do aluguer é de 100 euros.";
        String result = Bloco2.obterPrecoAluguer("Terça", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerQuartaXCompleto() {
        String expected = "O valor do aluguer é de 100 euros.";
        String result = Bloco2.obterPrecoAluguer("Quarta", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerQuintaXCompleto() {
        String expected = "O valor do aluguer é de 100 euros.";
        String result = Bloco2.obterPrecoAluguer("Quinta", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSextaXCompleto() {
        String expected = "O valor do aluguer é de 100 euros.";
        String result = Bloco2.obterPrecoAluguer("Sexta", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSabadoXCompleto() {
        String expected = "O valor do aluguer é de 140 euros.";
        String result = Bloco2.obterPrecoAluguer("Sábado", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerDomingoXCompleto() {
        String expected = "O valor do aluguer é de 140 euros.";
        String result = Bloco2.obterPrecoAluguer("Domingo", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerFeriadoXCompleto() {
        String expected = "O valor do aluguer é de 140 euros.";
        String result = Bloco2.obterPrecoAluguer("Feriado", "Completo", 0);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSemanaXElementarXDistancia() {
        String expected = "O valor do aluguer é de 32 euros.";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Elementar", 1);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerFimSemanaXElementarXDistancia() {
        String expected = "O valor do aluguer é de 42 euros.";
        String result = Bloco2.obterPrecoAluguer("Sábado", "Elementar", 1);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSemanaXSemiCompletoXDistancia() {
        String expected = "O valor do aluguer é de 52 euros.";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Semi-completo", 1);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerFimSemanaXSemiCompletoXDistancia() {
        String expected = "O valor do aluguer é de 72 euros.";
        String result = Bloco2.obterPrecoAluguer("Sábado", "Semi-completo", 1);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerSemanaXCompletoXDistancia() {
        String expected = "O valor do aluguer é de 102 euros.";
        String result = Bloco2.obterPrecoAluguer("Segunda", "Completo", 1);
        assertEquals(expected, result);
    }

    @Test
    void ensurePrecoAluguerFimSemanaXCompletoXDistancia() {
        String expected = "O valor do aluguer é de 142 euros.";
        String result = Bloco2.obterPrecoAluguer("Sábado", "Completo", 1);
        assertEquals(expected, result);
    }

}