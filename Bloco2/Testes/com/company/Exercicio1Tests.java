package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio1Tests {

    @Test
    void getMediaPonderadaTest1() {
        double expected = 1.33;
        double result = Bloco2.getMediaPonderada(2, 1, 1, 1, 1, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMediaPonderadaTest2() {
        double nota1 = 1;
        double nota2 = 2;
        double nota3 = 1;
        double peso1 = 1;
        double peso2 = 1;
        double peso3 = 1;
        double expected = 1.33;

        double result = Bloco2.getMediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMediaPonderadaTest3() {
        double nota1 = 1;
        double nota2 = 1;
        double nota3 = 2;
        double peso1 = 1;
        double peso2 = 1;
        double peso3 = 1;
        double expected = 1.33;

        double result = Bloco2.getMediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMediaPonderadaTest4() {
        double nota1 = 1;
        double nota2 = 1;
        double nota3 = 1;
        double peso1 = 2;
        double peso2 = 1;
        double peso3 = 1;
        double expected = 1;

        double result = Bloco2.getMediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMediaPonderadaTest5() {
        double nota1 = 1;
        double nota2 = 1;
        double nota3 = 1;
        double peso1 = 1;
        double peso2 = 2;
        double peso3 = 1;
        double expected = 1;

        double result = Bloco2.getMediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getMediaPonderadaTest6() {
        double nota1 = 1;
        double nota2 = 1;
        double nota3 = 1;
        double peso1 = 1;
        double peso2 = 1;
        double peso3 = 2;
        double expected = 1;

        double result = Bloco2.getMediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        assertEquals(expected, result, 0.01);
    }
}