package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio8Tests {

    @Test
    void ensureMultiploOuDivisorOfBothZero() {
        String expected = "O zero não é divisor de si mesmo.";
        String result = Bloco2.obterMultiploOuDivisor(0, 0);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorOfZeroTest1() {
        String expected = "O zero é múltiplo de qualquer número.";
        String result = Bloco2.obterMultiploOuDivisor(1, 0);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorOfZeroTest2() {
        String expected = "O zero é múltiplo de qualquer número.";
        String result = Bloco2.obterMultiploOuDivisor(0, 1);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorOfBothNegativeNumbers() {
        String expected = "O conceito de múltiplo não se aplica a números negativos.";
        String result = Bloco2.obterMultiploOuDivisor(-1, -1);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorOfNegativeNumbersTest1() {
        String expected = "O conceito de múltiplo não se aplica a números negativos.";
        String result = Bloco2.obterMultiploOuDivisor(-1, 5);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorOfNegativeNumbersTest2() {
        String expected = "O conceito de múltiplo não se aplica a números negativos.";
        String result = Bloco2.obterMultiploOuDivisor(8, -5);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorIsXMultiploOfYTest1() {
        String expected = "X é múltiplo de Y.";
        String result = Bloco2.obterMultiploOuDivisor(10, 5);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorIsXMultiploOfYTest2() {
        String expected = "X é múltiplo de Y.";
        String result = Bloco2.obterMultiploOuDivisor(10, 1);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorIsYMultiploOfXTest1() {
        String expected = "Y é múltiplo de X.";
        String result = Bloco2.obterMultiploOuDivisor(6, 12);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorIsYMultiploOfXTest2() {
        String expected = "Y é múltiplo de X.";
        String result = Bloco2.obterMultiploOuDivisor(1, 23);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorIsNoneTest1() {
        String expected = "X não é múltiplo nem divisor de Y.";
        String result = Bloco2.obterMultiploOuDivisor(23, 13);
        assertEquals(expected, result);
    }

    @Test
    void ensureMultiploOuDivisorIsNoneTest2() {
        String expected = "X não é múltiplo nem divisor de Y.";
        String result = Bloco2.obterMultiploOuDivisor(23, 28);
        assertEquals(expected, result);
    }


}