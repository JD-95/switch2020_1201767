package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio19Tests {

    @Test
    void ensureSalarioSemanalIs269Dot25() {
        String expected = "O salário semanal é de 269,25 euros";
        String result = Bloco2.obterSalarioSemanal(35.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs270() {
        String expected = "O salário semanal é de 270,00 euros";
        String result = Bloco2.obterSalarioSemanal(36);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs271() {
        String expected = "O salário semanal é de 271,00 euros";
        String result = Bloco2.obterSalarioSemanal(36.1);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs280() {
        String expected = "O salário semanal é de 280,00 euros";
        String result = Bloco2.obterSalarioSemanal(37);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs319() {
        String expected = "O salário semanal é de 319,00 euros";
        String result = Bloco2.obterSalarioSemanal(40.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs320() {
        String expected = "O salário semanal é de 320,00 euros";
        String result = Bloco2.obterSalarioSemanal(41);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs321Dot50() {
        String expected = "O salário semanal é de 321,50 euros";
        String result = Bloco2.obterSalarioSemanal(41.1);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs335() {
        String expected = "O salário semanal é de 335,00 euros";
        String result = Bloco2.obterSalarioSemanal(42);
        assertEquals(expected, result);
    }

    @Test
    void ensureSalarioSemanalIs455() {
        String expected = "O salário semanal é de 455,00 euros";
        String result = Bloco2.obterSalarioSemanal(50);
        assertEquals(expected, result);
    }
}