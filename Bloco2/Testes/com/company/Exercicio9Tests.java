package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio9Tests {

    @Test
    void ensureDigitoCentenasIs1() {
        int expected = 1;
        int result = Bloco2.obterDigitoCentenas(100);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoCentenasIs9() {
        int expected = 9;
        int result = Bloco2.obterDigitoCentenas(999);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoDezenasIs1Test1() {
        int expected = 1;
        int result = Bloco2.obterDigitoDezenas(210);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoDezenasIs1Test2() {
        int expected = 1;
        int result = Bloco2.obterDigitoDezenas(910);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoDezenasIs9Test1() {
        int expected = 9;
        int result = Bloco2.obterDigitoDezenas(290);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoDezenasIs9Test2() {
        int expected = 9;
        int result = Bloco2.obterDigitoDezenas(890);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoUnidadesIs1Test1() {
        int expected = 1;
        int result = Bloco2.obterDigitoUnidades(201);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoUnidadesIs1Test2() {
        int expected = 1;
        int result = Bloco2.obterDigitoUnidades(901);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoUnidadesIs9Test1() {
        int expected = 9;
        int result = Bloco2.obterDigitoUnidades(209);
        assertEquals(expected, result);
    }

    @Test
    void ensureDigitoUnidadesIs9Test2() {
        int expected = 9;
        int result = Bloco2.obterDigitoUnidades(809);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsNotPossibleTest1() {
        String expected = "O número introduzido não é um inteiro positivo com 3 dígitos.";
        String result = Bloco2.obterOrdemCrescente(99);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsNotPossibleTest2() {
        String expected = "O número introduzido não é um inteiro positivo com 3 dígitos.";
        String result = Bloco2.obterOrdemCrescente(1000);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsNotPossibleTest3() {
        String expected = "O número introduzido não é um inteiro positivo com 3 dígitos.";
        String result = Bloco2.obterOrdemCrescente(-100);
        assertEquals(expected, result);
    }


    @Test
    void ensureOrdemCrescenteIsNotCrescenteTest1() {
        String expected = "A sequência de algarismos não é crescente.";
        String result = Bloco2.obterOrdemCrescente(100);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsNotCrescenteTest2() {
        String expected = "A sequência de algarismos não é crescente.";
        String result = Bloco2.obterOrdemCrescente(543);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsNotCrescenteTest3() {
        String expected = "A sequência de algarismos não é crescente.";
        String result = Bloco2.obterOrdemCrescente(999);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsCrescenteTest1() {
        String expected = "A sequência de algarismos é crescente.";
        String result = Bloco2.obterOrdemCrescente(123);
        assertEquals(expected, result);
    }

    @Test
    void ensureOrdemCrescenteIsCrescenteTest2() {
        String expected = "A sequência de algarismos é crescente.";
        String result = Bloco2.obterOrdemCrescente(579);
        assertEquals(expected, result);
    }
}