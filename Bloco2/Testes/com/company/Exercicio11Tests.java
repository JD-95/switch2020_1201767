package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio11Tests {

    @Test
    void ensureQualidadeTurmaIsNoneTest1() {
        String expected = "Valor inválido";
        String result = Bloco2.obterQualidadeTurma(1.01, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsNoneTest2() {
        String expected = "Valor inválido";
        String result = Bloco2.obterQualidadeTurma(-0.01, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsMaTest1() {
        String expected = "Turma Má";
        String result = Bloco2.obterQualidadeTurma(0, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsMaTest2() {
        String expected = "Turma Má";
        String result = Bloco2.obterQualidadeTurma(0.19, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsFracaTest1() {
        String expected = "Turma Fraca";
        String result = Bloco2.obterQualidadeTurma(0.2, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsFracaTest2() {
        String expected = "Turma Fraca";
        String result = Bloco2.obterQualidadeTurma(0.49, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsRazoavelTest1() {
        String expected = "Turma Razoável";
        String result = Bloco2.obterQualidadeTurma(0.5, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsRazoavelTest2() {
        String expected = "Turma Razoável";
        String result = Bloco2.obterQualidadeTurma(0.69, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsBoaTest1() {
        String expected = "Turma Boa";
        String result = Bloco2.obterQualidadeTurma(0.70, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsBoaTest2() {
        String expected = "Turma Boa";
        String result = Bloco2.obterQualidadeTurma(0.89, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsExcelenteTest1() {
        String expected = "Turma Excelente";
        String result = Bloco2.obterQualidadeTurma(0.90, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsExcelenteTest2() {
        String expected = "Turma Excelente";
        String result = Bloco2.obterQualidadeTurma(0.99, 0.2, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }


    @Test
    void ensureQualidadeTurmaIsMaTest3() {
        String expected = "Turma Má";
        String result = Bloco2.obterQualidadeTurma(0.3, 0.4, 0.5, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsFracaTest3() {
        String expected = "Turma Fraca";
        String result = Bloco2.obterQualidadeTurma(0.5, 0.2, 0.6, 0.7, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsRazoavelTest3() {
        String expected = "Turma Razoável";
        String result = Bloco2.obterQualidadeTurma(0.7, 0.2, 0.5, 0.8, 0.9);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsBoaTest3() {
        String expected = "Turma Boa";
        String result = Bloco2.obterQualidadeTurma(0.9, 0.2, 0.5, 0.7, 0.95);
        assertEquals(expected, result);
    }

    @Test
    void ensureQualidadeTurmaIsExcelenteTest3() {
        String expected = "Turma Excelente";
        String result = Bloco2.obterQualidadeTurma(0.95, 0.2, 0.5, 0.7, 0.92);
        assertEquals(expected, result);
    }
}