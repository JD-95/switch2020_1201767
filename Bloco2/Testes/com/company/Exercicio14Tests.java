package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio14Tests {

    @Test
    void ensureDistanciaMediaDiariaIsNegativeTest1() {
        String expected = "Valor negativo inserido" ;
        String result = Bloco2.obterDistanciaMediaDiaria(-1,0,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIsNegativeTest2() {
        String expected = "Valor negativo inserido" ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,-1,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIsNegativeTest3() {
        String expected = "Valor negativo inserido" ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,0,-1,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIsNegativeTest4() {
        String expected = "Valor negativo inserido" ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,0,0,-1,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIsNegativeTest5() {
        String expected = "Valor negativo inserido" ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,0,0,0,-1);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs0Dot322Test1() {
        String expected = "A distância média diária da semana de trabalho foi de 0,322 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(1,0,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs0Dot322Test2() {
        String expected = "A distância média diária da semana de trabalho foi de 0,322 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,1,0,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs0Dot322Test3() {
        String expected = "A distância média diária da semana de trabalho foi de 0,322 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,0,1,0,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs0Dot322Test4() {
        String expected = "A distância média diária da semana de trabalho foi de 0,322 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,0,0,1,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs0Dot322Test5() {
        String expected = "A distância média diária da semana de trabalho foi de 0,322 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(0,0,0,0,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs1Dot609() {
        String expected = "A distância média diária da semana de trabalho foi de 1,609 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(1,1,1,1,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureDistanciaMediaDiariaIs4Dot827() {
        String expected = "A distância média diária da semana de trabalho foi de 4,827 km." ;
        String result = Bloco2.obterDistanciaMediaDiaria(1,2,3,4,5);
        assertEquals(expected, result);
    }



}