package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio7Tests {

    @Test
    void ensureCustoTintaIsEqualTo1() {
        double expected = 1;
        double result = Bloco2.obterCustoTinta(1, 1, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTintaDependsOfAreaEdificio() {
        double expected = 10;
        double result = Bloco2.obterCustoTinta(10, 1, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTintaDependsOfRendimentoLitroTinta() {
        double expected = 0.1;
        double result = Bloco2.obterCustoTinta(1, 10, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTintaDependsOfPrecoLitroTinta() {
        double expected = 5;
        double result = Bloco2.obterCustoTinta(1, 1, 5);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureNumeroPintoresIsNegativeTest1() {
        int expected = -1;
        int result = Bloco2.obterNumeroPintores(0);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIsNegativeTest2() {
        int expected = -1;
        int result = Bloco2.obterNumeroPintores(-5);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs1Test1() {
        int expected = 1;
        int result = Bloco2.obterNumeroPintores(1);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs1Test2() {
        int expected = 1;
        int result = Bloco2.obterNumeroPintores(99);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs2Test1() {
        int expected = 2;
        int result = Bloco2.obterNumeroPintores(100);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs2Test2() {
        int expected = 2;
        int result = Bloco2.obterNumeroPintores(299);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs3Test1() {
        int expected = 3;
        int result = Bloco2.obterNumeroPintores(300);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs3Test2() {
        int expected = 3;
        int result = Bloco2.obterNumeroPintores(999);
        assertEquals(expected, result);
    }

    @Test
    void ensureNumeroPintoresIs4Test1() {
        int expected = 4;
        int result = Bloco2.obterNumeroPintores(1000);
        assertEquals(expected, result);
    }


    @Test
    void ensureCustoMaoObraIsEqualTo1() {
        double expected = 1;
        double result = Bloco2.obterCustoMaoObra(1, 1, 1, 8);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoMaoObraDependsOfAreaEdificio() {
        double expected = 10;
        double result = Bloco2.obterCustoMaoObra(10, 1, 1, 8);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoMaoObraDependsOfRendimentoTrabalho() {
        double expected = 0.5;
        double result = Bloco2.obterCustoMaoObra(1, 2, 1, 8);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoMaoObraDependsOfNumeroPintores() {
        double expected = 1;
        double result = Bloco2.obterCustoMaoObra(1, 1, 3, 8);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoMaoObraDependsOfSalarioPintor() {
        double expected = 0.5;
        double result = Bloco2.obterCustoMaoObra(1, 1, 1, 4);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoMaoObraIsEqualTo1Dot33() {
        double expected = 1.33;
        double result = Bloco2.obterCustoMaoObra(8, 3, 6, 4);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsNegativeIfAreaEdificioNull() {
        double expected = -1;
        double result = Bloco2.obterCustoTotalPintura(0, 1, 1, 1, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsNegativeIfRendimentoLitroTintaNull() {
        double expected = -1;
        double result = Bloco2.obterCustoTotalPintura(1, 0, 1, 1, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsNegativeIfPrecoTintaNull() {
        double expected = -1;
        double result = Bloco2.obterCustoTotalPintura(1, 1, 0, 1, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsNegativeIfSalarioPintorNull() {
        double expected = -1;
        double result = Bloco2.obterCustoTotalPintura(1, 1, 1, 0, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsNegativeIfRendimentoTrabalhoNull() {
        double expected = -1;
        double result = Bloco2.obterCustoTotalPintura(1, 1, 1, 1, 0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsEqualsTo2() {
        double expected = 2;
        double result = Bloco2.obterCustoTotalPintura(1, 1, 1, 8, 1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensureCustoTotalPinturaIsEqualsTo1Dot66() {
        double expected = 1.66;
        double result = Bloco2.obterCustoTotalPintura(1, 2, 3, 5, 4);
        assertEquals(expected, result, 0.01);
    }
}