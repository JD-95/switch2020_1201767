package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio15Tests {

    @Test
    void obterCategoriaTrianguloNaoPossivelTest1() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(0,1,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest2() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(1,0,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest3() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(1,1,0);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest4() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(3,1,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest5() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(1,3,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest6() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(1,1,3);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest7() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(2,1,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest8() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(1,2,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloNaoPossivelTest9() {
        String expected = "O triângulo não é possível";
        String result = Bloco2.obterCategoriaTrianguloLados(1,1,2);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsIsoscelesTest1() {
        String expected = "O triângulo é isósceles";
        String result = Bloco2.obterCategoriaTrianguloLados(3,2,2);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsIsoscelesTest2() {
        String expected = "O triângulo é isósceles";
        String result = Bloco2.obterCategoriaTrianguloLados(2,3,2);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsIsoscelesTest3() {
        String expected = "O triângulo é isósceles";
        String result = Bloco2.obterCategoriaTrianguloLados(2,2,3);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsEquilateroTest1() {
        String expected = "O triângulo é equilátero";
        String result = Bloco2.obterCategoriaTrianguloLados(1,1,1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsEquilateroTest2() {
        String expected = "O triângulo é equilátero";
        String result = Bloco2.obterCategoriaTrianguloLados(0.1,0.1,0.1);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsEscalenoTest1() {
        String expected = "O triângulo é escaleno";
        String result = Bloco2.obterCategoriaTrianguloLados(1.3,2,3.2);
        assertEquals(expected, result);
    }

    @Test
    void obterCategoriaTrianguloIsEscalenoTest2() {
        String expected = "O triângulo é escaleno";
        String result = Bloco2.obterCategoriaTrianguloLados(0.1,1.55,1.5);
        assertEquals(expected, result);
    }

}