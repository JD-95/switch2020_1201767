package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio5Tests {

    @Test
    void obterVolumeCuboTest1() {
        double expected = -1;
        double result = Bloco2.obterVolumeCubo(0);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void obterVolumeCuboTest2() {
        double expected = 8;
        double result = Bloco2.obterVolumeCubo(24);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void obterVolumeCuboTest3() {
        double expected = -1;
        double result = Bloco2.obterVolumeCubo(-10);
        assertEquals(expected, result, 0.001);
    }

    @Test
    void obterClassificacaoDoCuboTest1() {
        String expected = "pequeno";
        String result = Bloco2.obterClassificacaoDoCubo(0);
        assertEquals(expected, result);
    }

    @Test
    void obterClassificacaoDoCuboTest2() {
        String expected = "pequeno";
        String result = Bloco2.obterClassificacaoDoCubo(1000);
        assertEquals(expected, result);
    }

    @Test
    void obterClassificacaoDoCuboTest3() {
        String expected = "médio";
        String result = Bloco2.obterClassificacaoDoCubo(1001);
        assertEquals(expected, result);
    }

    @Test
    void obterClassificacaoDoCuboTest4() {
        String expected = "médio";
        String result = Bloco2.obterClassificacaoDoCubo(2000);
        assertEquals(expected, result);
    }

    @Test
    void obterClassificacaoDoCuboTest5() {
        String expected = "grande";
        String result = Bloco2.obterClassificacaoDoCubo(2001);
        assertEquals(expected, result);
    }
}