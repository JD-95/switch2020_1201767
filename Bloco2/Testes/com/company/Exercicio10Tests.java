package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio10Tests {

    @Test
    void ensurePrecoSaldoIsNegativeTest1() {
        double expected = -1;
        double result = Bloco2.obterPrecoSaldo(0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIsNegativeTest2() {
        double expected = -1;
        double result = Bloco2.obterPrecoSaldo(-1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs0Eur80Cent() {
        double expected = 0.80;
        double result = Bloco2.obterPrecoSaldo(1);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs40Eur() {
        double expected = 40;
        double result = Bloco2.obterPrecoSaldo(50);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs35Eur70Cent() {
        double expected = 35.70;
        double result = Bloco2.obterPrecoSaldo(51);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs70Eur() {
        double expected = 70;
        double result = Bloco2.obterPrecoSaldo(100);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs60Eur60Cent() {
        double expected = 60.60;
        double result = Bloco2.obterPrecoSaldo(101);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs120Eur() {
        double expected = 120;
        double result = Bloco2.obterPrecoSaldo(200);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs80Eur40Cent() {
        double expected = 80.4;
        double result = Bloco2.obterPrecoSaldo(201);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ensurePrecoSaldoIs37Eur55Cent() {
        double expected = 37.55;
        double result = Bloco2.obterPrecoSaldo(53.65);
        assertEquals(expected, result, 0.01);
    }
}