package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio16Tests {

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest1() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(0,179,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest2() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(179,0,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest3() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(179,1,0);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest4() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(-1,179,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest5() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(179,-1,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest6() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(179,1,-1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest7() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(179,1,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosNaoPossivelTest8() {
        String expected = "O triângulo não é possível" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(90,45,46);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉRetanguloTest1() {
        String expected = "O triângulo é retângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(90,45,45);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉRetanguloTest2() {
        String expected = "O triângulo é retângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(20,90,70);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉRetanguloTest3() {
        String expected = "O triângulo é retângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(55,35,90);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉObtusanguloTest1() {
        String expected = "O triângulo é obtusângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(91,9,80);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉObtusanguloTest2() {
        String expected = "O triângulo é obtusângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(59,91,30);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉObtusanguloTest3() {
        String expected = "O triângulo é obtusângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(50,39,91);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉObtusanguloTest4() {
        String expected = "O triângulo é obtusângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(178,1,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉObtusanguloTest5() {
        String expected = "O triângulo é obtusângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(1,178,1);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉObtusanguloTest6() {
        String expected = "O triângulo é obtusângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(1,1,178);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉAcutanguloTest1() {
        String expected = "O triângulo é acutângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(89,45,46);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉAcutanguloTest2() {
        String expected = "O triângulo é acutângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(45,89,46);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉAcutanguloTest3() {
        String expected = "O triângulo é acutângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(46,45,89);
        assertEquals(expected, result);
    }

    @Test
    void ensureCategoriaTrianguloAngulosÉAcutanguloTest4() {
        String expected = "O triângulo é acutângulo" ;
        String result = Bloco2.obterCategoriaTrianguloAngulos(60,60,60);
        assertEquals(expected, result);
    }


}