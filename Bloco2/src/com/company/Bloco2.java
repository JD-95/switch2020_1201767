package com.company;

import java.util.Scanner;

public class Bloco2 {

    // user interface
    public static void main(String[] args) {
        //exercicio1();
        //exercicio2();
        //exercicio3();
        //exercicio4();
        //exercicio5();
        //exercicio6();
        //exercicio7();
        //exercicio8();
        //exercicio9();
        //exercicio10();
        //exercicio11();
        //exercicio12();
        //exercicio13();
        //exercicio14();
        //exercicio15();
        //exercicio16();
        //exercicio17();
        //exercicio18();
        //exercicio19();
        //exercicio20();

    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static void exercicio1() {
        // estrutura de dados
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPonderada;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a nota que o aluno obteve na disciplina 1 (arrendondada à unidade):");
        nota1 = ler.nextInt();
        System.out.print("Introduza a nota que o aluno obteve na disciplina 2 (arrendondada à unidade):");
        nota2 = ler.nextInt();
        System.out.print("Introduza a nota que o aluno obteve na disciplina 3 (arrendondada à unidade):");
        nota3 = ler.nextInt();
        System.out.print("Introduza o peso atribuído à disciplina 1:");
        peso1 = ler.nextInt();
        System.out.print("Introduza o peso atribuído à disciplina 2:");
        peso2 = ler.nextInt();
        System.out.print("Introduza o peso atribuído à disciplina 3:");
        peso3 = ler.nextInt();

        // processamento
        mediaPonderada = getMediaPonderada(nota1, nota2, nota3, peso1, peso2, peso3);

        // saida de dados
        if (mediaPonderada >= 8)
            System.out.println("O aluno cumpre a nota mínima exigida, com uma média ponderada de " + String
                    .format("%.1f", mediaPonderada) + " valores.");

        else
            System.out.println(
                    "Infelizmente, o aluno NÃO CUMPRE a nota mínima exigida, com uma média ponderada de " + String
                            .format("%.1f", mediaPonderada) + " valores.");
    }

    public static void exercicio2() {
        // estrutura de dados
        //int numero, digito1, digito2, digito3;

        // leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o número:");
        int numero = ler.nextInt();

        // processamento
        if (numero < 100 || numero > 999) {
            System.out.print("Número introduzido não tem 3 digitos.");
        } else {
            int digito3 = numero % 10;
            int digito2 = (numero / 10) % 10;
            int digito1 = (numero / 100) % 10;
            System.out.print(digito1 + " " + digito2 + " " + digito3);
        }
    }

    public static void exercicio3() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o valor de x para o ponto 1:");
        double x1 = ler.nextDouble();
        System.out.print("Introduza o valor de y para o ponto 1:");
        double y1 = ler.nextDouble();
        System.out.print("Introduza o valor de x para o ponto 2:");
        double x2 = ler.nextDouble();
        System.out.print("Introduza o valor de y para o ponto 2:");
        double y2 = ler.nextDouble();

        // processamento
        double distanciaEntreOsPontos = obterDistanciaEntre2Pontos(x1, y1, x2, y2);

        // saida de dados
        System.out.print("A distância entre os dois pontos introduzidos é de " + String
                .format("%.2f", distanciaEntreOsPontos));
    }

    public static double obterDistanciaEntre2Pontos(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static void exercicio4() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o valor de x:");
        double x = ler.nextDouble();

        // invocar processamento
        double funcaoDeX = obterFuncaoDeX(x);

        // saida de dados
        System.out.print("O valor da função de x é " + funcaoDeX);
    }

    public static Double obterFuncaoDeX(double x) {
        double funcaoDeX;
        if (x <= 0) {                          // assumindo que se x for negativo ou zero o valor da função será igual a x
            funcaoDeX = x;
        } else {
            funcaoDeX = (x * x) - 2 * x;
        }
        return funcaoDeX;
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static double obterVolumeCubo(double area) {
        if (area > 0) {
            double aresta = Math.sqrt(area / 6);
            return Math.pow(aresta, 3);
        } else {
            return -1; // em caso de area invalida retorna sempre -1
        }
    }

    public static String obterClassificacaoDoCubo(double volume) {
        if (volume < 0) {
            return "area/volume inválido"; // assumindo que a area invalida retona um volume de -1
        } else if (volume <= 1000) {  // 1000 cm3 = 1 dm3 , mesma conversão abaixo
            return "pequeno";
        } else if (volume > 1000 && volume <= 2000) {
            return "médio";
        } else {
            return "grande";
        }
    }

    public static void exercicio5() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a área do cubo:");
        double area = ler.nextDouble();

        // invocar o processamento
        double volume = obterVolumeCubo(area);
        String categoria = obterClassificacaoDoCubo(volume);

        // saída de dados
        if (volume > 0) {
            System.out.print("O volume do cubo é de " + String
                    .format("%.1f", volume) + " cm3 e é considerado um cubo " + categoria + ".");
        } else {
            System.out.print("O valor da área introduzido não é válido");
        }
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static int obterHoras(int tempo) {
        return tempo / 3600;
    }

    public static int obterMinutos(int tempo) {
        return (tempo % 3600) / 60;
    }

    public static int obterSegundos(int tempo) {
        return (tempo % 3600) % 60;
    }

    public static String obterSaudacao(int tempo) {

        if (tempo <= 86399 && tempo >= 0) {  // impedir entrada de formato maior que 24 horas e números negativos
            if (tempo >= 21600 && tempo <= 43200) {   // corresponde a 6h às 12h
                return "bom dia";
            }
            if (tempo >= 43201 && tempo <= 72000) {  // corresponde 12h00m01 às 20h
                return "boa tarde";
            } else {
                return "boa noite";   // corresponde 20h00m01 às 5h59m59
            }
        } else {
            return "invalido";
        }
    }

    public static void exercicio6() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o tempo em segundos:");
        int tempo = ler.nextInt();

        // invocar processamento
        int horas = obterHoras(tempo);
        int minutos = obterMinutos(tempo);
        int segundos = obterSegundos(tempo);
        String saudacao = obterSaudacao(tempo);

        if (saudacao.equals("invalido")) {
            System.out.print("O tempo introduzido não é válido.");
        } else {
            System.out
                    .print("O momento do dia em que se encontra é " + horas + "h:" + minutos + "m:" + segundos + "s. Está na hora de dizer " + saudacao + " :D!");
        }
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static double obterCustoTinta(double areaEdificio, double rendimentoLitroTinta, double precoLitroTinta) {
        return (areaEdificio / rendimentoLitroTinta) * precoLitroTinta;  // rendimento tinta = m2 / l de tinta
    }

    public static int obterNumeroPintores(double areaEdificio) {
        if (areaEdificio <= 0) {
            return -1;                 // retorna -1 quando a area não é válida
        }
        if (areaEdificio > 0 && areaEdificio < 100) {
            return 1;
        } else if (areaEdificio >= 100 && areaEdificio < 300) {
            return 2;
        } else if (areaEdificio >= 300 && areaEdificio < 1000) {
            return 3;
        } else {
            return 4;
        }
    }

    public static double obterCustoMaoObra(double areaEdificio, double rendimentoTrabalho, int numeroPintores,
            double salarioPintor) {
        return (areaEdificio / (rendimentoTrabalho * numeroPintores)) * numeroPintores * (salarioPintor / 8); // salario ao dia -> divindo por 8 horas temos o salário por hora de trabalho
    }

    public static double obterCustoTotalPintura(double areaEdificio, double rendimentoLitroTinta, double precoTinta,
            double salarioPintor, double rendimentoTrabalho) {
        int numeroPintores = obterNumeroPintores(areaEdificio);
        if (numeroPintores <= 0 || salarioPintor <= 0 || rendimentoLitroTinta <= 0 || precoTinta <= 0 || rendimentoTrabalho <= 0) {
            return -1;  // retorna -1 para valores negativos ou de zero das dados introduzidos
        } else {
            double custoTinta = obterCustoTinta(areaEdificio, rendimentoLitroTinta, precoTinta);
            double custoMaoObra = obterCustoMaoObra(areaEdificio, rendimentoTrabalho, numeroPintores, salarioPintor);
            return custoMaoObra + custoTinta;
        }
    }

    public static void exercicio7() {
        //entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a area do prédio (em m2):");
        double areaEdificio = ler.nextDouble();
        System.out.print("Introduza o rendimento do litro de tinta (em m2/litro de tinta):");
        double rendimentoLitroTinta = ler.nextDouble();
        System.out.print("Introduza o preço do litro de tinta:");
        double precoTinta = ler.nextDouble();
        System.out.print("Introduza o salario do pintor ao dia (8 horas de trabalho):");
        double salarioPintor = ler.nextDouble();
        double rendimentoTrabalho = 2;  // metros quadrados por hora de trabalho, assumido no exercicio

        // invocar processamento
        double custoTinta = obterCustoTinta(areaEdificio, rendimentoLitroTinta, precoTinta);
        int numeroPintores = obterNumeroPintores(areaEdificio);
        double custoMaoObra = obterCustoMaoObra(areaEdificio, rendimentoTrabalho, numeroPintores, salarioPintor);
        double custoPinturaEdificio = obterCustoTotalPintura(areaEdificio, rendimentoLitroTinta, precoTinta,
                salarioPintor, rendimentoTrabalho);

        // saida de dados
        if (custoPinturaEdificio > 0) {
            System.out
                    .print("o custo de pintura do edificio é de " + custoPinturaEdificio + " euros, (custo de tinta = " + custoTinta + " + custo da mão de obra = " + custoMaoObra + " euros).");
        } else {
            System.out.print("Cálculo inválido: um ou mais valores introduzidos são inferiores ou iguais a zero.");
        }
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterMultiploOuDivisor(double numeroX, double numeroY) {
        double restoDivisaoXPorY = numeroX % numeroY;
        double restoDivisaoYPorX = numeroY % numeroX;

        if (numeroX < 0 || numeroY < 0) {   // assumindo que múltiplos são todos os números que se obtêm multiplicando um dado número NATURAL por 1,2,3,4,5,…
            return "O conceito de múltiplo não se aplica a números negativos.";
        } else if (numeroX == 0 && numeroY == 0) {
            return "O zero não é divisor de si mesmo.";
        } else if (numeroX == 0 || numeroY == 0) {
            return "O zero é múltiplo de qualquer número.";
        }

        //else if (numeroX == numeroY) {
        //   return "X e Y são múltiplos um do outro.";
        //}

        else {
            if (restoDivisaoXPorY == 0) {
                return "X é múltiplo de Y.";
            } else if (restoDivisaoYPorX == 0) {
                return "Y é múltiplo de X.";
            } else {
                return "X não é múltiplo nem divisor de Y.";
            }
        }
    }

    public static void exercicio8() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o valor do número X:");
        double numeroX = ler.nextDouble();
        System.out.print("Introduza o valor do número Y:");
        double numeroY = ler.nextDouble();

        // invocar processamento
        String multiplo = obterMultiploOuDivisor(numeroX, numeroY);

        // saida de dados
        System.out.print(multiplo);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static int obterDigitoCentenas(int numero3Digitos) {
        return numero3Digitos / 100;
    }

    public static int obterDigitoDezenas(int numero3Digitos) {
        return (numero3Digitos % 100) / 10;
    }

    public static int obterDigitoUnidades(int numero3Digitos) {
        return ((numero3Digitos % 100) % 10);
    }

    public static String obterOrdemCrescente(int numero3Digitos) {
        int digito1 = obterDigitoCentenas(numero3Digitos);
        int digito2 = obterDigitoDezenas(numero3Digitos);
        int digito3 = obterDigitoUnidades(numero3Digitos);

        if (numero3Digitos < 100 || numero3Digitos > 999) { // assumindo números inteiros entre 100 e 999
            return "O número introduzido não é um inteiro positivo com 3 dígitos.";
        } else {

            if (digito1 < digito2 && digito2 < digito3) {
                return "A sequência de algarismos é crescente.";
            } else {
                return "A sequência de algarismos não é crescente.";
            }
        }
    }

    public static void exercicio9() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza um número com 3 dígitos:");
        int numero3Digitos = ler.nextInt();

        // invocar processamento
        String crescenteOuNao = obterOrdemCrescente(numero3Digitos);

        // saida de dados
        System.out.print(crescenteOuNao);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static double obterPrecoSaldo(double precoOriginal) {
        if (precoOriginal <= 0) {
            return -1;  // retorna -1 quando preço for zero ou negativo
        } else if (precoOriginal <= 50) {                     // se passa aqui sabemos que é maior que 50
            return precoOriginal - (precoOriginal * 0.2);
        } else if (precoOriginal <= 100) {  // precoOriginal > 50 isto passa a nao ser significar nada e por assim adiante
            return precoOriginal - (precoOriginal * 0.3);
        } else if (precoOriginal <= 200) {
            return precoOriginal - (precoOriginal * 0.4);
        } else {
            return precoOriginal - (precoOriginal * 0.6);
        }
    }

    public static void exercicio10() {
        // entrada de dados
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o preço original do artigo (em euros):");
        double precoOriginal = ler.nextDouble();

        // invocar processamento
        double precoSaldo = obterPrecoSaldo(precoOriginal);

        // saida de dados
        System.out.print("O preço do artigo em saldo é de " + String.format("%.2f",
                precoSaldo) + " euros."); //assumindo a segunda casa decimal como a final neste tipo de negócio
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterQualidadeTurma(double taxaAprovados, double limiteMa, double limiteFraca,
            double limiteRazoavel, double limiteBoa) {
        if (taxaAprovados < 0 || taxaAprovados > 1) {
            return "Valor inválido";
        } else if (taxaAprovados < limiteMa) {
            return "Turma Má";
        } else if (taxaAprovados < limiteFraca) {
            return "Turma Fraca";
        } else if (taxaAprovados < limiteRazoavel) {
            return "Turma Razoável";
        } else if (taxaAprovados < limiteBoa) {
            return "Turma Boa";
        } else {
            return "Turma Excelente";
        }

    }

    public static void exercicio11() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a taxa de aprovados (entre 0 e 1):");
        double taxaAprovados = ler.nextDouble();
        if (taxaAprovados < 0 || taxaAprovados > 1) {
            System.out.print("O valor da taxa de aprovados é inválido.");

        } else {
            System.out.print("Introduza o limite superior da categoria Turma Má (entre 0 e 1):");
            double limiteMa = ler.nextDouble();
            System.out.print("Introduza o limite superior da categoria Turma Fraca (entre 0 e 1):");
            double limiteFraca = ler.nextDouble();
            System.out.print("Introduza o limite superior da categoria Turma Razoável (entre 0 e 1):");
            double limiteRazoavel = ler.nextDouble();
            System.out.print("Introduza o limite superior da categoria Turma Boa (entre 0 e 1):");
            double limiteBoa = ler.nextDouble();

            String qualidadeTurma = obterQualidadeTurma(taxaAprovados, limiteMa, limiteFraca, limiteRazoavel,
                    limiteBoa);

            System.out.print("A sua turma está categorizada como " + qualidadeTurma + ".");
        }
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterNotificacaoProtecaoAmbiente(double nivelPoluicao) {
        if (nivelPoluicao < 0) {
            return "Demasiado otimista, não há nível de poluição negativo.";
        }
        if (nivelPoluicao <= 0.3) {
            return "O nível de poluição é aceitável.";
        }
        if (nivelPoluicao <= 0.4) {
            return "Intimidar indústrias do 1º GRUPO a SUSPENDEREM a sua atividade.";
        }
        if (nivelPoluicao <= 0.5) {
            return "Intimidar indústrias do 1º e 2º GRUPO a SUSPENDEREM a sua atividade.";
        } else {
            return "Intimidar indústrias do 1º, 2º e 3º GRUPO a PARALISAREM a sua atividade.";
        }
    }

    public static void exercicio12() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o nível de poluição atual:");
        double nivelPoluicao = ler.nextDouble();

        String notificacaoProtecaoAmbiente = obterNotificacaoProtecaoAmbiente(nivelPoluicao);

        System.out.print(notificacaoProtecaoAmbiente);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static double obterHorasJardinagem(double areaGrama, int numeroArvores, int numeroArbustos) {
        if (areaGrama < 0 || numeroArvores < 0 || numeroArbustos < 0) {
            return -1;   // retorna -1 caso algum dado negativo fornecido
        } else {
            return ((areaGrama * 300) + (numeroArvores * 600) + (numeroArbustos * 400)) / 3600;
        }
    }

    public static double obterCustoJardinagem(double areaGrama, int numeroArvores, int numeroArbustos) {
        double horasJardinagem = obterHorasJardinagem(areaGrama, numeroArvores, numeroArbustos);
        if (horasJardinagem < 0) {
            return -1;   // retorna -1 caso algum dado negativo fornecido
        } else {
            return ((areaGrama * 10) + (numeroArvores * 20) + (numeroArbustos * 15)) + (horasJardinagem * 10);
        }
    }

    public static void exercicio13() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a área em m2 onde será colocada a grama:");
        double areaGrama = ler.nextDouble();
        System.out.print("Introduza o número de árvores:");
        int numeroArvores = ler.nextInt();
        System.out.print("Introduza o número de arbustos:");
        int numeroArbustos = ler.nextInt();

        double custoJardinagem = obterCustoJardinagem(areaGrama, numeroArvores, numeroArbustos);
        double horasJardinagem = obterHorasJardinagem(areaGrama, numeroArvores, numeroArbustos);

        System.out.print("O serviço contratado necessita de " + Math
                .round(horasJardinagem) + " horas e terá um custo associado de " + String
                .format("%.2f", custoJardinagem) + " euros.");
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterChegadaDoComboio(int horaPartida, int minutoPartida, int horasDecorridas,
            int minutosDecorridos) {
        // assumindo que não há duração de viagem superior a 24 horas
        String diaChegada = "hoje";
        int chegada = ((horaPartida * 60) + minutoPartida) + ((horasDecorridas * 60) + minutosDecorridos);
        int horaChegada = chegada / 60;
        int minutoChegada = chegada % 60;

        if (horasDecorridas >= 24) {
            return "Duração inválida: superior a 24 horas";
        }
        if (horaPartida >= 24) {
            return "Hora de partida inválida: superior a 23h";
        }

        if (minutoPartida >= 60) {
            return "Minuto de partida inválido: superior a 59 min";
        }

        if (horaChegada > 23) {
            horaChegada = horaChegada - 24;
            diaChegada = "amanhã";
        }

        return "Chegada às " + horaChegada + " horas e " + minutoChegada + " minutos do dia de " + diaChegada + ".";
    }

    public static void exercicio17() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a hora de partida do comboio:");
        int horaPartida = ler.nextInt();
        System.out.print("Introduza o minuto de partida do comboio:");
        int minutoPartida = ler.nextInt();
        System.out.print("Introduza as horas de duração da viagem do comboio:");
        int horasDecorridas = ler.nextInt();
        System.out.print("Introduza os minutos de duração da viagem do comboio:");
        int minutosDecorridos = ler.nextInt();

        String chegadaComboio = obterChegadaDoComboio(horaPartida, minutoPartida, horasDecorridas, minutosDecorridos);

        System.out.print(chegadaComboio);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterFimProcessamento(int horaInicio, int minutoInicio, int segundoInicio, int segundosProcessamento) {
        int tempoInicial = (horaInicio * 3600) + (minutoInicio * 60) + segundoInicio;
        int tempoFinal = tempoInicial + segundosProcessamento;
        int horaFinal = tempoFinal / 3600;
        int minutoFinal = (tempoFinal % 3600) / 60;
        int segundoFinal = (tempoFinal % 3600) % 60;

        if (horaInicio >= 0 && horaInicio <= 23 && minutoInicio >= 0 && minutoInicio <= 59 && segundoInicio >=
                0 && segundoInicio <= 59) {
            if (horaFinal > 23) {
                horaFinal = horaFinal - 24;
                return "O processamento termina às " + horaFinal + ":" + minutoFinal + ":" + segundoFinal + " do dia seguinte";
            }

            return "O processamento termina às " + horaFinal + ":" + minutoFinal + ":" + segundoFinal;
        } else {
            return "Formato de dados inválido";
        }
    }

    public static void exercicio18() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a hora inicial do processamento:");
        int horaInicial = ler.nextInt();
        System.out.print("Introduza o minuto inicial do processamento:");
        int minutoInicial = ler.nextInt();
        System.out.print("Introduza o segundo inicial do processamento:");
        int segundoInicial = ler.nextInt();
        System.out.print("Introduza os segundos de duração do processamento:");
        int segundosProcessamento = ler.nextInt();

        String tempoFinal = obterFimProcessamento(horaInicial, minutoInicial, segundoInicial, segundosProcessamento);

        System.out.print(tempoFinal);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterDistanciaMediaDiaria(double distanciaDia1, double distanciaDia2, double distanciaDia3,
            double distanciaDia4, double distanciaDia5) {
        double distanciaMediaDiaria = ((distanciaDia1 + distanciaDia2 + distanciaDia3 + distanciaDia4 + distanciaDia5) / 5.000) * 1.609; //*1.609 convertendo assim milhas para km
        if (distanciaDia1 < 0 || distanciaDia2 < 0 || distanciaDia3 < 0 || distanciaDia4 < 0 || distanciaDia5 < 0) {
            return "Valor negativo inserido"; // retorna -1 caso distancias negativas inseridas
        } else {
            return "A distância média diária da semana de trabalho foi de " + String
                    .format("%.3f", distanciaMediaDiaria) + " km.";
        }
    }

    public static void exercicio14() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a distância percorrida no dia 1 (em milhas):");
        double distanciaDia1 = ler.nextDouble();
        System.out.print("Introduza a distância percorrida no dia 2 (em milhas):");
        double distanciaDia2 = ler.nextDouble();
        System.out.print("Introduza a distância percorrida no dia 3 (em milhas):");
        double distanciaDia3 = ler.nextDouble();
        System.out.print("Introduza a distância percorrida no dia 4 (em milhas):");
        double distanciaDia4 = ler.nextDouble();
        System.out.print("Introduza a distância percorrida no dia 5 (em milhas):");
        double distanciaDia5 = ler.nextDouble();

        String distanciaMediaDiaria = obterDistanciaMediaDiaria(distanciaDia1, distanciaDia2, distanciaDia3, distanciaDia4,
                distanciaDia5);

        System.out.print(distanciaMediaDiaria);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterCategoriaTrianguloLados(double ladoATriangulo, double ladoBTriangulo, double ladoCTriangulo) {
        if (ladoATriangulo > 0 && ladoBTriangulo > 0 && ladoCTriangulo > 0 && ladoATriangulo < ladoBTriangulo + ladoCTriangulo && ladoBTriangulo < ladoATriangulo + ladoCTriangulo && ladoCTriangulo < ladoBTriangulo + ladoATriangulo) {

            if (ladoATriangulo == ladoBTriangulo && ladoATriangulo == ladoCTriangulo && ladoCTriangulo == ladoBTriangulo) {
                return "O triângulo é equilátero";
            } else if (ladoATriangulo == ladoBTriangulo || ladoATriangulo == ladoCTriangulo || ladoCTriangulo == ladoBTriangulo) {
                return "O triângulo é isósceles";
            } else {
                return "O triângulo é escaleno";
            }

        } else {
            return "O triângulo não é possível";
        }
    }

    public static void exercicio15() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a lado A do triângulo:");
        double ladoATriangulo = ler.nextDouble();
        System.out.print("Introduza a lado B do triângulo:");
        double ladoBTriangulo = ler.nextDouble();
        System.out.print("Introduza a lado C do triângulo:");
        double ladoCTriangulo = ler.nextDouble();

        String categoriaTrianguloLados = obterCategoriaTrianguloLados(ladoATriangulo, ladoBTriangulo, ladoCTriangulo);

        System.out.print(categoriaTrianguloLados);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterCategoriaTrianguloAngulos(double anguloATriangulo, double anguloBTriangulo,
            double anguloCTriangulo) {
        if (anguloATriangulo > 0 && anguloBTriangulo > 0 && anguloCTriangulo > 0 && anguloATriangulo + anguloBTriangulo + anguloCTriangulo == 180) {

            if (anguloATriangulo == 90 || anguloBTriangulo == 90 || anguloCTriangulo == 90) {
                return "O triângulo é retângulo";
            } else if (anguloATriangulo > 90 || anguloBTriangulo > 90 || anguloCTriangulo > 90) {
                return "O triângulo é obtusângulo";
            } else {
                return "O triângulo é acutângulo";
            }

        } else {
            return "O triângulo não é possível";
        }
    }

    public static void exercicio16() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza a ângulo A do triângulo:");
        double anguloATriangulo = ler.nextDouble();
        System.out.print("Introduza a ângulo B do triângulo:");
        double anguloBTriangulo = ler.nextDouble();
        System.out.print("Introduza a ângulo C do triângulo:");
        double anguloCTriangulo = ler.nextDouble();

        String categoriaTrianguloAngulos = obterCategoriaTrianguloAngulos(anguloATriangulo, anguloBTriangulo, anguloCTriangulo);

        System.out.print(categoriaTrianguloAngulos);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterSalarioSemanal(double horasTrabalhadas) {
        if (horasTrabalhadas <= 36) {
            double salarioSemanal = horasTrabalhadas * 7.5;
            return "O salário semanal é de " + String.format("%.2f", salarioSemanal) + " euros";
        } else if (horasTrabalhadas > 36 && horasTrabalhadas <= 41) {
            double horasExtra10Euros = (horasTrabalhadas - 36);
            double horasNormais = (horasTrabalhadas - horasExtra10Euros);
            double salarioSemanal = (horasNormais * 7.5) + (horasExtra10Euros * 10);
            return "O salário semanal é de " + String.format("%.2f", salarioSemanal) + " euros";
        } else {
            double horasExtra10Euros = 5;
            double horasExtra15Euros = (horasTrabalhadas - 41);
            double horasNormais = (horasTrabalhadas - horasExtra10Euros - horasExtra15Euros);
            double salarioSemanal = (horasNormais * 7.5) + (horasExtra10Euros * 10) + (horasExtra15Euros * 15);
            return "O salário semanal é de " + String.format("%.2f", salarioSemanal) + " euros";
        }
    }

    public static void exercicio19() {
        Scanner ler = new Scanner(System.in);
        System.out.print("Introduza o número de horas trabalhas:");
        double horasTabalhadas = ler.nextDouble();

        String salarioSemanal = obterSalarioSemanal(horasTabalhadas);

        System.out.print(salarioSemanal);
    }

//--------------------------------------------------------------------------------------------------------------------------------

    public static String obterPrecoAluguer(String diaDaSemana, String tipoKit, double distanciaCasaCliente) {
        boolean fimDeSemanaOuFeriado = diaDaSemana.equals("Sábado") || diaDaSemana.equals("Domingo") || diaDaSemana
                .equals("Feriado");
        boolean diaDeSemana = diaDaSemana.equals("Segunda") || diaDaSemana.equals("Terça") || diaDaSemana
                .equals("Quarta") || diaDaSemana
                .equals("Quinta") || diaDaSemana.equals("Sexta");

        if (tipoKit.equals("Elementar")) {
            if (fimDeSemanaOuFeriado) {
                double precoAluguer = 40 + (distanciaCasaCliente * 2);
                return "O valor do aluguer é de " + String.format("%.0f", precoAluguer) + " euros.";
            }
            if (diaDeSemana) {
                double precoAluguer = 30 + (distanciaCasaCliente * 2);
                return "O valor do aluguer é de " + String.format("%.0f", precoAluguer) + " euros.";
            } else {
                return "Formato de dia da semana inválido.";
            }
        }

        if (tipoKit.equals("Semi-completo")) {
            if (fimDeSemanaOuFeriado) {
                double precoAluguer = 70 + (distanciaCasaCliente * 2);
                return "O valor do aluguer é de " + String.format("%.0f", precoAluguer) + " euros.";
            }
            if (diaDeSemana) {
                double precoAluguer = 50 + (distanciaCasaCliente * 2);
                return "O valor do aluguer é de " + String.format("%.0f", precoAluguer) + " euros.";
            } else {
                return "Formato de dia da semana inválido.";
            }
        }

        if (tipoKit.equals("Completo")) {
            if (fimDeSemanaOuFeriado) {
                double precoAluguer = 140 + (distanciaCasaCliente * 2);
                return "O valor do aluguer é de " + String.format("%.0f", precoAluguer) + " euros.";
            }
            if (diaDeSemana) {
                double precoAluguer = 100 + (distanciaCasaCliente * 2);
                return "O valor do aluguer é de " + String.format("%.0f", precoAluguer) + " euros.";
            } else {
                return "Formato de dia da semana inválido.";
            }

        } else {
            return "Formato de kit introduzido inválido";
        }
    }

    public static void exercicio20() {
        Scanner ler = new Scanner(System.in);
        System.out
                .print("Introduza o dia da semana (no formato \"Segunda\", \"Terça\", ...), e se for Feriado, insira \"Feriado\": ");
        String diaDaSemana = ler.next();
        System.out.print("Introduza o tipo de kit pretendido (\"Elementar\", \"Semi-completo\" ou \"Completo\"): ");
        String tipoKit = ler.next();
        System.out.print("Introduza a distância da sua casa à empresa (em km):");
        Double distanciaCasaCliente = ler.nextDouble();

        String custoAluguer = obterPrecoAluguer(diaDaSemana, tipoKit, distanciaCasaCliente);

        System.out.print(custoAluguer);


    }

//--------------------------------------------------------------------------------------------------------------------------------


    // para realizar os testes
    public static String exercicio2V2(int numero) {
        String result;
        if (numero < 100 || numero > 999) {
            result = "nao tem 3 digitos";
        } else {
            int digito3 = numero % 10;
            int digito2 = (numero / 10) % 10;
            int digito1 = (numero / 100) % 10;
            result = digito1 + " " + digito2 + " " + digito3;
        }
        return result;
    }


    // processamentos isolados
    public static double getMediaPonderada(double nota1, double nota2, double nota3, double peso1, double peso2,
            double peso3) {
        return ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
    }


    // exemplo aula terça
    public static String parOuImpar(int valor) {
        if (valor % 2 == 0)
            return "par";
        else
            return "impar";
    }
// teste commit

}
