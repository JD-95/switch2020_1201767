public class Vector {
    // Attributes
    private int[] vector;

    // Constructor
    public Vector() {
        this.vector = new int[0];
    }

    public Vector(int[] vector) {
        if (vector == null) {
            throw new IllegalArgumentException("array should not be null");
        }
        this.vector = copyElementsFromArray(vector, vector.length);
    }

    // Operations
    private int[] copyElementsFromArray(int[] vector, int size) {

        int[] copyArray = new int[size];

        for (int index = 0; index < size && index < vector.length; index++) {
            copyArray[index] = vector[index];
        }
        return copyArray;
    }

    private int[] copyElementsFromArray(int[] vector, int size, int startIndex) {
        int[] copyArray = new int[size];

        for (int index = 0, index2 = startIndex; index < size && index < vector.length; index++) {
            copyArray[index] = vector[index2];
            index2++;
        }
        return copyArray;
    }

    public Vector createCopy() {
        int[] elements = this.toArray();
        Vector copy = new Vector(elements);
        return copy;
    }

    public boolean addElement(int newElementValue) {
        int newSize = this.vector.length + 1;
        this.vector = copyElementsFromArray(this.vector, newSize);
        this.vector[newSize - 1] = newElementValue;
        return true;
    }

    public boolean removeElement(int indexElement) {
        if (indexElement < 0 || indexElement >= this.vector.length) {
            throw new ArrayIndexOutOfBoundsException("index out of range");
        }
        int[] leftSide = copyElementsFromArray(this.vector, indexElement);
        int[] rightSide = copyElementsFromArray(this.vector, this.vector.length - indexElement - 1, indexElement + 1);

        this.vector = join(leftSide, rightSide);
        return true;
    }

    private int[] join(int[] vector1, int[] vector2) {

        int[] result = new int[vector1.length + vector2.length];

        for (int index = 0; index < vector1.length; index++) {
            result[index] = vector1[index];
        }
        for (int index = 0; index < vector2.length; index++) {
            result[index + vector1.length] = vector2[index];
        }

        return result;
    }

    public boolean removeFirstElementOfGivenValue(int value) {
        int index = getIndexOfFirstElementOfGivenValue(value);
        removeElement(index);
        return true;
    }

    private int getIndexOfFirstElementOfGivenValue(int value) {
        if (!exists(value)) {
            throw new IllegalArgumentException("value not found in array");
        }

        int index = 0;
        boolean firstElement = false;
        for (int pos = 0; pos < this.vector.length && !firstElement; pos++) {
            if (this.vector[pos] == value) {
                index = pos;
                firstElement = true;
            }
        }
        return index;
    }

    private boolean exists(int value) {
        for (int element : this.vector) {
            if (element == value) {
                return true;
            }
        }
        return false;
    }

    public int[] toArray() {
        return this.copyElementsFromArray(this.vector, this.vector.length);
    }

    public int getValueOfElement(int indexElement) {
        if (indexElement < 0 || indexElement >= this.vector.length) {
            throw new ArrayIndexOutOfBoundsException("index out of range");
        }
        return this.vector[indexElement];
    }

    public int length() {
        return this.vector.length;
    }

    public boolean isEmpty() {
        return this.vector.length == 0;
    }

    private void checkEmpty() {
        if (isEmpty()) {
            throw new IllegalStateException("array without elements");
        }
    }

    public boolean isSingleElement() {
        return this.vector.length == 1;
    }

    public int getMaxValue() {
        checkEmpty();
        int maxValue = this.vector[0];

        for (int element : this.vector) {
            if (element > maxValue) {
                maxValue = element;
            }
        }

        return maxValue;
    }

    public Vector getIndexOfGivenValue(int value) {
        checkEmpty();
        Vector result = new Vector();

        for (int index = 0; index < this.length(); index++) {
            if (this.vector[index] == value) {
                result.addElement(index);
            }
        }

        return result;
    }

    public int getMinValue() {
        checkEmpty();
        int minValue = this.vector[0];

        for (int element : this.vector) {
            if (element < minValue) {
                minValue = element;
            }
        }

        return minValue;
    }

    public double getElementsAverage() {
        checkEmpty();
        return getElementsSum() / (double) length();
    }

    public int getElementsSum() {
        int elementsSum = 0;
        for (int element : this.vector) {
            elementsSum += element;
        }

        return elementsSum;
    }

    public double getEvenElementsAverage() {
        Vector evenElementsVector = getEvenElements();
        if (evenElementsVector.isEmpty()) {
            // throw new IllegalArgumentException("array without even elements"); // verificar se isto é correto
            return 0;
        }
        return evenElementsVector.getElementsAverage();
    }

    public double getOddElementsAverage() {
        Vector evenElementsVector = getOddElements();
        if (evenElementsVector.isEmpty()) {
            //throw new IllegalArgumentException("array without odd elements"); // verificar se isto é correto
            return 0;
        }
        return evenElementsVector.getElementsAverage();
    }

    private Vector getEvenElements() {
        checkEmpty();
        Vector evenElementsVector = new Vector();

        for (int element : this.vector) {
            if (Utilities.isEvenNumber(element)) {
                evenElementsVector.addElement(element);
            }
        }

        return evenElementsVector;
    }

    private Vector getOddElements() {
        checkEmpty();
        Vector oddElementsVector = new Vector();

        for (int element : this.vector) {
            if (!Utilities.isEvenNumber(element)) {
                oddElementsVector.addElement(element);
            }
        }

        return oddElementsVector;
    }

    private Vector getMultiples(int number) {
        checkEmpty();
        Vector multiplesVector = new Vector();

        if (number != 0) {
            for (int element : this.vector) {
                if ((element % number) == 0) {
                    multiplesVector.addElement(element);
                }
            }
        }

        return multiplesVector;
    }

    public double getMultiplesAverage(int number) {
        Vector multiplesVector = getMultiples(number);
        if (multiplesVector.isEmpty()) {
            return 0;
        }
        return multiplesVector.getElementsAverage();
    }

    public void sortAsc() {
        for (int index1 = 0; index1 < this.vector.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.vector.length; index2++) {
                if (this.vector[index1] > this.vector[index2]) {
                    int temp = this.vector[index1];
                    this.vector[index1] = this.vector[index2];
                    this.vector[index2] = temp;
                }
            }
        }
    }

    public void sortDesc() {
        for (int index1 = 0; index1 < this.vector.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.vector.length; index2++) {
                if (this.vector[index1] < this.vector[index2]) {
                    int temp = this.vector[index1];
                    this.vector[index1] = this.vector[index2];
                    this.vector[index2] = temp;
                }
            }
        }
    }

    public boolean hasOnlyEvenElements() {
        checkEmpty();
        for (int element : this.vector) {
            if (!Utilities.isEvenNumber(element)) {
                return false;
            }
        }
        return true;
    }

    public boolean hasOnlyOddElements() {
        checkEmpty();
        for (int element : this.vector) {
            if (Utilities.isEvenNumber(element)) {
                return false;
            }
        }
        return true;
    }

    public boolean hasRepeatedElements() {
        checkEmpty();
        for (int index1 = 0; index1 < this.vector.length; index1++) {
            for (int index2 = index1 + 1; index2 < this.vector.length; index2++) {
                if (this.vector[index1] == this.vector[index2]) {
                    return true;
                }
            }
        }
        return false;
    }

    private int getNumberOfDigitsAverage() {
        int numberDigitsSum = 0;

        for (int element : this.vector) {
            numberDigitsSum += Utilities.getNumberOfDigits(element);
        }

        return numberDigitsSum / length();
    }

    public Vector getElementsWithMoreDigitsThanAverage() {
        checkEmpty();
        Vector resultVector = new Vector();
        int average = getNumberOfDigitsAverage();

        for (int element : this.vector) {
            if ((Utilities.getNumberOfDigits(element)) > average) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    private double getPercentageOfEvenDigits() {
        double counter = 0;
        for (int element : this.vector) {
            int[] digits = Utilities.getVectorWithDigits(element);
            double evenPercentage = Utilities.getEvenRatio(digits);
            counter += evenPercentage;
        }

        return counter / (double) length();
    }

    public Vector getElementsWithEvenPercentageAboveAverage() {
        checkEmpty();
        Vector resultVector = new Vector();
        double average = getPercentageOfEvenDigits();

        for (int element : this.vector) {
            int[] digits = Utilities.getVectorWithDigits(element);
            if (Utilities.getEvenRatio(digits) > average) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public Vector getElementsWithOnlyEvenDigits() {
        checkEmpty();
        Vector resultVector = new Vector();

        for (int element : this.vector) {
            int[] digits = Utilities.getVectorWithDigits(element);
            if (Utilities.getEvenRatio(digits) == 1) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public Vector getElementsWithAscendingDigits() {
        checkEmpty();
        Vector resultVector = new Vector();

        for (int element : this.vector) {
            int[] digits = Utilities.getVectorWithDigits(element);
            if (Utilities.isAscending(digits)) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public Vector getCapicuaElements() {
        checkEmpty();
        Vector resultVector = new Vector();

        for (int element : this.vector) {
            if (Utilities.isCapicua(element)) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public Vector getElementsWithSameDigits() {
        checkEmpty();
        Vector resultVector = new Vector();

        for (int element : this.vector) {
            int[] digits = Utilities.getVectorWithDigits(element);
            if (Utilities.isSameDigits(digits)) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public Vector getNotArmstrongElements() {
        checkEmpty();
        Vector resultVector = new Vector();

        for (int element : this.vector) {
            if (!Utilities.isArmstrongNumber(element)) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public Vector getElementsWithAtLeastXAscendingDigits(int numberOfAscendingDigits) {
        checkEmpty();
        Vector resultVector = new Vector();

        for (int element : this.vector) {
            int[] digits = Utilities.getVectorWithDigits(element);
            if (Utilities.isAscending(digits, numberOfAscendingDigits)) {
                resultVector.addElement(element);
            }
        }

        return resultVector;
    }

    public int countNonNullElements() {
        checkEmpty();
        int counter = 0;

        for (int element : this.vector) {
            if (element != 0) {
                counter++;
            }
        }
        return counter;
    }

    public boolean reverseVector() {
        if (isEmpty()) {
            return true;
        }

        Vector result = new Vector();
        for (int index = this.vector.length - 1; index >= 0; index--) {
            result.addElement(this.getValueOfElement(index));
        }

        this.vector = result.toArray();
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this.vector == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector other = (Vector) o;
        boolean isEquals = true;

        if (this.vector.length == other.vector.length) {
            for (int index = 0; index < this.vector.length && isEquals; index++) {
                if (this.vector[index] != other.vector[index]) {
                    isEquals = false;
                }
            }
        } else {
            isEquals = false;
        }
        return isEquals;
    }


}
