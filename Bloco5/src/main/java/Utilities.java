public class Utilities {

    public static int getNumberOfDigits(int number) {

        int numberOfDigits = 0;

        if (number < 0) {
            number *= -1;
        }

        if (number == 0) {
            numberOfDigits = 1;
        } else {
            while (number > 0) {
                number /= 10;
                numberOfDigits++;
            }
        }
        return numberOfDigits;
    }

    public static int[] getVectorWithDigits(int number) {

        int numberOfDigits = getNumberOfDigits(number);
        int[] vectorWithDigits = new int[numberOfDigits];

        for (int i = (numberOfDigits - 1); i >= 0; i--) {
            vectorWithDigits[i] = number % 10;
            number /= 10;
        }

        return vectorWithDigits;
    }

    public static boolean isEvenNumber(int number) {
        return (number % 2) == 0;
    }

    public static double getEvenRatio(int[] digits) {

        int counter = 0;
        for (int element : digits) {
            if (isEvenNumber(element)) {
                counter++;
            }
        }

        return counter / (double) digits.length;
    }

    public static boolean isAscending(int[] digits) {

        if (digits.length == 1) {
            return false;
        }

        for (int index = 0; index < digits.length - 1; index++) {
            if (digits[index] >= digits[index + 1]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isCapicua(int number) {
        boolean capicua = true;

        if (number < 0) {
            return false;
        }
        int[] digitsVector = getVectorWithDigits(number);
        int index = 0;

        while (index < digitsVector.length && capicua) {
            if (digitsVector[index] == digitsVector[digitsVector.length - 1 - index]) {
                index++;
            } else {
                capicua = false;
            }
        }

        return capicua;

    }

    public static boolean isSameDigits(int[] digits) {

        for (int index = 0; index < digits.length - 1; index++) {
            if (digits[index] != digits[index + 1]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isArmstrongNumber(int number) {
        return number == getSumOfEachDigitRaisedToNumberOfDigits(number);
    }

    public static int getSumOfEachDigitRaisedToNumberOfDigits(int number) {
        int numberOfDigits = getNumberOfDigits(number);
        int sumOfDigitsCube = 0;
        int[] digits = getVectorWithDigits(number);

        for (int digit : digits) {
            sumOfDigitsCube += Math.pow(digit, numberOfDigits);
        }
        return sumOfDigitsCube;
    }

    public static boolean isAscending(int[] digits, int numberOfAscendingDigits) {

        if (digits.length == 1 || numberOfAscendingDigits == 1) {
            return false;
        }
        int counter = 0;

        for (int index = 0; index < digits.length - 1; index++) {
            if (digits[index] < digits[index + 1]) {
                counter++;
                if (counter == numberOfAscendingDigits - 1) {
                    return true;
                }
            } else {
                counter = 0;
            }
        }
        return false;
    }

}
