package WordSoup;

import java.util.ArrayList;
import java.util.Arrays;

public class Solutions {
    private final ArrayList<String> solutions;

    /**
     * Solutions construct. Allows to generate a Solutions object that should have at least one solution included.
     * @param solutions Receive a array of Strings with the solutions.
     */
    public Solutions(String[] solutions) {
        if (solutions == null) {
            throw new IllegalArgumentException("Solutions array cannot be null");
        }
        if (solutions.length == 0) {
            throw new IllegalArgumentException("Solutions list should have at least one solution included");
        }
        toUpperCase(solutions);
        this.solutions = new ArrayList<>();
        this.solutions.addAll(Arrays.asList(solutions));
        this.removeRepeated();
    }


    /**
     * Allows to print the list of solutions.
     */
    public void print() {
        System.out.println("A lista de soluções é a seguinte:");
        for (String solution : this.solutions) {
            System.out.println("- " + solution);
        }
    }

    /**
     * Allows to remove repeated words in solutions list.
     * @return True if repeated words exists and are successful removed.
     */
    private boolean removeRepeated() {
        String[] copy = this.toArray();
        boolean result = false;
        for (int index = 0; index < copy.length - 1; index++) {
            String word = copy[index];
            for (int index2 = index + 1; index2 < copy.length; index2++) {
                if (copy[index2].equals(word)) {
                    this.solutions.remove(word);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * Allows to verify if determined word is one of the solutions.
     * @param word Receive the word to verify.
     * @return True if word is one of the solutions.
     */
    public boolean contains(String word) {
        word = word.toUpperCase();
        return this.solutions.contains(word);
    }

    /**
     * Allows to modify an array of Strings to upper case.
     * @param solutions Receive an array of Strings.
     */
    private void toUpperCase(String[] solutions) {
        for (int index = 0; index < solutions.length; index++) {
            solutions[index] = solutions[index].toUpperCase();
        }
    }

    /**
     * Allows to count the number of solutions.
     * @return The number of solutions.
     */
    public int count() {
        return this.solutions.size();
    }

    /**
     * Allows to get solution contained in a specific position.
     * @param index Receive the position.
     * @return The requested solution.
     */
    public String get(int index) {
        return this.solutions.get(index);
    }

    /**
     * Allows to convert Solution into array of Strings.
     * @return An array of Strings with the solutions.
     */
    public String[] toArray() {
        String[] result = new String[this.solutions.size()];

        for (int index = 0; index < this.solutions.size(); index++) {
            result[index] = this.solutions.get(index);
        }

        return result;
    }

    /**
     * Allows to remove a specific word from solutions list.
     * @param word The word to remove.
     * @return True if word is found and successful removed.
     */
    public boolean remove(String word) {
        word = word.toUpperCase();
        if (this.contains(word)) {
            this.solutions.remove(word);
            return true;
        }
        return false;
    }

}
