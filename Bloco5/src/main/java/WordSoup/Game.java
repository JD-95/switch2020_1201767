package WordSoup;

import java.util.Scanner;

public class Game {
    private final Grid grid;
    private final Solutions solutions;
    private int nrMoves;

    /**
     * Grid constructor. Allows to generate a Game object with the corresponding grid and list of solutions.
     * @param grid Receive the grid for the game.
     * @param solutions Receive the list of solution for the game.
     */
    public Game(Grid grid, Solutions solutions) {
        if (!this.validateSolutions(grid, solutions)) {
            throw new IllegalArgumentException("Solutions not match with grid");
        }
        this.grid = grid;
        this.solutions = solutions;
        this.nrMoves = 0;
    }

    /**
     * Check if given solutions is all contained in the given grid.
     * @param grid Receive the game grid.
     * @param solutions Receive the list of solutions.
     * @return True if solutions are all valid.
     */
    private boolean validateSolutions(Grid grid, Solutions solutions) {

        for (int index = 0; index < solutions.count(); index++) {
            if (!grid.exists(solutions.get(index))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Allows to start wordsoup game.
     * Ends when all words contained is solutions list are found by the user.
     */
    public void startGame() {
        System.out.println("Esta é a sua sopa de letras:");
        this.grid.printGrid();
        Scanner ler = new Scanner(System.in);
        System.out.println("Pretende saber as palavras a encontrar (S|N):");
        String showSolutions = ler.next();
        if (showSolutions.equals("S") || showSolutions.equals("s")) {
            this.solutions.print();
        }
        System.out.println("Boa Sorte :D");
        long initialTime = System.currentTimeMillis();
        int numberOfSolutions = this.solutions.count();
        while (!isEndGame()) {
            this.move();
            this.nrMoves++;
        }
        double efficacy = Math.round((numberOfSolutions / (double) this.nrMoves) * 100);
        long finalTime = System.currentTimeMillis();
        long gameTime = (long) ((finalTime - initialTime) * 0.001);
        double timeByWord = Math.round((double) gameTime / numberOfSolutions);
        System.out.println("PARABÉNS, encontrou todas as palavras!");
        System.out.println("Precisou de " + this.nrMoves + " jogadas, teve uma eficácia de " + efficacy + " %.");
        System.out.println(
                "Demorou " + gameTime + " segundos a descobrir todas as palavras, com uma média de " + timeByWord + " segundos por cada palavra " +
                        "encontrada.");

    }


    /**
     * Allows to the user set a start and final cell and verify if found sequence is one of the solutions.
     */
    public void move() {
        Scanner ler = new Scanner(System.in);
        int startRow = setStartRow(ler);
        int startColumn = setStartColumn(ler);
        int lastRow = setLastRow(ler);
        int lastColumn = setLastColumn(ler);
        while (!this.grid.validateFinalCell(startRow, startColumn, lastRow, lastColumn)) {
            System.out.println("A célula final não é válida!");
            lastRow = setLastRow(ler);
            lastColumn = setLastColumn(ler);
        }

        System.out.println("Selecionou a sequência entre a célula: " + startRow + ":" + startColumn + " e a " + lastRow + ":" + lastColumn);

        String foundWord = this.grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        if (this.solutions.contains(foundWord)) {
            System.out.println("Parabéns encontrou a seguinta palavra: " + foundWord + ".");
            this.solutions.remove(foundWord);
        } else {
            System.out.println("A palavra encontrada (" + foundWord + ") não faz parte das soluções!");
        }
    }

    /**
     * Allows to set the final cell's column.
     * Verify if given value is valid within grid boundaries.
     * @param ler Receive a value that corresponds to final cell's column.
     * @return The final cell's column.
     */
    private int setLastColumn(Scanner ler) {
        System.out.println("Indique qual a coluna da célula final (a numeração das colunas começa no zero):");
        int lastColumn = ler.nextInt();
        while (lastColumn < 0 || lastColumn > this.grid.getNumberOfColumns()) {
            this.errorInvalidColumn();
            System.out.println("Indique qual a coluna da célula final (a numeração das colunas começa no zero):");
            lastColumn = ler.nextInt();
        }
        return lastColumn;
    }

    /**
     * Allows to set the final cell's row.
     * Verify if given value is valid within grid boundaries.
     * @param ler Receive a value that corresponds to final cell's row.
     * @return The final cell's row.
     */
    private int setLastRow(Scanner ler) {
        System.out.println("Indique qual a linha da célula final (a numeração das linhas começa no zero):");
        int lastRow = ler.nextInt();
        while (lastRow < 0 || lastRow > this.grid.getNumberOfRows()) {
            this.errorInvalidRow();
            System.out.println("Indique qual a linha da célula final (a numeração das linhas começa no zero):");
            lastRow = ler.nextInt();
        }
        return lastRow;
    }

    /**
     * Allows to set the start cell's column.
     * Verify if given value is valid within grid boundaries.
     * @param ler Receive a value that corresponds to start cell's column.
     * @return The start cell's column.
     */
    private int setStartColumn(Scanner ler) {
        System.out.println("Indique qual a coluna da célula inicial (a numeração das colunas começa no zero):");
        int startColumn = ler.nextInt();
        while (startColumn < 0 || startColumn >= this.grid.getNumberOfColumns()) {
            this.errorInvalidColumn();
            System.out.println("Indique qual a coluna da célula inicial (a numeração das colunas começa no zero):");
            startColumn = ler.nextInt();
        }
        return startColumn;
    }

    /**
     * Allows to set the start cell's row.
     * Verify if given value is valid within grid boundaries.
     * @param ler Receive a value that corresponds to start cell's row.
     * @return The start cell's row.
     */
    private int setStartRow(Scanner ler) {
        System.out.println("Indique qual a linha da célula inicial (a numeração das linhas começa no zero):");
        int startRow = ler.nextInt();
        while (startRow < 0 || startRow >= this.grid.getNumberOfRows()) {
            this.errorInvalidRow();
            System.out.println("Indique qual a linha da célula inicial (a numeração das linhas começa no zero):");
            startRow = ler.nextInt();
        }
        return startRow;
    }

    /**
     * Check if is endgame (when no more words to found).
     * @return True if is endgame.
     */
    private boolean isEndGame() {
        return this.solutions.count() == 0;
    }

    /**
     * Print an error message to the user due to invalid row.
     */
    private void errorInvalidRow() {
        System.out.println("Linha inválida: selecione uma linha contida entre 0 e " + (this.grid.getNumberOfRows() - 1) + ".");
    }

    /**
     * Print an error message to the user due to invalid column.
     */
    private void errorInvalidColumn() {
        System.out.println("Coluna inválida: selecione uma coluna contida entre 0 e " + (this.grid.getNumberOfColumns() - 1) + ".");
    }


}
