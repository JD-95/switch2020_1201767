package WordSoup;

import java.util.ArrayList;

public class Grid {
    private char[][] grid;


    /**
     * Grid constructor. Allows to generate a Grid object that shoud be a square or rectangular matrix of chars.
     * @param givenGrid Receive a matrix of chars that would be the grid for the wordsoup game.
     */
    public Grid(char[][] givenGrid) {
        if (givenGrid == null) {
            throw new IllegalArgumentException("Grid cannot be null");
        }
        if (givenGrid.length == 0) {
            throw new IllegalArgumentException("Grid cannot be empty");
        }
        if (!checkSameNumberOfColumns(givenGrid)) {
            throw new IllegalArgumentException("Grid should have same number of columns in each row");
        }
        copyElementsToGrid(givenGrid);
    }

    /**
     * Checks if two cells are in the same grid position (row A = row B and column A = column B).
     * @param rowCellA The first cell's row.
     * @param columnCellA The first cell's column.
     * @param rowCellB The second cell's row.
     * @param columnCellB The second cell's column.
     * @return True if are in the same position.
     */
    private static boolean isSameCell(int rowCellA, int columnCellA, int rowCellB, int columnCellB) {
        return rowCellA == rowCellB && columnCellA == columnCellB;
    }

    /**
     * Allows to copy each char (and ensured that is upper case) for given matrix to the grid.
     * @param givenGrid A matrix of chars.
     */
    private void copyElementsToGrid(char[][] givenGrid) {
        this.grid = new char[givenGrid.length][givenGrid[0].length];
        for (int row = 0; row < givenGrid.length; row++) {
            for (int column = 0; column < givenGrid[0].length; column++) {
                char letter = givenGrid[row][column];
                this.grid[row][column] = Character.toUpperCase(letter);
            }
        }
    }

    /**
     * Check if given grid has same number os columns in each row.
     * @param givenGrid Receive a matrix of chars.
     * @return True if same number of columns in each row.
     */
    private boolean checkSameNumberOfColumns(char[][] givenGrid) {
        int numberOfColumns = givenGrid[0].length;
        for (int row = 1; row < givenGrid.length; row++) {
            if (givenGrid[row].length != numberOfColumns) {
                return false;
            }
        }
        return true;
    }

    /**
     * Allows to verify if given word exits in grid.
     * @param word Receive a word.
     * @return True if word exists in grid.
     */
    public boolean exists(String word) {
        word = word.toUpperCase();
        for (int row = 0; row < this.grid.length; row++) {
            for (int column = 0; column < this.grid[0].length; column++) {
                if (this.grid[row][column] == word.charAt(0)) {
                    ArrayList<String> possibleSequences = this.getPossibleSequences(row, column, word.length());
                    if (possibleSequences.contains(word)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Allows to verify if determined cell is contained within the boundaries of the grid.
     * @param row Receive the position of row.
     * @param column Receive the position of column.
     * @return True if cell is valid.
     */
    private boolean isValidCell(int row, int column) {
        return row >= 0 && row < this.grid.length && column >= 0 && column < this.grid.length;
    }

    /**
     * Allows to get the settings related to each direction (for row and column growth).
     * @param direction Receive the direction (between 0 and 7).
     * @return The setting of received direction.
     */
    public int[] directionSettings(int direction) {

        int[][] directionSettings = new int[8][2];

        directionSettings[0] = new int[]{0, 1};  // direita
        directionSettings[1] = new int[]{0, -1};  // esquerda
        directionSettings[2] = new int[]{-1, 0};   // cima
        directionSettings[3] = new int[]{1, 0};   // baixo
        directionSettings[4] = new int[]{-1, -1};    // diag principal baixo-cima
        directionSettings[5] = new int[]{1, 1};  // diag principal cima-baixo
        directionSettings[6] = new int[]{-1, 1};   // diag secundaria baixo-cima
        directionSettings[7] = new int[]{1, -1};   // diag secundaria cima-baixo


        return directionSettings[direction];
    }

    /**
     * Allows to get the possible sequences that are obtained starting in determined cell.
     * @param startRow Receive the cell´s row.
     * @param startColumn Receive the cell's column.
     * @param wordLength Receive the length of the sequences to check.
     * @return The list of found sequences.
     */
    private ArrayList<String> getPossibleSequences(int startRow, int startColumn, int wordLength) {

        ArrayList<String> result = new ArrayList<>();

        for (int direction = 0; direction < 8; direction++) {
            int[] finalCell = getFinalCell(startRow, startColumn, wordLength, direction);
            if (isValidCell(finalCell[0], finalCell[1])) {
                result.add(this.getSequence(startRow, startColumn, finalCell[0], finalCell[1]));
            }
        }

        return result;
    }

    /**
     * Allows to get the position of row and column of a final cell starting in a determined cell.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param length Receive the length of the sequence that goes to final cell.
     * @param direction Receive the direction of sequence.
     * @return An array with final cell's row in position zero and cell's column in position one.
     */
    private int[] getFinalCell(int startRow, int startColumn, int length, int direction) {
        int[] result = new int[2];
        result[0] = startRow + ((length - 1) * directionSettings(direction)[0]);
        result[1] = startColumn + ((length - 1) * directionSettings(direction)[1]);
        return result;
    }

    /**
     * Allows to get a sequence between a start and final cell.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param lastRow The final cell's row.
     * @param lastColumn The final cell's column.
     * @return A String with the found sequence.
     */
    public String getSequence(int startRow, int startColumn, int lastRow, int lastColumn) {

        if (!isValidCell(lastRow, lastColumn) || !isValidCell(startRow, startColumn)) {
            return null;
        }
        int direction = this.checkDirection(startRow, startColumn, lastRow, lastColumn);
        StringBuilder sequenceFound = new StringBuilder();
        int rowSettings = directionSettings(direction)[0];
        int columnSettings = directionSettings(direction)[1];
        int length = this.getLength(startRow, startColumn, lastRow, lastColumn);

        for (int row = startRow, column = startColumn; sequenceFound.length() < length; row += rowSettings, column += columnSettings) {
            sequenceFound.append(this.grid[row][column]);
        }
        return sequenceFound.toString();
    }

    /**
     * Allows to print the wordsoup grid.
     */
    public void printGrid() {

        for (char[] chars : this.grid) {
            for (int index = 0; index < this.grid[0].length; index++) {
                System.out.print(chars[index] + " ");
            }
            System.out.println(" ");
        }
    }

    /**
     * Allows to convert Grid into two-dimensional array of chars.
     * @return A two-dimensional array of chars with the cells of grid.
     */
    public char[][] toArray() {
        char[][] result = new char[this.grid.length][this.grid[0].length];

        for (int row = 0; row < this.grid.length; row++) {
            System.arraycopy(this.grid[row], 0, result[row], 0, this.grid[0].length);
        }
        return result;
    }

    /**
     * Allows to check direction from start and final cell.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param lastRow The final cell's row.
     * @param lastColumn The final cell's column.
     * @return The direction that connect start to final cell.
     */
    public int checkDirection(int startRow, int startColumn, int lastRow, int lastColumn) {

        int direction;
        if (startRow == lastRow && startColumn < lastColumn) {
            direction = 0;
        } else if (startRow == lastRow && startColumn > lastColumn) {
            direction = 1;
        } else if (startRow > lastRow && startColumn == lastColumn) {
            direction = 2;
        } else if (startRow < lastRow && startColumn == lastColumn) {
            direction = 3;
        } else if (startRow > lastRow && startColumn > lastColumn) {
            direction = 4;
        } else if (startRow < lastRow && startColumn < lastColumn) {
            direction = 5;
        } else if (startRow > lastRow && startColumn < lastColumn) {
            direction = 6;
        } else if (startRow < lastRow && startColumn > lastColumn) {
            direction = 7;
        } else {
            direction = -1;
        }

        return direction;

    }

    /**
     * Allows to get length from start to final cell.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param lastRow The final cell's row.
     * @param lastColumn The final cell's column.
     * @return The length that connect start to final cell.
     */
    public int getLength(int startRow, int startColumn, int lastRow, int lastColumn) {


        int length = 1;
        if (startRow == lastRow && startColumn < lastColumn) {
            length += lastColumn - startColumn;
        } else if (startRow == lastRow && startColumn > lastColumn) {
            length += startColumn - lastColumn;
        } else if (startRow > lastRow && startColumn == lastColumn) {
            length += startRow - lastRow;
        } else if (startRow < lastRow && startColumn == lastColumn) {
            length += lastRow - startRow;
        } else if (startRow > lastRow && startColumn > lastColumn) {
            length += startRow - lastRow;
        } else if (startRow < lastRow && startColumn < lastColumn) {
            length += lastRow - startRow;
        } else if (startRow > lastRow && startColumn < lastColumn) {
            length += startRow - lastRow;
        } else if (startRow < lastRow && startColumn > lastColumn) {
            length += lastRow - startRow;
        } else {
            length = -1;
        }
        return length;
    }

    /**
     * Allows to get grid's number of rows.
     * @return The grid's number of rows.
     */
    public int getNumberOfRows() {
        return this.grid.length;
    }

    /**
     * Allows to get grid's number of columns.
     * @return The grid's number of columns.
     */
    public int getNumberOfColumns() {
        return this.grid[0].length;
    }

    /**
     * Evaluates if final cell is valid in grid, taking into account the 8 possible directions (in row, column, principal diagonal and secondary
     * diagonal) and limits of grid.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param finalRow The final cell's row.
     * @param finalColumn The final cell's column.
     * @return True if final cell is valid in grid.
     */
    public boolean validateFinalCell(int startRow, int startColumn, int finalRow, int finalColumn) {
        if (this.isValidCell(startRow, startColumn) && this.isValidCell(finalRow, finalColumn) && !isSameCell(startRow, startColumn, finalRow, finalColumn)) {
            return this.isFinalCellInSameRow(startRow, finalRow) || this.isFinalCellInSameColumn(startColumn, finalColumn) || this
                    .isCellInPrincipalDiagonal(startRow, startColumn, finalRow, finalColumn) || this
                    .isCellInSecondaryDiagonal(startRow, startColumn, finalRow, finalColumn);
        }
        return false;
    }

    /**
     * Checks if final cell is in the same row of start cell.
     * @param startRow The start cell's row.
     * @param finalRow The final cell's row.
     * @return True if final cell in the same row.
     */
    private boolean isFinalCellInSameRow(int startRow, int finalRow) {
        return startRow == finalRow;
    }

    /**
     * Checks if final cell is in the same column of start cell.
     * @param startColumn The start cell's column.
     * @param finalColumn The final cell's column.
     * @return True if final cell in the same column.
     */
    private boolean isFinalCellInSameColumn(int startColumn, int finalColumn) {
        return startColumn == finalColumn;
    }

    /**
     * Checks if cell is in the principal diagonal.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param finalRow The final cell's row.
     * @param finalColumn The final cell's column.
     * @return True if cell in the principal diagonal.
     */
    private boolean isCellInPrincipalDiagonal(int startRow, int startColumn, int finalRow, int finalColumn) {
        for (int rowGrid = startRow + 1, columnGrid = startColumn + 1; rowGrid < this.grid.length; rowGrid++, columnGrid++)
            if (finalRow == rowGrid && finalColumn == columnGrid) {
                return true;
            }
        for (int rowGrid = startRow - 1, columnGrid = startColumn - 1; rowGrid >= 0; rowGrid--, columnGrid--)
            if (finalRow == rowGrid && finalColumn == columnGrid) {
                return true;
            }
        return false;
    }

    /**
     * Checks if cell is in the secondary diagonal.
     * @param startRow The start cell's row.
     * @param startColumn The start cell's column.
     * @param finalRow The final cell's row.
     * @param finalColumn The final cell's column.
     * @return True if cell in the secondary diagonal.
     */
    private boolean isCellInSecondaryDiagonal(int startRow, int startColumn, int finalRow, int finalColumn) {
        for (int rowGrid = startRow + 1, columnGrid = startColumn - 1; rowGrid < this.grid.length; rowGrid++, columnGrid--)
            if (finalRow == rowGrid && finalColumn == columnGrid) {
                return true;
            }
        for (int rowGrid = startRow - 1, columnGrid = startColumn + 1; rowGrid >= 0; rowGrid--, columnGrid++)
            if (finalRow == rowGrid && finalColumn == columnGrid) {
                return true;
            }
        return false;
    }

    /*
    public enum Direction {
        HORIZONTAL_RIGHT(new int[]{1, 0}),
        HORIZONTAL_LEFT(new int[]{-1, 0}),
        VERTICAL_UP(new int[]{0, -1}),
        VERTICAL_DOWN(new int[]{0, 1}),
        DIAGONAL_UP_LEFT(new int[]{-1, -1}),
        DIAGONAL_UP_RIGHT(new int[]{-1, 1}),
        DIAGONAL_DOWN_LEFT(new int[]{1, -1}),
        DIAGONAL_DOWN_RIGHT(new int[]{1, 1});

        public int[] settings;

        Direction(int[] next) {
            settings = next;
        }
    }
     */

}
