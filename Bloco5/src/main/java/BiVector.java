import java.util.Arrays;

public class BiVector {
    //Attributes
    private Vector[] biVector;

    // Constructor
    public BiVector() {
        this.biVector = new Vector[0];
    }

    public BiVector(int[][] biVector) {
        if (biVector == null) {
            throw new IllegalArgumentException("two-dimensional array should not be null");
        }
        this.biVector = copyElementsFromBiArray(biVector);
    }

    private BiVector(Vector[] biVector) {
        if (biVector == null) {
            throw new IllegalArgumentException("two-dimensional array should not be null");
        }
        this.biVector = copyElementsFromBiArray(biVector);
    }


    private Vector[] copyElementsFromBiArray(int[][] biVector) {

        Vector[] copyBiArray = new Vector[biVector.length];

        for (int row = 0; row < biVector.length; row++) {
            copyBiArray[row] = new Vector();
            for (int element : biVector[row]) {
                copyBiArray[row].addElement(element);
            }
        }

        return copyBiArray;
    }

    private Vector[] copyElementsFromBiArray(Vector[] biVector) {

        Vector[] copyBiArray = new Vector[biVector.length];

        for (int row = 0; row < biVector.length; row++) {
            copyBiArray[row] = biVector[row].createCopy();
        }

        return copyBiArray;
    }

    public int[][] toArray() {
        int[][] result = new int[this.biVector.length][];

        for (int row = 0; row < this.biVector.length; row++) {
            result[row] = new int[this.biVector[row].length()];
            for (int column = 0; column < this.biVector[row].length(); column++) {
                result[row][column] = biVector[row].getValueOfElement(column);
            }
        }
        return result;
    }

    /**
     * Allows to add a new element to the two-dimensional vector.
     * @param newElement Receive the value of the new element.
     * @param row Receive the position of the row where new element insert.
     * @return True if new element is added.
     */
    public boolean addElement(int newElement, int row) {
        if (isEmpty()) {
            this.biVector = generateEmptyRows(row);
        }
        if (!checkRow(row)) {
            return false;
        }
        this.biVector[row].addElement(newElement);
        return true;
    }

    /**
     * Allows to generate empty rows until the position defined.
     * @param lastRow Receive the position of the last row to generate.
     * @return The array of empty vectors.
     */
    private Vector[] generateEmptyRows(int lastRow) {
        Vector[] result = new Vector[lastRow + 1];
        for (int row = 0; row <= lastRow; row++) {
            result[row] = new Vector();
        }
        return result;
    }

    /**
     * Checks if row position exists in the two-dimensional vector.
     * @param row Receive the position of the row to verify.
     * @return True if row position in included in two-dimensional vector.
     */
    private boolean checkRow(int row) {
        return row >= 0 && row < this.biVector.length;
    }

    /**
     * Checks if two-dimensional vector is empty.
     * @return Returns true if is empty.
     */
    public boolean isEmpty() {
        return this.biVector.length == 0;
    }

    /**
     * Checks if two-dimensional vector is empty, if so, throw the exception described.
     */
    private void checkEmpty() {
        if (isEmpty()) {
            throw new IllegalStateException("two-dimensional array without elements");
        }
    }

    /**
     * Allows to remove first element found with a given value.
     * @param value Receives an integer.
     * @return The two-dimensional vector without the removed element.
     */
    public boolean removeFirstElementOfGivenValue(int value) {
        checkEmpty();
        Vector[] copyBiVector = copyElementsFromBiArray(this.biVector);
        for (int row = 0; row < copyBiVector.length; row++) {
            for (int index = 0; index < copyBiVector[row].length(); index++) {
                if (copyBiVector[row].getValueOfElement(index) == value) {
                    copyBiVector[row].removeElement(index);
                    this.biVector = copyBiVector;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Allows to obtain the maximum value in the two-dimensional vector.
     * @return The maximum value.
     */
    public int getMaxValue() {
        checkEmpty();
        Vector vector = this.toSingleVector();
        int maxValue = vector.getMaxValue();
        return maxValue;

        /* versão anterior
        int firstRowNotEmpty = getFirstRowNotEmpty();
        int maxValue = this.biVector[firstRowNotEmpty].getMaxValue();
        for (int row = firstRowNotEmpty + 1; row < this.biVector.length; row++) {
            if (!this.biVector[row].isEmpty() && maxValue < this.biVector[row].getMaxValue()) {
                maxValue = this.biVector[row].getMaxValue();
            }
        }
        return maxValue;
         */
    }

    /**
     * Allows to obtain the minimum value in the two-dimensional vector.
     * @return The minimum value.
     */
    public int getMinValue() {
        checkEmpty();
        Vector vector = this.toSingleVector();
        int minValue = vector.getMinValue();
        return minValue;

        /* versão anterior
        int firstRowNotEmpty = getFirstRowNotEmpty();
        int minValue = this.biVector[firstRowNotEmpty].getMinValue();
        for (int row = firstRowNotEmpty; row < this.biVector.length; row++) {
            if (!this.biVector[row].isEmpty() && minValue < this.biVector[row].getMinValue()) {
                minValue = this.biVector[row].getMinValue();
            }
        }
        return minValue;
         */
    }

    /* método deixou de ser necessário
    private int getFirstRowNotEmpty() {
        checkEmpty();
        Vector[] copyBiVector = copyElementsFromBiArray(this.biVector);
        for (int row = 0; row < copyBiVector.length; row++) {
            if (!copyBiVector[row].isEmpty()) {
                return row;
            }
        }
        return -1;
    } */

    /**
     * Allows to obtain average of the two-dimensional vector elements.
     * @return The elements average.
     */
    public double getElementsAverage() {
        checkEmpty();
        int elementsSum = 0;
        for (int row = 0; row < this.biVector.length; row++) {
            if (!this.biVector[row].isEmpty()) {
                elementsSum += biVector[row].getElementsSum();
            }
        }
        double average = elementsSum / (double) countElements();
        return average;
    }

    /**
     * Allows to count elements of the two-dimensional vector.
     * @return The number of elements.
     */
    private int countElements() {
        int counter = 0;
        for (int row = 0; row < this.biVector.length; row++) {
            if (!this.biVector[row].isEmpty()) {
                counter += biVector[row].length();
            }
        }
        return counter;
    }

    /**
     * Allows to obtain sum of the elements of each row in two-dimensional vector.
     * @return Vector with the sum of each row (each position in return vector corresponds to the row in two-dimensional vector).
     */
    public Vector getSumOfEachRow() {
        checkEmpty();
        Vector result = new Vector();

        for (int row = 0; row < this.biVector.length; row++) {
            int sum = biVector[row].getElementsSum();
            result.addElement(sum);
        }

        return result;
    }

    /**
     * Allows to obtain sum of the elements of each column in two-dimensional vector.
     * @return Vector with the sum of each column (each position in return vector corresponds to the column in two-dimensional vector).
     */
    public Vector getSumOfEachColumn() {
        checkEmpty();
        Vector result = new Vector();
        Vector[] copyBiVector = copyElementsFromBiArray(this.biVector);

        for (int column = 0; column < this.getMaxNumberOfColumns(); column++) {
            int sum = 0;
            for (int row = 0; row < copyBiVector.length; row++) {
                if (column < copyBiVector[row].length()) {
                    sum += copyBiVector[row].getValueOfElement(column);
                }
                if (isLastRow(row)) {
                    result.addElement(sum);
                }
            }
        }

        return result;
    }

    /**
     * Checks if is the last line of two-dimensional vector.
     * @param row Receive the position of row.
     * @return True if row is the last in the two-dimensional vector.
     */
    private boolean isLastRow(int row) {
        return row == this.biVector.length - 1;
    }

    /**
     * Allows to obtain the maximum number of columns in the two-dimensional vector.
     * @return The maximum number od columns found.
     */
    private int getMaxNumberOfColumns() {
        int result = 0;
        for (Vector row : this.biVector) {
            if (row.length() > result) {
                result = row.length();
            }
        }
        return result;
    }

    /**
     * Allows to obtain the row(s) with the maximum sum in the two-dimensional vector.
     * @return Vector with the row(s) with the maximum sum.
     */
    public Vector getRowWithMaxSum() {
        checkEmpty();
        Vector sumRows = this.getSumOfEachRow();
        int max = sumRows.getMaxValue();
        Vector result = sumRows.getIndexOfGivenValue(max);
        return result;
    }

    /**
     * Checks if two-dimensional is square (same number of columns in each row, and number of rows and column equals).
     * @return True if square.
     */
    public boolean isSquare() {
        checkEmpty();
        int numberOfColumns = this.biVector[0].length();
        int numberOfRows = this.biVector.length;

        return isSameNumberOfColumnsInEachRow() && numberOfColumns == numberOfRows;
    }

    /**
     * Checks if two-dimensional have the same number of columns in each row.
     * @return True if number of columns are the same in each row.
     */
    private boolean isSameNumberOfColumnsInEachRow() {
        if (this.isEmpty()) {
            return false;
        }
        boolean result = true;

        for (int row = 1; row < this.biVector.length && result; row++) {
            if (this.biVector[row].length() != this.biVector[row - 1].length() || this.biVector[row].length() == 0) {
                result = false;
            }
        }

        return result;
    }

    public boolean isSymmetric() {
        if (!isEmpty() && isSquare()) {
            Vector[] copy = this.copyElementsFromBiArray(biVector);
            BiVector transpose = new BiVector(copy);
            transpose.transpose();
            return this.equals(transpose);
        }
        return false;


        /* método antigo
        if (!isSquare()) {
            return false;
        }
        for (int row = 0; row < this.biVector.length; row++) {
            if (!this.biVector[row].equals(this.getElementsInColumn(row))) {
                return false;
            }
        }
        return true; */
    }

    /**
     * Allows to get the elements in a given column of the two-dimensional square or rectangular vector.
     * @param column Receives the column position.
     * @return Vector with the selected elements.
     */
    private Vector getElementsInColumn(int column) {
        if (!this.isSameNumberOfColumnsInEachRow()) {
            return null;
        }
        Vector result = new Vector();

        for (int row = 0; row < this.biVector.length; row++) {
            int element = this.biVector[row].getValueOfElement(column);
            result.addElement(element);
        }
        return result;
    }

    /**
     * Allows to count non-null elements (other than zero) on the principal diagonal.
     * @return The number of non-null elements.
     */
    public int countNonNullElementsInPrincipalDiagonal() {
        if (!isSquare()) {
            return -1;
        }
        return this.getPrincipalDiagonal().countNonNullElements();
    }

    /**
     * Allows to obtain the principal diagonal (runs from the top left corner to the bottom right corner).
     * @return Vector with the principal diagonal.
     */
    private Vector getPrincipalDiagonal() {
        checkEmpty();
        if (!isSquare()) {
            return new Vector();
        }

        Vector[] copy = this.copyElementsFromBiArray(this.biVector);
        Vector principalDiagonal = new Vector();

        for (int row = 0, column = 0; row < copy.length; row++, column++) {
            principalDiagonal.addElement(copy[row].getValueOfElement(column));
        }

        return principalDiagonal;
    }

    /**
     * Allows to obtain the secondary diagonal (runs from the top right corner to the bottom left corner).
     * @return Vector with the secondary diagonal.
     */
    private Vector getSecondaryDiagonal() {
        checkEmpty();
        if (!isSquare()) {
            return new Vector();
        }

        Vector[] copy = this.copyElementsFromBiArray(this.biVector);
        Vector principalDiagonal = new Vector();

        for (int row = 0, column = copy[0].length() - 1; row < copy.length; row++, column--) {
            principalDiagonal.addElement(copy[row].getValueOfElement(column));
        }

        return principalDiagonal;
    }

    /**
     * Checks if primary and secondary diagonal of two-dimensional square vector are equals.
     * @return True if they are equals.
     */
    public boolean isPrincipalAndSecondaryDiagonalEquals() {
        if (!this.isSquare()) {
            return false;
        }
        return this.getPrincipalDiagonal().equals(this.getSecondaryDiagonal());
    }

    /**
     * Allows to obtain all elements that the two-dimensional vector contains.
     * @return The Vector with the elements of the two-dimensional vector.
     */
    private Vector toSingleVector() {
        if (isEmpty()) {
            return new Vector();
        }
        Vector result = new Vector();

        for (Vector row : this.biVector) {
            if (row.length() != 0) {
                for (int column = 0; column < row.length(); column++) {
                    int newElement = row.getValueOfElement(column);
                    result.addElement(newElement);
                }
            }
        }
        return result;
    }

    /**
     * Allows to obtain the elements whose number of digits is greater than the average number of digits of all elements.
     * @return The Vector with the selected elements.
     */
    public Vector getElementsWithMoreDigitsThanAverage() {
        Vector vector = this.toSingleVector();
        if (vector.isEmpty()) {
            return new Vector();
        }
        Vector result = vector.getElementsWithMoreDigitsThanAverage();
        return result;
    }

    /**
     * Allows to obtain the elements whose percentage of even digits is greater than the average percentage of even digits of all elements.
     * @return The Vector with the selected elements.
     */
    public Vector getElementsWithEvenPercentageAboveAverage() {
        Vector vector = this.toSingleVector();
        if (vector.isEmpty()) {
            return new Vector();
        }
        Vector result = vector.getElementsWithEvenPercentageAboveAverage();
        return result;
    }

    /**
     * Allows to invert the elements order in each row of the two-dimensional vector.
     * @return True if inversion performed.
     */
    public boolean reverseRowElements() {

        Vector[] result = copyElementsFromBiArray(this.biVector);

        for (Vector vector : result) {
            vector.reverseVector();
        }

        this.biVector = result;
        return true;
    }

    /**
     * Allows to invert the rows position in the two-dimensional vector.
     * @return True if rows inversion performed.
     */
    public boolean reverseRows() {
        int numberOfRows = this.biVector.length;
        Vector[] result = new Vector[numberOfRows];
        Vector[] copy = copyElementsFromBiArray(this.biVector);

        for (int row = 0; row < numberOfRows; row++) {
            result[row] = new Vector();
            result[row] = copy[numberOfRows - 1 - row];
        }
        this.biVector = result;
        return true;
    }

    /**
     * It allows rotate the two-dimensional square or rectangular vector 90 degrees to the right.
     * @return True if rotation performed.
     */
    private boolean rotateNinetyDegreesRight(int rotations) {
        if (!isEmpty() && isSameNumberOfColumnsInEachRow()) {

            for (int rotate = 0; rotate < rotations; rotate++) {
                this.transpose();
                this.reverseRowElements();
            }
            return true;
        }
        return false;
    }

    /**
     * It allows rotate the two-dimensional square or rectangular vector 90 degrees positive (to the right).
     * @return True if rotation performed.
     */
    public boolean rotateNinetyDegreesPositive() {
        return this.rotateNinetyDegreesRight(1);
    }

    /**
     * It allows rotate the two-dimensional square or rectangular vector 90 degrees negative (to the left).
     * @return True if rotation performed.
     */
    public boolean rotateNinetyDegreesNegative() {
        return this.rotateNinetyDegreesRight(3);
    }

    /**
     * It allows rotate the two-dimensional square or rectangular vector 180 degrees.
     * @return True if rotation performed.
     */
    public boolean rotateOneHundredAndEightyDegrees() {
        return this.rotateNinetyDegreesRight(2);
    }

    /**
     * It allows to transform the two-dimensional square or rectangular vector into its corresponding transpose (inversion of columns by lines).
     */
    private void transpose() {
        if (!isEmpty() && isSameNumberOfColumnsInEachRow()) {
            Vector[] result = new Vector[this.biVector[0].length()];
            for (int row = 0, column = 0; row < result.length; row++, column++) {
                result[row] = this.getElementsInColumn(column);
            }
            this.biVector = result;
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BiVector biVector1 = (BiVector) o;
        return Arrays.equals(biVector, biVector1.biVector);
    }

}