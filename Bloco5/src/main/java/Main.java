import WordSoup.Game;
import WordSoup.Grid;
import WordSoup.Solutions;

public class Main {
    public static void main(String[] args) {
        Solutions solutions = new Solutions(new String[]{"LARANJA", "azul"});
        Grid grid = new Grid(new char[][]{
                {'P', 'R', 'P', 'A', 'R', 'J', 'U', 'N'},
                {'O', 'L', 'H', 'V', 'R', 'L', 'H', 'R'},
                {'A', 'J', 'V', 'B', 'M', 'A', 'B', 'M'},
                {'O', 'H', 'L', 'E', 'M', 'R', 'E', 'V'},
                {'T', 'R', 'E', 'R', 'A', 'A', 'D', 'A'},
                {'E', 'O', 'E', 'N', 'Z', 'N', 'R', 'V'},
                {'R', 'V', 'C', 'Z', 'U', 'J', 'E', 'A'},
                {'P', 'O', 'A', 'B', 'L', 'A', 'V', 'E'}
        });

        Game game = new Game(grid, solutions);
        game.startGame();
    }
}
