import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BiVectorTests {

    @Test
    void createEmptyBiVector() {
        BiVector biVector = new BiVector();
        int[][] expected = {};

        assertArrayEquals(expected, biVector.toArray());
    }


    @Test
    void createBiVector_Null() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new BiVector(null));
        String expectedMessage = "two-dimensional array should not be null";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void createBiVector_OneLine() {
        int[][] elements = {{0, 1}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 1}};

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void createBiVector_SomeLines() {
        int[][] elements = {{0, 1}, {}, {-1, 2, 1}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 1}, {}, {-1, 2, 1}};

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void addElementIntoEmptyBiVector_FirstLine() {
        int newElement = 0;
        int line = 0;
        BiVector biVector = new BiVector();
        int[][] expected = {{0}};

        biVector.addElement(newElement, line);

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void addElementIntoEmptyBiVector_OtherLine() {
        int newElement = 1;
        int line = 2;
        BiVector biVector = new BiVector();
        int[][] expected = {{}, {}, {1}};

        biVector.addElement(newElement, line);

        assertArrayEquals(expected, biVector.toArray());
    }

    @Test
    void addElementIntoBiVectorWithSingleLine() {
        int newElement = -1;
        int line = 0;
        int[][] elements = {{0, 1}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 1, -1}};

        boolean add = biVector.addElement(newElement, line);

        assertArrayEquals(expected, biVector.toArray());
        assertTrue(add);
    }

    @Test
    void addElementIntoBiVectorWithSomeLines() {
        int newElement = 1;
        int line = 2;
        int[][] elements = {{0, 1}, {}, {1, -1, 3}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 1}, {}, {1, -1, 3, 1}};

        boolean add = biVector.addElement(newElement, line);

        assertArrayEquals(expected, biVector.toArray());
        assertTrue(add);
    }

    @Test
    void addElementIntoBiVector_LineBellowZero() {
        int newElement = 1;
        int line = -1;
        int[][] elements = {{0, 1}, {}, {1, -1, 3}};
        BiVector biVector = new BiVector(elements);

        boolean add = biVector.addElement(newElement, line);

        assertFalse(add);
    }

    @Test
    void addElementIntoBiVector_LineAboveNumberOfLines() {
        int newElement = 1;
        int line = 3;
        int[][] elements = {{0, 1}, {}, {1, -1, 3}};
        BiVector biVector = new BiVector(elements);

        boolean add = biVector.addElement(newElement, line);

        assertFalse(add);
    }

    @Test
    void isEmpty_True() {
        BiVector biVector = new BiVector();

        boolean isEmpty = biVector.isEmpty();

        assertTrue(isEmpty);
    }


    @Test
    void isEmpty_False() {
        int[][] elements = {{}, {1, 2}};
        BiVector biVector = new BiVector(elements);

        boolean isEmpty = biVector.isEmpty();

        assertFalse(isEmpty);
    }


    @Test
    void checkEmpty() {
        int value = 1;
        BiVector biVector = new BiVector();

        Exception exception = assertThrows(IllegalStateException.class, () -> biVector.removeFirstElementOfGivenValue(value));
        String expectedMessage = "two-dimensional array without elements";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void removeFirstElementOfGivenValue_ElementNotFound() {
        int value = 1;
        int[][] elements = {{0, 2}, {}, {5, -1, 3}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 2}, {}, {5, -1, 3}};

        boolean removeFistElementWithGivenValue = biVector.removeFirstElementOfGivenValue(value);

        assertArrayEquals(expected, biVector.toArray());
        assertFalse(removeFistElementWithGivenValue);
    }

    @Test
    void removeFirstElementOfGivenValue_Unique() {
        int value = 1;
        int[][] elements = {{0, 2}, {}, {1, -1, 3}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 2}, {}, {-1, 3}};

        boolean removeFistElementWithGivenValue = biVector.removeFirstElementOfGivenValue(value);

        assertArrayEquals(expected, biVector.toArray());
        assertTrue(removeFistElementWithGivenValue);
    }

    @Test
    void removeFirstElementOfGivenValue_Duplicated() {
        int value = 1;
        int[][] elements = {{0, 1}, {}, {1, -1, 3}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0}, {}, {1, -1, 3}};

        boolean removeFistElementWithGivenValue = biVector.removeFirstElementOfGivenValue(value);

        assertArrayEquals(expected, biVector.toArray());
        assertTrue(removeFistElementWithGivenValue);
    }

    @Test
    void removeFirstElementOfGivenValue_LineWithTheValue() {
        int value = 4;
        int[][] elements = {{0, 1}, {4}, {1, -1, 3}};
        BiVector biVector = new BiVector(elements);
        int[][] expected = {{0, 1}, {}, {1, -1, 3}};
        BiVector expected2 = new BiVector(new int[][]{{0, 1}, {}, {1, -1, 3}});

        boolean removeFistElementWithGivenValue = biVector.removeFirstElementOfGivenValue(value);

        // check BiVector equality
        assertEquals(expected2, biVector);
        // check Array equality
        assertArrayEquals(expected, biVector.toArray());
        assertTrue(removeFistElementWithGivenValue);
    }


    @Test
    void getMaxValue_SingleElement() {
        int[][] elements = {{}, {1}, {}};
        BiVector biVector = new BiVector(elements);
        int expected = 1;

        int result = biVector.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    void getMaxValue_SomeElements() {
        int[][] elements = {{1}, {}, {2, 3, 1}};
        BiVector biVector = new BiVector(elements);
        int expected = 3;

        int result = biVector.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    void getMaxValue_SomeNegativeElements() {
        int[][] elements = {{}, {}, {-2, -3, -1}};
        BiVector biVector = new BiVector(elements);
        int expected = -1;

        int result = biVector.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    void getMinValue_SingleElement() {
        int[][] elements = {{}, {1}, {}};
        BiVector biVector = new BiVector(elements);
        int expected = 1;

        int result = biVector.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    void getMinValue_SomeNegativeElements() {
        int[][] elements = {{}, {}, {-2, -3, -1}};
        BiVector biVector = new BiVector(elements);
        int expected = -3;

        int result = biVector.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    void getMinValue_SomeElements() {
        int[][] elements = {{1}, {}, {2, 3, 1}};
        BiVector biVector = new BiVector(elements);
        int expected = 1;

        int result = biVector.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    void getElementsAverage_SingleElement() {
        int[][] elements = {{}, {1}, {}};
        BiVector biVector = new BiVector(elements);
        double expected = 1;

        double result = biVector.getElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getElementsAverage_SomeElements() {
        int[][] elements = {{5, -2}, {1, 0}, {-1, 1, 2}, {}};
        BiVector biVector = new BiVector(elements);
        double expected = 0.86;

        double result = biVector.getElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getElementsAverage_SomeElements_NegativeAvg() {
        int[][] elements = {{-1, 1, 2}, {}, {-2, 0}, {-7, 2}};
        BiVector biVector = new BiVector(elements);
        double expected = -0.71;

        double result = biVector.getElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void getSumOfEachLine_SingleElement() {
        int[][] elements = {{}, {1}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{0, 1});

        Vector result = biVector.getSumOfEachRow();

        assertEquals(result, expected);
    }

    @Test
    void getSumOfEachLine_SomeElements() {
        int[][] elements = {{0}, {1}, {1, 1}, {-1, -1}, {1, -1}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{0, 1, 2, -2, 0});

        Vector result = biVector.getSumOfEachRow();

        assertEquals(result, expected);
    }


    @Test
    void getSumOfEachColumn_SingleElement() {
        int[][] elements = {{}, {1}, {}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{1});

        Vector result = biVector.getSumOfEachColumn();

        assertEquals(result, expected);
    }

    @Test
    void getSumOfEachColumn_SomeElements() {
        int[][] elements = {{1}, {}, {5, -1, 3}, {2, -5}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{8, -6, 3});

        Vector result = biVector.getSumOfEachColumn();

        assertEquals(result, expected);
    }

    @Test
    void getSumOfEachColumn_SquareBiVector() {
        int[][] elements = {{1, -1, 0}, {1, 5, -2}, {5, -1, 3}, {2, 0, -5}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{9, 3, -4});

        Vector result = biVector.getSumOfEachColumn();

        assertEquals(result, expected);
    }

    @Test
    void getLineWithMaxSum_LinesWithSameSum_WithEmptyLines() {
        int[][] elements = {{}, {0}, {}, {}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{0, 1, 2, 3});

        Vector result = biVector.getRowWithMaxSum();

        assertEquals(result, expected);
    }

    @Test
    void getLineWithMaxSum_LinesWithSameSum() {
        int[][] elements = {{0}, {1, -1}, {-2, 2}, {0, 5, -5}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{0, 1, 2, 3});

        Vector result = biVector.getRowWithMaxSum();

        assertEquals(result, expected);
    }

    @Test
    void getLineWithMaxSum_LinesWithDistinctSum() {
        int[][] elements = {{5, -5, 2}, {-2, 3, 2}, {-2, 2}, {5, 2, -8}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{1});

        Vector result = biVector.getRowWithMaxSum();

        assertEquals(result, expected);
    }

    @Test
    void isSquare_False() {
        int[][] elements = {{5, -5, 2}, {-2, 3, 2}, {-2, 2, 1}, {5, 2, -8}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSquare();

        assertFalse(result);
    }

    @Test
    void isSquare_OrderOne() {
        int[][] elements = {{1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSquare();

        assertTrue(result);
    }

    @Test
    void isSquare_OrderTwo() {
        int[][] elements = {{1, 0}, {-1, 5}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSquare();

        assertTrue(result);
    }

    @Test
    void isSquare_OrderThree() {
        int[][] elements = {{1, 0, 10}, {-1, 0, 5}, {0, 1, 0}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSquare();

        assertTrue(result);
    }

    @Test
    void isSymmetric_NotSquare() {
        int[][] elements = {{1, 1}, {1, 1}, {1, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSymmetric();

        assertFalse(result);
    }

    @Test
    void isSymmetric_NotSymmetric() {
        int[][] elements = {{1, 2}, {1, 2}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSymmetric();

        assertFalse(result);
    }

    @Test
    void isSymmetric_SameElements() {
        int[][] elements = {{1, 1}, {1, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSymmetric();

        assertTrue(result);
    }

    @Test
    void isSymmetric_DifferentElements() {
        int[][] elements = {{1, 2}, {2, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSymmetric();

        assertTrue(result);
    }

    @Test
    void isSymmetric_DifferentElements_OrderThree() {
        int[][] elements = {{5, 6, 7}, {6, 3, 2}, {7, 2, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isSymmetric();

        assertTrue(result);
    }

    @Test
    void countNonNullElementsInPrincipalDiagonal_NonSquare() {
        int[][] elements = {{1, 1}, {1, 1}, {1, 1}};
        BiVector biVector = new BiVector(elements);
        int expected = -1;

        int result = biVector.countNonNullElementsInPrincipalDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void countNonNullElementsInPrincipalDiagonal_AllElementsNull() {
        int[][] elements = {{0, 1, 1}, {1, 0, 1}, {1, 1, 0}};
        BiVector biVector = new BiVector(elements);
        int expected = 0;

        int result = biVector.countNonNullElementsInPrincipalDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void countNonNullElementsInPrincipalDiagonal_WithoutNullElements() {
        int[][] elements = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        BiVector biVector = new BiVector(elements);
        int expected = 3;

        int result = biVector.countNonNullElementsInPrincipalDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void countNonNullElementsInPrincipalDiagonal_MixedElements() {
        int[][] elements = {{0, 1, 2}, {0, -1, 2}, {5, 2, 0}};
        BiVector biVector = new BiVector(elements);
        int expected = 1;

        int result = biVector.countNonNullElementsInPrincipalDiagonal();

        assertEquals(result, expected);
    }

    @Test
    void isPrincipalAndSecondaryDiagonalEquals_False() {
        int[][] elements = {{1, 0, 1}, {1, 0, 1}, {0, 0, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isPrincipalAndSecondaryDiagonalEquals();

        assertFalse(result);
    }

    @Test
    void isPrincipalAndSecondaryDiagonalEquals_NonSquare() {
        int[][] elements = {{1, 1}, {1, 1}, {1, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isPrincipalAndSecondaryDiagonalEquals();

        assertFalse(result);
    }

    @Test
    void isPrincipalAndSecondaryDiagonalEquals_SameElements() {
        int[][] elements = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isPrincipalAndSecondaryDiagonalEquals();

        assertTrue(result);
    }

    @Test
    void isPrincipalAndSecondaryDiagonalEquals_DifferentElements() {
        int[][] elements = {{-1, 5, -1}, {-2, 0, -3}, {1, 2, 1}};
        BiVector biVector = new BiVector(elements);

        boolean result = biVector.isPrincipalAndSecondaryDiagonalEquals();

        assertTrue(result);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_Empty() {
        int[][] elements = {{}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{});

        Vector result = biVector.getElementsWithMoreDigitsThanAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_ElementsWithSameNumberOfDigits() {
        int[][] elements = {{}, {-2, 0, -3}, {1, 2, 1}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{});

        Vector result = biVector.getElementsWithMoreDigitsThanAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_OneElementWithMoreDigits() {
        int[][] elements = {{}, {-2, 0, -3}, {1, 20, 1}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{20});

        Vector result = biVector.getElementsWithMoreDigitsThanAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_MixedElements() {
        int[][] elements = {{}, {-2, 0, -356}, {122, 20, 1}, {100, -2546}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{-356, 122, 100, -2546});

        Vector result = biVector.getElementsWithMoreDigitsThanAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_Empty() {
        int[][] elements = {};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{});

        Vector result = biVector.getElementsWithEvenPercentageAboveAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_OnlyEvenDigits() {
        int[][] elements = {{}, {2, 4}, {8, 0}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{});

        Vector result = biVector.getElementsWithEvenPercentageAboveAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_WithoutEvenDigits() {
        int[][] elements = {{}, {1, 35}, {9, 71}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{});

        Vector result = biVector.getElementsWithEvenPercentageAboveAverage();

        assertEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_MixedElements() {
        int[][] elements = {{}, {12, 32}, {0, 886}, {654, 52, 1, 50}};
        BiVector biVector = new BiVector(elements);
        Vector expected = new Vector(new int[]{0, 886, 654});

        Vector result = biVector.getElementsWithEvenPercentageAboveAverage();

        assertEquals(result, expected);
    }

    @Test
    void reverseRowElements_SomeElements() {
        int[][] elements = {{1, 2, 3}, {0, -1, 2, 5}, {22, 30}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{3, 2, 1}, {5, 2, -1, 0}, {30, 22}});

        boolean reverse = biVector.reverseRowElements();

        assertEquals(biVector, expected);
        assertTrue(reverse);
    }

    @Test
    void reverseRowElements_SingleElements() {
        int[][] elements = {{1}, {-1}, {}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{1}, {-1}, {}});

        boolean reverse = biVector.reverseRowElements();

        assertEquals(biVector, expected);
        assertTrue(reverse);
    }

    @Test
    void reverseRows_SingleElements() {
        int[][] elements = {{1}, {-1}, {}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{}, {-1}, {1}});

        boolean reverse = biVector.reverseRows();

        assertEquals(biVector, expected);
        assertTrue(reverse);
    }

    @Test
    void reverseRows_SomeElements() {
        int[][] elements = {{1}, {-1, 2, 3}, {}, {0, 5}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{0, 5}, {}, {-1, 2, 3}, {1}});

        boolean reverse = biVector.reverseRows();

        assertEquals(biVector, expected);
        assertTrue(reverse);
    }

    @Test
    void rotateNinetyDegreesPositive_Empty() {
        int[][] elements = {};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{});

        boolean rotate = biVector.rotateNinetyDegreesPositive();

        assertEquals(biVector, expected);
        assertFalse(rotate);
    }

    @Test
    void rotateNinetyDegreesPositive_NotSameNumberOfRowsAndColumns() {
        int[][] elements = {{1, 2, 3}, {1, 2}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{1, 2, 3}, {1, 2}});

        boolean rotate = biVector.rotateNinetyDegreesPositive();

        assertEquals(biVector, expected);
        assertFalse(rotate);
    }

    @Test
    void rotateNinetyDegreesPositive_SquareBiVector() {
        int[][] elements = {{1, 2}, {3, 4}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{3, 1}, {4, 2}});

        boolean rotate = biVector.rotateNinetyDegreesPositive();

        assertEquals(biVector, expected);
        assertTrue(rotate);
    }

    @Test
    void rotateNinetyDegreesPositive_RectangularBiVector() {
        int[][] elements = {{1, 2}, {3, 4}, {5, 6}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{5, 3, 1}, {6, 4, 2}});

        boolean rotate = biVector.rotateNinetyDegreesPositive();

        assertEquals(biVector, expected);
        assertTrue(rotate);
    }

    @Test
    void rotateNinetyDegreesNegative_Empty() {
        int[][] elements = {};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{});

        boolean rotate = biVector.rotateNinetyDegreesNegative();

        assertEquals(biVector, expected);
        assertFalse(rotate);
    }

    @Test
    void rotateNinetyDegreesNegative_NotSameNumberOfRowsAndColumns() {
        int[][] elements = {{1, 2, 3}, {1, 2}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{1, 2, 3}, {1, 2}});

        boolean rotate = biVector.rotateNinetyDegreesNegative();

        assertEquals(biVector, expected);
        assertFalse(rotate);
    }

    @Test
    void rotateNinetyDegreesNegative_SquareBiVector() {
        int[][] elements = {{1, 2}, {3, 4}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{2, 4}, {1, 3}});

        boolean rotate = biVector.rotateNinetyDegreesNegative();

        assertEquals(biVector, expected);
        assertTrue(rotate);
    }

    @Test
    void rotateNinetyDegreesNegative_RectangularBiVector() {
        int[][] elements = {{1, 2}, {3, 4}, {5, 6}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{2, 4, 6}, {1, 3, 5}});

        boolean rotate = biVector.rotateNinetyDegreesNegative();

        assertEquals(biVector, expected);
        assertTrue(rotate);
    }

    @Test
    void rotateOneHundredAndEightyDegrees_Empty() {
        int[][] elements = {};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{});

        boolean rotate = biVector.rotateOneHundredAndEightyDegrees();

        assertEquals(biVector, expected);
        assertFalse(rotate);
    }

    @Test
    void rotateOneHundredAndEightyDegrees_NotSameNumberOfRowsAndColumns() {
        int[][] elements = {{1, 2, 3}, {1, 2}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{1, 2, 3}, {1, 2}});

        boolean rotate = biVector.rotateOneHundredAndEightyDegrees();

        assertEquals(biVector, expected);
        assertFalse(rotate);
    }

    @Test
    void rotateOneHundredAndEightyDegrees_SquareBiVector() {
        int[][] elements = {{1, 2}, {3, 4}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{4, 3}, {2, 1}});

        boolean rotate = biVector.rotateOneHundredAndEightyDegrees();

        assertEquals(biVector, expected);
        assertTrue(rotate);
    }

    @Test
    void rotateOneHundredAndEightyDegrees_RectangularBiVector() {
        int[][] elements = {{1, 2}, {3, 4}, {5, 6}};
        BiVector biVector = new BiVector(elements);
        BiVector expected = new BiVector(new int[][]{{6, 5}, {4, 3}, {2, 1}});

        boolean rotate = biVector.rotateOneHundredAndEightyDegrees();

        assertEquals(biVector, expected);
        assertTrue(rotate);
    }

}
