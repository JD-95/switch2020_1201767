import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTests {

    @Test
    void createEmptyVector() {
        Vector vector1 = new Vector();
        int[] expected = {};

        int[] result = vector1.toArray();

        assertArrayEquals(expected, result);
        assertNotSame(result, expected);
    }

    @Test
    void createVector_InvalidArray() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Vector(null));
        String expectedMessage = "array should not be null";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void createVectorWithOneElement() {
        int[] elements = {0};
        Vector vector = new Vector(elements);

        int[] result = vector.toArray();

        assertArrayEquals(elements, result);
        assertNotSame(result, elements);
    }

    @Test
    void createVectorWithSomeElements() {
        int[] elements = {0, 2, 1};
        Vector vector = new Vector(elements);

        int[] result = vector.toArray();

        assertArrayEquals(elements, result);
        assertNotSame(result, elements);
    }

    @Test
    void addNewNumberIntoEmptyVector() {
        int newElement = 0;
        Vector vector = new Vector();
        int[] expected = {0};

        boolean add = vector.addElement(newElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(add);
    }

    @Test
    void addNewNumberIntoVectorWithSomeElements() {
        int newElement = 0;
        int[] elements = new int[]{1, 2};
        Vector vector = new Vector(elements);
        int[] expected = {1, 2, 0};

        boolean add = vector.addElement(newElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(add);
    }

    @Test
    void addNewNumberIntoVectorWithEqualElement() {
        int newElement = 1;
        int[] elements = new int[]{1};
        Vector vector = new Vector(elements);
        int[] expected = {1, 1};

        boolean add = vector.addElement(newElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(add);
    }

    @Test
    void addNegativeNumberIntoVectorWithSomeElements() {
        int newElement = -1;
        int[] elements = new int[]{1, 0};
        Vector vector = new Vector(elements);
        int[] expected = {1, 0, -1};

        boolean add = vector.addElement(newElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(add);
    }

    @Test
    void removeElement_FirstElement() {
        int indexElement = 0;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);
        int[] expected = {1, 2, 3};

        boolean remove = vector.removeElement(indexElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(remove);
    }

    @Test
    void removeElement_LastElement() {
        int indexElement = 3;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);
        int[] expected = {0, 1, 2};

        boolean remove = vector.removeElement(indexElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(remove);
    }

    @Test
    void removeElement_MiddleElement() {
        int indexElement = 1;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);
        int[] expected = {0, 2, 3};

        boolean remove = vector.removeElement(indexElement);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(remove);
    }

    @Test
    void removeElement_IndexBellowRange() {
        int indexElement = -1;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);

        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> vector.removeElement(indexElement));
        String expectedMessage = "index out of range";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void removeElement_IndexAboveRange() {
        int indexElement = 4;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);

        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> vector.removeElement(indexElement));
        String expectedMessage = "index out of range";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void removeFirstElementOfGivenValue_UniqueInSomeElements() {
        int value = 1;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);
        int[] expected = {0, 2, 3};

        boolean removeFirstElementOfGivenValue = vector.removeFirstElementOfGivenValue(value);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(removeFirstElementOfGivenValue);
    }

    @Test
    void removeFirstElementOfGivenValue_DuplicatedInSomeElements() {
        int value = 1;
        int[] elements = new int[]{0, 1, 2, 1};
        Vector vector = new Vector(elements);
        int[] expected = {0, 2, 1};

        boolean removeFirstElementOfGivenValue = vector.removeFirstElementOfGivenValue(value);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(removeFirstElementOfGivenValue);
    }

    @Test
    void removeFirstElementOfGivenValue_EqualToUniqueElement() {
        int value = 1;
        int[] elements = new int[]{1};
        Vector vector = new Vector(elements);
        int[] expected = {};

        boolean removeFirstElementOfGivenValue = vector.removeFirstElementOfGivenValue(value);
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
        assertTrue(removeFirstElementOfGivenValue);
    }

    @Test
    void removeFirstElementOfGivenValue_ValueNotFound() {
        int value = -1;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> vector.removeFirstElementOfGivenValue(value));
        String expectedMessage = "value not found in array";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void getValueOfElement_FirstElement() {
        int index = 0;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);
        int expected = 0;

        int result = vector.getValueOfElement(index);

        assertEquals(expected, result);
    }

    @Test
    void getValueOfElement_LastElement() {
        int index = 3;
        int[] elements = new int[]{0, 1, 2, 3};
        Vector vector = new Vector(elements);
        int expected = 3;

        int result = vector.getValueOfElement(index);

        assertEquals(expected, result);
    }

    @Test
    void getValueOfElement_MiddleElement() {
        int index = 1;
        int[] elements = new int[]{0, -1, 2, 3};
        Vector vector = new Vector(elements);
        int expected = -1;

        int result = vector.getValueOfElement(index);

        assertEquals(expected, result);
    }

    @Test
    void getValueOfElement_IndexBellowRange() {
        int index = -1;
        int[] elements = new int[]{0, -1, 2, 3};
        Vector vector = new Vector(elements);

        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> vector.getValueOfElement(index));
        String expectedMessage = "index out of range";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void getValueOfElement_IndexAboveRange() {
        int index = 4;
        int[] elements = new int[]{0, -1, 2, 3};
        Vector vector = new Vector(elements);

        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> vector.getValueOfElement(index));
        String expectedMessage = "index out of range";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void countElements_EmptyVector() {
        Vector vector = new Vector();
        int expected = 0;

        int result = vector.length();

        assertEquals(expected, result);
    }

    @Test
    void countElements_SingleElement() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        int expected = 1;

        int result = vector.length();

        assertEquals(expected, result);
    }

    @Test
    void countElements_SomeElements() {
        int[] elements = {1, 0, -1, 10, -10};
        Vector vector = new Vector(elements);
        int expected = 5;

        int result = vector.length();

        assertEquals(expected, result);
    }

    @Test
    void checkEmpty() {
        Vector vector = new Vector();

        Exception exception = assertThrows(IllegalStateException.class, vector::getMaxValue);
        String expectedMessage = "array without elements";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void maxValue_SingleElement() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        int expected = 1;

        int result = vector.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    void maxValue_SomeElements() {
        int[] elements = {0, 2, 5, 1, 5};
        Vector vector = new Vector(elements);
        int expected = 5;

        int result = vector.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    void maxValue_SomeNegativeElements() {
        int[] elements = {-1, -2, -3, -4, -5};
        Vector vector = new Vector(elements);
        int expected = -1;

        int result = vector.getMaxValue();

        assertEquals(expected, result);
    }

    @Test
    void minValue_SingleElement() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        int expected = 1;

        int result = vector.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    void minValue_SomeElements() {
        int[] elements = {0, 2, 0, 1, 5};
        Vector vector = new Vector(elements);
        int expected = 0;

        int result = vector.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    void minValue_SomeNegativeElements() {
        int[] elements = {-1, -2, -3, -4, -5};
        Vector vector = new Vector(elements);
        int expected = -5;

        int result = vector.getMinValue();

        assertEquals(expected, result);
    }

    @Test
    void elementsAverage_SingleElementZero() {
        int[] elements = {0};
        Vector vector = new Vector(elements);
        double expected = 0;

        double result = vector.getElementsAverage();

        assertEquals(expected, result);
    }

    @Test
    void elementsAverage_SingleElementPositive() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        double expected = 1;

        double result = vector.getElementsAverage();

        assertEquals(expected, result);
    }

    @Test
    void elementsAverage_SingleElementNegative() {
        int[] elements = {-1};
        Vector vector = new Vector(elements);
        double expected = -1;

        double result = vector.getElementsAverage();

        assertEquals(expected, result);
    }

    @Test
    void elementsAverage_SomeNegativeElements() {
        int[] elements = {-2, -2, -1};
        Vector vector = new Vector(elements);
        double expected = -1.66;

        double result = vector.getElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void elementsAverage_SomePositiveElements() {
        int[] elements = {2, 2, 1};
        Vector vector = new Vector(elements);
        double expected = 1.66;

        double result = vector.getElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void evenElementsAverage_OnlyEvenElements() {
        int[] elements = {-2, 0, 2, 4};
        Vector vector = new Vector(elements);
        double expected = 1;

        double result = vector.getEvenElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void evenElementsAverage_WithoutEvenElements() {
        int[] elements = {-3, -1, 1, 3, 5};
        Vector vector = new Vector(elements);
        double expected = 0;

        double result = vector.getEvenElementsAverage();

        assertEquals(expected, result);
    }

    @Test
    void evenElementsAverage_MixedElements() {
        int[] elements = {-3, -2, -1, 1, 2, 3, 4, 5};
        Vector vector = new Vector(elements);
        double expected = 1.33;

        double result = vector.getEvenElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void oddElementsAverage_OnlyOddElements() {
        int[] elements = {-3, -1, 1, 3, 5};
        Vector vector = new Vector(elements);
        double expected = 1;

        double result = vector.getOddElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void oddElementsAverage_WithoutOddElements() {
        int[] elements = {-2, 0, 2, 4};
        Vector vector = new Vector(elements);
        double expected = 0;

        double result = vector.getOddElementsAverage();

        assertEquals(expected, result);
    }

    @Test
    void oddElementsAverage_MixedElements() {
        int[] elements = {-5, -2, -1, 1, 2, 3, 4};
        Vector vector = new Vector(elements);
        double expected = -0.5;

        double result = vector.getOddElementsAverage();

        assertEquals(expected, result, 0.01);
    }

    @Test
    void multiplesAverage_WithoutMultiples() {
        int number = 2;
        int[] elements = {1, 3, 5, 7};
        Vector vector = new Vector(elements);
        double expected = 0;

        double result = vector.getMultiplesAverage(number);

        assertEquals(expected, result);
    }

    @Test
    void multiplesAverage_WithOnlyMultiples() {
        int number = 1;
        int[] elements = {1, 3, 5, 7};
        Vector vector = new Vector(elements);
        double expected = 4;

        double result = vector.getMultiplesAverage(number);

        assertEquals(expected, result);
    }

    @Test
    void multiplesAverage_MixedElements() {
        int number = 2;
        int[] elements = {0, 1, 3, 4, 5, 6};
        Vector vector = new Vector(elements);
        double expected = 3.33;

        double result = vector.getMultiplesAverage(number);

        assertEquals(expected, result, 0.01);
    }

    @Test
    void sortAsc_SingleElement() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        int[] expected = {1};

        vector.sortAsc();
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortAsc_AlreadyOrdered() {
        int[] elements = {-2, -1, 0, 1, 2};
        Vector vector = new Vector(elements);
        int[] expected = {-2, -1, 0, 1, 2};

        vector.sortAsc();
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortAsc_NotOrdered() {
        int[] elements = {2, -1, 1, 0, -2};
        Vector vector = new Vector(elements);
        int[] expected = {-2, -1, 0, 1, 2};

        vector.sortAsc();
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortDesc_SingleElement() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        int[] expected = {1};

        vector.sortDesc();
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortDesc_AlreadyOrdered() {
        int[] elements = {2, 1, 0, -1, -2};
        Vector vector = new Vector(elements);
        int[] expected = {2, 1, 0, -1, -2};

        vector.sortDesc();
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void sortDesc_NotOrdered() {
        int[] elements = {-2, -1, 1, 0, 2};
        Vector vector = new Vector(elements);
        int[] expected = {2, 1, 0, -1, -2};

        vector.sortDesc();
        int[] result = vector.toArray();

        assertArrayEquals(expected, result);
    }

    @Test
    void isEmptyTrue() {
        Vector vector = new Vector();

        boolean result = vector.isEmpty();

        assertTrue(result);
    }

    @Test
    void isEmptyFalse() {
        int[] elements = {0};
        Vector vector = new Vector(elements);

        boolean result = vector.isEmpty();

        assertFalse(result);
    }

    @Test
    void isSingleElement_True() {
        int[] elements = {0};
        Vector vector = new Vector(elements);

        boolean result = vector.isSingleElement();

        assertTrue(result);
    }

    @Test
    void isSingleElement_MoreElements() {
        int[] elements = {0, 1};
        Vector vector = new Vector(elements);

        boolean result = vector.isSingleElement();

        assertFalse(result);
    }

    @Test
    void hasOnlyEvenElements_True() {
        int[] elements = {0, 2, 4, 6};
        Vector vector = new Vector(elements);

        boolean result = vector.hasOnlyEvenElements();

        assertTrue(result);
    }

    @Test
    void hasOnlyEvenElements_False() {
        int[] elements = {0, 1, 2, 4, 6};
        Vector vector = new Vector(elements);

        boolean result = vector.hasOnlyEvenElements();

        assertFalse(result);
    }

    @Test
    void hasOnlyOddElements_True() {
        int[] elements = {1, 3, 5, 7};
        Vector vector = new Vector(elements);

        boolean result = vector.hasOnlyOddElements();

        assertTrue(result);
    }

    @Test
    void hasOnlyOddElements_False() {
        int[] elements = {0, 1, 3, 5};
        Vector vector = new Vector(elements);

        boolean result = vector.hasOnlyOddElements();

        assertFalse(result);
    }

    @Test
    void hasRepeatedElements_False() {
        int[] elements = {0, 1, 2, 4, 6};
        Vector vector = new Vector(elements);

        boolean result = vector.hasRepeatedElements();

        assertFalse(result);
    }

    @Test
    void hasRepeatedElements_True() {
        int[] elements = {0, 1, 2, 4, 6, 1};
        Vector vector = new Vector(elements);

        boolean result = vector.hasRepeatedElements();

        assertTrue(result);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_AllWithSameNumberOfDigits() {
        int[] elements = {0, -1, 2, -3, 4};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithMoreDigitsThanAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_OneElementWithMoreDigits() {
        int[] elements = {0, -10};
        Vector vector = new Vector(elements);
        int[] expected = {-10};

        vector = vector.getElementsWithMoreDigitsThanAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithMoreDigitsThanAverage_WithDifferentNumberOfDigits() {
        int[] elements = {0, -10, 200, 3000};
        Vector vector = new Vector(elements);
        int[] expected = {200, 3000};

        vector = vector.getElementsWithMoreDigitsThanAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_OnlyEvenElements() {
        int[] elements = {0, 2, 4, 6, 8};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithEvenPercentageAboveAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_WithoutEvenElements() {
        int[] elements = {1, 3, 5, 7};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithEvenPercentageAboveAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_OneElementWithMoreEvenPercentage() {
        int[] elements = {12, 36, 41, 80};
        Vector vector = new Vector(elements);
        int[] expected = {80};

        vector = vector.getElementsWithEvenPercentageAboveAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithEvenPercentageAboveAverage_WithDifferentNumberOfDigits() {
        int[] elements = {0, 1, 25, 110, 240, 665, 68, 2};
        Vector vector = new Vector(elements);
        int[] expected = {0, 240, 68, 2};

        vector = vector.getElementsWithEvenPercentageAboveAverage();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithOnlyEvenDigits_OnlyEvenDigits() {
        int[] elements = {0, 24, 4, 60, 8};
        Vector vector = new Vector(elements);
        int[] expected = {0, 24, 4, 60, 8};

        vector = vector.getElementsWithOnlyEvenDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithOnlyEvenDigits_OnlyOddDigits() {
        int[] elements = {1, 37, 3, 95, 7};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithOnlyEvenDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithOnlyEvenDigits_MixedElements() {
        int[] elements = {1, 68, 3, 40, 95, 2};
        Vector vector = new Vector(elements);
        int[] expected = {68, 40, 2};

        vector = vector.getElementsWithOnlyEvenDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAscendingDigits_AllElementsWithAscendingDigits() {
        int[] elements = {12, 234, 3456, 45678};
        Vector vector = new Vector(elements);
        int[] expected = {12, 234, 3456, 45678};

        vector = vector.getElementsWithAscendingDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAscendingDigits_WithoutElementsWithAscendingDigits() {
        int[] elements = {0, 11, 121, 321};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithAscendingDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAscendingDigits_MixedElements() {
        int[] elements = {5, 12, 22, 456, 45676, 5678, 987};
        Vector vector = new Vector(elements);
        int[] expected = {12, 456, 5678};

        vector = vector.getElementsWithAscendingDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getCapicuaElements_WithOnlyCapicuaElements() {
        int[] elements = {0, 2, 8, 9, 11, 22, 33, 474, 515};
        Vector vector = new Vector(elements);
        int[] expected = {0, 2, 8, 9, 11, 22, 33, 474, 515};

        vector = vector.getCapicuaElements();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getCapicuaElements_WithoutCapicuaElements() {
        int[] elements = {-1, -10, 10, 12, 21, 34, 100, 475, 514};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getCapicuaElements();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getCapicuaElements_MixedElements() {
        int[] elements = {-10, -5, 0, 5, 10, 11, 33, 40, 100, 325, 475, 515};
        Vector vector = new Vector(elements);
        int[] expected = {0, 5, 11, 33, 515};

        vector = vector.getCapicuaElements();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithSameDigits_AllElementsWithSameDigits() {
        int[] elements = {0, 1, 22, 333, 4444, 55555};
        Vector vector = new Vector(elements);
        int[] expected = {0, 1, 22, 333, 4444, 55555};

        vector = vector.getElementsWithSameDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithSameDigits_WithoutElementsWithSameDigits() {
        int[] elements = {10, 21, 1112, 311};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithSameDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithSameDigits_MixedElements() {
        int[] elements = {0, 21, 33, 5, 321, 332, 55};
        Vector vector = new Vector(elements);
        int[] expected = {0, 33, 5, 55};

        vector = vector.getElementsWithSameDigits();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getNotArmstrongElements_OnlyArmstrongElements() {
        int[] elements = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370, 371, 407};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getNotArmstrongElements();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getNotArmstrongElements_OnlyNotArmstrongElements() {
        int[] elements = {10, 154, 369, 372, 408};
        Vector vector = new Vector(elements);
        int[] expected = {10, 154, 369, 372, 408};

        vector = vector.getNotArmstrongElements();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getNotArmstrongElements_MixedElements() {
        int[] elements = {153, 154, 369, 371, 5, 0, 6, 100, 408, 407};
        Vector vector = new Vector(elements);
        int[] expected = {154, 369, 100, 408};

        vector = vector.getNotArmstrongElements();
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAtLeastXAscendingDigits_TwoAscendingElements() {
        int ascendingDigits = 2;
        int[] elements = {1, 12, 123, 1234, 12345, 123456};
        Vector vector = new Vector(elements);
        int[] expected = {12, 123, 1234, 12345, 123456};

        vector = vector.getElementsWithAtLeastXAscendingDigits(ascendingDigits);
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAtLeastXAscendingDigits_ThreeAscendingElements() {
        int ascendingDigits = 3;
        int[] elements = {1, 12, 123, 1234, 12345, 123456};
        Vector vector = new Vector(elements);
        int[] expected = {123, 1234, 12345, 123456};

        vector = vector.getElementsWithAtLeastXAscendingDigits(ascendingDigits);
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAtLeastXAscendingDigits_SixAscendingElements() {
        int ascendingDigits = 6;
        int[] elements = {1, 12, 123, 1234, 12345, 123456};
        Vector vector = new Vector(elements);
        int[] expected = {123456};

        vector = vector.getElementsWithAtLeastXAscendingDigits(ascendingDigits);
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAtLeastXAscendingDigits_OneAscendingElements() {
        int ascendingDigits = 1;
        int[] elements = {1, 12, 123, 1234, 12345, 123456};
        Vector vector = new Vector(elements);
        int[] expected = {};

        vector = vector.getElementsWithAtLeastXAscendingDigits(ascendingDigits);
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void getElementsWithAtLeastXAscendingDigits_MixedElements() {
        int ascendingDigits = 3;
        int[] elements = {5, 321, 159, 5879, 3459, 651, 1122342};
        Vector vector = new Vector(elements);
        int[] expected = {159, 3459, 1122342};

        vector = vector.getElementsWithAtLeastXAscendingDigits(ascendingDigits);
        int[] result = vector.toArray();

        assertArrayEquals(result, expected);
    }

    @Test
    void equals_SameElements() {
        int[] elements = {1, 2, 3, 4};
        Vector vector1 = new Vector(elements);
        Vector vector2 = new Vector(elements);

        boolean result = vector1.equals(vector2);

        assertTrue(result);
    }

    @Test
    void equals_VectorItself() {
        int[] elements = {1, 2, 3, 4};
        Vector vector = new Vector(elements);

        boolean result = vector.equals(vector);

        assertTrue(result);
    }


    @Test
    void equals_NotSameTypeOfObject() {
        int[] elements = {1, 2, 3, 4};
        Vector vector1 = new Vector(elements);
        int[] vector2 = {1, 2, 3, 4};

        boolean result = vector1.equals(vector2);

        assertFalse(result);
    }

    @Test
    void equals_NotSameElements() {
        int[] elements1 = {1, 2, 3, 4};
        int[] elements2 = {1, 2, 3, 3};
        Vector vector1 = new Vector(elements1);
        Vector vector2 = new Vector(elements2);

        boolean result = vector1.equals(vector2);

        assertFalse(result);
    }

    @Test
    void equals_VectorNull() {
        int[] elements1 = {1, 2, 3, 4};
        Vector vector1 = new Vector(elements1);
        Vector vector2 = null;

        boolean result = vector1.equals(vector2);

        assertFalse(result);
    }


    @Test
    void createCopy() {
        int[] elements = {1, 2, 3, 4};
        Vector vector1 = new Vector(elements);
        Vector vector2 = vector1.createCopy();

        assertEquals(vector1, vector2);
        assertNotSame(vector1, vector2);
    }

    @Test
    void countNonNullElements_AllElementsNull() {
        int[] elements = {0, 0, 0, 0};
        Vector vector = new Vector(elements);
        int expected = 0;

        int result = vector.countNonNullElements();

        assertEquals(expected, result);
    }

    @Test
    void countNonNullElements_NonNullElements() {
        int[] elements = {1, -1, 2, 5};
        Vector vector = new Vector(elements);
        int expected = 4;

        int result = vector.countNonNullElements();

        assertEquals(expected, result);
    }

    @Test
    void countNonNullElements_MixedElements() {
        int[] elements = {0, -1, 0, 5};
        Vector vector = new Vector(elements);
        int expected = 2;

        int result = vector.countNonNullElements();

        assertEquals(expected, result);
    }

    @Test
    void reverseVector_Empty() {
        Vector vector = new Vector();
        Vector expected = new Vector();

        boolean result = vector.reverseVector();

        assertTrue(result);
        assertEquals(vector, expected);
    }

    @Test
    void reverseVector_SingleElement() {
        int[] elements = {1};
        Vector vector = new Vector(elements);
        Vector expected = new Vector(elements);

        boolean result = vector.reverseVector();

        assertTrue(result);
        assertEquals(vector, expected);
    }

    @Test
    void reverseVector_SomeElements() {
        int[] elements = {1, 0, -1, 2};
        Vector vector = new Vector(elements);
        Vector expected = new Vector(new int[]{2, -1, 0, 1});

        boolean result = vector.reverseVector();

        assertTrue(result);
        assertEquals(vector, expected);
    }
}
