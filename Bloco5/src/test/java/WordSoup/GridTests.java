package WordSoup;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GridTests {

    @Test
    void createGrid_Invalid_Null() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Grid(null));
        String expectedMessage = "Grid cannot be null";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void createGrid_Invalid_Empty() {
        char[][] elements = new char[][]{};
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Grid(elements));
        String expectedMessage = "Grid cannot be empty";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void createGrid_Invalid_NotSameNumberOfColumns() {
        char[][] elements = new char[][]{{'a', 'b'}, {'a'}};
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Grid(elements));
        String expectedMessage = "Grid should have same number of columns in each row";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void createGrid_Valid() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        char[][] expected = {{'Y', 'A', 'B'}, {'A', 'Z', 'C'}, {'G', 'E', 'H'}};

        assertArrayEquals(expected, grid.toArray());
    }

    @Test
    void getSequence_HorizontalRight() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 0;
        int lastRow = 0;
        int lastColumn = 2;
        String expected = "YAB";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_HorizontalLeft() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 2;
        int lastRow = 0;
        int lastColumn = 1;
        String expected = "BA";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_VerticalUp() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 2;
        int startColumn = 1;
        int lastRow = 0;
        int lastColumn = 1;
        String expected = "EZA";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_VerticalDown() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 1;
        int startColumn = 0;
        int lastRow = 2;
        int lastColumn = 0;
        String expected = "AG";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_DiagonalLeftUp() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 1;
        int startColumn = 1;
        int lastRow = 0;
        int lastColumn = 0;
        String expected = "ZY";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_DiagonalRightDown() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 1;
        int startColumn = 0;
        int lastRow = 2;
        int lastColumn = 1;
        String expected = "AE";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_DiagonalRightUp() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 2;
        int startColumn = 0;
        int lastRow = 0;
        int lastColumn = 2;
        String expected = "GZB";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_DiagonalLeftDown() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 1;
        int lastRow = 1;
        int lastColumn = 0;
        String expected = "AA";

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertEquals(expected, sequenceFound);
    }

    @Test
    void getSequence_Invalid() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 0;
        int lastRow = 3;
        int lastColumn = 2;

        String sequenceFound = grid.getSequence(startRow, startColumn, lastRow, lastColumn);

        assertNull(sequenceFound);
    }

    @Test
    void exists_True() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        String word = "BCH";

        boolean exists = grid.exists(word);

        assertTrue(exists);
    }

    @Test
    void exists_WithLowerCase() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        String word = "YzH";

        boolean exists = grid.exists(word);

        assertTrue(exists);
    }

    @Test
    void exists_False() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        String word = "YAE";

        boolean exists = grid.exists(word);

        assertFalse(exists);
    }

    @Test
    void getNumberOfRows_SquareGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}};
        Grid grid = new Grid(elements);
        int expected = 3;

        int result = grid.getNumberOfRows();

        assertEquals(expected, result);
    }

    @Test
    void getNumberOfRows_RectangularGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'b'}, {'a', 'Z', 'C'}, {'g', 'e', 'H'}, {'j', 'K', 'O'}};
        Grid grid = new Grid(elements);
        int expected = 4;

        int result = grid.getNumberOfRows();

        assertEquals(expected, result);
    }

    @Test
    void getNumberOfColumns_SquareGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'b', 'F'}, {'a', 'Z', 'C', 'R'}, {'g', 'e', 'H', 'W'}};
        Grid grid = new Grid(elements);
        int expected = 4;

        int result = grid.getNumberOfColumns();

        assertEquals(expected, result);
    }

    @Test
    void getNumberOfColumns_RectangularGrid() {
        char[][] elements = new char[][]{{'Y', 'a'}, {'a', 'Z'}, {'g', 'e'}, {'j', 'K'}};
        Grid grid = new Grid(elements);
        int expected = 2;

        int result = grid.getNumberOfColumns();

        assertEquals(expected, result);
    }

    @Test
    void validateFinalCell_SameRow() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 0;
        int finalRow = 0;
        int finalColumn = 2;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertTrue(result);
    }

    @Test
    void validateFinalCell_SameRow_OutsideGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 0;
        int finalRow = 0;
        int finalColumn = 4;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertFalse(result);
    }


    @Test
    void validateFinalCell_SameColumn() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 1;
        int startColumn = 2;
        int finalRow = 2;
        int finalColumn = 2;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertTrue(result);
    }

    @Test
    void validateFinalCell_SameColumn_OutsideGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 3;
        int startColumn = 4;
        int finalRow = 0;
        int finalColumn = 4;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertFalse(result);
    }


    @Test
    void validateFinalCell_InPrincipalDiagonal() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 1;
        int startColumn = 1;
        int finalRow = 2;
        int finalColumn = 2;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertTrue(result);
    }

    @Test
    void validateFinalCell_InPrincipalDiagonal_OutsideGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 1;
        int startColumn = 1;
        int finalRow = 3;
        int finalColumn = 3;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertFalse(result);
    }


    @Test
    void validateFinalCellBothPrincipalAndSecondaryDiagonal() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 0;
        int finalRow = 1;
        int finalColumn = 1;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertTrue(result);
    }

    @Test
    void validateFinalCell_InSecondaryDiagonal() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 2;
        int finalRow = 2;
        int finalColumn = 0;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertTrue(result);
    }

    @Test
    void validateFinalCell_InSecondaryDiagonal_OutsideGrid() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 3;
        int finalRow = 3;
        int finalColumn = 0;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertFalse(result);
    }

    @Test
    void validateFinalCell_SameCell() {
        char[][] elements = new char[][]{{'Y', 'a', 'A'}, {'a', 'Z', 'W'}, {'g', 'e', 'F'}};
        Grid grid = new Grid(elements);
        int startRow = 0;
        int startColumn = 0;
        int finalRow = 0;
        int finalColumn = 0;

        boolean result = grid.validateFinalCell(startRow, startColumn, finalRow, finalColumn);

        assertFalse(result);
    }
}