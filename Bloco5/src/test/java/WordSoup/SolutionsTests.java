package WordSoup;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionsTests {

    @Test
    void Solutions_Invalid_Null() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Solutions(null));
        String expectedMessage = "Solutions array cannot be null";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void Solutions_Invalid_Empty() {
        String[] elements = new String[]{};

        Exception exception = assertThrows(IllegalArgumentException.class, () -> new Solutions(elements));
        String expectedMessage = "Solutions list should have at least one solution included";
        String resultMessage = exception.getMessage();

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    void Solutions_Valid() {
        Solutions solutions = new Solutions(new String[]{"AB", "BA", "AD", "DA", "AE", "EA", "BD", "DB"});
        String[] expectedSolution = new String[]{"AB", "BA", "AD", "DA", "AE", "EA", "BD", "DB"};

        assertArrayEquals(expectedSolution, solutions.toArray());
    }

    @Test
    void Solutions_WithLowerCaseLetters() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});
        String[] expectedSolution = new String[]{"AB", "BA", "AD", "DA", "AE", "EA", "BD", "DB"};

        assertArrayEquals(expectedSolution, solutions.toArray());
    }

    @Test
    void Solutions_RepeatedSolution() {
        Solutions solutions = new Solutions(new String[]{"aB", "AB", "BA", "Ad", "DA", "AE", "Ea", "da", "BD", "db"});
        String[] expectedSolution = new String[]{"AB", "BA", "AD", "AE", "EA", "DA", "BD", "DB"};

        assertArrayEquals(expectedSolution, solutions.toArray());
    }

    @Test
    void remove_SolutionExists() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});
        String[] expectedSolution = new String[]{"AB", "AD", "DA", "AE", "EA", "BD", "DB"};

        boolean remove = solutions.remove("Ba");

        assertArrayEquals(expectedSolution, solutions.toArray());
        assertTrue(remove);
    }

    @Test
    void remove_WordNotFound() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});
        String[] expectedSolution = new String[]{"AB", "BA", "AD", "DA", "AE", "EA", "BD", "DB"};

        boolean remove = solutions.remove("AA");

        assertArrayEquals(expectedSolution, solutions.toArray());
        assertFalse(remove);
    }

    @Test
    void remove_AlreadyRemoved() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});
        String[] expectedSolution = new String[]{"BA", "AD", "DA", "AE", "EA", "BD", "DB"};

        boolean remove = solutions.remove("AB");
        boolean remove2 = solutions.remove("AB");

        assertArrayEquals(expectedSolution, solutions.toArray());
        assertTrue(remove);
        assertFalse(remove2);
    }

    @Test
    void count_SomeElements() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});
        int expected = 8;

        int count = solutions.count();

        assertEquals(expected, count);
    }

    @Test
    void count_SingleElement() {
        Solutions solutions = new Solutions(new String[]{"aB"});
        int expected = 1;

        int count = solutions.count();

        assertEquals(expected, count);
    }


    @Test
    void isSolution_False() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});

        boolean contains = solutions.contains("ABA");

        assertFalse(contains);
    }

    @Test
    void isSolution_True() {
        Solutions solutions = new Solutions(new String[]{"aB", "BA", "Ad", "DA", "AE", "Ea", "BD", "db"});

        boolean contains = solutions.contains("ba");

        assertTrue(contains);
    }
}