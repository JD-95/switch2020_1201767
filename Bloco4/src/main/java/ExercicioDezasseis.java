public class ExercicioDezasseis {

    /**
     * Permite obter o determinante de uma matriz dada aplicando o teorema de laplace.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return O determinante da matriz.
     */
    public static Double obterDeterminanteMatriz(int[][] matriz) {

        // se matriz vazia, nula ou não quadrada retorna nulo
        if (ExercicioQuinze.matrizNaoPreenchida(matriz) || !ExercicioTreze.verificarMatrizQuadrada(matriz)) {
            return null;
        }

        double determinante = 0;
        int numeroLinhasColunas = matriz.length;
        int[][] matrizReduzida;

        // se a matriz já for de ordem = 2 ou 1, obter o determinante
        if (numeroLinhasColunas == 2 || numeroLinhasColunas == 1) {
            determinante = obterDeterminanteMatrizAteOrdemDois(matriz);
        } else {
            // para cada coluna da primeira linha obter a matriz reduzida
            for (int coluna = 0; coluna < numeroLinhasColunas; coluna++) {
                matrizReduzida = retirarLinhaEColunaMatrizQuadrada(matriz, 0, coluna);
                // calcular o determinante tendo em conta a posição do elemento
                determinante += matriz[0][coluna] * (Math.pow(-1, (coluna)) * obterDeterminanteMatriz(matrizReduzida));
            }
        }

        return determinante;
    }


    /**
     * Permite obter o determinante de uma matriz de ordem um ou dois.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return
     */
    public static Double obterDeterminanteMatrizAteOrdemDois(int[][] matriz) {

        // se matriz vazia, nula, não quadrada ou de ordem superior a 2 retorna nulo
        if (ExercicioQuinze.matrizNaoPreenchida(matriz) || !ExercicioTreze.verificarMatrizQuadrada(matriz) || matriz.length > 2) {
            return null;
        }

        // se matriz de ordem um, o determinante é igual ao valor contido na matriz
        if (matriz.length == 1) {
            return (double) matriz[0][0];
        }

        // calcular o produto dos elementos contidos na diagonal principal e secundária
        double produtoDiagonalPrincipal = ExercicioDez.obterProdutoDosElementos(ExercicioQuinze.obterDiagonalPrincipalMatriz(matriz));
        double produtoDiagonalSecundaria = ExercicioDez.obterProdutoDosElementos(ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz));

        // determinante dado através da subtração do produto da diagonal principal pela secundária
        return produtoDiagonalPrincipal - produtoDiagonalSecundaria;
    }


    /**
     * Permite retirar uma linha e coluna a uma matriz quadrada dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @param linhaEliminar Recebe a linha que se pretende eliminar.
     * @param colunaEliminar Recebe a coluna que se pretende eliminar.
     * @return Uma matriz quadrada sem a linha e coluna eliminadas.
     */
    public static int[][] retirarLinhaEColunaMatrizQuadrada(int[][] matriz, int linhaEliminar, int colunaEliminar) {

        // se matriz nula, vazia, de ordem um, ou linha e/ou coluna a eliminar não fazem parte da matriz retorna nulo
        if (ExercicioQuinze
                .matrizNaoPreenchida(matriz) || !ExercicioTreze.verificarMatrizQuadrada(matriz) || matriz.length == 1 || !ExercicioQuinze
                .elementoDentroMatriz(linhaEliminar, colunaEliminar, matriz) || !ExercicioTreze.verificarMatrizQuadrada(matriz)) {
            return null;
        }

        int numeroColunas = matriz[0].length;
        int numeroLinhas = matriz.length;
        // criar nova matriz com menos uma linha e sem colunas
        int[][] resultado = new int[(numeroLinhas - 1)][(0)];
        int linhaResultado = 0;

        // para cada elemento da coluna, verificar se pode ser copiado para a nova matriz (se não está na linha/coluna a eliminar
        for (int linha = 0; linha < numeroLinhas; linha++) {
            if (linha != linhaEliminar) {
                for (int coluna = 0; coluna < numeroColunas; coluna++) {
                    if (coluna != colunaEliminar) {
                        resultado[linhaResultado] = ExercicioQuinze.adicionarElementoAoVetor(resultado[linhaResultado], matriz[linha][coluna]);
                    }
                    if (coluna == (numeroColunas - 1)) {
                        linhaResultado++;
                    }
                }
            }
        }
        return resultado;
    }

}
