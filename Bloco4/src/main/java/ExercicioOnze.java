public class ExercicioOnze {

    /**
     * Permite obter o produto escalar (soma das multiplicações de elementos dispostos na mesma posição nos vetores) de dois vetores dados.
     * @param vetorA Recebe o primeiro vetor de números.
     * @param vetorB Recebe o segundo vetor de números.
     * @return O valor do produto escalar dos dois vetores dados.
     */
    public static Double obterProdutoEscalar(double[] vetorA, double[] vetorB) {

        // caso algum dos vetores seja vazio ou se o tamanho dos dois não for o mesmo retornar nulo
        if (vetorA.length == 0 || vetorB.length == 0 || vetorA.length != vetorB.length) {
            return null;
        }

        // iniciar o produto escalar a zero
        double produtoEscalar = 0;

        // realizar a multiplicação entre os elementos na mesma posição nos dois vetores, e ir realizando a soma destas multiplicações
        for (int posicao = 0; posicao < vetorA.length; posicao++) {
            produtoEscalar += (vetorA[posicao] * vetorB[posicao]);
        }

        return produtoEscalar;
    }
}
