public class ExercicioNove {
    // Construa uma solução em Java que dado um número inteiro positivo verifique se o mesmo é ou não capicua.

    /**
     * Permite verificar se um dado número inteiro é capicua (isto é se é igual ao seu inverso).
     * @param numero Recebe o velor do número inteiro a verificar.
     * @return Se é ou não capicua o número dado.
     */
    public static boolean verificarSeECapicua(int numero) {
        boolean numeroCapicua = true;

        // obter um vetor em que cada posição é um dos algarismos do número dado
        int[] vetorAlgarismos = ExercicioDois.obterVetorComDigitosDoNumero(numero);
        int posicaoVetor = 0;

        // enquanto a posição do vetor verificada for menor que o número de algarismos e enquanto não for falsa a condição a verificar
        while (posicaoVetor < vetorAlgarismos.length && numeroCapicua) {
            // se a posição avaliada for igual a que está exatamente no seu inverso, continua a verificação da posição seguinte
            if (vetorAlgarismos[posicaoVetor] == vetorAlgarismos[vetorAlgarismos.length - 1 - posicaoVetor]) {
                posicaoVetor++;
                // caso contrário o número deixa de ser capicua e para a verificação das restantes posições
            } else {
                numeroCapicua = false;
            }
        }

        return numeroCapicua;

    }
}
