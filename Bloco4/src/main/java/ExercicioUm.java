public class ExercicioUm {
    // Construa uma solução em Java que dado um número inteiro positivo retorne o número de dígitos desse número.

    public static int obterNumeroDeDigitos(int numero) {

        int numeroDeDigitos = 0;

        if (numero == 0) {
            numeroDeDigitos = 1;
        } else {
            while (numero > 0) {
                numero /= 10;
                numeroDeDigitos++;
            }
        }
        return numeroDeDigitos;
    }

}
