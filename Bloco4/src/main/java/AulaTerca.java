public class AulaTerca {

    public static int[] copiarVetor(int[] vetorOriginal, int tamanho) {

        // se tamanho inserido inferior a zero ou vetor original é nulo, retorna um vetor nulo
        if (tamanho < 0 && vetorOriginal == null) {
            return null;
        }

        // se o tamanho inserido para o vetor copiado for maior que o tamanho do vetor original, assumir o tamanho igual ao original
        if (tamanho > vetorOriginal.length) {
            tamanho = vetorOriginal.length;
        }

        // declarar o vetor copiado com o tamanho definido
        int[] vetorCopiado = new int[tamanho];

        // para cada posição até ao tamanho definido fazer a cópia para o novo vetor
        for (int index = 0; index < tamanho; index++) {
            vetorCopiado[index] = vetorOriginal[index];
        }

        return vetorCopiado;

    }






}
