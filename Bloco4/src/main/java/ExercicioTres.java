public class ExercicioTres {
    // Construa uma solução em Java que calcule a soma dos elementos de um vetor.

    public static int obterSomaDosElementosVetor(int[] vetor) {

        // inicializar a soma com zero
        int somaDosElementosVetor = 0;

        // verificar cada elemento do vetor, desde a posição zero até a última (corresponde ao tamanho do array menos um)
        for (int i = 0; i < vetor.length; i++) {
            somaDosElementosVetor += vetor[i];   // somar os elementos do array
        }
        return somaDosElementosVetor;
    }
}
