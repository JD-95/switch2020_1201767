public class ExercicioDois {
    //Dado um número inteiro positivo retorne um vetor em que os seus elementos correspondem aos dígitos do número dado.

    public static int[] obterVetorComDigitosDoNumero(int numero) {

        // atraves do metodo obter o numero de digitos do numero
        int numeroDeDigitos = ExercicioUm.obterNumeroDeDigitos(numero);
        // Declarar e criar o array que no final vai contar os digitos do numero inserido
        // o tamanho do array corresponde ao numero de digitos do número
        int[] vetorComDigitosDoNumero = new int[numeroDeDigitos];

        // para preencher o array com os valores do número
        for (int i = (numeroDeDigitos - 1); i >= 0; i--) {
            int algarismo = numero % 10;   // o resto da divisão inteira por 10 dá o último algarismo do número
            vetorComDigitosDoNumero[i] = algarismo;  // incluir o algarismo na posição inversa no array, para no final ficar igual ao numero
            numero /= 10;  // dividir o numero por 10 e assim eliminar o algarismo ja analisado
        }
        return vetorComDigitosDoNumero;
    }

    public static int[] obterVetorComDigitosDoNumeroV2(int numero) {

        // atraves do metodo obter o numero de digitos do numero
        int numeroDeDigitos = ExercicioUm.obterNumeroDeDigitos(numero);
        // Declarar e criar o array que no final vai contar os digitos do numero inserido
        // o tamanho do array corresponde ao numero de digitos do número
        int[] vetorComDigitosDoNumero = new int[numeroDeDigitos];
        // criar uma String que é igual ao número inserido
        String numeroString = String.valueOf(numero);

        // para preencher o array com os valores do número
        for (int i = 0; i < numeroDeDigitos; i++) {
            int algarismo = numeroString.charAt(i);  // obter o algarismo (em caracter) de cada posição na string criada
            vetorComDigitosDoNumero[(i)] = Character.getNumericValue(algarismo);
            // incluir o algarismo (transformando novamente em dado numérico) na posição correspondente no array
        }
        return vetorComDigitosDoNumero;
    }

}
