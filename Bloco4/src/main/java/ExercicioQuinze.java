public class ExercicioQuinze {

    //*********************************** Exercicio 15A ***********************************

    /**
     * Permite obter o menor elemento de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return O menor elemento da matriz dada.
     */
    public static Integer obterMenorElementoMatriz(int[][] matriz) {

        // se matriz dada é vazia ou nula retorna nulo
        if (matrizNaoPreenchida(matriz)) {
            return null;
        }

        // iniciado no maior inteiro possível
        int menorElemento = 2147483647;

        // verificar cada coluna de cada linha, e atualizar o menor elemento quando o elemento encontrado seja inferior
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] < menorElemento) {
                    menorElemento = matriz[linha][coluna];
                }
            }
        }
        return menorElemento;
    }

    //*********************************** Exercicio 15B ***********************************

    /**
     * Permite obter o maior elemento de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return O maior elemento da matriz dada.
     */
    public static Integer obterMaiorElementoMatriz(int[][] matriz) {

        // se matriz dada é vazia ou nula retorna nulo
        if (matrizNaoPreenchida(matriz)) {
            return null;
        }

        // iniciado no menor inteiro possível
        int maiorElemento = -2147483648;

        // verificar cada coluna de cada linha, e atualizar o maior elemento quando o elemento encontrado seja superior
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] > maiorElemento) {
                    maiorElemento = matriz[linha][coluna];
                }
            }
        }
        return maiorElemento;
    }

    //*********************************** Exercicio 15C ***********************************

    /**
     * Permite obter o valor médio dos elementos de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return O valor médio dos elementos de uma matriz dada.
     */
    public static Double obterMediaElementosMatriz(int[][] matriz) {

        // se matriz dada é vazia ou nula retorna nulo
        if (matrizNaoPreenchida(matriz)) {
            return null;
        }

        int somaElementos = 0;
        int numeroElementos = 0;

        // verificar cada elemento da matriz e adicionar o valor do mesmo à soma e ir contabilizando o número de elementos
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                somaElementos += matriz[linha][coluna];
                numeroElementos++;
            }
        }

        // através da razão soma dos elementos e número de elementos obtém-se a média dos elementos
        return somaElementos / (double) numeroElementos;
    }


    //*********************************** Exercicio 15D ***********************************

    public static Integer obterProdutoElementosMatriz(int[][] matriz) {

        // se matriz dada é vazia ou nula retorna nulo
        if (matrizNaoPreenchida(matriz)) {
            return null;
        }

        // inicializar a 1 para não influenciar multiplicações seguintes
        int produtoElementos = 1;

        // verificar cada elemento da matriz e ir multiplicando os mesmos
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                produtoElementos *= matriz[linha][coluna];
            }
        }

        return produtoElementos;
    }

    //*********************************** Exercicio 15E ***********************************

    /**
     * Permite obter os elementos não repetidos de uma matriz dada.
     * @param matriz Recebe a matriz de números inteiros.
     * @return Um vetor com os elementos não repetidos da matriz.
     */
    public static int[] obterElementosNaoRepetidosMatriz(int[][] matriz) {

        // se matriz dada é vazia ou nula retorna nulo
        if (matrizNaoPreenchida(matriz)) {
            return null;
        }

        // criar um vetor que vai conter os elementos não repetidos
        int[] elementosNaoRepetidos = new int[0];

        // para cada elemento da matriz verificar se o número já existe no vetor criado, se não copiar o mesmo
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (!verificarSeExisteValorNumVetor(elementosNaoRepetidos, matriz[linha][coluna])) {
                    elementosNaoRepetidos = adicionarElementoAoVetor(elementosNaoRepetidos, matriz[linha][coluna]);
                }
            }
        }

        return elementosNaoRepetidos;
    }

    /**
     * Permite adicionar um novo elemento a um vetor dado (inserido na última posição).
     * @param vetor Recebe um vetor de números inteiros.
     * @param numero Recebe um número inteiro.
     * @return O vetor com os elementos do vetor original mais o novo elemento.
     */
    public static int[] adicionarElementoAoVetor(int[] vetor, int numero) {

        // se vetor dado nulo retornar nulo
        if (vetor == null) {
            return null;
        }

        // o tamanho do vetor resultado será o tamanho do original mais uma posição
        int tamanhoVetorResultado = vetor.length + 1;
        int[] vetorResultado = new int[tamanhoVetorResultado];

        // se vetor preeenchido copiar os elementos do vetor dado para o vetor resultado
        if (vetorPreenchido(vetor)) {
            for (int posicao = 0; posicao < vetor.length; posicao++) {
                vetorResultado[posicao] = vetor[posicao];
            }
        }

        // inserir o número na última posição do novo vetor
        vetorResultado[(tamanhoVetorResultado - 1)] = numero;

        return vetorResultado;
    }

    /**
     * Permite verificar se um vetor dado se encontra preenchido (não é portanto um vetor vazio ou nulo).
     * @param vetor Recebe um vetor de números inteiros.
     * @return Se o vetor se encontra ou não preenchido.
     */
    public static boolean vetorPreenchido(int[] vetor) {
        return (vetor != null && vetor.length != 0);
    }

    /**
     * Permite verificar a existência de um dado número num vetor dado.
     * @param vetor Recebe um vetor de números inteiros.
     * @param numero Recebe um número inteiro.
     * @return Se o número existe ou não no vetor.
     */
    public static boolean verificarSeExisteValorNumVetor(int[] vetor, int numero) {

        boolean valorExisteNoVetor = false;

        // se vetor nulo ou vazio retorna falso
        if (!vetorPreenchido(vetor)) {
            return false;
        }

        // para casa posição do vetor verificar se existe o número dado
        for (int posVetor = 0; posVetor < vetor.length && !valorExisteNoVetor; posVetor++) {
            if (vetor[posVetor] == numero) {
                valorExisteNoVetor = true;
            }
        }

        return valorExisteNoVetor;
    }

    //*********************************** Exercicio 15F ***********************************

    /**
     * Permite obter os elementos primos de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return O vetor com os elementos primos.
     */
    public static int[] obterElementosPrimosMatriz(int[][] matriz) {

        // se matriz for nula ou vazia retorna nulo
        if (matrizNaoPreenchida(matriz)) {
            return null;
        }

        // criar um vetor que vai conter os elementos primos
        int[] vetorElementosPrimos = new int[0];

        // verificar se cada elemento da matriz é primo e se sim adicionar ao vetor criado
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (ExercicioDez.verificarSeEPrimo(matriz[linha][coluna])) {
                    vetorElementosPrimos = adicionarElementoAoVetor(vetorElementosPrimos, matriz[linha][coluna]);
                }
            }
        }

        return vetorElementosPrimos;
    }

    /**
     * Permite verificar se uma matriz dada não está preeenchida ( matriz vazia ou matriz nula).
     * @param matriz Recebe uma matriz de números inteiros.
     * @return Se a matriz está ou não preenchida.
     */
    public static boolean matrizNaoPreenchida(int[][] matriz) {
        return matriz == null || matriz.length == 0;
    }


    //*********************************** Exercicio 15G ***********************************

    /**
     * Permite obter a diagonal principal (inicia no canto superior esquerdo da matriz) de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return Um vetor com os elementos que se encontram na diagonal principal da matriz.
     */
    public static int[] obterDiagonalPrincipalMatriz(int[][] matriz) {

        // se a matriz não tiver o mesmo número de colunas em todas as linhas retorna nulo
        if (!ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz)) {
            return null;
        }

        // criar um vetor que vai conter os elementos da diagonal principal da matriz
        int[] vetorDiagonalPrincipal = new int[0];

        int linhaElemento = 0;
        int colunaElemento = 0;

        // enquanto o elemento se encontrar dentro da matriz continuar a seguir a diagonal e armazenar os elementos no vetor criado
        while (elementoDentroMatriz(linhaElemento, colunaElemento, matriz)) {
            vetorDiagonalPrincipal = adicionarElementoAoVetor(vetorDiagonalPrincipal, matriz[linhaElemento][colunaElemento]);
            linhaElemento++;
            colunaElemento++;
        }


        return vetorDiagonalPrincipal;
    }

    /**
     * Permite verificar se um dado elemento se encontra dentro da matriz dada.
     * @param linhaElemento Recebe a linha onde se encontra o elemento na matriz.
     * @param colunaElemento Recebe a coluna onde se encontra o elemento na matriz.
     * @param matriz Recebe a matriz de números inteiros.
     * @return Se o elemento se encontra ou não dentro da matriz.
     */
    public static boolean elementoDentroMatriz(int linhaElemento, int colunaElemento, int[][] matriz) {
        return linhaElemento < matriz.length && linhaElemento >= 0 && colunaElemento < matriz[linhaElemento].length && colunaElemento >= 0;
    }


    //*********************************** Exercicio 15H ***********************************

    /**
     * Permite obter a diagonal secundária (inicia no canto superior direito da matriz) de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return Um vetor com os elementos que se encontram na diagonal secundária da matriz.
     */
    public static int[] obterDiagonalSecundariaMatriz(int[][] matriz) {

        // se a matriz não tiver o mesmo número de colunas em todas as linhas retorna nulo
        if (!ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz)) {
            return null;
        }

        // criar um vetor que vai conter os elementos da diagonal principal da matriz
        int[] vetorDiagonalSecundaria = new int[0];

        int linhaElemento = 0;
        int numeroColunasMatriz = (matriz[0].length - 1);
        // iniciar no canto superior direito
        int colunaElemento = numeroColunasMatriz;

        // enquanto o elemento se encontrar dentro da matriz continuar a seguir a diagonal e armazenar os elementos no vetor criado
        while (elementoDentroMatriz(linhaElemento, colunaElemento, matriz)) {
            vetorDiagonalSecundaria = adicionarElementoAoVetor(vetorDiagonalSecundaria, matriz[linhaElemento][colunaElemento]);
            linhaElemento++;
            colunaElemento--;
        }


        return vetorDiagonalSecundaria;
    }

    //*********************************** Exercicio 15I ***********************************

    /**
     * Permite verificar se uma matriz é identidade (elementos da diagonal principal são todos iguais a 1 e os restantes iguais a zero).
     * @param matriz Recebe uma matriz de números inteiros.
     * @return Se a matriz dada é uma matriz identidade ou não.
     */
    public static boolean matrizIdentidade(int[][] matriz) {
        // se matriz vazia ou nula retornar nulo
        if (matrizNaoPreenchida(matriz)) {
            return false;
        }

        // iniciar a posição da diagonal na coluna zero
        int posDiagonalNaLinha = 0;
        boolean matrizIdentidade = true;
        int numeroColunas = matriz[0].length;

        // verificar em cada linha se na diagonal existe o um, e nos restantes elementos o zero
        for (int linha = 0; linha < matriz.length && matrizIdentidade; linha++) {
            if (posDiagonalNaLinha < numeroColunas) {
                if (!linhaComApenasUmElementoDiferenteDeZero(matriz[linha], posDiagonalNaLinha, 1)) {
                    matrizIdentidade = false;
                }
                posDiagonalNaLinha++;
            } else {
                if (!linhaPreenchidaComZeros(matriz[linha])) {
                    matrizIdentidade = false;
                }
            }
        }

        return matrizIdentidade;
    }

    /**
     * Permite verificar se uma determinada linha da matriz está preenchida com zeros e um só elemento diferente de zero.
     * @param linha Recebe a linha de números inteiros da matriz a verificar.
     * @param posicaoDoElemento Recebe a posição do elementos que é diferente de zero.
     * @param valorElemento Recebe o valor do elemento que é diferente de zero.
     * @return Se a linha dada está preenchida com zeros e um elemento diferente de zero.
     */
    public static boolean linhaComApenasUmElementoDiferenteDeZero(int[] linha, int posicaoDoElemento, int valorElemento) {
        boolean resultado = true;

        // se a linha for vazia ou nula retornar falso
        if (linha == null || linha.length == 0) {
            return false;
        }

        // se for a posição do elemento diferente de zero verificar se o seu valor é o dado, e se os restantes elementos são zeros
        for (int pos = 0; pos < linha.length; pos++) {
            if (pos == posicaoDoElemento) {
                if (linha[pos] != valorElemento) {
                    resultado = false;
                }
            } else {
                if (linha[pos] != 0) {
                    resultado = false;
                }
            }
        }
        return resultado;
    }

    /**
     * Permite verificar se uma dada linha está preenchida apenas com zeros.
     * @param linha Recebe a linha de números inteiros.
     * @return Se a linha dada está preenchida só com zeros.
     */
    public static boolean linhaPreenchidaComZeros(int[] linha) {
        boolean resultado = true;

        // se linha nula ou vazia retorna zero
        if (linha == null || linha.length == 0) {
            return false;
        }

        // verificar se existe algum elemento diferente de zero
        for (int elemento : linha) {
            if (elemento != 0) {
                resultado = false;
            }
        }
        return resultado;
    }

    //*********************************** Exercicio 15K ***********************************

    /**
     * Permite obter a matriz transposta (matriz que se obtém da troca de linhas por colunas) de uma matriz dada.
     * @param matriz Recebe uma matriz de números inteiros.
     * @return A matriz transposta da matriz dada.
     */
    public static double[][] obterMatrizTransposta(double[][] matriz) {

        // se matriz nula ou vazia retorna nulo
        if (matriz == null || matriz.length == 0) {
            return null;
        }

        // criar matriz transposta com tantas linhas como colunas da original e tantas colunas como linhas da original
        double[][] matrizTransposta = new double[matriz[0].length][matriz.length];

        // para cada elemento da matriz original copiar o mesmo para a transposta, trocando a linha pela coluna
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                matrizTransposta[coluna][linha] = matriz[linha][coluna];
            }
        }
        return matrizTransposta;
    }


    //*********************************** Exercicio 15J ***********************************

    /**
     * Permite obter a matriz dos cofatores associados a cada elemento de uma matriz quadrada dada.
     * @param matriz Recebe uma matriz quadrada de números inteiros.
     * @return A matriz com os cofatores.
     */
    public static double[][] obterMatrizCofatores(int[][] matriz) {

        // se matriz vazia, nula, não quadrada ou quadrada de ordem um retorna nulo
        if (matrizNaoPreenchida(matriz) || !ExercicioTreze.verificarMatrizQuadrada(matriz) || matriz.length == 1 || matriz[0].length == 1) {
            return null;
        }

        // criar uma matriz para armazenar os cofatores
        double[][] matrizCofatores = new double[matriz.length][matriz.length];
        int[][] matrizReduzida;
        double determinante;

        // para cada elemento, encontrar a sua matriz reduzida (sem a sua linha e coluna) e calcular o determinante e cofator associado
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz.length; coluna++) {
                matrizReduzida = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linha, coluna);
                determinante = ExercicioDezasseis.obterDeterminanteMatriz(matrizReduzida);
                matrizCofatores[linha][coluna] = obterCofatorElemento(linha, coluna, determinante);
            }
        }
        return matrizCofatores;
    }

    /**
     * Permite obter o cofator de um dado elemento de uma matriz.
     * @param linhaElemento Recebe a linha onde se encontra o elemento na matriz.
     * @param colunaElemento Recebe a coluna onde se encontra o elemento na matriz.
     * @param determinanteMatrizReduzida Recebe o determinante da matriz reduzida (matriz sem a coluna e a linha onde se encontra o elemento).
     * @return O valor do cofator do elemento.
     */
    public static double obterCofatorElemento(int linhaElemento, int colunaElemento, double determinanteMatrizReduzida) {
        return Math.pow(-1, (linhaElemento + colunaElemento + 2)) * determinanteMatrizReduzida;
    }


    /**
     * Permite obter a matriz inversa de uma matriz quadrada dada.
     * @param matrizOriginal Recebe uma matriz quadrada de números inteiros.
     * @return A matriz inversa.
     */
    public static float[][] obterMatrizInversa(int[][] matrizOriginal) {

        // se matriz nula, vazia ou não quadrada retorna nulo
        if (matrizNaoPreenchida(matrizOriginal) || !ExercicioTreze.verificarMatrizQuadrada(matrizOriginal)) {
            return null;
        }

        double determinante = ExercicioDezasseis.obterDeterminanteMatriz(matrizOriginal);
        // se determinante igual a zero a matriz não possuí inversa
        if (determinante == 0) {
            return null;
        }

        float[][] matrizInversa = new float[matrizOriginal.length][matrizOriginal[0].length];
        // se matriz de ordem um, inversa é 1/elemento
        if (matrizOriginal.length == 1) {
            matrizInversa[0][0] = 1 / (float) matrizOriginal[0][0];
        } else {
            // criar a matriz dos cofatores, matriz adjunta, matriz inversa e calcular o inverso do determinante
            double[][] matrizAdjunta = obterMatrizTransposta(obterMatrizCofatores(matrizOriginal));
            float inversoDeterminante = (float) (1 / determinante);

            // para cada elemento determinar qual o seu valor na matriz inversa
            for (int linha = 0; linha < matrizOriginal.length; linha++) {
                for (int coluna = 0; coluna < matrizOriginal[linha].length; coluna++) {
                    matrizInversa[linha][coluna] = (float) (inversoDeterminante * matrizAdjunta[linha][coluna]);
                }
            }
        }
        return matrizInversa;
    }

}


