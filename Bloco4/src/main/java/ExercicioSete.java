public class ExercicioSete {

    /********************************** MULTIPLOS DE 3 **********************************************/
    // Construa uma solução em Java que determine os múltiplos de 3 num dado intervalo.
    public static int[] obterMultiplosDeTresNumIntervalo(int limiteInferior, int limiteSuperior) {

        // obter o vetor com os números incluidos no intervalo
        int[] vetorIntervalo = obterVetorDefinidoUmIntervalo(limiteInferior, limiteSuperior);

        // obter o vetor com os multiplos de 3
        int[] vetorComMultiplosDeTres = obterVetorComMultiplosDeTres(vetorIntervalo);

        return vetorComMultiplosDeTres;
    }

    public static int[] obterVetorDefinidoUmIntervalo(int limiteInferior, int limiteSuperior) {

        int tamanhoDoIntervalo = 0;
        // obter o tamanho do intervalo, tendo em conta se é crescente ou decrescente
        if (limiteInferior < limiteSuperior) {  // crescente
            tamanhoDoIntervalo = (limiteSuperior - limiteInferior) + 1;
        } else { // decrescente
            tamanhoDoIntervalo = (limiteInferior - limiteSuperior) + 1;
        }

        // declarar o vetor que vai conter os números no intervalo dado
        int[] vetorIntervalo = new int[tamanhoDoIntervalo];

        // se intervalo crescente ir percorrendo os numeros no intervalo somando sempre uma unidade a partir do limite inferior
        if (limiteInferior <= limiteSuperior) {
            // para cada posição no vetor criado inserir o número correspondente no intervalo, começanco no limite inferior
            for (int index = 0, numeroNoIntervalo = limiteInferior; index < tamanhoDoIntervalo; index++) {
                vetorIntervalo[index] = numeroNoIntervalo;
                numeroNoIntervalo++; // ir percorrendo os números no intervalo à medida que percorremos as posições no vetor
            }
            // se intervalo decrescente ir percorrendo os numeros no intervalo subtraindo sempre uma unidade a partir do limite inferior
        } else {
            // para cada posição no vetor criado inserir o número correspondente no intervalo, começanco no limite inferior
            for (int index = 0, numeroNoIntervalo = limiteInferior; index < tamanhoDoIntervalo; index++) {
                vetorIntervalo[index] = numeroNoIntervalo;
                numeroNoIntervalo--; // ir percorrendo os números no intervalo à medida que percorremos as posições no vetor
            }
        }
        return vetorIntervalo;
    }

    public static int[] obterVetorComMultiplosDeTres(int[] vetorOriginal) {

        // definir um vetor intermedio que terá como tamanho o do vetor original
        int[] vetorIntermedio = new int[vetorOriginal.length];
        // iniciar a contagem dos multiplos a zero
        int numeroDeMultiplosDeTres = 0;

        // percorrer cada posição no vetor original
        for (int index = 0; index < vetorOriginal.length; index++) {
            if ((vetorOriginal[index] % 3) == 0) {   // verificar se o número nessa posição é múltiplo de 3 ( se divisão por 3 dá resto zero)
                vetorIntermedio[numeroDeMultiplosDeTres] = vetorOriginal[index];  // se sim inserir esse número no vetor intermedio criado
                numeroDeMultiplosDeTres++;  // ir contabilizando o numero de multiplos de 3
            }
        }

        // criar um vetor que terá só os multiplos de 3
        // atraves do metodo criado truncamos o novo vetor para o tamanho ser igual ao numero de multiplos
        int[] vetorComMultiplosDeTres = ExercicioSeis.obterCopiaDeVetor(vetorIntermedio, numeroDeMultiplosDeTres);

        return vetorComMultiplosDeTres;
    }

    /********************************** MULTIPLOS DE X **********************************************/
    // Generalize a solução anterior de forma a que determine os múltiplos de qualquer número dado
    public static int[] obterMultiplosDeNumeroDadoNumIntervalo(int limiteInferior, int limiteSuperior, int numeroAVerificar) {

        // obter o vetor com os números incluidos no intervalo
        int[] vetorIntervalo = obterVetorDefinidoUmIntervalo(limiteInferior, limiteSuperior);

        // obter o vetor com os multiplos de dado numero
        int[] vetorComMultiplos = obterVetorComMultiplosDeDadoNumero(vetorIntervalo, numeroAVerificar);

        return vetorComMultiplos;
    }


    public static int[] obterVetorComMultiplosDeDadoNumero(int[] vetorOriginal, int divisor) {

        // definir um vetor intermedio que terá como tamanho o do vetor original
        int[] vetorIntermedio = new int[vetorOriginal.length];
        // iniciar a contagem dos multiplos a zero
        int numeroDeMultiplos = 0;


        // não dá para dividir por zero, logo só verificamos os outros números
        if (divisor != 0) {
            // percorrer cada posição no vetor original
            for (int index = 0; index < vetorOriginal.length; index++) {
                if ((vetorOriginal[index] % divisor) == 0) {
                    // verificar se o número nessa posição é múltiplo de divisor dado ( se a divisão inteira dá resto zero)
                    vetorIntermedio[numeroDeMultiplos] = vetorOriginal[index];  // se sim inserir esse número no vetor intermedio criado
                    numeroDeMultiplos++;  // ir contabilizando o numero de multiplos
                }
            }
        }
        // se o divisor for zero retorna um vetor vazio
        else {
            vetorIntermedio = new int[]{};
        }

        // criar um vetor que terá só os multiplos
        // atraves do metodo criado truncamos o novo vetor para o tamanho ser igual ao numero de multiplos
        int[] vetorComMultiplos = ExercicioSeis.obterCopiaDeVetor(vetorIntermedio, numeroDeMultiplos);

        return vetorComMultiplos;
    }

}
