public class ExercicioDezoito {

    public static void main(String[] args) {
        //imprimirMatrizSopaDeLetras();
        /*obterMatrizMascaraLetra(new char[][]{
                {'p', 'r', 'p', 'a', 'r', 'j', 'u', 'n'},
                {'o', 'l', 'h', 'v', 'r', 'l', 'h', 'r'},
                {'a', 'j', 'v', 'b', 'm', 'a', 'b', 'm'},
                {'o', 'h', 'l', 'e', 'm', 'r', 'e', 'v'},
                {'t', 'r', 'e', 'r', 'a', 'a', 'd', 'a'},
                {'e', 'o', 'e', 'n', 'z', 'n', 'r', 'v'},
                {'r', 'v', 'c', 'z', 'u', 'j', 'e', 'a'},
                {'p', 'o', 'a', 'b', 'l', 'a', 'v', 'e'}
        }, 'l'); */
        //obterVetorComElementosString("Dado");
        /*obterCombinacoesPossiveis(new char[][]{
                {'p', 'r', 'p', 'a', 'r', 'j', 'u', 'n'},
                {'o', 'l', 'h', 'v', 'r', 'l', 'h', 'r'},
                {'a', 'j', 'v', 'b', 'm', 'a', 'b', 'm'},
                {'o', 'h', 'l', 'e', 'm', 'r', 'e', 'v'},
                {'t', 'r', 'e', 'r', 'a', 'a', 'd', 'a'},
                {'e', 'o', 'e', 'n', 'z', 'n', 'r', 'v'},
                {'r', 'v', 'c', 'z', 'u', 'j', 'e', 'a'},
                {'p', 'o', 'a', 'b', 'l', 'a', 'v', 'e'}
        }, 5, 3, 4); */
        /*palavraExisteNaSopaLetras(new char[][]{
                {'p', 'r', 'p', 'a', 'r', 'j', 'u', 'n'},
                {'o', 'l', 'h', 'v', 'r', 'l', 'h', 'r'},
                {'a', 'j', 'v', 'b', 'm', 'a', 'b', 'm'},
                {'o', 'h', 'l', 'e', 'm', 'r', 'e', 'v'},
                {'t', 'r', 'e', 'r', 'a', 'a', 'd', 'a'},
                {'e', 'o', 'e', 'n', 'z', 'n', 'r', 'v'},
                {'r', 'v', 'c', 'z', 'u', 'j', 'e', 'a'},
                {'p', 'o', 'a', 'b', 'l', 'a', 'v', 'e'}
        }, "laranja"); */
        palavraContidaNaSequencia(new char[][]{
                {'p', 'r', 'p', 'a', 'r', 'j', 'u', 'n'},
                {'o', 'l', 'h', 'v', 'r', 'l', 'h', 'r'},
                {'a', 'j', 'v', 'b', 'm', 'a', 'b', 'm'},
                {'o', 'h', 'l', 'e', 'm', 'r', 'e', 'v'},
                {'t', 'r', 'e', 'r', 'a', 'a', 'd', 'a'},
                {'e', 'o', 'e', 'n', 'z', 'n', 'r', 'v'},
                {'r', 'v', 'c', 'z', 'u', 'j', 'e', 'a'},
                {'p', 'o', 'a', 'b', 'l', 'a', 'v', 'e'}
        }, 1, 5, 7, 5, "laranja");
    }


    public static void imprimirMatrizSopaDeLetras() {

        char[][] sopaDeLetras = new char[][]{
                {'p', 'r', 'p', 'a', 'r', 'j', 'u', 'n'},
                {'o', 'l', 'h', 'v', 'r', 'l', 'h', 'r'},
                {'a', 'j', 'v', 'b', 'm', 'a', 'b', 'm'},
                {'o', 'h', 'l', 'e', 'm', 'r', 'e', 'v'},
                {'t', 'r', 'e', 'r', 'a', 'a', 'd', 'a'},
                {'e', 'o', 'e', 'n', 'z', 'n', 'r', 'v'},
                {'r', 'v', 'c', 'z', 'u', 'j', 'e', 'a'},
                {'p', 'o', 'a', 'b', 'l', 'a', 'v', 'e'}
        };

        for (int linha = 0; linha < sopaDeLetras.length; linha++) {
            for (int coluna = 0; coluna < sopaDeLetras[linha].length; coluna++) {
                System.out.print(sopaDeLetras[linha][coluna] + " ");
            }
            System.out.println(" ");
        }
    }

    //*********************************** Exercicio 18A ***********************************

    public static int[][] obterMatrizMascaraLetra(char[][] matriz, char letra) {

        int[][] matrizMascara = new int[matriz.length][matriz[0].length];

        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[0].length; coluna++) {
                if (matriz[linha][coluna] == letra) {
                    matrizMascara[linha][coluna] = 1;
                }
            }
        }
        return matrizMascara;
    }

    //*********************************** Exercicio 18B ***********************************

    public static boolean palavraExisteNaSopaLetras(char[][] sopaDeLetras, String palavra) {

        char[] letrasPalavra = obterVetorComElementosString(palavra);
        boolean palavraEncontrada = false;

        for (int linha = 0; linha < sopaDeLetras.length && !palavraEncontrada; linha++) {
            for (int coluna = 0; coluna < sopaDeLetras[0].length && !palavraEncontrada; coluna++) {
                if (sopaDeLetras[linha][coluna] == letrasPalavra[0]) {
                    char[][] sequenciasEncontradas = obterSequenciasEncontradas(sopaDeLetras, linha, coluna, palavra.length());
                    for (int pos = 0; pos < sequenciasEncontradas.length && !palavraEncontrada; pos++) {
                        if (sequenciasLetrasIguais(letrasPalavra, sequenciasEncontradas[pos])) {
                            palavraEncontrada = true;
                        }
                    }
                }
            }
        }

        return palavraEncontrada;
    }


    public static char[] obterVetorComElementosString(String string) {

        char[] vetorResultado = new char[string.length()];

        for (int pos = 0; pos < vetorResultado.length; pos++) {
            vetorResultado[pos] = string.charAt(pos);
        }

        return vetorResultado;
    }

    public static char[][] obterSequenciasEncontradas(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[][] sequenciasEncontradas = new char[8][0];

        sequenciasEncontradas[0] = obterSequenciaDireita(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[1] = obterSequenciaEsquerda(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[2] = obterSequenciaCima(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[3] = obterSequenciaBaixo(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[4] = obterDiagonalPrincipalAscendente(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[5] = obterDiagonalPrincipalDescendente(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[6] = obterDiagonalSecundariaAscendente(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);
        sequenciasEncontradas[7] = obterDiagonalSecundariaDescendente(sopaDeLetras, linhaElemento, colunaElemento, tamanhoPalavra);


        return sequenciasEncontradas;
    }

    public static char[] obterSequenciaDireita(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciasDireita = new char[0];
        boolean sequenciaMaxima = false;

        if (colunaElemento + (tamanhoPalavra - 1) >= sopaDeLetras[linhaElemento].length) {
            return sequenciasDireita;
        } else {
            for (int coluna = colunaElemento; !sequenciaMaxima; coluna++) {
                sequenciasDireita = adicionarLetraAoVetor(sequenciasDireita, sopaDeLetras[linhaElemento][coluna]);
                if (sequenciasDireita.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciasDireita;
    }

    public static char[] obterSequenciaEsquerda(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciasEsquerda = new char[0];
        boolean sequenciaMaxima = false;

        if (colunaElemento - (tamanhoPalavra - 1) < 0) {
            return sequenciasEsquerda;
        } else {
            for (int coluna = colunaElemento; !sequenciaMaxima; coluna--) {
                sequenciasEsquerda = adicionarLetraAoVetor(sequenciasEsquerda, sopaDeLetras[linhaElemento][coluna]);
                if (sequenciasEsquerda.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciasEsquerda;
    }

    public static char[] obterSequenciaCima(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciaCima = new char[0];
        boolean sequenciaMaxima = false;

        if (linhaElemento - (tamanhoPalavra - 1) < 0) {
            return sequenciaCima;
        } else {
            for (int linha = linhaElemento; !sequenciaMaxima; linha--) {
                sequenciaCima = adicionarLetraAoVetor(sequenciaCima, sopaDeLetras[linha][colunaElemento]);
                if (sequenciaCima.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciaCima;
    }

    public static char[] obterSequenciaBaixo(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciaBaixo = new char[0];
        boolean sequenciaMaxima = false;

        if (linhaElemento + (tamanhoPalavra - 1) >= sopaDeLetras.length) {
            return sequenciaBaixo;
        } else {
            for (int linha = linhaElemento; !sequenciaMaxima; linha++) {
                sequenciaBaixo = adicionarLetraAoVetor(sequenciaBaixo, sopaDeLetras[linha][colunaElemento]);
                if (sequenciaBaixo.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciaBaixo;
    }

    public static char[] obterDiagonalPrincipalAscendente(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciaDiagPrincAscendente = new char[0];
        boolean sequenciaMaxima = false;

        if (linhaElemento - (tamanhoPalavra - 1) < 0 || colunaElemento - (tamanhoPalavra - 1) < 0) {
            return sequenciaDiagPrincAscendente;
        } else {
            for (int linha = linhaElemento, coluna = colunaElemento; !sequenciaMaxima; linha--, coluna--) {
                sequenciaDiagPrincAscendente = adicionarLetraAoVetor(sequenciaDiagPrincAscendente, sopaDeLetras[linha][coluna]);
                if (sequenciaDiagPrincAscendente.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciaDiagPrincAscendente;
    }

    public static char[] obterDiagonalPrincipalDescendente(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciaDiagPrincDescendente = new char[0];
        boolean sequenciaMaxima = false;

        if (linhaElemento + (tamanhoPalavra - 1) >= sopaDeLetras.length || colunaElemento + (tamanhoPalavra - 1) >= sopaDeLetras[0].length) {
            return sequenciaDiagPrincDescendente;
        } else {
            for (int linha = linhaElemento, coluna = colunaElemento; !sequenciaMaxima; linha++, coluna++) {
                sequenciaDiagPrincDescendente = adicionarLetraAoVetor(sequenciaDiagPrincDescendente, sopaDeLetras[linha][coluna]);
                if (sequenciaDiagPrincDescendente.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciaDiagPrincDescendente;
    }

    public static char[] obterDiagonalSecundariaAscendente(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciaDiagSecAscendente = new char[0];
        boolean sequenciaMaxima = false;

        if (linhaElemento - (tamanhoPalavra - 1) < 0 || colunaElemento + (tamanhoPalavra - 1) >= sopaDeLetras[0].length) {
            return sequenciaDiagSecAscendente;
        } else {
            for (int linha = linhaElemento, coluna = colunaElemento; !sequenciaMaxima; linha--, coluna++) {
                sequenciaDiagSecAscendente = adicionarLetraAoVetor(sequenciaDiagSecAscendente, sopaDeLetras[linha][coluna]);
                if (sequenciaDiagSecAscendente.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciaDiagSecAscendente;
    }

    public static char[] obterDiagonalSecundariaDescendente(char[][] sopaDeLetras, int linhaElemento, int colunaElemento, int tamanhoPalavra) {

        char[] sequenciaDiagSecDescendente = new char[0];
        boolean sequenciaMaxima = false;

        if (linhaElemento + (tamanhoPalavra - 1) >= sopaDeLetras.length || colunaElemento - (tamanhoPalavra - 1) < 0) {
            return sequenciaDiagSecDescendente;
        } else {
            for (int linha = linhaElemento, coluna = colunaElemento; !sequenciaMaxima; linha++, coluna--) {
                sequenciaDiagSecDescendente = adicionarLetraAoVetor(sequenciaDiagSecDescendente, sopaDeLetras[linha][coluna]);
                if (sequenciaDiagSecDescendente.length == tamanhoPalavra) {
                    sequenciaMaxima = true;
                }
            }
        }
        return sequenciaDiagSecDescendente;
    }


    public static char[] adicionarLetraAoVetor(char[] vetor, char letra) {

        // se vetor dado nulo retornar nulo
        if (vetor == null) {
            return null;
        }

        // o tamanho do vetor resultado será o tamanho do original mais uma posição
        int tamanhoVetorResultado = vetor.length + 1;
        char[] vetorResultado = new char[tamanhoVetorResultado];

        // se vetor preeenchido copiar os elementos do vetor dado para o vetor resultado
        if (vetor.length != 0) {
            for (int posicao = 0; posicao < vetor.length; posicao++) {
                vetorResultado[posicao] = vetor[posicao];
            }
        }

        // inserir o número na última posição do novo vetor
        vetorResultado[(tamanhoVetorResultado - 1)] = letra;

        return vetorResultado;
    }

    public static boolean sequenciasLetrasIguais(char[] sequenciaA, char[] sequenciaB) {

        boolean resultado = true;

        if (sequenciaA.length != sequenciaB.length) {
            resultado = false;
        }

        for (int pos = 0; pos < sequenciaA.length && resultado; pos++) {
            if (sequenciaA[pos] != sequenciaB[pos]) {
                resultado = false;
            }
        }

        return resultado;
    }

    //*********************************** Exercicio 18C ***********************************

    public static char[] obterSequenciaNumaDadaDirecao(char[][] sopaDeLetras, int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal) {

        char[] sequenciaEncontrada = new char[0];
        int tamanhoSequencia;

        if (linhaInicial == linhaFinal && colunaFinal > colunaInicial) {
            tamanhoSequencia = colunaFinal - colunaInicial + 1;
            sequenciaEncontrada = obterSequenciaDireita(sopaDeLetras, linhaInicial, colunaInicial, tamanhoSequencia);
        } else if (linhaInicial == linhaFinal && colunaFinal < colunaInicial) {
            tamanhoSequencia = colunaInicial - colunaFinal + 1;
            sequenciaEncontrada = obterSequenciaEsquerda(sopaDeLetras, linhaInicial, colunaInicial, tamanhoSequencia);
        } else if (linhaFinal < linhaInicial && colunaFinal == colunaInicial) {
            tamanhoSequencia = linhaInicial - linhaFinal + 1;
            sequenciaEncontrada = obterSequenciaCima(sopaDeLetras, linhaInicial, colunaInicial, tamanhoSequencia);
        } else if (linhaFinal > linhaInicial && colunaFinal == colunaInicial) {
            tamanhoSequencia = linhaFinal - linhaInicial + 1;
            sequenciaEncontrada = obterSequenciaBaixo(sopaDeLetras, linhaInicial, colunaInicial, tamanhoSequencia);
        } else if (linhaFinal < linhaInicial && colunaFinal < colunaInicial) {
            tamanhoSequencia = linhaInicial - linhaFinal + 1;
            sequenciaEncontrada = obterDiagonalPrincipalAscendente(sopaDeLetras, colunaInicial, linhaFinal, tamanhoSequencia);
        } else if (linhaFinal > linhaInicial && colunaFinal > colunaInicial) {
            tamanhoSequencia = linhaFinal - linhaInicial + 1;
            sequenciaEncontrada = obterDiagonalPrincipalDescendente(sopaDeLetras, colunaInicial, linhaFinal, tamanhoSequencia);
        } else if (linhaFinal < linhaInicial && colunaFinal > colunaInicial) {
            tamanhoSequencia = linhaInicial - linhaFinal + 1;
            sequenciaEncontrada = obterDiagonalSecundariaAscendente(sopaDeLetras, colunaInicial, linhaFinal, tamanhoSequencia);
        } else if (linhaFinal > linhaInicial && colunaFinal < colunaInicial) {
            tamanhoSequencia = linhaFinal - linhaInicial + 1;
            sequenciaEncontrada = obterDiagonalSecundariaDescendente(sopaDeLetras, colunaInicial, linhaFinal, tamanhoSequencia);
        }

        return sequenciaEncontrada;
    }

    public static boolean palavraContidaNaSequencia(char[][] sopaDeLetras, int linhaInicial, int colunaInicial, int linhaFinal, int colunaFinal,
            String palavra) {

        char[] sequenciaEncontrada = obterSequenciaNumaDadaDirecao(sopaDeLetras, linhaInicial, colunaInicial, linhaFinal, colunaFinal);
        char[] letrasPalavra = obterVetorComElementosString(palavra);

        return sequenciasLetrasIguais(sequenciaEncontrada, letrasPalavra);
    }
}
