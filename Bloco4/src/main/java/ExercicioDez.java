public class ExercicioDez {

    //*********************************** Exercicio 10A ***********************************

    /**
     * Permite obter o menor elemento de um vetor dado.
     * @param vetor Recebe o vetor de números inteiros para verificar os seus elementos.
     * @return O menor elemento do vetor.
     */
    public static Integer obterElementoDeMenorValor(int[] vetor) {

        // se for dado um vetor vazio retornar nulo
        if (vetor.length == 0) {
            return null;
        }
        // inicialmente o menor elemento será o que está na posição zero do vetor
        int menorElemento = vetor[0];

        // verificar cada posição do vetor a partir da posição 1
        for (int index = 1; index < vetor.length; index++) {
            if (vetor[index] < menorElemento) {  // se o elemento verificado for menor que o anteriormente menor
                menorElemento = vetor[index];  // esse elemento assume o seu lugar
            }
        }

        return menorElemento;
    }

    //*********************************** Exercicio 10B ***********************************

    /**
     * Permite obter o maior elemento de um vetor dado.
     * @param vetor Recebe o vetor de números inteiros para verificar os seus elementos.
     * @return O maior elemento do vetor.
     */
    public static Integer obterElementoDeMaiorValor(int[] vetor) {

        // se for dado um vetor vazio retornar nulo
        if (vetor.length == 0) {
            return null;
        }

        // inicialmente o maior elemento será o que está na posição zero do vetor
        int maiorElemento = vetor[0];

        // verificar cada posição do vetor dado
        for (int index = 0; index < vetor.length; index++) {
            if (vetor[index] > maiorElemento) { // se o elemento verificado for maior que o anteriormente maior
                maiorElemento = vetor[index]; // esse elemento assume o seu lugar
            }
        }

        return maiorElemento;
    }

    //*********************************** Exercicio 10C ***********************************

    /**
     * Permite obter o valor médio dos elementos de um vetor dado.
     * @param vetor Recebe o vetor de números inteiros para verificar os seus elementos.
     * @return O valor médio dos elementos do vetor.
     */
    public static Double obterMediaDosElementos(int[] vetor) {

        // se for dado um vetor vazio retornar nulo
        if (vetor.length == 0) {
            return null;
        }

        // soma dos elementos inicializada a zero
        int somaDosElementos = 0;

        // verificar cada elemento do vetor e somar todos
        for (int index = 0; index < vetor.length; index++) {
            somaDosElementos += vetor[index];
        }

        // através da razão da soma com o número de elementos (= ao tamanho do vetor) obter a média
        double mediaElementos = somaDosElementos / (double) vetor.length;

        return mediaElementos;
    }

    //*********************************** Exercicio 10D ***********************************

    /**
     * Permite obter o produto dos elementos de um vetor dado.
     * @param vetor Recebe o vetor de números inteiros para verificar os seus elementos.
     * @return O produto dos elementos do vetor.
     */
    public static Integer obterProdutoDosElementos(int[] vetor) {

        // se for dado um vetor vazio retornar nulo
        if (vetor.length == 0) {
            return null;
        }

        // produto dos elementos inicializada a um, para não interferir com as multiplicações seguintes
        int produtoElementos = 1;

        // verificar cada elemento do vetor e multiplicar todos
        for (int index = 0; index < vetor.length; index++) {
            produtoElementos *= vetor[index];
        }

        return produtoElementos;
    }

    //*********************************** Exercicio 10E ***********************************

    /**
     * Permite eliminar a repetição de elementos de um vetor.
     * @param vetorOriginal Recebe um vetor de números inteiros.
     * @return Um vetor de números inteiros sem repetição de qualquer elemento.
     */
    public static int[] obterVetorComElementosNaoRepetidos(int[] vetorOriginal) {

        // se for dado um vetor vazio retornar nulo
        if (vetorOriginal.length == 0) {
            return null;
        }

        // criado o vetor intermédio para armazenar os elementos não repetidos
        int[] vetorIntermedio = new int[vetorOriginal.length];

        // copiar o elementos na posição 0 do vetor original para o intermédio
        vetorIntermedio[0] = vetorOriginal[0];
        // k = posição no vetor intermédio - começar o preenchimento do intermédio na posição 1
        int k = 1;

        // i = posição no vetor original
        for (int i = 0; i < vetorOriginal.length; i++) {
            // j = posição no vetor original - comparar cada elemento do vetor original com todos os elementos do próprio vetor
            for (int j = 0; j < vetorIntermedio.length; j++) {
                // se os elementos forem diferentes
                if (vetorOriginal[i] != vetorIntermedio[j]) {
                    // se o elemento verificado for diferente de todos os restantes guardar o mesmo no vetor intermédio
                    if (j == (vetorIntermedio.length - 1)) {
                        vetorIntermedio[k] = vetorOriginal[i];
                        k++;
                    }
                } else {
                    j = vetorIntermedio.length;
                }
            }
        }

        int[] vetorComElementosNaoRepetidos = ExercicioSeis.obterCopiaDeVetor(vetorIntermedio, k);
        return vetorComElementosNaoRepetidos;
    }

    //*********************************** Exercicio 10F ***********************************

    /**
     * Permite obter um vetor com os elementos invertidos de um vetor dado.
     * @param vetorOriginal Recebe um vetor de números inteiros.
     * @return Um vetor de números inteiros com os elementos pela ordem inversa do vetor dado.
     */
    public static int[] obterVetorInvertido(int[] vetorOriginal) {

        // se for dado um vetor vazio retornar nulo
        if (vetorOriginal.length == 0) {
            return null;
        }

        // criar o vetor invertido que vai ter exatamente o mesmo tamanho que o vetor original
        int[] vetorInvertido = new int[vetorOriginal.length];
        // iniciar a posição do vetor invertido na última posição
        int posicaoInvertido = (vetorOriginal.length - 1);

        // para cada elemento do vetor original copiar o mesmo para a posição inversa no vetor invertido
        for (int posicaoOriginal = 0; posicaoOriginal < vetorOriginal.length; posicaoOriginal++) {
            vetorInvertido[posicaoInvertido] = vetorOriginal[posicaoOriginal];
            posicaoInvertido--;
        }

        return vetorInvertido;
    }

    //*********************************** Exercicio 10G ***********************************

    /**
     * Permite obter um vetor com os elementos primos de um vetor dado.
     * @param vetorOriginal Recebe um vetor de números inteiros.
     * @return Um vetor de números inteiros com os elementos primos do vetor dado.
     */
    public static int[] obterVetorComElementosPrimos(int[] vetorOriginal) {

        // se for dado um vetor vazio retornar nulo
        if (vetorOriginal.length == 0) {
            return null;
        }

        // criar o vetor intermédio para armazenar os números primos, iniciado na posição zero
        int[] vetorIntermedio = new int[vetorOriginal.length];
        int posicaoIntermedio = 0;

        // verificar se cada elemento no vetor original é primo, se sim guardar no vetor intermédio
        for (int posicaoOriginal = 0; posicaoOriginal < vetorOriginal.length; posicaoOriginal++) {
            if (verificarSeEPrimo(vetorOriginal[posicaoOriginal])) {
                vetorIntermedio[posicaoIntermedio] = vetorOriginal[posicaoOriginal];
                posicaoIntermedio++;
            }
        }

        // criar o vetor com os elementos primos através do vetor intermédio e com o tamanho dimensionado
        int[] vetorComElementosPrimos = ExercicioSeis.obterCopiaDeVetor(vetorIntermedio, posicaoIntermedio);

        return vetorComElementosPrimos;
    }

    /**
     * Permite verificar se um número inteiro dado é primo ou não.
     * @param numero Recebe um número inteiro.
     * @return Se o número dado é primo ou não.
     */
    public static boolean verificarSeEPrimo(int numero) {

        // se o número dado for igual ao inferior a zero nunca é primo
        if (numero <= 0) {
            return false;
        }

        // criar um vetor que contém os possíveis divisores do número dado, entre 1 e o próprio número
        int[] vetorPossiveisDivisores = ExercicioSete.obterVetorDefinidoUmIntervalo(1, numero);
        // criar um vetor para guardar os divisores e iniciado na posição zero
        int[] vetorIntermedio = new int[vetorPossiveisDivisores.length];
        int posicaoIntermedio = 0;

        // para cada possível divisor verificar se o resto da divisão do número por ele é igual a zero, se sim contabiliza como divisor
        for (int posicaoPossivelDivisor = 0; posicaoPossivelDivisor < vetorPossiveisDivisores.length; posicaoPossivelDivisor++) {
            if (numero % vetorPossiveisDivisores[posicaoPossivelDivisor] == 0) {
                vetorIntermedio[posicaoIntermedio] = vetorPossiveisDivisores[posicaoPossivelDivisor];
                posicaoIntermedio++;
            }
        }

        // criar o vetor com os divisores a partir do intermédio e com o tamanho dimensionado
        int[] vetorDivisores = ExercicioSeis.obterCopiaDeVetor(vetorIntermedio, posicaoIntermedio);

        // caso o número só tenha 2 divisores (1 e o próprio) é considerado primo
        return vetorDivisores.length == 2 && vetorDivisores[0] == 1 && vetorDivisores[1] == numero;
    }

}