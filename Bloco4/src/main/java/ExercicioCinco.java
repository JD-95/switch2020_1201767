public class ExercicioCinco {
    // Construa uma solução em Java que calcule a soma dos elementos pares de um número inteiro positivo.

    // atraves dos metodos criados retornar a soma dos algarismos pares de um número
    public static int obterSomaDosAlgarismosPares(int numero) {

        int[] vetorComDigitos;
        vetorComDigitos = obterVetorComOsDigitosDeUmNumero(numero);

        int[] vetorComDigitosPares;
        vetorComDigitosPares = ExercicioQuatro.obterVetorComNumerosPares(vetorComDigitos);

        return obterSomaDosElementosVetor(vetorComDigitosPares);
    }

    // atraves dos metodos criados retornar a soma dos algarismos impares de um número
    public static int obterSomaDosAlgarismosImpares(int numero) {

        int[] vetorComDigitos;
        vetorComDigitos = obterVetorComOsDigitosDeUmNumero(numero);

        int[] vetorComDigitosImpares;
        vetorComDigitosImpares = ExercicioQuatro.obterVetorComNumerosImpares(vetorComDigitos);

        return obterSomaDosElementosVetor(vetorComDigitosImpares);
    }


    /***************************************************************************/


    // obter um vetor com os digitos de um dado numero
    public static int[] obterVetorComOsDigitosDeUmNumero(int numero) {

        int numeroDeDigitos = ExercicioUm.obterNumeroDeDigitos(numero);
        int[] vetorComOsDigitos = new int[numeroDeDigitos];

        for (int i = (numeroDeDigitos - 1); i >= 0; i--) {
            vetorComOsDigitos[i] = numero % 10;
            numero /= 10;
        }

        return vetorComOsDigitos;
    }


    // obter soma dos elementos de um vetor
    public static int obterSomaDosElementosVetor(int[] vetorOriginal) {

        int somaDosElementosVetor = 0;

        for (int i = 0; i < vetorOriginal.length; i++) {
            somaDosElementosVetor += vetorOriginal[i];
        }

        return somaDosElementosVetor;
    }
}
