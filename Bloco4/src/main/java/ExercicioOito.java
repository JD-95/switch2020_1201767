public class ExercicioOito {
    // Construa uma solução em Java que determine os múltiplos comuns de vários números inteiros (N) num dado intervalo.

    /**
     * Bloco4 ExercicioOito, permite obter um vetor com os múltiplos comuns de N números dados dentro de um intervalo dado (crescente ou não).
     * @param limiteInferior Recebe o valor inteiro que define o limite inferior do intervalo a considerar.
     * @param limiteSuperior Recebe o valor inteiro que define o limite superior do intervalo a considerar.
     * @param numerosAVerificar Recebe o vetor com os números inteiros dos quais se pretende obter os múltiplos em comum.
     * @return Vetor com os múltiplos comuns dos números dados.
     */
    public static int[] obterVetorComOsMultiplosComuns(int limiteInferior, int limiteSuperior, int[] numerosAVerificar) {

        // se vetor com os números a verificar for vazio, returna null
        if (numerosAVerificar.length == 0) {
            return null;
        }

        // através do método ja definido obter o vetor com os valores do intervalo dado
        int[] vetorIntervalo = ExercicioSete.obterVetorDefinidoUmIntervalo(limiteInferior, limiteSuperior);
        // criar o vetor intermédio que permite armazenar os múltiplos comuns encontrados, é iniciado com o tamanho do intervalo
        int[] vetorIntermedio = new int[vetorIntervalo.length];
        // iniciar a contagem dos múltiplos comuns a zero
        int numeroDeMultiplosComuns = 0;

        // percorrer cada número do vetor intervalo
        for (int posicaoVetorIntervalo = 0, posicaoVetorIntermedio = 0; posicaoVetorIntervalo < vetorIntervalo.length; posicaoVetorIntervalo++) {

            // percorrer cada número do qual se pretende verificar os seus múltiplos
            for (int posicaoVetorNumeros = 0; posicaoVetorNumeros < numerosAVerificar.length; posicaoVetorNumeros++) {

                // verificar se o número no intervalo é múltiplo de cada número a avaliar
                if (verificarSeXMultiploDeY(vetorIntervalo[posicaoVetorIntervalo], numerosAVerificar[posicaoVetorNumeros])) {

                    // quando chegarmos ao ultimo número a avaliar e sendo todos os anteriores já verificados
                    if (posicaoVetorNumeros == (numerosAVerificar.length - 1)) {
                        // guardar o número do intervalo no vetor intermédio
                        vetorIntermedio[posicaoVetorIntermedio] = vetorIntervalo[posicaoVetorIntervalo];
                        posicaoVetorIntermedio++; // avançar para o preenchimento da próxima posição do vetor intermédio
                        numeroDeMultiplosComuns++; // contabilizar o número de múltiplos comuns
                    }

                } else { // caso o número no intervalo não seja múltiplo do número a verificar parar a verificação dos próximos
                    posicaoVetorNumeros = numerosAVerificar.length;
                }
            }
        }

        // através do método copiar o vetor intermédio para um vetor que vai ter só os múltiplos comuns encontrados
        int[] vetorComMultiplosComuns = ExercicioSeis.obterCopiaDeVetor(vetorIntermedio, numeroDeMultiplosComuns);

        return vetorComMultiplosComuns;
    }


    public static boolean verificarSeXMultiploDeY(int X, int Y) {
        boolean seXMultiploDeY = false;
        if (Y != 0) { // não existem multiplos de zero
            if ((X % Y) == 0) {   // se o resto da divisão inteira de X por Y for igual a zero, diz-se que X é multiplo de Y
                seXMultiploDeY = true;
            }
        }
        return seXMultiploDeY;
    }


}
