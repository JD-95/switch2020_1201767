public class ExercicioSeis {
    // Dado um vetor de números inteiros, retorne um outro vetor com apenas os primeiros N elementos do vetor recebido.

    public static int[] obterCopiaDeVetor(int[] vetorOriginal, int tamanhoNovoVetor) {

        // se o tamanho do vetor copia for inferior ao tamanho do vetor original
        // ou o vetor original for nulo retornar em ambos os casos nulo
        if (tamanhoNovoVetor < 0 || vetorOriginal == null) {
            return null;
        }

        // se o tamanho do novo vetor for superior ao tamanho do vetor original, assumir que o tamanho do novo passa a ser iguala o original
        if (tamanhoNovoVetor > vetorOriginal.length) {
            tamanhoNovoVetor = vetorOriginal.length;
        }

        // declarar o vetor copia que terá como tamanho o definido
        int[] vetorCopia = new int[tamanhoNovoVetor];

        // realizar a copia de cada posição do vetor original para o vetor copia até ao tamanho definido
        for (int index = 0; index < tamanhoNovoVetor; index++) {
            vetorCopia[index] = vetorOriginal[index];
        }

        return vetorCopia;
    }

}

