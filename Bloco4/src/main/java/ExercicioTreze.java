public class ExercicioTreze {

    /**
     * Permite verificar se uma matriz dada é quadrada (número de colunas igual ao número de linhas).
     * @param matriz Recebe a matriz de números inteiros.
     * @return Se a matriz dada é quadrada ou não.
     */
    public static boolean verificarMatrizQuadrada(int[][] matriz) {

        // assume-se como número de colunas o que existe na primeira linha
        int numeroColunas = matriz[0].length;
        int numeroLinhas = matriz.length;

        // caso o número de colunas seja igual em todas as linhas, e igual ao número de linhas, a matriz é quadrada
        return verificarNumeroColunasIgualEmCadaLinha(matriz) && numeroColunas == numeroLinhas;
    }

    /**
     * Permite verificar se uma matriz dada possuí o mesmo número de colunas em todas as linhas.
     * @param matriz Recebe a matriz de números inteiros.
     * @return Se a matriz possuí ou não o mesmo número de colunas em todas as linhas.
     */
    public static boolean verificarNumeroColunasIgualEmCadaLinha(int[][] matriz) {

        boolean numeroColunasIgualEmCadaLinha = true;

        // avaliar para cada linha da matriz se possuí o mesmo número de colunas da linha anterior, e se não contém um array vazio
        for (int linha = 1; linha < matriz.length && numeroColunasIgualEmCadaLinha; linha++) {
            if (matriz[linha].length != matriz[linha - 1].length || matriz[linha].length == 0) {
                numeroColunasIgualEmCadaLinha = false;
            }
        }

        return numeroColunasIgualEmCadaLinha;
    }
}
