public class ExercicioDoze {

    /**
     * Permite obter o número de colunas de uma matriz dada.
     * @param matriz Recebe a matriz de números inteiros.
     * @return O número de colunas que a matriz possuí.
     */
    public static int obterNumeroDeColunasMatriz(int[][] matriz) {
        // se a matriz dada tiver o mesmo número de colunas em todas as linhas,
        // retornar o número de colunas (obtido neste caso do tamanho da primeira linha)
        // se não for retornar -1, uma vez que possuí diferentes colunas em cada linha
        if (ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz)) {
            return matriz[0].length;
        } else {
            return -1;
        }
    }

}
