public class ExercicioQuatro {
    // Dado um vetor de números inteiros retorne um outro vetor contendo apenas os elementos pares do vetor recebido.

    public static void main(String[] args) {
        //obterVetorComNumerosPares(new int[]{1, 2, 3, 4, 3, 4, 8, 6, 0});
        //obterVetorComNumerosImpares(new int[]{1, 2, 3, 4, 3, 4, 8, 6, 0});
    }

    public static int[] obterVetorComNumerosPares(int[] vetorOriginal) {

        // Declarar um array para guardar os numeros pares do vetor original
        // Como não sabemos o tamanho do array colocamos o máximo que pode ter, que é o tamanho do vetor original
        int[] vetorComNumerosParesIntermedio = new int[vetorOriginal.length];

        // inicializar a contagem dos números pares a zero
        int numeroPares = 0;

        // verificar cada numero armazenado no vetor original
        for (int i = 0, j = 0; i < vetorOriginal.length; i++) {
            if (verificarSeEPar(vetorOriginal[i])) {      // verificar se cada numero é par
                vetorComNumerosParesIntermedio[j] = vetorOriginal[i];  // se sim armazenar o mesmo no vetor criado
                numeroPares++;   // além disso contabilizar o número de pares
                j++;  // ir também mudando o indice do vetor criado
            }
        }

        // criar um novo vetor que vai ter o tamanho condicionado pelo número de pares no vetor original
        int[] vetorComNumerosPares = new int[numeroPares];


        // para cada numero do vetor intermédio até atingir o numero de pares definido
        for (int i = 0; i < numeroPares; i++) {
            vetorComNumerosPares[i] = vetorComNumerosParesIntermedio[i];
            // deste modo os pares armazenas nas primeiras posições do vetor intermedio são copiadas para o vetor final a apresentar
        }

        return vetorComNumerosPares;
    }

    public static int[] obterVetorComNumerosImpares(int[] vetorOriginal) {

        // Declarar um array para guardar os numeros pares do vetor original
        // Como não sabemos o tamanho do array colocamos o máximo que pode ter, que é o tamanho do vetor original
        int[] vetorComNumerosImparesIntermedio = new int[vetorOriginal.length];

        // inicializar a contagem dos números impares a zero
        int numeroImpares = 0;

        // verificar cada numero armazenado no vetor original
        for (int i = 0, j = 0; i < vetorOriginal.length; i++) {
            if (!verificarSeEPar(vetorOriginal[i])) {      // verificar se cada numero é impar
                vetorComNumerosImparesIntermedio[j] = vetorOriginal[i];  // se sim armazenar o mesmo no vetor criado
                numeroImpares++;   // além disso contabilizar o número de impares
                j++;  // ir também mudando o indice do vetor criado
            }
        }

        // criar um novo vetor que vai ter o tamanho condicionado pelo número de pares no vetor original
        int[] vetorComNumerosImpares = new int[numeroImpares];

        // para cada numero do vetor intermédio até atingir o numero de impares definido
        for (int i = 0; i < numeroImpares; i++) {
            vetorComNumerosImpares[i] = vetorComNumerosImparesIntermedio[i];
            // deste modo os pares armazenas nas primeiras posições do vetor intermedio são copiadas para o vetor final a apresentar
        }

        return vetorComNumerosImpares;
    }

    public static boolean verificarSeEPar(int numeroAVerificar) {
        return (numeroAVerificar % 2) == 0;    // se a divisao inteira de um numero por 2 = 0, entao esse numero é par
    }
}
