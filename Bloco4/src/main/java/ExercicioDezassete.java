public class ExercicioDezassete {

    //*********************************** Exercicio 17A ***********************************

    /**
     * Permite obter o resultado da multiplicação de uma matriz por uma constante.
     * @param matriz Recebe uma matriz de números inteiros.
     * @param constante Recebe o valor da constante.
     * @return A matriz resultante da multiplicação efetuada.
     */
    public static int[][] obterProdutoMatrizPorConstante(int[][] matriz, int constante) {

        // se a matriz for nula ou vazia retorna nulo
        if (ExercicioQuinze.matrizNaoPreenchida(matriz)) {
            return null;
        }

        // criar o vetor que vai armazenar o resultado
        int[][] resultado = new int[matriz.length][0];

        // multiplicar cada elemento pela constante e armazenar na matriz resultado
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                int elementoMultiplicado = matriz[linha][coluna] * constante;
                resultado[linha] = ExercicioQuinze.adicionarElementoAoVetor(resultado[linha], elementoMultiplicado);
            }
        }

        return resultado;
    }

    //*********************************** Exercicio 17B ***********************************

    /**
     * Permite obter a soma de duas matrizes que partilham a mesma estrutura (numero de linhas e numero de colunas em cada linha).
     * @param matrizA Recebe a primeira matriz de números inteiros.
     * @param matrizB Recebe a segunda matriz de números inteiros.
     * @return A matriz com o resultado da soma das duas matrizes dadas.
     */
    public static int[][] obterSomaDuasMatrizes(int[][] matrizA, int[][] matrizB) {

        // se matriz A não partilhar a mesma estrutura da B retornar nulo
        if (!matrizesComMesmaEstrutura(matrizA, matrizB)) {
            return null;
        }

        int[][] somaMatrizes = new int[matrizA.length][0];

        // somar os elementos na mesma posição em cada matriz
        for (int linha = 0; linha < matrizA.length; linha++) {
            for (int coluna = 0; coluna < matrizA[linha].length; coluna++) {
                int elementoSomado = matrizA[linha][coluna] + matrizB[linha][coluna];
                somaMatrizes[linha] = ExercicioQuinze.adicionarElementoAoVetor(somaMatrizes[linha], elementoSomado);
            }
        }
        return somaMatrizes;
    }

    /**
     * Permite verificar se duas matrizes partilham a mesma estrutura (numero de linhas e numero de colunas em cada linha).
     * @param matrizA Recebe a primeira matriz de números inteiros.
     * @param matrizB Recebe a segunda matriz de números inteiros.
     * @return Se as matrizes partilham ou não a mesma estrutura.
     */
    public static boolean matrizesComMesmaEstrutura(int[][] matrizA, int[][] matrizB) {

        // se alguma das matrizes for nula ou vazia retorna falso
        if (ExercicioQuinze.matrizNaoPreenchida(matrizA) || ExercicioQuinze.matrizNaoPreenchida(matrizB)) {
            return false;
        }
        boolean resultado = true;
        // verificar se o número de linhas é diferente entre as matrizes
        if (matrizA.length != matrizB.length) {
            resultado = false;
        }
        // verificar se o número de colunas em cada linha é diferente entre as matrizes
        for (int linha = 0; linha < matrizA.length && resultado; linha++) {
            if (matrizA[linha].length != matrizB[linha].length) {
                resultado = false;
            }
        }
        return resultado;
    }

    //*********************************** Exercicio 17C ***********************************

    /**
     * Permite obter a multiplicação de duas matrizes dadas.
     * @param matrizA Recebe a primeira matriz de números inteiros.
     * @param matrizB Recebe a segunda matriz de números inteiros.
     * @return A matriz resultante da multiplicação das duas matrizes dadas.
     */
    public static int[][] obterProdutoDuasMatrizes(int[][] matrizA, int[][] matrizB) {

        // se matrizes vazias, nulas, sem o mesmo número de colunas em cada linha
        // ou se o numero de colunas de A não for igual ao numero de linhas de B retornar nulo
        if (ExercicioQuinze.matrizNaoPreenchida(matrizA) || ExercicioQuinze.matrizNaoPreenchida(matrizB) || !ExercicioTreze
                .verificarNumeroColunasIgualEmCadaLinha(matrizA) || !ExercicioTreze
                .verificarNumeroColunasIgualEmCadaLinha(matrizB) || matrizA[0].length != matrizB.length) {
            return null;
        }

        int[][] produtoMatrizes = new int[matrizA.length][0];
        int elementoMultiplicado = 0;

        // para cada elemento da nova matriz realizar o produto da linha correspondente na matrizA e da coluna correspondente na matrizB
        for (int linhaA = 0; linhaA < matrizA.length; linhaA++) {
            for (int colunaB = 0; colunaB < matrizB[0].length; colunaB++) {
                int[] vetorLinha = obterVetorLinhaMatriz(matrizA, linhaA);
                int[] vetorColuna = obterVetorColunaMatriz(matrizB, colunaB);
                elementoMultiplicado = obterProdutoEscalarLinhaColuna(vetorLinha, vetorColuna);
                produtoMatrizes[linhaA] = ExercicioQuinze.adicionarElementoAoVetor(produtoMatrizes[linhaA], elementoMultiplicado);
            }
        }
        return produtoMatrizes;
    }

    /**
     * Permite obter um vetor com os elementos da linha selecionada de uma dada matriz.
     * @param matriz Recebe uma matriz de números inteiros.
     * @param linha Recebe a linha selecionada.
     * @return O vetor com os elementos da linha.
     */
    public static int[] obterVetorLinhaMatriz(int[][] matriz, int linha) {

        // se linha dada não se encontrar na matriz
        if (linha >= matriz.length || linha < 0) {
            return null;
        }
        int[] vetorLinha = new int[matriz[linha].length];

        // copiar cada elemento na linha para o vetor criado
        for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
            vetorLinha[coluna] = matriz[linha][coluna];
        }

        return vetorLinha;
    }

    /**
     * Permite obter um vetor com os elementos da coluna selecionada de uma dada matriz.
     * @param matriz Recebe uma matriz de números inteiros.
     * @param coluna Recebe a coluna selecionada.
     * @return O vetor com os elementos da coluna.
     */
    public static int[] obterVetorColunaMatriz(int[][] matriz, int coluna) {

        // se coluna dada não se encontrar na matriz
        if (coluna >= matriz[0].length || coluna < 0) {
            return null;
        }

        int[] vetorColuna = new int[matriz.length];

        // copiar cada elemento na coluna para o vetor criado
        for (int linha = 0; linha < matriz.length; linha++) {
            vetorColuna[linha] = matriz[linha][coluna];
        }

        return vetorColuna;
    }

    /**
     * Permite obter o produto escalar de uma linha por uma coluna de uma dada matriz (soma das multiplicações de elementos dispostos na mesma
     * posição nos vetores).
     * @param vetorLinha Recebe um vetor de números inteiros com os elementos da linha da matriz.
     * @param vetorColuna Recebe um vetor de números inteiros com os elementos da coluna da matriz.
     * @return O resultado do produto escalar entre a linha e a coluna.
     */
    public static Integer obterProdutoEscalarLinhaColuna(int[] vetorLinha, int[] vetorColuna) {

        // caso algum dos vetores seja vazio ou se o tamanho dos dois não for o mesmo retornar nulo
        if (vetorLinha.length == 0 || vetorColuna.length == 0 || vetorLinha.length != vetorColuna.length) {
            return null;
        }

        // iniciar o produto escalar a zero
        int produtoEscalar = 0;

        // realizar a multiplicação entre os elementos na mesma posição nos dois vetores, e ir realizando a soma destas multiplicações
        for (int posicao = 0; posicao < vetorLinha.length; posicao++) {
            produtoEscalar += (vetorLinha[posicao] * vetorColuna[posicao]);
        }

        return produtoEscalar;
    }
}
