public class ExercicioCatorze {

    /**
     * Permite verificar se uma matriz dada é retangular (número de linhas diferente do número de colunas).
     * @param matriz Recebe a matriz de números inteiros.
     * @return Se a matriz dada é retangular ou não.
     */
    public static boolean verificarMatrizRetangular(int[][] matriz) {

        // assume-se como número de colunas o que existe na primeira linha
        int numeroColunas = matriz[0].length;
        int numeroLinhas = matriz.length;

        // caso o número de colunas seja igual em todas as linhas, e diferente do número de linhas, a matriz é retangular
        return ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz) && numeroLinhas != numeroColunas;
    }

}

