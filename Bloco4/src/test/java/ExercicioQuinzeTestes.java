import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioQuinzeTestes {

    @Test
    void obterMenorElementoMatriz_MatrizNula() {
        int[][] matriz = null;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMenorElementoMatriz_MatrizVazia() {
        int[][] matriz = {};
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertNull(resultado);
    }


    @Test
    void obterMenorElementoMatriz_UmaLinhaVazia_TesteUm() {
        int[][] matriz = {{0}, {}};
        Integer esperado = 0;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMenorElementoMatriz_UmaLinhaVazia_TesteDois() {
        int[][] matriz = {{}, {0}};
        Integer esperado = 0;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMenorElementoMatriz_LinhasIguais() {
        int[][] matriz = {{0}, {0}};
        Integer esperado = 0;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMenorElementoMatriz_ValorNegativoPositivo_TesteUm() {
        int[][] matriz = {{-1}, {1}};
        Integer esperado = -1;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMenorElementoMatriz_ValorNegativoPositivo_TesteDois() {
        int[][] matriz = {{1}, {-1}};
        Integer esperado = -1;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMenorElementoMatriz_ValoresNegativos() {
        int[][] matriz = {{-2}, {-1}};
        Integer esperado = -2;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMenorElementoMatriz_MatrizComNumeroColunasDiferente() {
        int[][] matriz = {{0, 5, -6}, {-7}, {22, 6}};
        Integer esperado = -7;
        Integer resultado = ExercicioQuinze.obterMenorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }


    /***********************************************************************************************************/

    @Test
    void obterMaiorElementoMatriz_MatrizNula() {
        int[][] matriz = null;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMaiorElementoMatriz_MatrizVazia() {
        int[][] matriz = {};
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMaiorElementoMatriz_UmaLinhaVazia_TesteUm() {
        int[][] matriz = {{0}, {}};
        Integer esperado = 0;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMaiorElementoMatriz_UmaLinhaVazia_TesteDois() {
        int[][] matriz = {{}, {0}};
        Integer esperado = 0;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMaiorElementoMatriz_LinhasIguais() {
        int[][] matriz = {{0}, {0}};
        Integer esperado = 0;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMaiorElementoMatriz_ValorNegativoPositivo_TesteUm() {
        int[][] matriz = {{-1}, {1}};
        Integer esperado = 1;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMaiorElementoMatriz_ValorNegativoPositivo_TesteDois() {
        int[][] matriz = {{1}, {-1}};
        Integer esperado = 1;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMaiorElementoMatriz_ValoresNegativos() {
        int[][] matriz = {{-2}, {-1}};
        Integer esperado = -1;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMaiorElementoMatriz_MatrizComNumeroColunasDiferente() {
        int[][] matriz = {{0, 5, -6}, {-7}, {22, 6}};
        Integer esperado = 22;
        Integer resultado = ExercicioQuinze.obterMaiorElementoMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    /***********************************************************************************************************/

    @Test
    void obterMediaElementosMatriz_MatrizNula() {
        int[][] matriz = null;
        Double esperado = null;
        Double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMediaElementosMatriz_MatrizVazia() {
        int[][] matriz = {};
        Double esperado = null;
        Double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMediaElementosMatriz_LinhaVazia() {
        int[][] matriz = {{1}, {}};
        Double esperado = 1.0;
        Double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterMediaElementosMatriz_ElementosIguais() {
        int[][] matriz = {{1}, {1}};
        double esperado = 1.0;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void obterMediaElementosMatriz_ValorNegativoPositivo() {
        int[][] matriz = {{-1}, {1}};
        double esperado = 0.0;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void obterMediaElementosMatriz_ValorPositivoNegativo() {
        int[][] matriz = {{-1}, {1}};
        double esperado = 0.0;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void obterMediaElementosMatriz_ValoresNegativos() {
        int[][] matriz = {{-1}, {-2}};
        double esperado = -1.5;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void obterMediaElementosMatriz_ValoresPositivos() {
        int[][] matriz = {{1}, {2}};
        double esperado = 1.5;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void obterMediaElementosMatriz_ElementoZero() {
        int[][] matriz = {{0}, {1}};
        double esperado = 0.5;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void obterMediaElementosMatriz_NumeroColunasDiferente() {
        int[][] matriz = {{0, 5, -6}, {-7}, {22, 6}};
        double esperado = 3.33;
        double resultado = ExercicioQuinze.obterMediaElementosMatriz(matriz);
        assertEquals(esperado, resultado, 0.01);
    }

    /***********************************************************************************************************/

    @Test
    void obterProdutoElementosMatriz_MatrizNula() {
        int[][] matriz = null;
        Integer esperado = null;
        Integer resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_MatrizVazia() {
        int[][] matriz = {};
        Integer esperado = null;
        Integer resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_LinhaVazia() {
        int[][] matriz = {{2}, {}};
        Integer esperado = 2;
        Integer resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_ZeroxZero() {
        int[][] matriz = {{0}, {0}};
        int esperado = 0;
        int resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_ZeroxUm() {
        int[][] matriz = {{0}, {1}};
        int esperado = 0;
        int resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_UmxUm() {
        int[][] matriz = {{1}, {1}};
        int esperado = 1;
        int resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_UmxNegUm() {
        int[][] matriz = {{1}, {-1}};
        int esperado = -1;
        int resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_NegUmxNegUm() {
        int[][] matriz = {{-1}, {-1}};
        int esperado = 1;
        int resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoElementosMatriz_NumeroColunasDiferente() {
        int[][] matriz = {{1, 5, -6}, {-7}, {22, 6}};
        int esperado = 27720;
        int resultado = ExercicioQuinze.obterProdutoElementosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    /***********************************************************************************************************/


    @Test
    void verificarElementosNaoRepetidosMatriz_MatrizNula() {
        int[][] matriz = null;
        int[] esperado = null;
        int[] resultado = ExercicioQuinze.obterElementosNaoRepetidosMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementosNaoRepetidosMatriz_MatrizVazia() {
        int[][] matriz = {{}};
        int[] esperado = {};
        int[] resultado = ExercicioQuinze.obterElementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarElementosNaoRepetidosMatriz_ElementosRepetidosLinhas() {
        int[][] matriz = {{0}, {0}};
        int[] esperado = {0};
        int[] resultado = ExercicioQuinze.obterElementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarElementosNaoRepetidosMatriz_ElementosNaoRepetidosLinhas() {
        int[][] matriz = {{0}, {1}};
        int[] esperado = {0, 1};
        int[] resultado = ExercicioQuinze.obterElementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarElementosNaoRepetidosMatriz_ElementosRepetidosColunas() {
        int[][] matriz = {{0, 0}, {1, 1}};
        int[] esperado = {0, 1};
        int[] resultado = ExercicioQuinze.obterElementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarElementosNaoRepetidosMatriz_ElementosNaoRepetidosColunas() {
        int[][] matriz = {{0, 1}, {0, 1}};
        int[] esperado = {0, 1};
        int[] resultado = ExercicioQuinze.obterElementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    /***********************************************************************************************************/


    @Test
    void verificarAdicionarElementoAoVetor_VetorNulo() {
        int[] vetor = null;
        int numero = 1;
        int[] esperado = null;
        int[] resultado = ExercicioQuinze.adicionarElementoAoVetor(vetor, numero);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarAdicionarElementoAoVetor_VetorVazio() {
        int[] vetor = {};
        int numero = 0;
        int[] esperado = {0};
        int[] resultado = ExercicioQuinze.adicionarElementoAoVetor(vetor, numero);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarAdicionarElementoAoVetor_VetorComUmElemento() {
        int[] vetor = {0};
        int numero = 1;
        int[] esperado = {0, 1};
        int[] resultado = ExercicioQuinze.adicionarElementoAoVetor(vetor, numero);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarAdicionarElementoAoVetor_VetorComDoisElementos() {
        int[] vetor = {1, 0};
        int numero = -1;
        int[] esperado = {1, 0, -1};
        int[] resultado = ExercicioQuinze.adicionarElementoAoVetor(vetor, numero);
        assertArrayEquals(esperado, resultado);
    }

    /***********************************************************************************************************/


    @Test
    void verificarSeExisteValorNumVetor_VetorNulo() {
        int[] vetor = null;
        int numero = 1;
        boolean resultado = ExercicioQuinze.verificarSeExisteValorNumVetor(vetor, numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeExisteValorNumVetor_VetorVazio() {
        int[] vetor = {};
        int numero = 1;
        boolean resultado = ExercicioQuinze.verificarSeExisteValorNumVetor(vetor, numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeExisteValorNumVetor_VetorSemOValor() {
        int[] vetor = {1};
        int numero = 0;
        boolean resultado = ExercicioQuinze.verificarSeExisteValorNumVetor(vetor, numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeExisteValorNumVetor_VetorComOValor() {
        int[] vetor = {0};
        int numero = 0;
        boolean resultado = ExercicioQuinze.verificarSeExisteValorNumVetor(vetor, numero);
        assertTrue(resultado);
    }

    @Test
    void verificarSeExisteValorNumVetor_VetorComOValorEntreElementos() {
        int[] vetor = {0, 1, -2, 3};
        int numero = -2;
        boolean resultado = ExercicioQuinze.verificarSeExisteValorNumVetor(vetor, numero);
        assertTrue(resultado);
    }

    @Test
    void verificarSeExisteValorNumVetor_VetorComOValorRepetido() {
        int[] vetor = {1, 1, 1, 1, 1, 1};
        int numero = 1;
        boolean resultado = ExercicioQuinze.verificarSeExisteValorNumVetor(vetor, numero);
        assertTrue(resultado);
    }

    /**************************************************************************************************/

    @Test
    void vetorPreenchido_Nulo() {
        int[] vetor = null;
        boolean resultado = ExercicioQuinze.vetorPreenchido(vetor);
        assertFalse(resultado);
    }

    @Test
    void vetorPreenchido_Vazio() {
        int[] vetor = {};
        boolean resultado = ExercicioQuinze.vetorPreenchido(vetor);
        assertFalse(resultado);
    }

    @Test
    void vetorPreenchido_UmElemento() {
        int[] vetor = {0};
        boolean resultado = ExercicioQuinze.vetorPreenchido(vetor);
        assertTrue(resultado);
    }

    @Test
    void vetorPreenchido_TresElementos() {
        int[] vetor = {0, -1, 1};
        boolean resultado = ExercicioQuinze.vetorPreenchido(vetor);
        assertTrue(resultado);
    }

    /************************************************************************************************/

    @Test
    void obterElementosPrimosMatriz_MatrizNula() {
        int[][] matriz = null;
        int[] resultado = ExercicioQuinze.obterElementosPrimosMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterElementosPrimosMatriz_MatrizVazia() {
        int[][] matriz = {};
        int[] resultado = ExercicioQuinze.obterElementosPrimosMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterElementosPrimosMatriz_LinhaVazia() {
        int[][] matriz = {{}, {2}};
        int[] esperado = {2};
        int[] resultado = ExercicioQuinze.obterElementosPrimosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterElementosPrimosMatriz_ElementosPrimos() {
        int[][] matriz = {{3, 7}, {2, 5}};
        int[] esperado = {3, 7, 2, 5};
        int[] resultado = ExercicioQuinze.obterElementosPrimosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterElementosPrimosMatriz_SemElementosPrimos() {
        int[][] matriz = {{6, 1}, {4, 0}};
        int[] esperado = {};
        int[] resultado = ExercicioQuinze.obterElementosPrimosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterElementosPrimosMatriz_SemEComElementosPrimos() {
        int[][] matriz = {{-2, 2}, {4, 7}};
        int[] esperado = {2, 7};
        int[] resultado = ExercicioQuinze.obterElementosPrimosMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    /****************************************************************************************************/

    @Test
    void matrizNaoPreenchida_MatrizNula() {
        int[][] matriz = null;
        boolean resultado = ExercicioQuinze.matrizNaoPreenchida(matriz);
        assertTrue(resultado);
    }

    @Test
    void matrizNaoPreenchida_MatrizVazia() {
        int[][] matriz = {};
        boolean resultado = ExercicioQuinze.matrizNaoPreenchida(matriz);
        assertTrue(resultado);
    }

    @Test
    void matrizNaoPreenchida_MatrizComUmaLinhaVazia() {
        int[][] matriz = {{}, {0}};
        boolean resultado = ExercicioQuinze.matrizNaoPreenchida(matriz);
        assertFalse(resultado);
    }

    @Test
    void matrizNaoPreenchida_MatrizPreenchida() {
        int[][] matriz = {{1}, {0}};
        boolean resultado = ExercicioQuinze.matrizNaoPreenchida(matriz);
        assertFalse(resultado);
    }

    /******************************************************************************************************/

    @Test
    void obterDiagonalPrincipalMatriz_MatrizSemIgualNumeroDeColunasEmCadaLinha() {
        int[][] matriz = {{0, 1}, {1}, {1, 0}};
        int[] resultado = ExercicioQuinze.obterDiagonalPrincipalMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDiagonalPrincipalMatriz_Quadrada() {
        int[][] matriz = {{0, 1, 3}, {1, 2, 0}, {4, 0, 1}};
        int[] esperado = {0, 2, 1};
        int[] resultado = ExercicioQuinze.obterDiagonalPrincipalMatriz(matriz);
        assertArrayEquals(resultado, esperado);
    }

    @Test
    void obterDiagonalPrincipalMatriz_RetangularMaisLinhas() {
        int[][] matriz = {{0, 1, 3}, {1, 2, 0}, {5, 0, 1}, {2, 1, 0}};
        int[] esperado = {0, 2, 1};
        int[] resultado = ExercicioQuinze.obterDiagonalPrincipalMatriz(matriz);
        assertArrayEquals(resultado, esperado);
    }

    @Test
    void obterDiagonalPrincipalMatriz_RetangularMaisColunas() {
        int[][] matriz = {{0, 1, 2, 3}, {1, 3, 2, 0}, {2, 0, 1, 3}};
        int[] esperado = {0, 3, 1};
        int[] resultado = ExercicioQuinze.obterDiagonalPrincipalMatriz(matriz);
        assertArrayEquals(resultado, esperado);
    }

    /***********************************************************************************************/

    @Test
    void elementoDentroMatriz_MatrizVazia() {
        int linha = 0;
        int coluna = 0;
        int[][] matriz = {};
        boolean resultado = ExercicioQuinze.elementoDentroMatriz(linha, coluna, matriz);
        assertFalse(resultado);
    }

    @Test
    void elementoDentroMatriz_TesteUm() {
        int linha = 0;
        int coluna = 0;
        int[][] matriz = {{1}};
        boolean resultado = ExercicioQuinze.elementoDentroMatriz(linha, coluna, matriz);
        assertTrue(resultado);
    }

    @Test
    void elementoDentroMatriz_TesteDois() {
        int linha = 1;
        int coluna = 1;
        int[][] matriz = {{1, 1}, {1, 1}};
        boolean resultado = ExercicioQuinze.elementoDentroMatriz(linha, coluna, matriz);
        assertTrue(resultado);
    }

    @Test
    void elementoForaMatriz_ForaDasLinhas() {
        int linha = 2;
        int coluna = 1;
        int[][] matriz = {{1, 1}, {1, 1}};
        boolean resultado = ExercicioQuinze.elementoDentroMatriz(linha, coluna, matriz);
        assertFalse(resultado);
    }

    @Test
    void elementoForaMatriz_ForaDasColunas() {
        int linha = 1;
        int coluna = 3;
        int[][] matriz = {{1, 1}, {1, 1}};
        boolean resultado = ExercicioQuinze.elementoDentroMatriz(linha, coluna, matriz);
        assertFalse(resultado);
    }

    /*****************************************************************************************************************/

    @Test
    void obterDiagonalSecundariaMatriz_MatrizSemIgualNumeroDeColunasEmCadaLinha() {
        int[][] matriz = {{0, 1}, {1}, {1, 0}};
        int[] resultado = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDiagonalSecundariaMatriz_MatrizQuadrada() {
        int[][] matriz = {{0, 1, 3}, {1, 2, 0}, {4, 0, 1}};
        int[] esperado = {3, 2, 4};
        int[] resultado = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterDiagonalSecundariaMatriz_RetangularMaisLinhas() {
        int[][] matriz = {{0, 1, 3}, {1, 2, 0}, {5, 0, 1}, {2, 1, 0}};
        int[] esperado = {3, 2, 5};
        int[] resultado = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterDiagonalSecundariaMatriz_RetangularMaisColunas() {
        int[][] matriz = {{0, 1, 2, 3}, {1, 3, 2, 0}, {2, 0, 1, 3}};
        int[] esperado = {3, 2, 0};
        int[] resultado = ExercicioQuinze.obterDiagonalSecundariaMatriz(matriz);
        assertArrayEquals(esperado, resultado);
    }

    /********************************************************************************************/

    @Test
    void matrizIdentidade_MatrizNula() {
        int[][] matriz = null;
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertFalse(resultado);
    }

    @Test
    void matrizIdentidade_MatrizVazia() {
        int[][] matriz = {};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertFalse(resultado);
    }

    @Test
    void matrizIdentidade_MatrizQuadrada() {
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertTrue(resultado);
    }

    @Test
    void matrizIdentidade_MatrizRetangularMaisLinhas() {
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, 0}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertTrue(resultado);
    }

    @Test
    void matrizIdentidade_MatrizRetangularMaisColunas() {
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertTrue(resultado);
    }

    @Test
    void matrizIdentidade_DiagonalComUmZero() {
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}, {0, 0, 0}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertFalse(resultado);
    }

    @Test
    void matrizIdentidade_NumeroDiferenteDeZeroNumDosElementosDaMatriz() {
        int[][] matriz = {{1, 0, 1}, {0, 1, 0}, {0, 0, 1}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertFalse(resultado);
    }

    @Test
    void matrizIdentidade_DiagonalSecundaria() {
        int[][] matriz = {{0, 0, 1}, {0, 1, 0}, {1, 0, 0}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertFalse(resultado);
    }

    @Test
    void matrizIdentidade_LinhaVazia() {
        int[][] matriz = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {}};
        boolean resultado = ExercicioQuinze.matrizIdentidade(matriz);
        assertFalse(resultado);
    }

    /***************************************************************************************************************************/

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_LinhaNula() {
        int[] linha = null;
        int posicaoDoElemento = 0;
        int valorElemento = 0;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertFalse(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_LinhaVazia() {
        int[] linha = {};
        int posicaoDoElemento = 0;
        int valorElemento = 0;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertFalse(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_ElementoZero() {
        int[] linha = {0, 0, 0, 0, 0};
        int posicaoDoElemento = 0;
        int valorElemento = 0;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertTrue(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_ElementoUm() {
        int[] linha = {1, 0, 0, 0, 0};
        int posicaoDoElemento = 0;
        int valorElemento = 1;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertTrue(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_ElementoNegUm() {
        int[] linha = {0, 0, 0, -1, 0};
        int posicaoDoElemento = 3;
        int valorElemento = -1;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertTrue(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_MaisQueUmElementoDiferenteDeZero() {
        int[] linha = {0, 1, 0, 1, 0};
        int posicaoDoElemento = 3;
        int valorElemento = 1;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertFalse(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_PosicaoErrada() {
        int[] linha = {0, 0, 0, 1, 0};
        int posicaoDoElemento = 1;
        int valorElemento = 1;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertFalse(resultado);
    }

    @Test
    void linhaComApenasUmElementoDiferenteDeZero_ElementoErrado() {
        int[] linha = {0, 0, 0, 1, 0};
        int posicaoDoElemento = 3;
        int valorElemento = 2;
        boolean resultado = ExercicioQuinze.linhaComApenasUmElementoDiferenteDeZero(linha, posicaoDoElemento, valorElemento);
        assertFalse(resultado);
    }

    /***********************************************************************************************************************/

    @Test
    void linhaPreenchidaComZeros_LinhaNula() {
        int[] linha = null;
        boolean resultado = ExercicioQuinze.linhaPreenchidaComZeros(linha);
        assertFalse(resultado);
    }

    @Test
    void linhaPreenchidaComZeros_LinhaVazia() {
        int[] linha = {};
        boolean resultado = ExercicioQuinze.linhaPreenchidaComZeros(linha);
        assertFalse(resultado);
    }

    @Test
    void linhaPreenchidaComZeros_LinhaSoComZeros() {
        int[] linha = {0, 0, 0, 0, 0, 0};
        boolean resultado = ExercicioQuinze.linhaPreenchidaComZeros(linha);
        assertTrue(resultado);
    }

    @Test
    void linhaPreenchidaComZeros_LinhaComElementoDiferenteDeZero() {
        int[] linha = {0, 0, 0, 1, 0, 0};
        boolean resultado = ExercicioQuinze.linhaPreenchidaComZeros(linha);
        assertFalse(resultado);
    }

    /*********************************************************************************************/

    @Test
    void obterMatrizTransposta_Nula() {
        double[][] matriz = null;
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizTransposta_Vazia() {
        double[][] matriz = {};
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizTransposta_QuadradaOrdemUm() {
        double[][] matriz = {{1}};
        double[][] esperado = {{1}};
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizTransposta_QuadradaOrdemDois() {
        double[][] matriz = {{1, 2}, {3, 4}};
        double[][] esperado = {{1, 3}, {2, 4}};
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizTransposta_QuadradaOrdemTres() {
        double[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] esperado = {{1, 4, 7}, {2, 5, 8}, {3, 6, 9}};
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizTransposta_RetangularMaisLinhas() {
        double[][] matriz = {{1, 2}, {3, 4}, {5, 6}};
        double[][] esperado = {{1, 3, 5}, {2, 4, 6}};
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizTransposta_RetangularMaisColunas() {
        double[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        double[][] esperado = {{1, 4}, {2, 5}, {3, 6}};
        double[][] resultado = ExercicioQuinze.obterMatrizTransposta(matriz);
        assertArrayEquals(esperado, resultado);
    }

    /**************************************************************************************************/

    @Test
    void obterMatrizCofatores_Nula() {
        int[][] matriz = null;
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizCofatores_Vazia() {
        int[][] matriz = {};
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizCofatores_NaoQuadrada() {
        int[][] matriz = {{1, 2}, {3, 4}, {5, 6}};
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizCofatores_QuadradaOrdemUm() {
        int[][] matriz = {{1}};
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizCofatores_QuadradaOrdemDois() {
        int[][] matriz = {{1, 2}, {3, 4}};
        double[][] esperado = {{4, -3}, {-2, 1}};
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizCofatores_QuadradaOrdemTres() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] esperado = {{-3, 6, -3}, {6, -12, 6}, {-3, 6, -3}};
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizCofatores_QuadradaOrdemTres_ComNumerosNegativos() {
        int[][] matriz = {{1, 2, -3}, {4, -5, 6}, {7, 8, 9}};
        double[][] esperado = {{-93, 6, 67}, {-42, 30, 6}, {-3, -18, -13}};
        double[][] resultado = ExercicioQuinze.obterMatrizCofatores(matriz);
        assertArrayEquals(esperado, resultado);
    }

    /**************************************************************************************************/


    @Test
    void obterMatrizInversa_Nula() {
        int[][] matriz = null;
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizInversa_Vazia() {
        int[][] matriz = {};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizInversa_NaoQuadrada() {
        int[][] matriz = {{1, 2}, {3, 4}, {5, 6}};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizInversa_SemInversa() {
        int[][] matriz = {{1, 2}, {1, 2}};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertNull(resultado);
    }

    @Test
    void obterMatrizInversa_QuadradaOrdemUm_Um() {
        int[][] matriz = {{1}};
        float[][] esperado = {{1f}};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizInversa_QuadradaOrdemUm_Dois() {
        int[][] matriz = {{2}};
        float[][] esperado = {{0.5f}};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizInversa_QuadradaOrdemDois() {
        int[][] matriz = {{1, 2}, {3, 4}};
        float[][] esperado = {{-2, 1}, {1.5f, -0.5f}};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizInversa_QuadradaOrdemTres() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 10}};
        float[][] esperado = {{-0.6666667f, -1.3333334f, 1}, {-0.6666667f, 3.6666667f, -2}, {1, -2, 1}};
        float[][] resultado = ExercicioQuinze.obterMatrizInversa(matriz);
        assertArrayEquals(esperado,resultado);
    }


    /**************************************************************************************************/

    @Test
    void obterCofatorElemento_DeterminanteZero() {
        int linha = 0;
        int coluna = 0;
        double determinante = 0;
        double esperado = 0;
        double resultado = ExercicioQuinze.obterCofatorElemento(linha, coluna, determinante);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterCofatorElemento_CofatorPositivo_DeterminantePositivo() {
        int linha = 0;
        int coluna = 0;
        double determinante = 1;
        double esperado = 1;
        double resultado = ExercicioQuinze.obterCofatorElemento(linha, coluna, determinante);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterCofatorElemento_CofatorPositivo_DeterminanteNegativo() {
        int linha = 0;
        int coluna = 1;
        double determinante = -1;
        double esperado = 1;
        double resultado = ExercicioQuinze.obterCofatorElemento(linha, coluna, determinante);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterCofatorElemento_CofatorNegativo_DeterminantePositivo() {
        int linha = 0;
        int coluna = 1;
        double determinante = 1;
        double esperado = -1;
        double resultado = ExercicioQuinze.obterCofatorElemento(linha, coluna, determinante);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterCofatorElemento_CofatorNegativo_DeterminanteNegativo() {
        int linha = 0;
        int coluna = 0;
        double determinante = -1;
        double esperado = -1;
        double resultado = ExercicioQuinze.obterCofatorElemento(linha, coluna, determinante);
        assertEquals(resultado, esperado);
    }
}