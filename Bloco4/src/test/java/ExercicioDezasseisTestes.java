import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseisTestes {

    @Test
    void obterMatrizEliminandoColunaELinha_Nula() {
        int[][] matriz = null;
        int linhaEliminar = 0;
        int colunaEliminar = 0;
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertNull(resultado);
    }

    @Test
    void obterMatrizEliminandoColunaELinha_Vazia() {
        int[][] matriz = {};
        int linhaEliminar = 0;
        int colunaEliminar = 0;
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertNull(resultado);
    }

    @Test
    void obterMatrizEliminandoColunaELinha_OrdemUm() {
        int[][] matriz = {{1}};
        int linhaEliminar = 0;
        int colunaEliminar = 0;
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertNull(resultado);
    }

    @Test
    void obterMatrizEliminandoColunaELinha_LinhaAEliminarInvalida() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int linhaEliminar = 3;
        int colunaEliminar = 0;
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertNull(resultado);
    }

    @Test
    void obterMatrizEliminandoColunaELinha_ColunaAEliminarInvalida() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int linhaEliminar = 0;
        int colunaEliminar = 3;
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertNull(resultado);
    }

    @Test
    void obterMatrizEliminandoColunaELinha_NaoQuadrada() {
        int[][] matriz = {{1, 2, 3}, {4, 6}, {7, 8, 9}};
        int linhaEliminar = 0;
        int colunaEliminar = 0;
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertNull(resultado);
    }

    @Test
    void obterMatrizEliminandoColunaZeroELinhaZero_OrdemDois() {
        int[][] matriz = {{1, 2}, {3, 4}};
        int linhaEliminar = 0;
        int colunaEliminar = 0;
        int[][] esperado = {{4}};
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizEliminandoColunaZeroELinhaZero_OrdemTres() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int linhaEliminar = 0;
        int colunaEliminar = 0;
        int[][] esperado = {{5, 6}, {8, 9}};
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizEliminandoColunaUmELinhaZero_OrdemTres() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int linhaEliminar = 0;
        int colunaEliminar = 1;
        int[][] esperado = {{4, 6}, {7, 9}};
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizEliminandoColunaDoisELinhaZero_OrdemTres() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int linhaEliminar = 0;
        int colunaEliminar = 2;
        int[][] esperado = {{4, 5}, {7, 8}};
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterMatrizEliminandoColunaZeroELinhaUm_OrdemTres() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int linhaEliminar = 1;
        int colunaEliminar = 0;
        int[][] esperado = {{2, 3}, {8, 9}};
        int[][] resultado = ExercicioDezasseis.retirarLinhaEColunaMatrizQuadrada(matriz, linhaEliminar, colunaEliminar);
        assertArrayEquals(esperado, resultado);
    }

    /*********************************************************************************************************************************/

    @Test
    void obterDeterminanteMatriz_Nula() {
        int[][] matriz = null;
        Double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatriz_Vazia() {
        int[][] matriz = {};
        Double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatriz_NaoQuadrada() {
        int[][] matriz = {{1, 0}, {1}};
        Double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatriz_OrdemUm() {
        int[][] matriz = {{1}};
        double esperado = 1;
        double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterDeterminanteMatriz_OrdemDois() {
        int[][] matriz = {{1, 0}, {1, 0}};
        double esperado = 0;
        double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterDeterminanteMatriz_OrdemTres_IgualAZero() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double esperado = 0;
        double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterDeterminanteMatriz_OrdemTres_DiferenteDeZero() {
        int[][] matriz = {{1, 2, 10}, {4, 5, 0}, {7, 8, 0}};
        double esperado = -30;
        double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterDeterminanteMatriz_OrdemQuatro_IgualAZero() {
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        double esperado = 0;
        double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterDeterminanteMatriz_OrdemQuatro_DiferenteDeZero() {
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 0, 8}, {9, 10, 11, 0}, {13, 14, 15, 16}};
        double esperado = -1008;
        double resultado = ExercicioDezasseis.obterDeterminanteMatriz(matriz);
        assertEquals(esperado, resultado);
    }

    /************************************************************************************************/

    @Test
    void obterDeterminanteMatrizAteOrdemDois_MatrizNula() {
        int[][] matriz = null;
        Double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_MatrizVazia() {
        int[][] matriz = {};
        Double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_NaoQuadrada() {
        int[][] matriz = {{1, 2, 3}, {4, 6}, {7, 8, 9}};
        Double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_OrdemSuperiorADois() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        Double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertNull(resultado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_OrdemUm() {
        int[][] matriz = {{1}};
        double esperado = 1;
        double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_OrdemDois_SoZeros() {
        int[][] matriz = {{0, 0}, {0, 0}};
        double esperado = 0;
        double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_OrdemDois_SoUns() {
        int[][] matriz = {{1, 1}, {1, 1}};
        double esperado = 0;
        double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_OrdemDois_DeterminantePositivo() {
        int[][] matriz = {{1, 0}, {0, 1}};
        double esperado = 1;
        double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertEquals(resultado, esperado);
    }

    @Test
    void obterDeterminanteMatrizAteOrdemDois_OrdemDois_DeterminanteNegativo() {
        int[][] matriz = {{0, 1}, {1, 0}};
        double esperado = -1;
        double resultado = ExercicioDezasseis.obterDeterminanteMatrizAteOrdemDois(matriz);
        assertEquals(resultado, esperado);
    }
}