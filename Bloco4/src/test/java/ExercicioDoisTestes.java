import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDoisTestes {

    @Test
    void verificarVetorComDigitosDoNumero_UmAlgarismo() {
        int[] esperado = {0};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumero(0);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumero_DoisAlgarismos() {
        int[] esperado = {1,2};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumero(12);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumero_TresAlgarismos() {
        int[] esperado = {1,2,3};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumero(123);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumero_QuatroAlgarismos() {
        int[] esperado = {1,2,3,4};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumero(1234);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumero_CincoAlgarismos() {
        int[] esperado = {1,2,3,4,5};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumero(12345);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumero_MaiorNumeroDeAlgarismos() {
        int[] esperado = {2,1,4,7,4,8,3,6,4,7};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumero(2147483647);
        assertArrayEquals(esperado,resultado);
    }

    /************************************************************************************************/

    @Test
    void verificarVetorComDigitosDoNumeroV2_UmAlgarismo() {
        int[] esperado = {0};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumeroV2(0);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumeroV2_DoisAlgarismos() {
        int[] esperado = {1,2};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumeroV2(12);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumeroV2_TresAlgarismos() {
        int[] esperado = {1,2,3};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumeroV2(123);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumeroV2_QuatroAlgarismos() {
        int[] esperado = {1,2,3,4};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumeroV2(1234);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumeroV2_CincoAlgarismos() {
        int[] esperado = {1,2,3,4,5};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumeroV2(12345);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComDigitosDoNumeroV2_MaiorNumeroDeAlgarismos() {
        int[] esperado = {2,1,4,7,4,8,3,6,4,7};
        int[] resultado = ExercicioDois.obterVetorComDigitosDoNumeroV2(2147483647);
        assertArrayEquals(esperado,resultado);
    }

}