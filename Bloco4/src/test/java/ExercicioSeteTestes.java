import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeteTestes {

    @Test
    void verificarVetorComMultiplosDeTres_UmMultiploTeste1() {
        int[] original = {0};
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(original);
        assertArrayEquals(esperado, resultado);
        assertNotSame(original, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_UmMultiploTeste2() {
        int[] original = {3};
        int[] esperado = {3};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(original);
        assertArrayEquals(esperado, resultado);
        assertNotSame(original, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_UmMultiploTeste3() {
        int[] original = {6};
        int[] esperado = {6};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(original);
        assertArrayEquals(esperado, resultado);
        assertNotSame(original, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_SemMultiplos() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(new int[]{1});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_UmMultiplo_DoisElementos() {
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(new int[]{0, 1});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_UmMultiplo_DoisElementosInvertido() {
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(new int[]{1, 0});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_DoisMultiplos_QuatroElementos() {
        int[] esperado = {0, 3};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(new int[]{0, 1, 2, 3});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_MultiplosPositivos() {
        int[] esperado = {0, 3, 6, 9};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComMultiplosDeTres_MultiplosNegativos() {
        int[] esperado = {0, -3, -6, -9};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeTres(new int[]{0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10});
        assertArrayEquals(esperado, resultado);
    }

    /**************************************************************************************************************/

    @Test
    void verificarVetorDefinidoUmIntervalo_LimitesIguaisZero() {
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(0, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_LimitesIguaisUm() {
        int[] esperado = {1};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(1, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_IntervaloCrescente_Positivos() {
        int[] esperado = {0, 1, 2, 3, 4, 5};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(0, 5);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_IntervaloCrescente_Negativos() {
        int[] esperado = {-5, -4, -3, -2, -1};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(-5, -1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_IntervaloCrescente_Negativo_Positivo() {
        int[] esperado = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(-5, 5);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_IntervaloDecrescente_Positivos() {
        int[] esperado = {5, 4, 3, 2, 1, 0};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(5, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_IntervaloDecrescente_Negativos() {
        int[] esperado = {-1, -2, -3, -4, -5};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(-1, -5);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorDefinidoUmIntervalo_IntervaloDecrescente_Positivo_Negativo() {
        int[] esperado = {5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5};
        int[] resultado = ExercicioSete.obterVetorDefinidoUmIntervalo(5, -5);
        assertArrayEquals(esperado, resultado);
    }

    /***********************************************************************************************/

    @Test
    void verificarMultiplosDeTresNumIntervalo_UmMultiplo_Zero() {
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(0, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeTresNumIntervalo_UmMultiplo_Tres() {
        int[] esperado = {3};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(3, 3);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeTresNumIntervalo_DoisMultiplo_IntervaloCrescente() {
        int[] esperado = {0, 3};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(0, 3);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeTresNumIntervalo_DoisMultiplo_IntervaloDecrescente() {
        int[] esperado = {3, 0};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(3, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeTresNumIntervalo_SemMultiplos_IntervaloIgual() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(1, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeTresNumIntervalo_SemMultiplos_IntervaloCrescente() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(7, 8);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeTresNumIntervalo_SemMultiplos_IntervaloDecrescente() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterMultiplosDeTresNumIntervalo(8, 7);
        assertArrayEquals(esperado, resultado);
    }

    /***********************************************************************************************/

    @Test
    void verificarMultiplosDeZero_Teste1() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{0}, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeZero_Teste2() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{-4, -3, -2, -1, 0, 1, 2, 3, 4}, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUm_Proprio() {
        int[] original = {1};
        int[] esperado = {1};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(original, 1);
        assertArrayEquals(esperado, resultado);
        assertNotSame(original, resultado);
    }

    @Test
    void verificarMultiplosDeUm_UmMultiplo() {
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{0}, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUm_DoisMultiplos() {
        int[] esperado = {0, 1};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{0, 1}, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUm_TresMultiplo() {
        int[] esperado = {-1, 0, 1};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{-1, 0, 1}, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUm_QuatroMultiplos() {
        int[] esperado = {-1, 0, 1, 2};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{-1, 0, 1, 2}, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUm_QuatroMultiplosInvertido() {
        int[] esperado = {2, 1, 0, -1};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{2, 1, 0, -1}, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeDois_VariosMultiplos() {
        int[] esperado = {2, 0, 32, 8, -4, 6, 100};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{5, 2, 0, 65, 21, 32, 8, 9, -4, 6, 100}, 2);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeDois_SemMultiplos() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterVetorComMultiplosDeDadoNumero(new int[]{1, 3, 5, -3, -5}, 2);
        assertArrayEquals(esperado, resultado);
    }

    /*************************************************************************************************/

    @Test
    void verificarMultiplosDeZeroNumIntervaloIgual() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(0, 0, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeZeroNumIntervaloDiferente() {
        int[] esperado = {};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(-1, 1, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUmNumIntervaloZero() {
        int[] esperado = {0};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(0, 0, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeUmNumIntervaloDiferente() {
        int[] esperado = {-1,0,1};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(-1, 1, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeDoisNumIntervaloCrescente() {
        int[] esperado = {-4,-2,0,2,4};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(-4, 4, 2);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeDoisNumIntervaloDecrescente() {
        int[] esperado = {4,2,0,-2,-4};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(4, -4, 2);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarMultiplosDeCinco() {
        int[] esperado = {-20,-15,-10,-5,0,5,10,15};
        int[] resultado = ExercicioSete.obterMultiplosDeNumeroDadoNumIntervalo(-20, 15, 5);
        assertArrayEquals(esperado, resultado);
    }





}

