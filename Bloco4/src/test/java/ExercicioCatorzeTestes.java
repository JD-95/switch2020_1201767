import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ExercicioCatorzeTestes {

    @Test
    void verificarMatrizNaoRetangular_LinhasVazias() {
        int[][] matriz = {{}, {}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarMatrizNaoRetangular_LinhaVazia() {
        int[][] matriz = {{}, {0}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarMatrizNaoRetangular_DuasLinhasIgualColunas() {
        int[][] matriz = {{1, 1}, {1, 1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarMatrizNaoRetangular_TresLinhasIgualColunas() {
        int[][] matriz = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertFalse(resultado);
    }


    @Test
    void verificarMatrizRetangular_DuasLinhasUmaColuna() {
        int[][] matriz = {{1}, {1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizRetangular_TresLinhasUmaColuna() {
        int[][] matriz = {{1}, {1}, {1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizRetangular_TresLinhasDuasColunas() {
        int[][] matriz = {{1, 1}, {1, 1}, {1, 1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizRetangular_DuasLinhasTresColunas() {
        int[][] matriz = {{1, 1, 1}, {1, 1, 1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizRetangular_DuasLinhasQuatroColunas() {
        int[][] matriz = {{1, 1, 1, 1}, {1, 1, 1, 1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizRetangular_TresLinhasQuatroColunas() {
        int[][] matriz = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
        boolean resultado = ExercicioCatorze.verificarMatrizRetangular(matriz);
        assertTrue(resultado);
    }
}