import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioNoveTestes {

    @Test
    void verificarSeECapicua_Zero() {
        boolean resultado = ExercicioNove.verificarSeECapicua(0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeECapicua_Nove() {
        boolean resultado = ExercicioNove.verificarSeECapicua(0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeECapicua_Dez() {
        boolean resultado = ExercicioNove.verificarSeECapicua(10);
        assertFalse(resultado);
    }

    @Test
    void verificarSeECapicua_Onze() {
        boolean resultado = ExercicioNove.verificarSeECapicua(11);
        assertTrue(resultado);
    }

    @Test
    void verificarSeECapicua_Doze() {
        boolean resultado = ExercicioNove.verificarSeECapicua(12);
        assertFalse(resultado);
    }

    @Test
    void verificarSeECapicua_VinteEDois() {
        boolean resultado = ExercicioNove.verificarSeECapicua(22);
        assertTrue(resultado);
    }

    @Test
    void verificarSeECapicua_Cemm() {
        boolean resultado = ExercicioNove.verificarSeECapicua(100);
        assertFalse(resultado);
    }

    @Test
    void verificarSeECapicua_CentoEUm() {
        boolean resultado = ExercicioNove.verificarSeECapicua(101);
        assertTrue(resultado);
    }
}