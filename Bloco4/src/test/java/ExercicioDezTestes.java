import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezTestes {

    @Test
    void verificarElementoDeMenorValor_VetorVazio() {
        int[] vetor = {};
        Integer esperado = null;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }


    @Test
    void verificarElementoDeMenorValor_UmElemento_Zero() {
        int[] vetor = {0};
        Integer esperado = 0;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_UmElemento_Positivo() {
        int[] vetor = {1};
        Integer esperado = 1;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_UmElemento_Negativo() {
        int[] vetor = {-1};
        Integer esperado = -1;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_DoisElementos_Iguais() {
        int[] vetor = {1,1};
        Integer esperado = 1;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_DoisElementos_Positivos() {
        int[] vetor = {5,3};
        Integer esperado = 3;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_DoisElementos_Negativos() {
        int[] vetor = {-1,-4};
        Integer esperado = -4;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_DoisElementos_PositivoNegativo() {
        int[] vetor = {2,-2};
        Integer esperado = -2;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarElementoDeMenorValor_VariosElementos() {
        int[] vetor = {0,1,8,-2,-1};
        Integer esperado = -2;
        Integer resultado = ExercicioDez.obterElementoDeMenorValor(vetor);
        assertEquals(esperado, resultado);
    }

    /*********************************************************************************************************************/

    @Test
    void verificarElementoDeMaiorValor_VetorVazio() {
        Integer esperado = null;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_UmElemento_Zero() {
        Integer esperado = 0;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {0});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_UmElemento_Positivo() {
        Integer esperado = 1;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_UmElemento_Negativo() {
        Integer esperado = -1;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {-1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_DoisElementos_Iguais() {
        Integer esperado = 1;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {1,1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_DoisElementos_Positivos() {
        Integer esperado = 8;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {8,2});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_DoisElementos_Negativos() {
        Integer esperado = -3;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {-3,-7});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_DoisElementos_NegativoPositivo() {
        Integer esperado = 4;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {-4,4});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarElementoDeMaiorValor_VariosElementos() {
        Integer esperado = 5;
        Integer resultado = ExercicioDez.obterElementoDeMaiorValor(new int[] {2,-8,0,-6,-3,5,1});
        assertEquals(esperado,resultado);
    }

    /******************************************************************************************************/


    @Test
    void verificarMediaDosElementos_VetorVazio() {
        int[] vetor = {};
        Double esperado = null;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarMediaDosElementos_UmElemento_Zero() {
        int[] vetor = {0};
        Double esperado = 0.0;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarMediaDosElementos_UmElemento_Negativo() {
        int[] vetor = {-1};
        Double esperado = -1.0;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarMediaDosElementos_UmElemento_Positivo() {
        int[] vetor = {1};
        Double esperado = 1.0;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarMediaDosElementos_DoisElementos_ZeroUm() {
        int[] vetor = {0,1};
        double esperado = 0.5;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosElementos_DoisElementos_NegUmZero() {
        int[] vetor = {-1,0};
        double esperado = -0.5;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosElementos_DoisElementos_Iguais() {
        int[] vetor = {1,1};
        Double esperado = 1.0;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarMediaDosElementos_DoisElementos_Positivos() {
        int[] vetor = {1,2};
        double esperado = 1.5;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosElementos_DoisElementos_Negativos() {
        int[] vetor = {-1,-2};
        double esperado = -1.5;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosElementos_DoisElementos_NegativoPositivo() {
        int[] vetor = {-4,2};
        double esperado = -1.0;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosElementos_VariosElementos() {
        int[] vetor = {-4,7,3,42,595,-423,22,0,151};
        double esperado = 43.66;
        Double resultado = ExercicioDez.obterMediaDosElementos(vetor);
        assertEquals(esperado,resultado,0.01);
    }

    /****************************************************************************************************/

    @Test
    void verificarProdutoDosElementos_VetorVazio() {
        int[] vetor = {};
        Integer esperado = null;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_UmElemento_Zero() {
        int[] vetor = {0};
        Integer esperado = 0;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_UmElemento_Positivo() {
        int[] vetor = {1};
        Integer esperado = 1;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_UmElemento_Negativo() {
        int[] vetor = {-1};
        Integer esperado = -1;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_VariosElementos_ZeroEnvolvido() {
        int[] vetor = {0,1,-1,2,3,5};
        Integer esperado = 0;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_DoisElementos_Positivos() {
        int[] vetor = {1,3};
        Integer esperado = 3;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_DoisElementos_Negativos() {
        int[] vetor = {-1,-3};
        Integer esperado = 3;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_DoisElementos_NegativoPositivo() {
        int[] vetor = {-1,3};
        Integer esperado = -3;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_VariosElementos_ResultadoNegativo() {
        int[] vetor = {59,-22,6,4,-11,542,-8};
        Integer esperado = -1485825792;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoDosElementos_VariosElementos_ResultadoPositivo() {
        int[] vetor = {59,-22,6,4,11,542,-8};
        Integer esperado = 1485825792;
        Integer resultado = ExercicioDez.obterProdutoDosElementos(vetor);
        assertEquals(esperado,resultado);
    }

    /****************************************************************************************************/

    @Test
    void verificarVetorComElementosNaoRepetidos_VetorVazio() {
        int[] original = {};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(null,resultado);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_UmElemento_Zero() {
        int[] original = {0};
        int[] esperado = {0};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_UmElemento_Positivo() {
        int[] original = {1};
        int[] esperado = {1};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_UmElemento_Negativo() {
        int[] original = {-1};
        int[] esperado = {-1};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_DoisElementos_RepetidosPositivos() {
        int[] original = {1,1};
        int[] esperado = {1};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_DoisElementos_RepetidosNegativos() {
        int[] original = {-1,-1};
        int[] esperado = {-1};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_VariosElementos_Repetidos() {
        int[] original = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
        int[] esperado = {1};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_DoisElementos_NaoRepetidos() {
        int[] original = {1,2};
        int[] esperado = {1,2};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_DoisElementos_NaoRepetidos_PositivoNegativo() {
        int[] original = {-2,2};
        int[] esperado = {-2,2};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosNaoRepetidos_VariosElementos() {
        int[] original = {2,35,1548,256,-5,2,65,-256,-1548,65,35,-5};
        int[] esperado = {2,35,1548,256,-5,65,-256,-1548};
        int[] resultado = ExercicioDez.obterVetorComElementosNaoRepetidos(original);
        assertArrayEquals(esperado,resultado);
    }

    /*********************************************************************************************************/

    @Test
    void verificarVetorInvertido_VetorVazio() {
        int[] original = {};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(null,resultado);
    }

    @Test
    void verificarVetorInvertido_UmElemento_Zero() {
        int[] original = {0};
        int[] esperado = {0};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorInvertido_UmElemento_Positivo() {
        int[] original = {1};
        int[] esperado = {1};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorInvertido_UmElemento_Negativo() {
        int[] original = {-1};
        int[] esperado = {-1};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorInvertido_DoisElementos_Iguais() {
        int[] original = {1,1};
        int[] esperado = {1,1};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorInvertido_DoisElementos_Positivos() {
        int[] original = {2,6};
        int[] esperado = {6,2};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorInvertido_DoisElementos_Negativos() {
        int[] original = {-5,-2};
        int[] esperado = {-2,-5};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorInvertido_DoisElementos_NegativoPositivo() {
        int[] original = {-2,2};
        int[] esperado = {2,-2};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorInvertido_TresElementos() {
        int[] original = {2,-6,10};
        int[] esperado = {10,-6,2};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorInvertido_VariosElementos() {
        int[] original = {2,-5,10,546,28,2168,5,-888,0};
        int[] esperado = {0,-888,5,2168,28,546,10,-5,2};
        int[] resultado = ExercicioDez.obterVetorInvertido(original);
        assertArrayEquals(esperado,resultado);
    }

    /*********************************************************************************************************************/

    @Test
    void verificarSeEPrimo_Zero() {
        int numero = 0;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeEPrimo_Um() {
        int numero = 1;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeEPrimo_Dois() {
        int numero = 2;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertTrue(resultado);
    }

    @Test
    void verificarSeEPrimo_Tres() {
        int numero = 3;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertTrue(resultado);
    }

    @Test
    void verificarSeEPrimo_Quatro() {
        int numero = 4;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeEPrimo_Cinco() {
        int numero = 5;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertTrue(resultado);
    }

    @Test
    void verificarSeEPrimo_Seis() {
        int numero = 6;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertFalse(resultado);
    }

    @Test
    void verificarSeEPrimo_Sete() {
        int numero = 7;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertTrue(resultado);
    }

    @Test
    void verificarSeEPrimo_MaiorAteMil() {
        int numero = 997;
        boolean resultado = ExercicioDez.verificarSeEPrimo(numero);
        assertTrue(resultado);
    }

    /***************************************************************************************************/

    @Test
    void verificarVetorComElementosPrimos_VetorVazio() {
        int[] original = {};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(null,resultado);
    }

    @Test
    void verificarVetorComElementosPrimos_UmElemento_Zero() {
        int[] original = {0};
        int[] esperado = {};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosPrimos_UmElemento_UmPositivo() {
        int[] original = {1};
        int[] esperado = {};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosPrimos_UmElemento_UmNegativo() {
        int[] original = {-1};
        int[] esperado = {};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosPrimos_UmElemento_Dois() {
        int[] original = {2};
        int[] esperado = {2};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosPrimos_UmElemento_Tres() {
        int[] original = {3};
        int[] esperado = {3};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosPrimos_DoisElementos_Primos() {
        int[] original = {2,3};
        int[] esperado = {2,3};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosPrimos_DoisElementos_PrimosRepetidos() {
        int[] original = {2,2};
        int[] esperado = {2,2};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
        assertNotSame(resultado,original);
    }

    @Test
    void verificarVetorComElementosPrimos_DoisElementos_NaoPrimos() {
        int[] original = {1,4};
        int[] esperado = {};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosPrimos_DoisElementos_PrimoENaoPrimo() {
        int[] original = {2,1};
        int[] esperado = {2};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComElementosPrimos_VariosElementos() {
        int[] original = {6,-2,0,1,13,997,521,250,37};
        int[] esperado = {13,997,521,37};
        int[] resultado = ExercicioDez.obterVetorComElementosPrimos(original);
        assertArrayEquals(esperado,resultado);
    }




}