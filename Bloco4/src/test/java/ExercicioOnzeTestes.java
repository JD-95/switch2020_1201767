import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOnzeTestes {

    @Test
    void verificarProdutoEscalar_VetoresVazios() {
        double[] vetorA = {};
        double[] vetorB = {};
        Double esperado = null;
        Double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetorAVazio() {
        double[] vetorA = {};
        double[] vetorB = {1};
        Double esperado = null;
        Double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetorBVazio() {
        double[] vetorA = {1};
        double[] vetorB = {};
        Double esperado = null;
        Double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_TamanhosDiferentes_TesteUm() {
        double[] vetorA = {1};
        double[] vetorB = {1,2};
        Double esperado = null;
        Double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }


    @Test
    void verificarProdutoEscalar_TamanhosDiferentes_TesteDois() {
        double[] vetorA = {1,2};
        double[] vetorB = {1};
        Double esperado = null;
        Double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresComZero() {
        double[] vetorA = {0};
        double[] vetorB = {0};
        double esperado = 0;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetorAComZero() {
        double[] vetorA = {0};
        double[] vetorB = {1};
        double esperado = 0;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetorBComZero() {
        double[] vetorA = {1};
        double[] vetorB = {0};
        double esperado = 0;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoPositivoIgual() {
        double[] vetorA = {1};
        double[] vetorB = {1};
        double esperado = 1;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNaoInteiroPositivoIgual() {
        double[] vetorA = {0.5};
        double[] vetorB = {0.5};
        double esperado = 0.25;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoPositivoDiferente() {
        double[] vetorA = {1};
        double[] vetorB = {2};
        double esperado = 2;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNaoInteiroPositivoDiferente() {
        double[] vetorA = {0.5};
        double[] vetorB = {1.5};
        double esperado = 0.75;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNegativoIgual() {
        double[] vetorA = {-1};
        double[] vetorB = {-1};
        double esperado = 1;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNaoInteiroNegativoIgual() {
        double[] vetorA = {-0.5};
        double[] vetorB = {-0.5};
        double esperado = 0.25;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNegativoDiferente() {
        double[] vetorA = {-2};
        double[] vetorB = {-1};
        double esperado = 2;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNaoInteiroNegativoDiferente() {
        double[] vetorA = {-1.5};
        double[] vetorB = {-0.5};
        double esperado = 0.75;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresUmElementoNegativoPositivo() {
        double[] vetorA = {1};
        double[] vetorB = {-1};
        double esperado = -1;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresDoisElementos_TesteUm() {
        double[] vetorA = {1,2};
        double[] vetorB = {1,2};
        double esperado = 5;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresDoisElementos_TesteDois() {
        double[] vetorA = {2,1};
        double[] vetorB = {1,2};
        double esperado = 4;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresDoisElementos_TesteTres() {
        double[] vetorA = {1,2};
        double[] vetorB = {2,1};
        double esperado = 4;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarProdutoEscalar_VetoresVariosElementos() {
        double[] vetorA = {10,-5.3,2,1,22.5};
        double[] vetorB = {3.98,35,-6,11.25,-7};
        double esperado = -303.95;
        double resultado = ExercicioOnze.obterProdutoEscalar(vetorA,vetorB);
        assertEquals(esperado,resultado,0.01);
    }
}