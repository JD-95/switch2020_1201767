import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ExercicioCincoTestes {

    @Test
    void verificarSomaDosElementosVetor_Teste1() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{0});
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Teste2() {
        int esperado = 1;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{0, 1});
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Teste3() {
        int esperado = 3;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{0, 1, 2});
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Teste4() {
        int esperado = 6;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{0, 1, 2, 3});
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Teste5() {
        int esperado = 6;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{3, 2, 1, 0});
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Teste6() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{0, 0, 0, 0});
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Teste7() {
        int esperado = 4;
        int resultado = ExercicioCinco.obterSomaDosElementosVetor(new int[]{1, 1, 1, 1});
        assertEquals(esperado, resultado);
    }

    /****************************************************************************************************/

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste1() {
        int[] esperado = {0} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(0);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste2() {
        int[] esperado = {1} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(1);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste3() {
        int[] esperado = {2} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(2);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste4() {
        int[] esperado = {1,0} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(10);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste5() {
        int[] esperado = {1,0,2} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(102);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste6() {
        int[] esperado = {1,0,2,8} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(1028);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste7() {
        int[] esperado = {9,8,7} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(987);
        assertArrayEquals(esperado,resultado);
    }

    @Test
    void verificarVetorComOsDigitosDeUmNumero_Teste8() {
        int[] esperado = {7,8,9} ;
        int[] resultado = ExercicioCinco.obterVetorComOsDigitosDeUmNumero(789);
        assertArrayEquals(esperado,resultado);
    }

    /****************************************************************************************************/

    @Test
    void verificarSomaDosAlgarismosPares_Teste1() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste2() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste3() {
        int esperado = 2;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste4() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(3);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste5() {
        int esperado = 4;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(4);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste6() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste7() {
        int esperado = 2;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(12);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste8() {
        int esperado = 4;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(104);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste9() {
        int esperado = 12;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(2460);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Teste10() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosPares(1539);
        assertEquals(esperado,resultado);
    }

    /************************************************************************************/

    @Test
    void verificarSomaDosAlgarismosImpares_Teste1() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste2() {
        int esperado = 1;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste3() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste4() {
        int esperado = 3;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(3);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste5() {
        int esperado = 1;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste6() {
        int esperado = 2;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(11);
        assertEquals(esperado,resultado);
    }
    @Test
    void verificarSomaDosAlgarismosImpares_Teste7() {
        int esperado = 1;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(12);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste8() {
        int esperado = 3;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(300);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste9() {
        int esperado = 9;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(135);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste10() {
        int esperado = 9;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(351);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Teste11() {
        int esperado = 0;
        int resultado = ExercicioCinco.obterSomaDosAlgarismosImpares(2468);
        assertEquals(esperado,resultado);
    }
}