import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioSeisTestes {

    @Test
    void verificarCopiaDeVetor_TamanhoInferiorAZero() {
        int[] original = {0, 1, 2, 3};
        int[] esperado = null;
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, -1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCopiaDeVetor_VetorOriginalNulo() {
        int[] original = null;
        int[] esperado = null;
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, 1);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCopiaDeVetor_TamanhoSuperiorAoVetorOriginal() {
        int[] original = {0, 1, 2, 3};
        int[] esperado = {0, 1, 2, 3};
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, 5);
        assertArrayEquals(esperado, resultado);
        assertEquals(esperado.length, resultado.length);
        assertNotSame(resultado, original);
    }


    @Test
    void verificarCopiaDeVetor_TamanhoIgualAoVetorOriginal() {
        int[] original = {0, 1, 2, 3};
        int[] esperado = {0, 1, 2, 3};
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, 4);
        assertArrayEquals(esperado, resultado);
        assertEquals(esperado.length, resultado.length);
        assertNotSame(resultado, original);
    }


    @Test
    void verificarCopiaDeVetor_TamanhoInferiorAoVetorOriginal() {
        int[] original = {0, 1, 2, 3};
        int[] esperado = {0, 1, 2};
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, 3);
        assertArrayEquals(esperado, resultado);
        assertEquals(esperado.length, resultado.length);
    }

    @Test
    void verificarCopiaDeVetor_VetorOriginalVazio_TamanhoIgual() {
        int[] original = {};
        int[] esperado = {};
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, 0);
        assertArrayEquals(esperado, resultado);
        assertEquals(esperado.length, resultado.length);
        assertNotSame(resultado, original);
    }

    @Test
    void verificarCopiaDeVetor_VetorOriginalVazio_TamanhoInferior() {
        int[] original = {};
        int[] esperado = null;
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, -1);
        assertArrayEquals(esperado, resultado);
        assertNotSame(resultado, original);
    }

    @Test
    void verificarCopiaDeVetor_VetorOriginalVazio_TamanhoSuperior() {
        int[] original = {};
        int[] esperado = {};
        int[] resultado = ExercicioSeis.obterCopiaDeVetor(original, 1);
        assertArrayEquals(esperado, resultado);
        assertEquals(esperado.length, resultado.length);
        assertNotSame(resultado, original);
    }
}