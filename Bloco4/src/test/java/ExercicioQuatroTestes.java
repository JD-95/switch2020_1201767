import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ExercicioQuatroTestes {

    @Test
    void verificarVetorComNumerosPares_SemPares() {
        int[] esperado = {};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosPares(new int[]{1, 3, 5});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosPares_Um() {
        int[] esperado = {2};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosPares(new int[]{1, 2, 3});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosPares_Dois() {
        int[] esperado = {0, 2};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosPares(new int[]{0, 2, 3});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosPares_Tres() {
        int[] esperado = {0, 2, 8};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosPares(new int[]{0, 2, 8});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosPares_Quatro() {
        int[] esperado = {112,0,-10,50,1500632};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosPares(new int[]{1,112,23,0,-10,50,1500632});
        assertArrayEquals(esperado, resultado);
    }

    /****************************************************************************************/

    @Test
    void verificarVetorComNumerosImpares_SemImpares() {
        int[] esperado = {};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosImpares(new int[]{0, 2, 4});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosImpares_Um() {
        int[] esperado = {1};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosImpares(new int[]{0, 1, 4});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosImpares_Dois() {
        int[] esperado = {3,5};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosImpares(new int[]{3, 5, 4});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComNumerosImpares_Tres() {
        int[] esperado = {9,3,7};
        int[] resultado = ExercicioQuatro.obterVetorComNumerosImpares(new int[]{9, 3, 7});
        assertArrayEquals(esperado, resultado);
    }

    /*************************************************************************************/

    @Test
    void verificarSeZeroEPar() {
        boolean resultado = ExercicioQuatro.verificarSeEPar(0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNegUmEPar() {
        boolean resultado = ExercicioQuatro.verificarSeEPar(-1);
        assertFalse(resultado);
    }

    @Test
    void verificarSeNegDoisEPar() {
        boolean resultado = ExercicioQuatro.verificarSeEPar(-2);
        assertTrue(resultado);
    }

    @Test
    void verificarSeUmEPar() {
        boolean resultado = ExercicioQuatro.verificarSeEPar(1);
        assertFalse(resultado);
    }

    @Test
    void verificarSeDoisEPar() {
        boolean resultado = ExercicioQuatro.verificarSeEPar(2);
        assertTrue(resultado);
    }


}