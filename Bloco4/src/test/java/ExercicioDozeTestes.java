import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDozeTestes {

    @Test
    void verificarNumeroDeColunasMatriz_LinhasVazias() {
        int[][] matriz = {{},{}};
        int esperado = -1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_LinhaVazia_TesteUm() {
        int[][] matriz = {{},{1}};
        int esperado = -1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_LinhaVazia_TesteDois() {
        int[][] matriz = {{1},{}};
        int esperado = -1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_ColunasDiferentes_TesteUm() {
        int[][] matriz = {{1,1},{1}};
        int esperado = -1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_ColunasDiferentes_TesteDois() {
        int[][] matriz = {{1},{1,1}};
        int esperado = -1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_ColunasDiferentes_TesteTres() {
        int[][] matriz = {{1,2,0},{1,-5}, {25,1550,-55}};
        int esperado = -1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_ColunasIguais_TesteUm() {
        int[][] matriz = {{1},{1}};
        int esperado = 1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_ColunasIguais_TesteDois() {
        int[][] matriz = {{1},{-1}};
        int esperado = 1;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_TresColunas_TesteUm() {
        int[][] matriz = {{25,11,-56},{0,0,0},{-5,3,22}, {25,11,-56}};
        int esperado = 3;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeColunasMatriz_TresColunas_TesteDois() {
        int[][] matriz = {{25,11,-56},{0,0,0}};
        int esperado = 3;
        int resultado = ExercicioDoze.obterNumeroDeColunasMatriz(matriz);
        assertEquals(esperado,resultado);
    }


}