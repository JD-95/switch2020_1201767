import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioTresTestes {

    @Test
    void verificarSomaDosElementosVetor_Zero() {
        int esperado = 0;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {0});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Um() {
        int esperado = 1;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Zero_Um() {
        int esperado = 1;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {0,1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_Um_Zero() {
        int esperado = 1;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {1,0});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_TresElementos() {
        int esperado = 6;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {1,2,3});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_QuatroElementos() {
        int esperado = 10;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {1,2,3,4});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_CincoElementos() {
        int esperado = 15;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {5,4,3,2,1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_NumeroNegativoTeste1() {
        int esperado = 0;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {-1,1});
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosElementosVetor_NumeroNegativoTeste2() {
        int esperado = -3;
        int resultado = ExercicioTres.obterSomaDosElementosVetor(new int[] {-4,1});
        assertEquals(esperado,resultado);
    }
}