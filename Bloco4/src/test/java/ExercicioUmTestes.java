import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioUmTestes {

    @Test
    void verificarNumeroDeDigitosDeZero() {
        int esperado = 1;
        int resultado = ExercicioUm.obterNumeroDeDigitos(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeUm() {
        int esperado = 1;
        int resultado = ExercicioUm.obterNumeroDeDigitos(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeNove() {
        int esperado = 1;
        int resultado = ExercicioUm.obterNumeroDeDigitos(9);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeDez() {
        int esperado = 2;
        int resultado = ExercicioUm.obterNumeroDeDigitos(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeNoventaENove() {
        int esperado = 2;
        int resultado = ExercicioUm.obterNumeroDeDigitos(99);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeCem() {
        int esperado = 3;
        int resultado = ExercicioUm.obterNumeroDeDigitos(100);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeNovecentosENoventaENove() {
        int esperado = 3;
        int resultado = ExercicioUm.obterNumeroDeDigitos(999);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeMil() {
        int esperado = 4;
        int resultado = ExercicioUm.obterNumeroDeDigitos(1000);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeDigitosDeMaiorIntPossivel() {
        int esperado = 10;
        int resultado = ExercicioUm.obterNumeroDeDigitos(2147483647);
        assertEquals(esperado,resultado);
    }
}
