import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ExercicioTrezeTestes {


    @Test
    void verificarMatrizQuadrada_LinhaVazia() {
        int[][] matriz = {{0},{}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarMatrizQuadrada_LinhasDiferentes_TesteUm() {
        int[][] matriz = {{1}, {1}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarMatrizQuadrada_LinhasDiferentes_TesteDois() {
        int[][] matriz = {{1, 1}, {1, 1}, {1, 1}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNaoMatrizQuadrada_ColunasDiferentes_TesteUm() {
        int[][] matriz = {{1, 1}, {1}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNaoMatrizQuadrada_ColunasDiferentes_TesteDois() {
        int[][] matriz = {{1}, {1, 1}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNaoMatrizQuadrada_ColunasDiferentes_TesteTres() {
        int[][] matriz = {{7}, {-52, 0}, {152}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNaoMatrizQuadrada_ColunasDiferentes_TesteQuatro() {
        int[][] matriz = {{7, 2525, -999}, {-52, 0}, {152}, {120, -6, 22, 58}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarMatrizQuadrada_DuasLinhasColunas() {
        int[][] matriz = {{1, 1}, {1, 1}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizQuadrada_TresLinhasColunas() {
        int[][] matriz = {{55, 2, 6}, {-9, 33, -5555}, {0, 0, 0}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarMatrizQuadrada_QuatroLinhasColunas() {
        int[][] matriz = {{55, 2, 6, 22}, {-9, 33, -5555, 0}, {0, 0, 0, 0}, {564, 21, -999, 6}};
        boolean resultado = ExercicioTreze.verificarMatrizQuadrada(matriz);
        assertTrue(resultado);
    }

    /**********************************************************************************************************/

    @Test
    void verificarNumeroColunasIgualEmCadaLinha_LinhasVazias() {
        int[][] matriz = {{}, {}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNumeroColunasIgualEmCadaLinha_UmaLinhaVazia_TesteUm() {
        int[][] matriz = {{}, {0}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNumeroColunasIgualEmCadaLinha_UmaLinhaVazia_TesteDois() {
        int[][] matriz = {{0}, {}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNumeroColunasDiferentes_TesteUm() {
        int[][] matriz = {{1}, {1, 1}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNumeroColunasDiferentes_TesteDois() {
        int[][] matriz = {{1, 1}, {1}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNumeroColunasDiferentes_TesteTres() {
        int[][] matriz = {{1, 1}, {1}, {1, 1}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertFalse(resultado);
    }

    @Test
    void verificarNumeroDuasColunasIgualEmCadaLinha() {
        int[][] matriz = {{1, 1}, {1, 1}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarNumeroTresColunasIgualEmCadaLinha() {
        int[][] matriz = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertTrue(resultado);
    }

    @Test
    void verificarNumeroQuatroColunasIgualEmCadaLinha() {
        int[][] matriz = {{1, -55, 0, 1236}, {-55, 10, -888, 0}, {1, 2, 3, 4}, {1, 2, 3, 4}};
        boolean resultado = ExercicioTreze.verificarNumeroColunasIgualEmCadaLinha(matriz);
        assertTrue(resultado);
    }

}