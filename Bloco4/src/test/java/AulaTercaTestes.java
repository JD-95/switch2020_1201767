
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AulaTercaTestes {

    @Test
    void verificarcopiarVetor_Teste1() {
        int[] original = {3,5,7};
        int[] esperado = {3,5,7};
        int[] resultado = AulaTerca.copiarVetor(original,5 );
        assertArrayEquals(esperado, resultado);             // garantir que o esperado é igual ao resultado
        assertEquals(esperado.length, resultado.length);    // garantir que o tamanho do esperado e do resultado são iguais
        assertNotSame(resultado,original);                  // garantir que o resultado não é o mesmo do original
    }

}