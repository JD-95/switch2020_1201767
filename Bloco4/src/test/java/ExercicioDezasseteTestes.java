import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioDezasseteTestes {

    @Test
    void obterProdutoMatrizPorConstante_MatrizNula() {
        int[][] matriz = null;
        int constante = 1;
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertNull(resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizVazia() {
        int[][] matriz = {};
        int constante = 1;
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertNull(resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizQuadradaOrdemUm_ConstanteUm() {
        int[][] matriz = {{1}};
        int constante = 1;
        int[][] esperado = {{1}};
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizQuadradaOrdemUm_ConstanteZero() {
        int[][] matriz = {{1}};
        int constante = 0;
        int[][] esperado = {{0}};
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizQuadradaOrdemDois() {
        int[][] matriz = {{1, -1}, {0, 10}};
        int constante = 2;
        int[][] esperado = {{2, -2}, {0, 20}};
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizRetangular() {
        int[][] matriz = {{1, -1}, {0, 10}, {-5, 21}};
        int constante = -2;
        int[][] esperado = {{-2, 2}, {0, -20}, {10, -42}};
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizComLinhaVazia() {
        int[][] matriz = {{1, -1}, {}, {-5, 21}};
        int constante = -2;
        int[][] esperado = {{-2, 2}, {}, {10, -42}};
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoMatrizPorConstante_MatrizComColunasDiferentesEmCadaLinha() {
        int[][] matriz = {{1, -1}, {7}, {-5, 21, 0}};
        int constante = 3;
        int[][] esperado = {{3, -3}, {21}, {-15, 63, 0}};
        int[][] resultado = ExercicioDezassete.obterProdutoMatrizPorConstante(matriz, constante);
        assertArrayEquals(esperado, resultado);
    }

    /**************************************************************************************************************/

    @Test
    void matrizesComMesmaEstrutura_MatrizNula() {
        int[][] matrizA = null;
        int[][] matrizB = {{1}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertFalse(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_MatrizVazia() {
        int[][] matrizA = {};
        int[][] matrizB = {{1}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertFalse(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_MatrizBMaisColunas() {
        int[][] matrizA = {{0}};
        int[][] matrizB = {{1, 2}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertFalse(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_MatrizBMaisLinhas() {
        int[][] matrizA = {{0}};
        int[][] matrizB = {{1}, {2}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertFalse(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_MatrizQuadradaOrdemUm() {
        int[][] matrizA = {{0}};
        int[][] matrizB = {{1}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertTrue(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_MatrizQuadradaOrdemDois() {
        int[][] matrizA = {{0, 1}, {2, 3}};
        int[][] matrizB = {{4, 5}, {6, 7}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertTrue(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_MatrizRetangular() {
        int[][] matrizA = {{0, 1}, {2, 3}, {4, 5}};
        int[][] matrizB = {{6, 7}, {8, 9}, {10, 11}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertTrue(resultado);
    }

    @Test
    void matrizesComMesmaEstrutura_LinhaVazia() {
        int[][] matrizA = {{0, 1}, {}, {4, 5}};
        int[][] matrizB = {{6, 7}, {}, {10, 11}};
        boolean resultado = ExercicioDezassete.matrizesComMesmaEstrutura(matrizA, matrizB);
        assertTrue(resultado);
    }

    /***************************************************************************************/

    @Test
    void obterSomaDuasMatrizes_MatrizNula() {
        int[][] matrizA = null;
        int[][] matrizB = {{1}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterSomaDuasMatrizes_MatrizVazia() {
        int[][] matrizA = {};
        int[][] matrizB = {{1}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterSomaDuasMatrizes_MatrizSemAMesmaEstrutura() {
        int[][] matrizA = {{1, 2}};
        int[][] matrizB = {{1}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterSomaDuasMatrizes_MatrizQuadradaOrdemUm() {
        int[][] matrizA = {{0}};
        int[][] matrizB = {{1}};
        int[][] esperado = {{1}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterSomaDuasMatrizes_MatrizQuadradaOrdemDois() {
        int[][] matrizA = {{0, 1}, {-5, 2}};
        int[][] matrizB = {{-2, -3}, {1, 3}};
        int[][] esperado = {{-2, -2}, {-4, 5}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterSomaDuasMatrizes_MatrizRetangular() {
        int[][] matrizA = {{0, 1}, {-5, 2}, {-10, 3}};
        int[][] matrizB = {{-2, -3}, {1, 3}, {-5, 7}};
        int[][] esperado = {{-2, -2}, {-4, 5}, {-15, 10}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterSomaDuasMatrizes_ColunasDiferentesEmCadaLinha() {
        int[][] matrizA = {{0, 1}, {}, {-10, 3, 25}};
        int[][] matrizB = {{-2, -3}, {}, {-5, 7, 11}};
        int[][] esperado = {{-2, -2}, {}, {-15, 10, 36}};
        int[][] resultado = ExercicioDezassete.obterSomaDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    /**********************************************************************************************************/

    @Test
    void obterProdutoDuasMatrizes_Nula() {
        int[][] matrizA = {{1, 2}, {3, 4}};
        int[][] matrizB = null;
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_Vazia() {
        int[][] matrizA = {};
        int[][] matrizB = {{1, 2}, {3, 4}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_ColunasDiferentes() {
        int[][] matrizA = {{1, 2}, {3, 4}};
        int[][] matrizB = {{1, 2}, {3}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_NumeroDeColunasADiferenteLinhasB() {
        int[][] matrizA = {{1, 2}};
        int[][] matrizB = {{1, 2}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertNull(resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_QuadradaOrdemUm() {
        int[][] matrizA = {{1}};
        int[][] matrizB = {{2}};
        int[][] esperado = {{2}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_QuadradaOrdemDois() {
        int[][] matrizA = {{1, 0}, {2, -2}};
        int[][] matrizB = {{-2, 5}, {5, -1}};
        int[][] esperado = {{-2, 5}, {-14, 12}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_Retangular_TesteUm() {
        int[][] matrizA = {{1, -2}};
        int[][] matrizB = {{-2}, {-1}};
        int[][] esperado = {{0}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterProdutoDuasMatrizes_Retangular_TesteDois() {
        int[][] matrizA = {{1, -2, 5}, {20, -10, 3}};
        int[][] matrizB = {{-2, 5}, {-1, 0}, {2, 7}};
        int[][] esperado = {{10, 40}, {-24, 121}};
        int[][] resultado = ExercicioDezassete.obterProdutoDuasMatrizes(matrizA, matrizB);
        assertArrayEquals(esperado, resultado);
    }


    /**********************************************************************************************************/


    @Test
    void obterVetorLinhaMatriz_LinhaNaoValida() {
        int[][] matriz = {{1, 0, 2}, {0, 1, 3}};
        int[] resultado = ExercicioDezassete.obterVetorLinhaMatriz(matriz, 2);
        assertNull(resultado);
    }

    @Test
    void obterVetorLinhaMatriz_UmElemento() {
        int[][] matriz = {{1}};
        int[] esperado = {1};
        int[] resultado = ExercicioDezassete.obterVetorLinhaMatriz(matriz, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterVetorLinhaMatriz_DoisElementos() {
        int[][] matriz = {{1, 0}};
        int[] esperado = {1, 0};
        int[] resultado = ExercicioDezassete.obterVetorLinhaMatriz(matriz, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterVetorLinhaMatriz_TresElementos() {
        int[][] matriz = {{1, 0, 2}, {0, 1, 3}};
        int[] esperado = {0, 1, 3};
        int[] resultado = ExercicioDezassete.obterVetorLinhaMatriz(matriz, 1);
        assertArrayEquals(esperado, resultado);
    }

    /**************************************************************************************************/

    @Test
    void obterVetorColunaMatriz_ColunaNaoValida() {
        int[][] matriz = {{1, 0, 2}, {0, 1, 3}};
        int[] resultado = ExercicioDezassete.obterVetorColunaMatriz(matriz, 3);
        assertNull(resultado);
    }

    @Test
    void obterVetorColunaMatriz_UmElemento() {
        int[][] matriz = {{1}};
        int[] esperado = {1};
        int[] resultado = ExercicioDezassete.obterVetorColunaMatriz(matriz, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterVetorColunaMatriz_DoisElementos() {
        int[][] matriz = {{1, 0, 2}, {0, 1, 3}};
        int[] esperado = {1, 0};
        int[] resultado = ExercicioDezassete.obterVetorColunaMatriz(matriz, 0);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void obterVetorColunaMatriz_TresElementos() {
        int[][] matriz = {{1, 0, 2}, {0, 1, 3}, {5, -2, -6}};
        int[] esperado = {2, 3, -6};
        int[] resultado = ExercicioDezassete.obterVetorColunaMatriz(matriz, 2);
        assertArrayEquals(esperado, resultado);
    }

    /*************************************************************************************************/

    @Test
    void obterProdutoEscalarLinhaColuna_LinhaVazia() {
        int[] linha = {};
        int[] coluna = {1};
        Integer resultado = ExercicioDezassete.obterProdutoEscalarLinhaColuna(linha, coluna);
        assertNull(resultado);
    }

    @Test
    void obterProdutoEscalarLinhaColuna_ColunaVazia() {
        int[] linha = {1};
        int[] coluna = {};
        Integer resultado = ExercicioDezassete.obterProdutoEscalarLinhaColuna(linha, coluna);
        assertNull(resultado);
    }

    @Test
    void obterProdutoEscalarLinhaColuna_TamanhosDiferentes() {
        int[] linha = {1};
        int[] coluna = {1, 2};
        Integer resultado = ExercicioDezassete.obterProdutoEscalarLinhaColuna(linha, coluna);
        assertNull(resultado);
    }

    @Test
    void obterProdutoEscalarLinhaColuna_UmElemento() {
        int[] linha = {1};
        int[] coluna = {0};
        int esperado = 0;
        int resultado = ExercicioDezassete.obterProdutoEscalarLinhaColuna(linha, coluna);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoEscalarLinhaColuna_DoisElementos() {
        int[] linha = {1, 1};
        int[] coluna = {0, 1};
        int esperado = 1;
        int resultado = ExercicioDezassete.obterProdutoEscalarLinhaColuna(linha, coluna);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterProdutoEscalarLinhaColuna_TresElementos() {
        int[] linha = {1, -1, 2};
        int[] coluna = {-1, -1, 5};
        int esperado = 10;
        int resultado = ExercicioDezassete.obterProdutoEscalarLinhaColuna(linha, coluna);
        assertEquals(esperado, resultado);
    }
}