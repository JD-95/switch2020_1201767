import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExercicioOitoTestes {

    @Test
    void verificarVetorComOsMultiplosComuns_VetorVazioInserido() {
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(-100, 100, new int[]{});
        assertNull(resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_DeZero_IntervaloIgual_Zero() {
        int[] esperado = {};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(0, 0, new int[]{0});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_DeZeroEZero_IntervaloIgual_Zero() {
        int[] esperado = {};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(0, 0, new int[]{0, 0});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_SemMultiplosComunsDeZero() {
        int[] esperado = {};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(-100, 100, new int[]{0, 1});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_MultiplosDeUmNumero_IntervaloIgualAoNumero() {
        int[] original = {1};
        int[] esperado = {1};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(1, 1, original);
        assertArrayEquals(esperado, resultado);
        assertNotSame(resultado, original);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_MultiplosDeUmNumero_IntervaloAlargado() {
        int[] esperado = {0, 1, 2, 3, 4, 5};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(0, 5, new int[]{1});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_IntervaloCrescente_Positivos() {
        int[] esperado = {0, 5, 10};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(0, 10, new int[]{1, 5});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_IntervaloDecrescente_Positivos() {
        int[] esperado = {10, 5, 0};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(10, 0, new int[]{1, 5});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_IntervaloCrescente_NegativosEPositivos() {
        int[] esperado = {-8, -4, 0, 4, 8};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(-10, 10, new int[]{2, 4});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_IntervaloDecrescente_NegativosEPositivos() {
        int[] esperado = {8, 4, 0, -4, -8};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(10, -10, new int[]{2, 4});
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarVetorComOsMultiplosComuns_SemMultiplosComuns() {
        int[] esperado = {};
        int[] resultado = ExercicioOito.obterVetorComOsMultiplosComuns(10, 19, new int[]{4, 5});
        assertArrayEquals(esperado, resultado);
    }

    /**********************************************************************************************************************************/

    @Test
    void verificarSeZeroMultiploDeZero() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(0, 0);
        assertFalse(resultado);
    }

    @Test
    void verificarSeUmMultiploDeZero() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(1, 0);
        assertFalse(resultado);
    }

    @Test
    void verificarSeZeroMultiploDeUm() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(0, 1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNegUmMultiploDeUm() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(-1, 1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeUmMultiploDeNegUm() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(1, -1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeZeroMultiploDeQuinze() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(0, 15);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCincoMultiploDeUm() {
        boolean resultado = ExercicioOito.verificarSeXMultiploDeY(5, 1);
        assertTrue(resultado);
    }


}