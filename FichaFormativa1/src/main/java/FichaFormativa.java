public class FichaFormativa {

    // Alguns metódos não se encontram otimizados, seria necessário a sua otimização posterior.
    // Poderia ser aplicada também mais modularização.
    // Faltariam também alguns comentários ou alterações nos existentes.

    /**
     * Permite verificar o grau de semelhança entre duas palavras dadas.
     * @param palavraA Recebe a primeira palavra.
     * @param palavraB Recebe a segunda palavra.
     * @return O grau de semelhança das duas palavras entre zero e um.
     */
    public static double obterGrauDeSemelhanca(String palavraA, String palavraB) {

        // retirar o fator de letras maisculas ou minusculas
        palavraA = palavraA.toLowerCase();
        palavraB = palavraB.toLowerCase();

        int tamanhoPalavraA = palavraA.length();
        int tamanhoPalavraB = palavraB.length();
        double transposicoes = obterNumeroTransposicoes(palavraA, palavraB) / 2.0;
        char[] correspondentesA = obterCarateresCorrespondentesA(palavraA, palavraB);
        int numeroCarateresCorrespondentes = correspondentesA.length;

        // se palavras sem qualquer carater correspondente returnar zero
        if (numeroCarateresCorrespondentes == 0) {
            return 0.0;
        } else {
            return ((numeroCarateresCorrespondentes / (double) tamanhoPalavraA) + (numeroCarateresCorrespondentes / (double) tamanhoPalavraB) + ((numeroCarateresCorrespondentes - transposicoes) / (double) numeroCarateresCorrespondentes)) / 3.0;
        }
    }

    /**
     * Permite obter o número de transposições entre duas palavras.
     * @param palavraA Recebe a primeira palavra.
     * @param palavraB Recebe a segunda palavra.
     * @return O número de transposições.
     */
    public static int obterNumeroTransposicoes(String palavraA, String palavraB) {

        // obter as letras correspondentes de cada palavra
        char[] correspondentesA = obterCarateresCorrespondentesA(palavraA, palavraB);
        char[] correspondentesB = obterCarateresCorrespondentesB(palavraA, palavraB);
        int numeroTransposicoes = 0;

        // verificar se existem transposicoes, isto é, na mesma posição as letras não coincidem
        for (int posA = 0; posA < correspondentesA.length; posA++) {
            if (correspondentesA[posA] != correspondentesB[posA]) {
                numeroTransposicoes++;
            }
        }

        return numeroTransposicoes;
    }

    /**
     * Obter os carateres correspondentes da primeira palavra na segunda palavra.
     * @param palavraA Recebe a primeira palavra.
     * @param palavraB Recebe a segunda palavra.
     * @return Um array com os carateres encontrados.
     */
    public static char[] obterCarateresCorrespondentesA(String palavraA, String palavraB) {
        // obter os arrays com as letras das palavras
        char[] letrasPalavraA = palavraA.toCharArray();
        char[] letrasPalavraB = palavraB.toCharArray();
        int janelaCorrespondencia = obterJanelaCorrespondencia(letrasPalavraA, letrasPalavraB);
        // criar mascara de letras da palavra B
        int[] mascaraLetrasB = new int[letrasPalavraB.length];
        // criar array para armazenar valores encontrados, ainda sem o tamanho final
        char[] intermedio = new char[letrasPalavraA.length];
        int posCA = 0, numeroCorrespondentesA = 0;

        // verificar para cada posição na palavraA se existem correspondentes na B seguindo a regra da janela de correspondencia
        for (int posA = 0; posA < letrasPalavraA.length; posA++) {
            for (int posB = posA - janelaCorrespondencia; posB <= posA + janelaCorrespondencia; posB++) {
                if (elementoDentroDoVetor(letrasPalavraB, posB)) {
                    if (letrasPalavraA[posA] == letrasPalavraB[posB] && mascaraLetrasB[posB] != 1) {
                        intermedio[posCA] = letrasPalavraA[posA];
                        mascaraLetrasB[posB] = 1;
                        posCA++;
                        numeroCorrespondentesA++;
                        posB = letrasPalavraB.length;
                    }
                }
            }
        }
        return truncarVetor(intermedio, numeroCorrespondentesA);
    }

    /**
     * Permite truncar um vetor com o tamanho definido.
     * @param original Recebe um vetor de carateres.
     * @param novoTamanho Recebe o tamanho que o novo vetor vai assumir.
     * @return O novo vetor truncado.
     */
    public static char[] truncarVetor(char[] original, int novoTamanho) {

        char[] resultado = new char[novoTamanho];

        // se tamanho superior ao original returnar o vetor original
        if (novoTamanho > original.length) {
            resultado = original;
        } else {
            // copiar os elementos do vetor original até ao novo tamanho definido
            for (int pos = 0; pos < novoTamanho; pos++) {
                resultado[pos] = original[pos];
            }
        }

        return resultado;
    }


    /**
     * Obter os carateres correspondentes da segunda palavra na primeira palavra.
     * @param palavraA Recebe a primeira palavra.
     * @param palavraB Recebe a segunda palavra.
     * @return Um array com os carateres encontrados.
     */
    public static char[] obterCarateresCorrespondentesB(String palavraA, String palavraB) {
        // obter os arrays com as letras das palavras
        char[] letrasPalavraA = palavraA.toCharArray();
        char[] letrasPalavraB = palavraB.toCharArray();
        int janelaCorrespondencia = obterJanelaCorrespondencia(letrasPalavraA, letrasPalavraB);
        // criar mascara de letras da palavra A
        int[] mascaraLetrasA = new int[letrasPalavraA.length];
        // criar array para armazenar valores encontrados, ainda sem o tamanho final
        char[] intermedio = new char[letrasPalavraB.length];
        int posCB = 0, numeroCorrespondentesB = 0;

        // verificar para cada posição na palavraB se existem correspondentes na A seguindo a regra da janela de correspondencia
        for (int posB = 0; posB < letrasPalavraB.length; posB++) {
            for (int posA = posB - janelaCorrespondencia; posA <= posB + janelaCorrespondencia; posA++) {
                if (elementoDentroDoVetor(letrasPalavraA, posA)) {
                    if (letrasPalavraA[posA] == letrasPalavraB[posB] && mascaraLetrasA[posA] != 1) {
                        intermedio[posCB] = letrasPalavraB[posB];
                        mascaraLetrasA[posA] = 1;
                        posCB++;
                        numeroCorrespondentesB++;
                        posA = letrasPalavraA.length;
                    }
                }
            }
        }

        return truncarVetor(intermedio, numeroCorrespondentesB);
    }

    /**
     * Permite obter a janela de correspondencia (quais os elementos a verificar).
     * @param palavraA Recebe a primeira palavra.
     * @param palavraB Recebe a segunda palavra.
     * @return Retorna o valor da janela de correspondecia.
     */
    public static int obterJanelaCorrespondencia(char[] palavraA, char[] palavraB) {

        int janelaCorrespondencia;

        // utilizar a palavra de maior tamanho para o cálculo
        if (palavraA.length >= palavraB.length) {
            janelaCorrespondencia = (palavraA.length / 2) - 1;
        } else {
            janelaCorrespondencia = (palavraB.length / 2) - 1;
        }

        return janelaCorrespondencia;
    }

    /**
     * Permite verificar se uma posição dada não excede os limites do vetor dado.
     * @param vetor Recebe um vetor de carateres.
     * @param posicaoAVerificar Recebe a posição a verificar.
     * @return Se essa posição se encontra ou não dentro do vetor dado.
     */
    public static boolean elementoDentroDoVetor(char[] vetor, int posicaoAVerificar) {
        return posicaoAVerificar >= 0 && posicaoAVerificar < vetor.length;
    }


}
