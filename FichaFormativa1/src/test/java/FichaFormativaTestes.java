import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FichaFormativaTestes {

    @Test
    void verificarGrauDeSemelhanca_PalavrasSemQualquerSemelhanca_TesteUm() {
        String palavraA = "abc";
        String palavraB = "def";
        double esperado = 0;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarGrauDeSemelhanca_PalavrasSemQualquerSemelhanca_TesteDois() {
        String palavraA = "sol";
        String palavraB = "bar";
        double esperado = 0;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarGrauDeSemelhanca_PalavrasComAcento_TesteUm() {
        String palavraA = "mar";
        String palavraB = "maré";
        double esperado = 0.92;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarGrauDeSemelhanca_PalavrasComAcento_TesteDois() {
        String palavraA = "maré";
        String palavraB = "maresia";
        double esperado = 0.73;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarGrauDeSemelhanca_Iguais() {
        String palavraA = "ola";
        String palavraB = "ola";
        double esperado = 1;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarGrauDeSemelhanca_TamanhosDiferentes() {
        String palavraA = "amarrar";
        String palavraB = "terra";
        double esperado = 0.57;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarGrauDeSemelhanca_MesmoTamanho_TesteUm() {
        String palavraA = "abarcar";
        String palavraB = "amarrar";
        double esperado = 0.74;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarGrauDeSemelhanca_MesmoTamanho_TesteDois() {
        String palavraA = "maresia";
        String palavraB = "abarcar";
        double esperado = 0.62;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarGrauDeSemelhanca_MaisculaInicio() {
        String palavraA = "terra";
        String palavraB = "Terra";
        double esperado = 1;
        double resultado = FichaFormativa.obterGrauDeSemelhanca(palavraA, palavraB);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarElementoDentroDoVetor_Inferior() {
        char[] vetor = {'a', 'b', 'c'};
        int posicao = -1;
        boolean resultado = FichaFormativa.elementoDentroDoVetor(vetor, posicao);
        assertFalse(resultado);
    }

    @Test
    void verificarElementoDentroDoVetor_Superior() {
        char[] vetor = {'a', 'b', 'c'};
        int posicao = 3;
        boolean resultado = FichaFormativa.elementoDentroDoVetor(vetor, posicao);
        assertFalse(resultado);
    }

    @Test
    void verificarElementoDentroDoVetor_DentroVetor_Minimo() {
        char[] vetor = {'a', 'b', 'c'};
        int posicao = 0;
        boolean resultado = FichaFormativa.elementoDentroDoVetor(vetor, posicao);
        assertTrue(resultado);
    }

    @Test
    void verificarElementoDentroDoVetor_DentroVetor_Maximo() {
        char[] vetor = {'a', 'b', 'c'};
        int posicao = 2;
        boolean resultado = FichaFormativa.elementoDentroDoVetor(vetor, posicao);
        assertTrue(resultado);
    }

    @Test
    void verificarJanelaCorrespondencia_TamanhosIguais() {
        char[] palavraA = {'a', 'b', 'c'};
        char[] palavraB = {'a', 'b', 'c'};
        int esperado = 0;
        int resultado = FichaFormativa.obterJanelaCorrespondencia(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarJanelaCorrespondencia_PalavraAMaior() {
        char[] palavraA = {'a', 'b', 'c', 'd', 'e'};
        char[] palavraB = {'a', 'b', 'c'};
        int esperado = 1;
        int resultado = FichaFormativa.obterJanelaCorrespondencia(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarJanelaCorrespondencia_PalavraBMaior() {
        char[] palavraA = {'a', 'b', 'c'};
        char[] palavraB = {'a', 'b', 'c', 'd'};
        int esperado = 1;
        int resultado = FichaFormativa.obterJanelaCorrespondencia(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCarateresCorrespondentesB_SemCorrespondentes() {
        String palavraA = "sol";
        String palavraB = "bar";
        char[] esperado = {};
        char[] resultado = FichaFormativa.obterCarateresCorrespondentesB(palavraA, palavraB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCarateresCorrespondentesB_MesmoTamanho() {
        String palavraA = "abarcar";
        String palavraB = "amarrar";
        char[] esperado = {'a', 'a', 'r', 'r', 'a'};
        char[] resultado = FichaFormativa.obterCarateresCorrespondentesB(palavraA, palavraB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCarateresCorrespondentesB_TamanhosDiferentes() {
        String palavraA = "amarrar";
        String palavraB = "terra";
        char[] esperado = {'r', 'r', 'a'};
        char[] resultado = FichaFormativa.obterCarateresCorrespondentesB(palavraA, palavraB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCarateresCorrespondentesA_SemCorrespondentes() {
        String palavraA = "sol";
        String palavraB = "bar";
        char[] esperado = {};
        char[] resultado = FichaFormativa.obterCarateresCorrespondentesA(palavraA, palavraB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCarateresCorrespondentesA_MesmoTamanho() {
        String palavraA = "abarcar";
        String palavraB = "amarrar";
        char[] esperado = {'a', 'a', 'r', 'a', 'r'};
        char[] resultado = FichaFormativa.obterCarateresCorrespondentesA(palavraA, palavraB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarCarateresCorrespondentesA_TamanhosDiferentes() {
        String palavraA = "amarrar";
        String palavraB = "terra";
        char[] esperado = {'a', 'r', 'r'};
        char[] resultado = FichaFormativa.obterCarateresCorrespondentesA(palavraA, palavraB);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarTruncarVetor_TamanhoZero() {
        char[] vetor = {'a', 'b', 'c'};
        int tamanho = 0;
        char[] esperado = {};
        char[] resultado = FichaFormativa.truncarVetor(vetor, tamanho);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarTruncarVetor_TamanhoSuperior() {
        char[] vetor = {'a', 'b', 'c'};
        int tamanho = 4;
        char[] esperado = {'a', 'b', 'c'};
        char[] resultado = FichaFormativa.truncarVetor(vetor, tamanho);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarTruncarVetor_TamanhoIgual() {
        char[] vetor = {'a', 'b', 'c'};
        int tamanho = 3;
        char[] esperado = {'a', 'b', 'c'};
        char[] resultado = FichaFormativa.truncarVetor(vetor, tamanho);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarTruncarVetor_TamanhoMenorQueOriginal() {
        char[] vetor = {'a', 'b', 'c'};
        int tamanho = 2;
        char[] esperado = {'a', 'b'};
        char[] resultado = FichaFormativa.truncarVetor(vetor, tamanho);
        assertArrayEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroTransposicoes_PalavrasIguais() {
        String palavraA = "ola";
        String palavraB = "ola";
        int esperado = 0;
        int resultado = FichaFormativa.obterNumeroTransposicoes(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroTransposicoes_PalavrasMesmoTamanho() {
        String palavraA = "abarcar";
        String palavraB = "amarrar";
        int esperado = 2;
        int resultado = FichaFormativa.obterNumeroTransposicoes(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroTransposicoes_PalavrasTamanhoDiferente() {
        String palavraA = "amarrar";
        String palavraB = "terra";
        int esperado = 2;
        int resultado = FichaFormativa.obterNumeroTransposicoes(palavraA, palavraB);
        assertEquals(esperado, resultado);
    }


}