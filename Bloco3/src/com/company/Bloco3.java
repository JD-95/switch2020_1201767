package com.company;

import java.util.Scanner;

public class Bloco3 {

    public static void main(String[] args) {
        //exercicio1();
        //exercicio2();
        //exercicio3();
        //exercicio4A();
        //exercicio4B();
        //exercicio4C();
        //exercicio4D();
        //exercicio4E();
        //exercicio5A();
        //exercicio5B();
        //exercicio5C();
        //exercicio5D();
        //exercicio5E();
        //exercicio8();
        //exercicio9();
        //exercicio10();
        //exercicio11();
        //exercicio12();
        //exercicio13();
        //exercicio14();
        //exercicio15();
        //exercicio16();
        //exercicio17A();
        //exercicio17B();
        //exercicio18();
        //exercicio19();
        //exercicio19V2();
        //exercicio20();

    }

    /*************************************** Exercicio_1 ***************************************/

    public static void exercicio1() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza um número inteiro:");
        int numero = ler.nextInt();

        int fatorial = obterFatorial(numero);

        System.out.println("O fatorial de " + numero + " é " + fatorial + ".");
    }

    public static int obterFatorial(int num) {
        // obtem-se o fatorial de um numero (num) multiplicando todos os números inteiros positivos até ao mesmo

        if (num < 0) {   // o fatorial não se aplica números negativos
            return 0;
        }

        // ex: 5, 1º ciclo 1*5=5 -> 2º ciclo 5*4=20 -> 3º ciclo 20*3=60 -> 4º ciclo 60*2=120 -> 5ºciclo 120*1= 120 -> 6ºciclo o
        // i passa a ser zero e como zero não satisfaz a condição i >= 1
        // o ciclo não é mais iniciado, retornando o ultimo fatorial calculado = 120
        int fatorial = 1;
        for (int i = num; i >= 1; i--) {
            fatorial = fatorial * i;
        }
        return fatorial;
    }

    /*************************************** Exercicio_2 ***************************************/
    //notas que N alunos obtiveram numa disciplina e mostre a percentagem de notas positivas e a média das notas negativas
    public static void exercicio2() {

        int nrAlunos, nota;
        // contadores iniciados a zero
        int counterPositivas = 0;
        int counterNegativas = 0;
        int somaNegativas = 0;


        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de alunos:");
        nrAlunos = ler.nextInt();

        // caso o valor inserido seja negativo entra no ciclo até que seja introduzido um valor positivo
        while (nrAlunos <= 0) {
            System.out.println("Erro: Número de alunos introduzido inválido");
            System.out.println("Introduza novamente o número de alunos:");
            nrAlunos = ler.nextInt();
        }

        // vai ser introduzida a nota para cada aluno através do ciclo até todos os alunos terem nota atribuida
        for (int i = 1; i <= nrAlunos; i++) {
            System.out.println("Introduza a nota do aluno:");
            nota = ler.nextInt();
            // caso a nota não esteja na escala 0 - 20, entra neste ciclo até ser introduzida uma nota válida
            while (nota < 0 || nota > 20) {
                System.out.println("Erro: Nota introduzida inválida");
                System.out.println("Introduza a nota do aluno:");
                nota = ler.nextInt();
            }
            if (nota >= 10) {
                counterPositivas++;     // conta as notas positivas
            } else {
                somaNegativas += nota;  // soma as notas negativas
                counterNegativas++;     // conta as notas negativas
            }
        }
        // calcula a razão entre notas positivas e número de alunos, multiplica por 100 para obter a percentagem
        double percentagemPositivas = (counterPositivas / (double) nrAlunos) * 100.0;
        // calcula a média de negativas, divindo a soma das negativas pelo número das mesmas
        double mediaNegativas = somaNegativas / (double) (counterNegativas);


        if (counterNegativas == 0) {   // se não existir nenhuma nota negativa apresenta a seguinte mensagem
            System.out.println(
                    "PARABÉNS, a percentagem de notas positivas é de " + String.format("%.0f", percentagemPositivas) + " %.");
        } else {                       // se existirem notas negativas apresenta a seguinte mensagem, com a média das mesmas
            System.out.println("A percentagem de notas positivas é de " + String
                    .format("%.2f",
                            percentagemPositivas) + " % e a média das notas negativas é de " + mediaNegativas + " valores.");
        }
    }


    /*************************************** Exercicio_3 ***************************************/
    //ler uma sequência de números positivos terminada por um número não positivo e mostre a percentagem dos nº pares e a média dos ímpares
    public static void exercicio3() {

        int numero;
        int counterPares = 0;
        int counterImpares = 0;
        double somaImpares = 0;
        double percentagemDePares, mediaDeImpares;

        // assumindo que o primeiro número tem de ser positivo
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza um número:");
        numero = ler.nextInt();

        // quando o número introduzido não for positivo (<0) o ciclo termina
        do {
            if (verificarSeEPar(numero)) {  // atraves do metodo verifica se o número é par
                counterPares++;             // contabiliza os numeros pares
            } else {
                counterImpares++;           // contabiliza os numeros impares
                somaImpares += numero;      // soma os numeros impares
            }
            System.out.println("Introduza um número:");   // continua a leitura de dados
            numero = ler.nextInt();
        } while (numero >= 0);

        // calcula a razão entre numeros pares e número total de numeros inseridos, multiplica por 100 para obter a percentagem
        percentagemDePares = (counterPares / (double) (counterImpares + counterPares)) * 100;
        // calcula a média dos numeros impares, divindo a soma dos impares pelo número dos mesmos
        mediaDeImpares = somaImpares / (double) counterImpares;


        if (counterImpares == 0) {  // se não existir nenhum numero impar apresenta a seguinte mensagem
            System.out.println(
                    "A percentagem de números pares é de " + String.format("%.0f", percentagemDePares) + "%.");
        } else {
            System.out.println(     // se existirem numeros impares apresenta a seguinte mensagem, com a média dos mesmos
                    "A percentagem de números pares é de " + String
                            .format("%.2f", percentagemDePares) + "% e a média dos números impares é de " + String
                            .format("%.2f", mediaDeImpares) + ".");
        }
    }

    /***************************************  Exercicio 4A  ***************************************/
    // O número de múltiplos de 3 num intervalo dado.
    public static void exercicio4A() {
        int valorInicialDoIntervalo, valorFinalDoIntervalo;
        int numeroDeMultiplosDe3;


        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor inicial do intervalo:");
        valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o valor final do intervalo:");
        valorFinalDoIntervalo = ler.nextInt();

        numeroDeMultiplosDe3 = obterNumeroDeMultiplosDe3(valorInicialDoIntervalo, valorFinalDoIntervalo);

        System.out.println(
                "O número de múltiplos de 3 entre " + valorInicialDoIntervalo + " e " + valorFinalDoIntervalo + " é de " + numeroDeMultiplosDe3 +
                        ".");
    }

    public static int obterNumeroDeMultiplosDe3(int valorInicialDoIntervalo, int valorFinalDoIntervalo) {
        int counterMultiplosDeTres = 0;

        // percorrer todos os números entre o valor inicial e o valor final do intervalo
        for (int numero = valorInicialDoIntervalo; numero <= valorFinalDoIntervalo; numero++) {
            if ((numero % 3) == 0) {
                // se o resto da divisão inteira de um número por 3 for igual a zero, então diz-se que esse número é múltiplo de 3
                // neste caso considerou-se o zero como múltiplo de todos os números
                counterMultiplosDeTres++;  // vai contabilizando os múltiplos de 3
            }
        }
        return counterMultiplosDeTres;
    }

    /***************************************  Exercicio 4B  ***************************************/
    // O número de múltiplos de um dado número inteiro num intervalo dado.
    public static void exercicio4B() {
        int valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificar;
        int numeroDeMultiplos;

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor minimo do intervalo:");
        valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o valor maximo do intervalo:");
        valorFinalDoIntervalo = ler.nextInt();

        System.out.println("Introduza o valor do qual pretende calcular o número de múltiplos:");
        numeroAVerificar = ler.nextInt();

        numeroDeMultiplos = obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificar);

        System.out.println(
                "O número de múltiplos de " + numeroAVerificar + " entre " + valorInicialDoIntervalo + " e " + valorFinalDoIntervalo + " é de " + numeroDeMultiplos + ".");
    }

    public static int obterNumeroDeMultiplosNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo, int numeroAVerificar) {
        int counterMultiplos = 0;

        for (int numeroNoIntervalo = valorInicialDoIntervalo; numeroNoIntervalo <= valorFinalDoIntervalo; numeroNoIntervalo++) {
            if (verificarSeXMultiploDeY(numeroNoIntervalo, numeroAVerificar)) { // verifica se é múltiplo
                counterMultiplos++;   // vai contabilizando os múltiplos encontrados
            }
        }
        return counterMultiplos;
    }

    public static boolean verificarSeXMultiploDeY(int X, int Y) {
        boolean seXMultiploDeY = false;
        if (Y != 0) {  // o zero não tem múltiplos, uma vez que qualquer divisão por 0 é impossível.
            if ((X % Y) == 0) {
                // se o resto da divisão inteira de X por Y for igual a zero, diz-se que X é multiplo de Y
                // neste caso considerou-se o zero como múltiplo de todos os números
                seXMultiploDeY = true;
            }
        }
        return seXMultiploDeY;
    }

    /***************************************  Exercicio 4C  ***************************************/
    // O número de múltiplos de 3 e 5 num intervalo dado
    public static void exercicio4C() {
        int valorInicialDoIntervalo, valorFinalDoIntervalo;

        // forçar o utilizador a inserir os dados pretendidos
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor minimo do intervalo:");
        valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o valor maximo do intervalo:");
        valorFinalDoIntervalo = ler.nextInt();

        // atraves do metodo criado verificar o número de múltiplos de 3 e 5 no intervalo dado e a seguir somar ambos
        int numeroDeMultiplosDe3E5 = obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, 3, 5);

        System.out
                .println(
                        "O número de múltiplos de 3 e 5 entre " + valorInicialDoIntervalo + " e " + valorFinalDoIntervalo + " é de " + numeroDeMultiplosDe3E5 + ".");
    }

    /***************************************  Exercicio 4D  ***************************************/
    // O número de múltiplos de dois números inteiros num intervalo dado.
    public static int obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo, int numeroAVerificarA,
            int numeroAVerificarB) {
        return obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo,
                numeroAVerificarA) + obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo,
                numeroAVerificarB);
    }

    public static void exercicio4D() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor minimo do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o valor maximo do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();
        System.out.println("Introduza o primeiro número inteiro do qual pretende calcular o seu número de múltiplos:");
        int numeroAVerificarA = ler.nextInt();
        System.out.println("Introduza o segundo número inteiro do qual pretende calcular o seu número de múltiplos:");
        int numeroAVerificarB = ler.nextInt();

        // atraves do metodo criado obter o numero de multiplos de 2 números dados
        int numeroDeMultiplos = obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificarA,
                numeroAVerificarB);

        System.out.println(
                "O número de múltiplos de " + numeroAVerificarA + " e " + numeroAVerificarB + " entre " + valorInicialDoIntervalo + " e " + valorFinalDoIntervalo + " é de " + numeroDeMultiplos + ".");
    }

    /***************************************  Exercicio 4E  ***************************************/
    // A soma dos múltiplos de dois números inteiros num intervalo dado.
    public static int obterSomaDosMultiplosDeDoisNumerosNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo, int numeroAVerificarA,
            int numeroAVerificarB) {
        return obterSomaDosMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo,
                numeroAVerificarA) + obterSomaDosMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo,
                numeroAVerificarB);
    }

    public static void exercicio4E() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor minimo do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o valor maximo do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();
        System.out.println("Introduza o primeiro número inteiro do qual pretende calcular os seus múltiplos:");
        int numeroAVerificarA = ler.nextInt();
        System.out.println("Introduza o segundo número inteiro do qual pretende calcular os seus múltiplos:");
        int numeroAVerificarB = ler.nextInt();

        // atraves do metodo criado obter a soma dos multiplos de 2 números dados
        int somaDosMultiplos = obterSomaDosMultiplosDeDoisNumerosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificarA,
                numeroAVerificarB);

        System.out.println(
                "A soma dos múltiplos de " + numeroAVerificarA + " e " + numeroAVerificarB + " entre " + valorInicialDoIntervalo + " e " + valorFinalDoIntervalo + " é de " + somaDosMultiplos + ".");
    }


    /***************************************  Exercicio 5A  ***************************************/
    // soma de todos os números pares num dado intervalo, assumindo um intervalo crescente de números inteiros
    public static void exercicio5A() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o minimo do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o maximo do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();

        // atraves do método obter a soma dos números pares no intervalo
        int somaDosNumerosPares = obterSomaDosParesNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo);

        System.out.println("A soma dos pares no intervalo fornecido é de " + somaDosNumerosPares);
    }

    public static int obterSomaDosParesNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo) {
        // inicializar a soma dos pares com zero
        int somaDosNumeroPares = 0;

        // percorrer todos os números no intervalo dado
        for (int numeroNoIntervalo = valorInicialDoIntervalo; numeroNoIntervalo <= valorFinalDoIntervalo; numeroNoIntervalo++) {
            if (verificarSeEPar(numeroNoIntervalo)) {     // verifica se o número é par através do metodo criado
                somaDosNumeroPares += numeroNoIntervalo;  // somar todos os números pares
            }
        }
        return somaDosNumeroPares;
    }

    public static boolean verificarSeEPar(int numeroAVerificar) {
        return (numeroAVerificar % 2) == 0;    // se a divisao inteira de um numero por 2 = 0, entao esse numero é par
    }

    /***************************************  Exercicio 5B  ***************************************/
    // quantidade de todos os números pares num dado intervalo, assumindo um intervalo crescente de números inteiros
    public static void exercicio5B() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o minimo do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o maximo do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();

        // atraves do metodo obter o numero de numeros pares no intervalo dado
        int numeroDeParesNumIntervalo = obterNumeroDeParesNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo);

        System.out.println("A quantidade de pares no intervalo fornecido é de " + numeroDeParesNumIntervalo);
    }

    public static int obterNumeroDeParesNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo) {
        // inicializar a contagem dos pares com zero
        int numeroDePares = 0;

        // percorrer todos os números no intervalo dado
        for (int numeroNoIntervalo = valorInicialDoIntervalo; numeroNoIntervalo <= valorFinalDoIntervalo; numeroNoIntervalo++) {
            if (verificarSeEPar(numeroNoIntervalo)) {  // verifica se o número é par através do metodo criado
                numeroDePares++;                       // contar os números pares
            }
        }
        return numeroDePares;
    }

    /***************************************  Exercicio 5C  ***************************************/
    //  soma de todos os números ímpares num dado intervalo, assumindo um intervalo crescente de numeros inteiros
    public static void exercicio5C() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o minimo do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o maximo do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();

        // atraves do método obter a soma dos números impares no intervalo
        int somaDosNumerosImparesNumIntervalo = obterSomaDosImparesNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo);

        System.out.println("A soma dos números ímpares no intervalo fornecido é de " + somaDosNumerosImparesNumIntervalo);
    }

    public static int obterSomaDosImparesNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo) {
        // inicializar a soma dos impares com zero
        int somaDosNumeroImpares = 0;

        // percorrer todos os números no intervalo dado
        for (int numeroNoIntervalo = valorInicialDoIntervalo; numeroNoIntervalo <= valorFinalDoIntervalo; numeroNoIntervalo++) {
            if (!verificarSeEPar(numeroNoIntervalo)) {       // verifica se o número é impar através do metodo criado
                somaDosNumeroImpares += numeroNoIntervalo;  // somar todos os números impares
            }
        }
        return somaDosNumeroImpares;
    }

    /***************************************  Exercicio 5D  ***************************************/
    // quantidade de todos os números ímpares num dado intervalo, assumindo um intervalo crescente de numeros inteiros
    public static void exercicio5D() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o minimo do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o maximo do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();

        // atraves do método obter a quantidade dos números impares no intervalo
        int numeroDeImparesNumIntervalo = obterNumeroDeImparesNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo);

        System.out.println("A quantidade de impares no intervalo fornecido é de " + numeroDeImparesNumIntervalo);
    }

    public static int obterNumeroDeImparesNumIntervalo(int minimoDoIntervalo, int maximoDoIntervalo) {
        // inicializar a contagem dos impares com zero
        int numeroDeImpares = 0;

        // percorrer todos os números no intervalo dado
        for (int numeroNoIntervalo = minimoDoIntervalo; numeroNoIntervalo <= maximoDoIntervalo; numeroNoIntervalo++) {
            if (!verificarSeEPar(numeroNoIntervalo)) {  // verifica se o número é impar através do metodo criado
                numeroDeImpares++;                      // contar os números impares
            }
        }
        return numeroDeImpares;
    }

    /***************************************  Exercicio 5E  ***************************************/
    // A soma de todos os números múltiplos de um dado número num dado intervalo.
    // Os dois números, que definem os limites do intervalo, não estão necessariamente por ordem crescente.
    public static void exercicio5E() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor inicial do intervalo:");
        int valorInicialDoIntervalo = ler.nextInt();
        System.out.println("Introduza o valor final do intervalo:");
        int valorFinalDoIntervalo = ler.nextInt();
        System.out.println("Introduza o número do qual pretende verificar os seus multiplos:");
        int numeroAVerificar = ler.nextInt();

        // atraves do método obter a soma dos multiplos de dado numero no intervalo
        int somaMultiplosNumIntervalo = obterSomaDosMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificar);

        System.out.println("A soma dos múltiplos de " + numeroAVerificar + " no intervalo dado é de " + somaMultiplosNumIntervalo);
    }

    public static int obterSomaDosMultiplosNumIntervalo(int valorInicialIntervalo, int valorFinalDoIntervalo, int numeroAVerificar) {
        // inicializar a soma dos multiplos com zero
        int somaDosMultiplos = 0;

        if (valorInicialIntervalo < valorFinalDoIntervalo) {   // caso seja um intervalo crescente
            // percorrer todos os números no intervalo dado do valor inicial até ao final, aumentando o número
            for (int numeroNoIntervalo = valorInicialIntervalo; numeroNoIntervalo <= valorFinalDoIntervalo; numeroNoIntervalo++) {
                if (verificarSeXMultiploDeY(numeroNoIntervalo, numeroAVerificar)) {  // verifica se o número é multiplo
                    somaDosMultiplos += numeroNoIntervalo;                           // somar os multiplos encontrados
                }
            }
        } else {   // caso seja um intervalo decrescente
            // percorrer todos os números no intervalo dado do valor inicial até ao final, diminuindo o número
            for (int numeroNoIntervalo = valorInicialIntervalo; numeroNoIntervalo >= valorFinalDoIntervalo; numeroNoIntervalo--) {
                // o valor inicial é o maximo e a partir dele vamos descendo até chegarmos ao valor final, que é o valor mais baixo
                if (verificarSeXMultiploDeY(numeroNoIntervalo, numeroAVerificar)) { // verifica se o número é multiplo
                    somaDosMultiplos += numeroNoIntervalo;                          // somar os multiplos encontrados
                }
            }
        }
        return somaDosMultiplos;
    }

    /***************************************  Exercicio 5F  ***************************************/
    // O produto de todos os números múltiplos de um dado número num dado intervalo de números inteiros positivos
    public static int obterProdutoDosMultiplosNumIntervalo(int valorInicialIntervalo, int valorFinalDoIntervalo, int numeroAVerificar) {
        // inicializar o produto dos multiplos com 1 para não influenciar as multiplicações seguintes
        int produtoDosMultiplos = 1;
        if ((valorInicialIntervalo == 0 && numeroAVerificar == 0) || (valorFinalDoIntervalo == 0 && numeroAVerificar == 0)) {
            // caso o inicio ou fim do intervalo seja zero e procuremos os multiplos de zero, o valor final será sempre zero
            // uma vez que o zero não tem multiplos
            produtoDosMultiplos = 0;
        }

        if (valorInicialIntervalo < valorFinalDoIntervalo) {   // caso seja um intervalo crescente
            // percorrer todos os números no intervalo dado do valor inicial até ao final
            for (int numeroNoIntervalo = valorInicialIntervalo; numeroNoIntervalo <= valorFinalDoIntervalo; numeroNoIntervalo++) {
                if (verificarSeXMultiploDeY(numeroNoIntervalo, numeroAVerificar)) {  // verifica se o número é multiplo
                    produtoDosMultiplos *= numeroNoIntervalo;                        // vai multiplicando os multiplos encontrados
                }
            }
        } else {   // caso seja um intervalo decrescente
            // percorrer todos os números no intervalo dado do valor inicial até ao final
            for (int numeroNoIntervalo = valorInicialIntervalo; numeroNoIntervalo >= valorFinalDoIntervalo; numeroNoIntervalo--) {
                // o valor inicial é o maximo e a partir dele vamos descendo até chegarmos ao valor final, que é o valor mais baixo
                if (verificarSeXMultiploDeY(numeroNoIntervalo, numeroAVerificar)) { // verifica se o número é multiplo
                    produtoDosMultiplos *= numeroNoIntervalo;                       // vai multiplicando os multiplos encontrados
                }
            }
        }
        return produtoDosMultiplos;
    }

    /***************************************  Exercicio 5G  ***************************************/
    // A média dos múltiplos de um dado número num intervalo definido por dois números.
    public static double obterMediaDosMultiplosNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo, int numeroAVerificar) {
        double mediaDosMultiplosNumIntervalo;

        if (obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificar) == 0) {
            mediaDosMultiplosNumIntervalo = -1;  // caso não existam multiplos retorna -1 como média
        } else {
            mediaDosMultiplosNumIntervalo = (double) obterSomaDosMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo,
                    numeroAVerificar) / (double) obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, numeroAVerificar);
            // atraves da razão entre a soma dos multiplos e o numero dos mesmos obtemos a media dos multiplos
        }
        return mediaDosMultiplosNumIntervalo;
    }

    /***************************************  Exercicio 5H  ***************************************/
    // média dos múltiplos de X ou Y num intervalo definido por dois números num intervalo definido por dois numeros
    public static double obterMediaDosMultiplosDeDoisNumerosNumIntervalo(int valorInicialDoIntervalo, int valorFinalDoIntervalo, int multiplosDeA,
            int multiplosDeB) {
        double mediaDosMultiplosDeDoisNumerosNumIntervalo;

        // obter o numero de multiplos de A e de B separadamente
        int numeroDeMultiplosDeA = obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, multiplosDeA);
        int numeroDeMultiplosDeB = obterNumeroDeMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, multiplosDeB);

        // se não existirem multiplos nem de A nem de B então a media será -1, uma vez que não podemos dividir por zero
        if (numeroDeMultiplosDeA == 0 && numeroDeMultiplosDeB == 0) {
            mediaDosMultiplosDeDoisNumerosNumIntervalo = -1;
        } else {
            // obter a soma dos multiplos de A e de B em conjunto
            double somaDosMultiplosDeAeB = obterSomaDosMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo,
                    multiplosDeA) + obterSomaDosMultiplosNumIntervalo(valorInicialDoIntervalo, valorFinalDoIntervalo, multiplosDeB);
            // somar o numero de multiplos de A e B
            int numeroDosMultiplosDeAeB = numeroDeMultiplosDeA + numeroDeMultiplosDeB;

            // atraves da razão entre a soma dos multiplos de A e B e o numero de multiplos de A e B obtemos a média
            mediaDosMultiplosDeDoisNumerosNumIntervalo = somaDosMultiplosDeAeB / (double) numeroDosMultiplosDeAeB;
        }
        return mediaDosMultiplosDeDoisNumerosNumIntervalo;
    }

    /***************************************  Exercicio 6A  ***************************************/
    //O número de algarismos de um número inteiro longo
    public static int obterNumeroDeDigitos(long numero) {
        int numeroDigitos = 0;

        if (numero == 0) {
            return 1;   // caso o número introduzido seja um zero retorna 1 Digito como resultado
        } else {
            while (numero > 0) {
                numeroDigitos++; // obter o numero de digitos = igual ao numero de ciclos
                numero = retirarUltimoAlgarismoAoNumero(numero); // retirar o ultimo algarismo do número
            }
        }
        return numeroDigitos;
    }

    public static long retirarUltimoAlgarismoAoNumero(long numero) {
        long numeroMenosOUltimoAlgarismo;

        if (numero >= 10) {   // retira sempre o ultimo algarismo
            numeroMenosOUltimoAlgarismo = numero / 10;
        } else {
            numeroMenosOUltimoAlgarismo = -1;  // exceto se o numero já só tiver um algarismo, aí retorna -1
        }
        return numeroMenosOUltimoAlgarismo;
    }

    /***************************************  Exercicio 6B  ***************************************/
    // O número de algarismos pares de um número inteiro longo
    public static int obterNumeroDeAlgarismosPares(long numero) {
        int numeroAlgarismosPares = 0;

        if (numero == 0) {    // se numero introduzido for Zero existe apenas um par
            numeroAlgarismosPares = 1;
        }

        while (numero > 0) {
            int algarismo = obterUltimoAlgarismoDoNumero(numero);
            if (verificarSeEPar(algarismo)) {    // avaliar se cada algarismo é par
                numeroAlgarismosPares++;
            }
            numero = retirarUltimoAlgarismoAoNumero(numero);
        }
        return numeroAlgarismosPares;
    }

    public static int obterUltimoAlgarismoDoNumero(long numero) {
        return (int) numero % 10;  // o resto da divisão por 10 permite obter o ultimo digito do numero inserido
    }

    /***************************************  Exercicio 6C  ***************************************/
    // O número de algarismos ímpares de um número inteiro longo.
    public static int obterNumeroDeAlgarismosImpares(long numero) {
        int numeroAlgarismosImpares = 0;

        while (numero > 0) {
            int algarismo = (obterUltimoAlgarismoDoNumero(numero));
            if (!verificarSeEPar(algarismo)) {    // avaliar se cada algarismo é impar
                numeroAlgarismosImpares++;
            }
            numero = retirarUltimoAlgarismoAoNumero(numero);
        }
        return numeroAlgarismosImpares;
    }

    /***************************************  Exercicio 6D  ***************************************/
    // A soma dos algarismos de um número inteiro longo.
    public static int obterSomaDosAlgarismos(long numero) {
        int somaAlgarismos = 0;

        while (numero > 0) {
            int algarismo = obterUltimoAlgarismoDoNumero(numero);
            somaAlgarismos += algarismo; // somar algarismo a algarismo
            numero = retirarUltimoAlgarismoAoNumero(numero);
        }
        return somaAlgarismos;
    }

    /***************************************  Exercicio 6E  ***************************************/
    // A soma dos algarismos pares de um número inteiro longo.
    public static int obterSomaDosAlgarismosPares(long numero) {
        int somaAlgarismosPares = 0;

        while (numero > 0) {
            int algarismo = obterUltimoAlgarismoDoNumero(numero);
            if (verificarSeEPar(algarismo)) {    // avaliar se cada algarismo é par
                somaAlgarismosPares += algarismo;           // somar algarismos pares
            }
            numero = retirarUltimoAlgarismoAoNumero(numero);
        }
        return somaAlgarismosPares;
    }

    /***************************************  Exercicio 6F  ***************************************/
    // A soma dos algarismos impares de um número inteiro longo.
    public static int obterSomaDosAlgarismosImpares(long numero) {
        int somaAlgarismosImpares = 0;

        while (numero > 0) {
            int algarismo = (obterUltimoAlgarismoDoNumero(numero));
            if (!verificarSeEPar(algarismo)) {    // avaliar se cada alagarismo não é par
                somaAlgarismosImpares += algarismo;           // somar algarismos impares
            }
            numero = retirarUltimoAlgarismoAoNumero(numero);
        }
        return somaAlgarismosImpares;
    }

    /***************************************  Exercicio 6G  ***************************************/
    // A média dos algarismos de um número inteiro longo
    public static double obterMediaDosAlgarismos(long numero) {
        return (double) obterSomaDosAlgarismos(numero) / (double) obterNumeroDeDigitos(numero);
    }

    /***************************************  Exercicio 6H  ***************************************/
    // A média dos algarismos pares de um número inteiro longo.
    public static double obterMediaDosAlgarismosPares(long numero) {
        double mediaAlgarismosPares;

        if (obterNumeroDeAlgarismosPares(numero) == 0) {
            mediaAlgarismosPares = -1;   // retorna -1 caso não existam números pares
        } else {
            mediaAlgarismosPares = (double) obterSomaDosAlgarismosPares(numero) / (double) obterNumeroDeAlgarismosPares(numero);
        }
        return mediaAlgarismosPares;
    }

    /***************************************  Exercicio 6I  ***************************************/
    // A média dos algarismos ímpares de um número inteiro longo
    public static double obterMediaDosAlgarismosImpares(long numero) {
        double mediaAlgarismosImpares;

        if (obterNumeroDeAlgarismosImpares(numero) == 0) {
            mediaAlgarismosImpares = -1;   // retorna -1 caso não existam números impares
        } else {
            mediaAlgarismosImpares = (double) obterSomaDosAlgarismosImpares(numero) / (double) obterNumeroDeAlgarismosImpares(numero);
        }
        return mediaAlgarismosImpares;
    }

    /***************************************  Exercicio 6J  ***************************************/
    //Um número inteiro longo cujos dígitos estão pela ordem inversa (e.g. dado 987 retorna 789).
    public static long obterNumeroInvertido(long numero) {
        long numeroInvertido = 0;
        int numeroAlgarismos = obterNumeroDeDigitos(numero);

        for (int digito = 1; digito <= numeroAlgarismos; digito++) {             // começando no digito das unidades, depois dezenas, centenas, ...
            int algarismo = obterUltimoAlgarismoDoNumero(numero);                // obter o digito mais a direita
            numeroInvertido += algarismo * Math.pow(10, (numeroAlgarismos - digito));
            // inserir o algarimo obtido da esquerda para a direita, usando a potencia de 10 ao numero de digitos, ex 10^0 dá o algarismo das unidades
            numero = retirarUltimoAlgarismoAoNumero(numero);
        }
        return numeroInvertido;
    }

    /***************************************  Exercicio 7A  ***************************************/
    // Verifique se um número inteiro longo é capicua.
    public static boolean verificarSeNumeroECapicua(long numero) {
        return numero == obterNumeroInvertido(numero);
    }

    /***************************************  Exercicio 7B  ***************************************/
    // Verifique se um dado número é um número de Amstrong, i.e. se for igual à soma dos cubos dos seus algarismos.
    public static boolean verificarSeNumeroEAmstrong(long numero) {
        return numero == obterSomaDosCubosDosAlgarismos(numero);
    }

    public static long obterSomaDosCubosDosAlgarismos(long numero) {
        int numeroDeDigitos = obterNumeroDeDigitos(numero);
        long somaDosCubosDosAlgarismos = 0;

        for (int digito = 1; digito <= numeroDeDigitos; digito++) {
            int algarismo = obterUltimoAlgarismoDoNumero(numero);     // obter o ultimo algarismo
            somaDosCubosDosAlgarismos += Math.pow(algarismo, 3);      // calcular o cubo desse algarismo e ir somando os cubos
            numero = retirarUltimoAlgarismoAoNumero(numero);          // retirar o ultimo algarismo e continuar o ciclo
        }
        return somaDosCubosDosAlgarismos;
    }

    /***************************************  Exercicio 7C  ***************************************/
    // Retorne a primeira capicua num intervalo dado
    public static int obterPrimeiraCapicuaNumIntervalo(int limiteInferior, int limiteSuperior) {

        for (int numeroNoIntervalo = limiteInferior; numeroNoIntervalo <= limiteSuperior; numeroNoIntervalo++) {
            if (verificarSeNumeroECapicua(numeroNoIntervalo)) {
                return numeroNoIntervalo;     // retorna a primeira capicua que encontre
            }
        }
        return -1;  // caso não encontre nenhuma retorna -1
    }

    /***************************************  Exercicio 7D  ***************************************/
    // Retorne a maior capicua num intervalo dado.
    public static int obterMaiorCapicuaNumIntervalo(int limiteInferior, int limiteSuperior) {

        int maiorCapicuaNumIntervalo = -1;  // caso não encontre nenhuma retorna -1

        for (int numeroNoIntervalo = limiteInferior; numeroNoIntervalo <= limiteSuperior; numeroNoIntervalo++) {
            if (verificarSeNumeroECapicua(numeroNoIntervalo)) {       // encontra cada capicua
                if (numeroNoIntervalo > maiorCapicuaNumIntervalo) {   // verifica se é superior que a capicua maior encontrada
                    maiorCapicuaNumIntervalo = numeroNoIntervalo;     // se for superior passa a ser essa a maior
                }
            }
        }
        return maiorCapicuaNumIntervalo;
    }

    /***************************************  Exercicio 7E  ***************************************/
    // Retorne o número de capicuas num intervalo dado
    public static int obterNumeroCapicuasNumIntervalo(int limiteInferior, int limiteSuperior) {

        int numeroCapicuasNumIntervalo = 0;

        for (int numeroNoIntervalo = limiteInferior; numeroNoIntervalo <= limiteSuperior; numeroNoIntervalo++) {
            if (verificarSeNumeroECapicua(numeroNoIntervalo)) {       // encontra cada capicua
                numeroCapicuasNumIntervalo++;                        // vai somando o numero de capicuas
            }
        }
        return numeroCapicuasNumIntervalo;
    }

    /***************************************  Exercicio 7F  ***************************************/
    // Retorne o primeiro número de Amstrong num dado intervalo
    public static int obterPrimeiroAmstrongNumIntervalo(int limiteInferior, int limiteSuperior) {

        for (int numeroNoIntervalo = limiteInferior; numeroNoIntervalo <= limiteSuperior; numeroNoIntervalo++) {
            if (verificarSeNumeroEAmstrong(numeroNoIntervalo)) {
                return numeroNoIntervalo;     // retorna o primeiro numero de amstrong encontrado
            }
        }
        return -1;  // caso não encontre nenhum retorna -1
    }

    /***************************************  Exercicio 7G  ***************************************/
    // Retorne a quantidade de números de Amstrong num dado intervalo.
    public static int obterQuantidadeDeNumerosAmstrongNumIntervalo(int limiteInferior, int limiteSuperior) {

        int quantidadeDeNumerosAmstrongNumIntervalo = 0;

        for (int numeroNoIntervalo = limiteInferior; numeroNoIntervalo <= limiteSuperior; numeroNoIntervalo++) {
            if (verificarSeNumeroEAmstrong(numeroNoIntervalo)) {       // encontra cada número de amstrong
                quantidadeDeNumerosAmstrongNumIntervalo++;            // vai somando o quantidade de numeros de amstrong que encontra
            }
        }
        return quantidadeDeNumerosAmstrongNumIntervalo;
    }

    /***************************************  Exercicio 8  ****************************************/
    // Ler números positivos até que a soma acumulada destes seja superior a um dado número introduzido pelo utilizador.
    // Antes de terminar, o algoritmo deve mostrar o menor de todos esses números.
    public static void exercicio8() {
        int somaAcumulada = 0;
        int menorNumeroIntroduzido = 1000000000;   // assume se um valor alto para depois determinar o menor

        Scanner ler = new Scanner(System.in);
        System.out.println("A partir de que valor da soma acumulada pretende terminar a leitura de números:");
        int somaLimitante = ler.nextInt();

        do {
            System.out.println("Introduza um número:");
            int numero = ler.nextInt();
            if (numero < 0) {
                do {
                    System.out.println("Numero introduzido negativo. Introduza um novo número:");
                    numero = ler.nextInt();         // repete o passo de leitura do número caso o valor introduzido seja negativo
                } while (numero < 0);
            }
            somaAcumulada += numero;                  // vai somando cada número introduzido
            if (numero < menorNumeroIntroduzido) {    // caso o numero introduzido seja inferior ao anterior menor
                menorNumeroIntroduzido = numero;      // este assume o valor de menor
            }
        } while (somaAcumulada < somaLimitante);     // Enquanto o valor introduzido não ultrapassar o da soma acumulada definida continua a leitura

        System.out.println("O menor número que introduziu foi " + menorNumeroIntroduzido);
    }


    /***************************************  Exercicio 9  ****************************************/
    // Ler o número de horas extraordinárias e o salário base de cada funcionário;
    // Calcular e mostrar o salário mensal que cada empregado irá receber (salário base + valor referente às horas extraordinárias);
    // Calcular e mostrar a média dos salários mensais pagos pela empresa, no mês corrente.
    public static void exercicio9() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o salário base dos seus funcionários:");
        double salarioBase = ler.nextDouble();
        if (salarioBase <= 0) {
            do {
                System.out.println("Salário base inválido. Introduza o salário base correto dos seus funcionários:");
                salarioBase = ler.nextDouble();
            } while (salarioBase <= 0);     // obriga a inserir um salário superior a 0
        }

        double precoHoraExtra = salarioBase * 0.02;      // calcula o preço da hora extra, que corresponde a 2% do salário base
        double somaDosSalarios = 0;
        int numeroFuncionarios = 0;
        int numeroDeHorasExtra;

        do {
            System.out.println("Introduza o número de horas extra que o funcionário realizou no mês passado:");
            numeroDeHorasExtra = ler.nextInt();
            while (numeroDeHorasExtra < -1) {      // obriga a inserir horas extra positivas (exceto -1 que vai servir para parar a leitura)
                System.out.println("Numero de horas extra inválido. Introduza o número correto:");
                numeroDeHorasExtra = ler.nextInt();
            }
            if (numeroDeHorasExtra != -1) {     // se não for inserido o -1, realizar os calculos
                double salarioTotal = salarioBase + (numeroDeHorasExtra * precoHoraExtra);  // calcular o salario de cada funcionario
                System.out.println("O salário do funcionário é de " + salarioTotal + " euros");
                somaDosSalarios += salarioTotal;    // somar todos os salários pagos
                numeroFuncionarios++;               // contar o número de funcionários
            }
        } while (numeroDeHorasExtra != -1);         // enquanto não for inserido o -1 continuar a leitura de novos dados

        double mediaDosSalarios = somaDosSalarios / (double) numeroFuncionarios;     // calcular a media dos salarios pagos
        System.out.println("A média dos salários dos seus funcionários é de " + mediaDosSalarios + " euros.");
    }

    /***************************************  Exercicio 10  ***************************************/
    // Ler números positivos até que o produto acumulado destes seja superior a um dado número introduzido pelo utilizador.
    // Antes de terminar, o algoritmo deve mostrar o maior de todos esses números.
    public static void exercicio10() {
        int produtoAcumulado = 1;
        int maiorNumeroIntroduzido = -1;   // assume se um valor negativo para depois determinar o maior

        Scanner ler = new Scanner(System.in);
        System.out.println("A partir de que valor do produto acumulado pretende terminar a leitura de números:");
        int produtoLimitante = ler.nextInt();

        do {
            System.out.println("Introduza um número:");
            int numero = ler.nextInt();
            if (numero < 0) {
                do {
                    System.out.println("Numero introduzido negativo. Introduza um novo número:");
                    numero = ler.nextInt();         // repete o passo de leitura do número caso o valor introduzido seja negativo
                } while (numero < 0);
            }
            produtoAcumulado *= numero;               // vai multiplicando os números introduzido
            if (numero > maiorNumeroIntroduzido) {    // caso o numero introduzido seja superior ao anterior maior
                maiorNumeroIntroduzido = numero;      // este assume o valor de maior
            }
        } while (produtoAcumulado <= produtoLimitante);
        // Enquanto o valor introduzido não ultrapassar o do produto acumulado definido continua a leitura

        System.out.println("O maior número que introduziu foi " + maiorNumeroIntroduzido);
    }


    /***************************************  Exercicio 11  ***************************************/
    // Dado um número N de 1 a 20, apresente todas as maneiras possíveis de obter esse número N, somando dois números de 0 a 10,
    // independentemente da ordem desses dois números. No final deve indicar quantas maneiras diferentes foram encontradas.
    public static String obterNumeroDeSomasPossiveisParaObterUmNumero(int numero) {
        int numeroDeSomasPossiveisParaObterUmNumero = 0;
        String somasPossiveisParaObterUmNumero = "";

        if (numero < 1 || numero > 20) {
            return "Número fora do intervalo";             // caso número introduzido fora do intervalo 1 - 20
        }

        for (int numeroA = 0; numeroA <= 10; numeroA++) {                           // o número A vai de 0 a 10
            for (int numeroB = numeroA; numeroB <= 10; numeroB++) {
                // o número B começa no numero A e vai até 10, evita voltar a identificar a mesma combinação numa ordem contrária
                if ((numeroA + numeroB) == numero) {                      // verifica se a soma de A e B é igual ao numero introduzido
                    numeroDeSomasPossiveisParaObterUmNumero++;            // se sim aumenta as somas possiveis
                    somasPossiveisParaObterUmNumero += numeroA + "+" + numeroB + " ";   // e guarda os numeros em questao
                }
            }
        }

        if (numeroDeSomasPossiveisParaObterUmNumero == 1) {
            return "Existe " + numeroDeSomasPossiveisParaObterUmNumero + " maneira diferente de obter o número " + numero + " : " + somasPossiveisParaObterUmNumero;
        } else {
            return "Existem " + numeroDeSomasPossiveisParaObterUmNumero + " maneiras diferentes de obter o número " + numero + " : " + somasPossiveisParaObterUmNumero;
        }
    }


    /***************************************  Exercicio 12  ***************************************/
    // Determinar as raízes de N equações do 2º grau, representadas segundo a forma ax2+bx+c=0.
    // Os valores dos coeficientes (a e b) e do termo independente (c) são fornecidos.
    public static void exercicio12() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de equações que pretende testar:");
        int numeroDeEquacoes = ler.nextInt();

        // para o numero de equações dado vai determinar as raizes de cada uma delas
        for (int i = 1; i <= numeroDeEquacoes; i++) {
            System.out.println("Introduza o valor de a:");
            double a = ler.nextDouble();
            System.out.println("Introduza o valor de b:");
            double b = ler.nextDouble();
            System.out.println("Introduza o valor de c:");
            double c = ler.nextDouble();
            String raizesDaEquacao = obterRaizesEquacaoSegundoGrau(a, b, c);
            System.out.println(raizesDaEquacao);
        }
    }

    public static String obterRaizesEquacaoSegundoGrau(double a, double b, double c) {

        String raizesEquacaoSegundoGrau;

        if (a == 0) {
            raizesEquacaoSegundoGrau = "Não é equação do segundo grau";    // se a for 0, então a equação não é de segundo grau
        } else {
            double discriminante = Math.pow(b, 2) - (4 * a * c);     // o discriminante permite saber o tipo de raiz resultante da equação
            if (discriminante > 0) {
                raizesEquacaoSegundoGrau = "A equação tem duas raízes reais";       // se positivo
            } else if (discriminante == 0) {
                raizesEquacaoSegundoGrau = "A equação tem uma raiz dupla";          // se igual a zero
            } else {
                raizesEquacaoSegundoGrau = "A equação tem raízes imaginárias";      // se negativo
            }
        }
        return raizesEquacaoSegundoGrau;
    }

    /***************************************  Exercicio 13  ***************************************/
    // Receber códigos de um produto e classifica-os de acordo com a tabela
    // A leitura de códigos termina quando for introduzido 0
    public static void exercicio13() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o código do produto:");
        int codigoDoProduto = ler.nextInt();
        while (codigoDoProduto != 0) {        // ler produtos até ser introduzido o 0
            String classificacaoDoProduto = obterClassificacaoDoProduto(codigoDoProduto);  // obter a classificação atraves do metodo criado
            System.out.println("O produto pertence à categoria: " + classificacaoDoProduto);
            System.out.println("Introduza o código do próximo produto:");         // continuar a pedir mais dados
            codigoDoProduto = ler.nextInt();
        }
    }

    public static String obterClassificacaoDoProduto(int codigoDoProduto) {
        String classificacaoDoProduto;

        // conforme o código do produto ( varia entre 1 e 15) está associada uma categoria
        if (codigoDoProduto == 1) {
            classificacaoDoProduto = "Alimento não perecível";
        } else if (codigoDoProduto >= 2 && codigoDoProduto <= 4) {
            classificacaoDoProduto = "Alimento perecível";
        } else if (codigoDoProduto == 5 || codigoDoProduto == 6) {
            classificacaoDoProduto = "Vestuário";
        } else if (codigoDoProduto == 7) {
            classificacaoDoProduto = "Higiene pessoal";
        } else if (codigoDoProduto >= 8 && codigoDoProduto <= 15) {
            classificacaoDoProduto = "Limpeza e utensílios domésticos";
        } else {
            classificacaoDoProduto = "Código Inválido";
        }
        return classificacaoDoProduto;
    }

    /***************************************  Exercicio 14  ***************************************/
    // Obter o valor de câmbio de valores em euros, numa das moedas indicadas na tabela.
    // As opções de câmbio possíveis são as seguintes: D (dólar), L (libra), I (Iene), C (Coroa Sueca) e F (Franco Suíço).
    // A leitura de valores termina quando for introduzido um nº negativo
    public static void exercicio14() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Para cada moeda introduza o cógigo correspondente: Dólar - D, Libra - L, Iene - I, Coroa Sueca - CS e Franco Suíco - FS");
        System.out.println("Introduza a moeda da qual pretende fazer as conversões para euros:");
        String moedaACambiar = ler.next();
        System.out.println("Introduza o valor a cambiar para euros:");
        double valorACambiar = ler.nextDouble();

        while (valorACambiar >= 0) {     // enquanto não for introduzido um valor negativo continuam as conversões
            double cambioEmEuros = obterCambioEmEuros(valorACambiar, moedaACambiar);       // atraves do metodo calcular o câmbio
            System.out
                    .println("O valor de " + valorACambiar + " " + moedaACambiar + " equivale a " + String.format("%.2f", cambioEmEuros) + " euros.");
            System.out.println("Introduza o próximo valor a cambiar para euros:");      // pedir o próximo valor a cambiar
            valorACambiar = ler.nextDouble();
        }
    }

    public static double obterCambioEmEuros(double valorACambiar, String moedaACambiar) {
        double cambioEmEuros;

        // o câmbio para euros é feito de acordo com a taxa de câmbio de cada moeda, cada moeda é representada por um código
        if (moedaACambiar.equals("D")) {                       // Dólar
            cambioEmEuros = valorACambiar * 1.534;
        } else if (moedaACambiar.equals("L")) {                // Libra
            cambioEmEuros = valorACambiar * 0.774;
        } else if (moedaACambiar.equals("I")) {                // Iene
            cambioEmEuros = valorACambiar * 161.480;
        } else if (moedaACambiar.equals("CS")) {               // Coroa Sueca
            cambioEmEuros = valorACambiar * 9.593;
        } else if (moedaACambiar.equals("FS")) {               // Franco Suíço
            cambioEmEuros = valorACambiar * 1.601;
        } else {
            cambioEmEuros = -1;       // caso a moeda não faça parte das possibilidades retorna -1
        }
        return cambioEmEuros;         // retorna o valor em euros
    }

    /***************************************  Exercicio 15  ***************************************/
    //Receber as notas inteiras, entre 0 e 20, dos alunos de uma turma
    //Mostrar as notas qualitativas correspondentes, de acordo com a tabela de equivalências
    //A leitura das notas termina quando for introduzida uma nota negativa
    public static void exercicio15() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a nota do aluno:");
        int notaQuantitativa = ler.nextInt();

        while (notaQuantitativa >= 0) {                                     // enquanto não for introduzido um valor negativo continuam as conversões
            String notaQualitativa = obterNotaQualitativa(notaQuantitativa);       // atraves do metodo calcular a nota qualitativa
            System.out.println("A nota " + notaQuantitativa + " corresponde a " + notaQualitativa);
            System.out.println("Introduza a nota do próximo aluno:");              // pedir a próxima nota
            notaQuantitativa = ler.nextInt();
        }
    }

    public static String obterNotaQualitativa(int notaQuantitativa) {
        String notaQualitativa;

        // de acordo com a nota quantitativa existe a nota qualitativa correspondente
        if (notaQuantitativa >= 0 && notaQuantitativa <= 4) {
            notaQualitativa = "Mau";
        } else if (notaQuantitativa >= 5 && notaQuantitativa <= 9) {
            notaQualitativa = "Medíocre";
        } else if (notaQuantitativa >= 10 && notaQuantitativa <= 13) {
            notaQualitativa = "Suficiente";
        } else if (notaQuantitativa >= 14 && notaQuantitativa <= 17) {
            notaQualitativa = "Bom";
        } else if (notaQuantitativa >= 18 && notaQuantitativa <= 20) {
            notaQualitativa = "Muito Bom";
        } else {
            notaQualitativa = "Nota inválida";       // caso a nota quantitativa não se encontre entre 0 e 20, retorna nota inválida
        }
        return notaQualitativa;         // retorna a nota qualitativa
    }

    /***************************************  Exercicio 16  ***************************************/
    // Numa determinada empresa, o salário bruto dos seus trabalhadores está sujeito à seguinte regra de imposto a reter:
    // o montante até 500€ está sujeito a um imposto de 10%; o montante entre 500€ e 1000€
    // está sujeito a um imposto de 15%; e o montante acima de 1000€, a um imposto de 20%.
    // Dado o salário bruto de um trabalhador calcule o respetivo salário líquido.

    // caso o utilizador pretenda fazer várias leitura, até introduzir um valor negativo ou zero
    public static void exercicio16() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o salário bruto do trabalhador:");
        double salarioBruto = ler.nextDouble();

        while (salarioBruto > 0) {    // enquanto não for introduzido zero ou número negativo continuar as leituras
            double salarioLiquido = obterSalarioLiquido(salarioBruto);   // calcular o salário liquido através do método
            System.out.println("O salário bruto de " + String.format("%.2f", salarioBruto) + " euros corresponde ao salario liquido de " + String
                    .format("%.2f", salarioLiquido) + " euros.");
            System.out.println("Introduza o salário bruto do próximo trabalhador:");
            salarioBruto = ler.nextDouble();
        }
    }

    public static double obterSalarioLiquido(double salarioBruto) {

        double salarioLiquido;

        // para cada nível de salário bruto existe uma taxa de imposto associada, depois calcula-se o valor dessa taxa e subtrai-se ao salario bruto
        if (salarioBruto > 0 && salarioBruto <= 500) {
            salarioLiquido = salarioBruto - (salarioBruto * 0.10);
        } else if (salarioBruto > 500 && salarioBruto <= 1000) {
            salarioLiquido = salarioBruto - (salarioBruto * 0.15);
        } else if (salarioBruto > 1000) {
            salarioLiquido = salarioBruto - (salarioBruto * 0.20);
        } else {
            salarioLiquido = -1;                   // caso seja inserido um salário bruto negativo retorna sempre -1
        }
        return salarioLiquido;                     // retorna o valor do salário liquido (salario bruto menos os impostos associados)
    }

    /***************************************  Exercicio 17A  **************************************/
    // Dado o peso de um animal e a quantidade de ração que come diariamente, indique se essa quantidade é a adequada para um animal com esse peso.
    public static void exercicio17A() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o peso (em kg) do seu cão:");
        double pesoCao = ler.nextDouble();
        System.out.println("Qual a quantidade de ração (em g) que o seu cão come por dia:");
        int racaoPorDia = ler.nextInt();

        String categoriaRacaCao = obterCategoriaRacaCao(pesoCao);                             // obtem a categoria de raça do cão de acordo com o peso
        boolean racaoAdequada = verificarSeRacaoAdequada(categoriaRacaCao, racaoPorDia);      // através da raça e da categoria verifica se é adequada

        if (racaoAdequada) {      // se for adequada imprime a seguinte mensagem, se não imprime a seguinte
            System.out.println("A quantidade de ração É ADEQUADA ao tipo de raça do seu cão.");
        } else {
            System.out.println("A quantidade de ração NÃO É ADEQUADA ao tipo de raça do seu cão.");
        }
    }

    public static boolean verificarSeRacaoAdequada(String categoriaRacaCao, int racaoPorDia) {

        // variavel sempre falsa exceto nos casos abaixo
        boolean racaoAdequada = false;

        // de acordo com a categoria da raça do cão existe uma quantidade de ração adequado
        // nestes casos a variavel racaoAdequada assume o valor true
        // assumindo que o valor de raçao por dia introduzido tem de corresponder exatamente ao ideal para aquela categoria de raça
        if (categoriaRacaCao.equals("Pequena")) {
            if (racaoPorDia == 100) {
                racaoAdequada = true;
            }
        } else if (categoriaRacaCao.equals("Média")) {
            if (racaoPorDia == 250) {
                racaoAdequada = true;
            }
        } else if (categoriaRacaCao.equals("Grande")) {
            if (racaoPorDia == 300) {
                racaoAdequada = true;
            }
        } else if (categoriaRacaCao.equals("Gigante")) {
            if (racaoPorDia == 500) {
                racaoAdequada = true;
            }
        }
        return racaoAdequada;
    }

    public static String obterCategoriaRacaCao(double pesoCao) {
        // através do peso do cão em kg obtém-se a categoria da raça de acordo com o peso
        String categoriaRacaCao;
        if (pesoCao > 0 && pesoCao <= 10) {
            categoriaRacaCao = "Pequena";
        } else if (pesoCao > 10 && pesoCao <= 25) {
            categoriaRacaCao = "Média";
        } else if (pesoCao > 25 && pesoCao <= 45) {
            categoriaRacaCao = "Grande";
        } else if (pesoCao > 45) {
            categoriaRacaCao = "Gigante";
        } else {
            categoriaRacaCao = "Peso inválido";  // caso seja introduzido peso zero ou negativo, retorna que é inválido
        }
        return categoriaRacaCao;
    }

    /***************************************  Exercicio 17B  **************************************/
    // Dado o peso de um animal e a quantidade de ração que come diariamente, indique se essa quantidade é a adequada para um animal com esse peso.
    // Agora para um conjunto arbitrário de cães. A solução deverá terminar quando for lido um valor negativo para o peso do animal
    public static void exercicio17B() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Qual a quantidade de ração (em g) que o seu cão come por dia:");
        int racaoPorDia = ler.nextInt();
        while (racaoPorDia <= 0) {         // se o valor da ração for igual ao inferior a zero pedir novamente o valor
            System.out.println("Valor introduzido inválido. Introduza novamente a quantidade de ração (em g) que o seu cão come por dia:");
            racaoPorDia = ler.nextInt();
        }
        System.out.println("Introduza o peso (em kg) do seu cão:");
        double pesoCao = ler.nextDouble();

        while (pesoCao >= 0) {
            String categoriaRacaCao = obterCategoriaRacaCao(pesoCao);        // obtem a categoria de raça do cão de acordo com o peso
            boolean racaoAdequada = verificarSeRacaoAdequada(categoriaRacaCao, racaoPorDia); // através da raça e da categoria verifica se é adequada
            if (racaoAdequada) {   // se for adequada imprime a seguinte mensagem, se não imprime a seguinte
                System.out.println("A quantidade de ração É ADEQUADA ao tipo de raça do seu cão.");
            } else {
                System.out.println("A quantidade de ração NÃO É ADEQUADA ao tipo de raça do seu cão.");
            }
            System.out.println("Qual a quantidade de ração (em g) que o seu cão come por dia:");    // pede novamente os dados
            racaoPorDia = ler.nextInt();
            while (racaoPorDia <= 0) {
                System.out.println("Valor introduzido inválido. Introduza novamente a quantidade de ração (em g) que o seu cão come por dia:");
                racaoPorDia = ler.nextInt();
            }
            System.out.println("Introduza o peso (em kg) do seu cão:");
            pesoCao = ler.nextDouble();
        }
    }

    /***************************************  Exercicio 18  ***************************************/
    // Detetar erros de escrita do número do Bilhete de Identidade ou Cartão de Cidadão.
    public static void exercicio18() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o seu número de CC:");
        String numeroCC = ler.next();
        System.out.println("Introduza o seu digito de controlo do CC:");
        int digitoControloCC = ler.nextInt();

        boolean ccValido = verificarSeCCValido(numeroCC, digitoControloCC);  // atraves do metodo verifica a viabilidade do número de CC inserido

        if (ccValido) {
            System.out.println("O seu número de CC " + numeroCC + " É VÁLIDO");
        } else {
            System.out.println("O seu número de CC " + numeroCC + " NÃO É VÁLIDO");
        }
    }

    public static boolean verificarSeCCValido(String numeroCC, int digitoControloCC) {
        // verifica se a soma ponderada dos digitos do CC obtido atraves do metodo referido é múltipla de 11
        return verificarSeXMultiploDeY(obterSomaPonderadaNumeroCC(numeroCC, digitoControloCC), 11);
    }

    public static int obterSomaPonderadaNumeroCC(String numeroCC, int digitoControloCC) {
        // assumindo que o número do CC tem 8 digitos mais um digito de controlo separado

        int numeroDeDigitos = numeroCC.length();    // através do tamanho da string obter o número de digitos do CC
        int somaPonderada = digitoControloCC;  // a soma ponderada começa por incluir o digito de controlo (que é elevado a 1 e por isso não altera)

        if (numeroDeDigitos == 8 && digitoControloCC >= 0 && digitoControloCC <= 9) {
            // confirma que o número de digitos é 8, e que o digito controlo está incluido no intervalo 0 a 9
            for (int digito = 0; digito < numeroDeDigitos; digito++) {    // percorrer cada caracter da string - corresponde a cada digito do numero
                int algarismo = Character.getNumericValue(numeroCC.charAt(digito));  // retirar o número correspondente de cada digito na string
                somaPonderada += (algarismo * (9 - digito));
                // o algarismo em cada posição é multiplicado pelo valor da posição, neste caso a posição na string é inversa a do número do CC
                // começa no 9º digito, porque para este efeito o 1ª digito do CC será o de controlo, e depois os do numero da direita para a esquerda
            }
        } else {
            somaPonderada = -1;   // retorna -1 caso as condições expostas no inicio não se verificarem
        }
        return somaPonderada;
    }

    /***************************************  Exercicio 19  ***************************************/
    //Dada uma sequência de n números inteiros positivos de um só algarismo, a reorganize de modo a obter os pares à direita e os ímpares à esquerda.
    public static void exercicio19() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a sequencia de números:");
        // assumindo uma sequencia seguida em que cada algarismo será analisado individualmente
        String sequenciaOriginal = ler.next();

        String sequenciaAlterada = obterSequenciaAlterada(sequenciaOriginal);
        // impares esquerda + pares direita =  sequencia alterada

        System.out.println("A nova sequencia é " + sequenciaAlterada);
    }

    public static String obterSequenciaAlterada(String sequenciaOriginal) {

        String sequenciaImpares = "";
        String sequenciaPares = "";


        for (int digito = 0; digito < sequenciaOriginal.length(); digito++) {    // percorrer cada digito da string fornecida, até ao seu tamanho
            int algarismo = Character.getNumericValue(sequenciaOriginal.charAt(digito)); // retirar o número de cada posição na string
            if (verificarSeEPar(algarismo)) {
                sequenciaPares = sequenciaPares.concat(String.valueOf(algarismo));   // se for par adicionar o mesmo na sequencia dos numeros pares
            } else {
                sequenciaImpares = sequenciaImpares
                        .concat(String.valueOf(algarismo)); // se for impar adicionar o mesmo na sequencia dos numeros impares
            }
        }

        String sequenciaAlterada = sequenciaImpares.concat(sequenciaPares);
        // juntar a sequencia dos impares à esquerda com a sequencia dos pares à direita

        return sequenciaAlterada;
    }

    public static void exercicio19V2() {
        // o mesmo exercicio resolvido de outra forma
        // assumindo que a leitura de dados termina quando for introduzido um número negativo

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza um número:");
        // assumindo uma sequencia em que cada número é introduzido individualmente
        int numero = ler.nextInt();
        while (numero > 9) {      // se número não tiver um só algarismo pedir nova entrada de dados
            System.out.println("Número só pode ter 1 algarismo. Introduza novamente um número:");
            numero = ler.nextInt();
        }

        String sequenciaImpares = "";
        String sequenciaPares = "";

        while (numero >= 0) {   // quando for inserido um numero negativo a sequencia termina
            if (verificarSeEPar(numero)) {
                sequenciaPares = sequenciaPares.concat(String.valueOf(numero));  // se for par o numero é adicionado à sequencia correspondente
            } else {
                sequenciaImpares = sequenciaImpares.concat(String.valueOf(numero)); // se for impar o numero é adicionado à sequencia correspondente
            }
            System.out.println("Introduza o próximo número:");  // pedir o próximo número da sequencia
            numero = ler.nextInt();
            while (numero > 9) { // se número não tiver um só algarismo pedir nova entrada de dados
                System.out.println("Número só pode ter 1 algarismo. Introduza novamente um número:");
                numero = ler.nextInt();
            }
        }

        String sequenciaAlterada = sequenciaImpares.concat(sequenciaPares);
        // juntar a sequencia dos impares à esquerda com a sequencia dos pares à direita

        System.out.println("A nova sequencia é " + sequenciaAlterada);
    }

    /***************************************  Exercicio 20  ***************************************/
    // No século I D.C., os números inteiros positivos dividiam-se em três categorias: perfeitos, abundantes e reduzidos.
    // Elabore uma solução em Java que dado um número inteiro classifique esse número.
    public static void exercicio20() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número:");
        int numero = ler.nextInt();
        while (numero <= 0) {      // caso o número não pertenca aos números positivos é pedido nova leitura de dados
            System.out.println("Número inválido. Introduza um novo número:");
            numero = ler.nextInt();
        }

        String classificacaoDoNumero = obterClassificacaoDoNumero(numero);  // atraves do metodo obter a classificaçao do numero

        System.out.println("O número " + numero + " pertence à categoria dos números " + classificacaoDoNumero);
    }

    public static int obterSomaDosDivisores(int numero) {

        int somaDivisores = 0;

        for (int possivelDivisor = 1; possivelDivisor < numero; possivelDivisor++) {
            // começando no 1 e percorrendo todos os numeros até ao anterior ao numero inserido vamos testar cada um dos possiveis divisores
            if (verificarSeXMultiploDeY(numero, possivelDivisor)) {
                // atraves do metodo de verificar multiplo dá para verificar se o possivel dividor é mesmo divisor do numero inserido
                somaDivisores += possivelDivisor;  // se sim ir somando todos
            }
        }
        return somaDivisores;
    }

    public static String obterClassificacaoDoNumero(int numero) {

        String classificacaoDoNumero;
        int somaDivisores = obterSomaDosDivisores(numero);

        // atraves da tabela fornecida corresponder cada numero à sua categoria de acordo com a sua relação com a soma dos divisores (exceto si mesmo)
        if (numero <= 0) {
            classificacaoDoNumero = "Número inválido";
        } else if (numero == somaDivisores) {
            classificacaoDoNumero = "Perfeitos";
        } else if (numero < somaDivisores) {
            classificacaoDoNumero = "Abundantes";
        } else {
            classificacaoDoNumero = "Reduzidos";
        }
        return classificacaoDoNumero;
    }

}


