package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio7Testes {

    @Test
    void verificarSeNumeroZeroECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNumeroUmECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNumeroDezECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(10);
        assertFalse(resultado);
    }

    @Test
    void verificarSeNumeroOnzeECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(11);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNumeroCentoEVinteUmECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(121);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNumeroCentoEOnzeECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(111);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNumeroCemECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(100);
        assertFalse(resultado);
    }

    @Test
    void verificarSeNumeroMilQuatrocentosEQuarentaEUmECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(1441);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNumeroMilQuatrocentosETrintaEUmECapicua() {
        boolean resultado = Bloco3.verificarSeNumeroECapicua(1431);
        assertFalse(resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Zero() {
        long esperado = 0;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Um() {
        long esperado = 1;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Dois() {
        long esperado = 8;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Tres() {
        long esperado = 27;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(3);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Quatro() {
        long esperado = 64;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(4);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Dez() {
        long esperado = 1;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Onze() {
        long esperado = 2;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(11);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Doze() {
        long esperado = 9;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(12);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_Dezoito() {
        long esperado = 513;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(18);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_CentoEVinteETres() {
        long esperado = 36;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(123);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosCubosDosAlgarismos_NovecentosENoventaENove() {
        long esperado = 2187;
        long resultado = Bloco3.obterSomaDosCubosDosAlgarismos(999);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSeZeroEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeUmEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCincoEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(2);
        assertFalse(resultado);
    }

    @Test
    void verificarSeNoveEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(10);
        assertFalse(resultado);
    }

    @Test
    void verificarSeDezEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(10);
        assertFalse(resultado);
    }

    @Test
    void verificarSeCentoECinquentaETresEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(153);
        assertTrue(resultado);
    }

    @Test
    void verificarSeTrescentosESetentaEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(370);
        assertTrue(resultado);
    }

    @Test
    void verificarSeTrescentosESetentaEUmEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(371);
        assertTrue(resultado);
    }

    @Test
    void verificarSeQuatrocentosESeteEAmstrong() {
        boolean resultado = Bloco3.verificarSeNumeroEAmstrong(407);
        assertTrue(resultado);
    }

    @Test
    void assegurarPrimeiraCapicuaNumIntervalo_Zero_Um() {
        int esperado = 0 ;
        int resultado = Bloco3.obterPrimeiraCapicuaNumIntervalo(0,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void assegurarPrimeiraCapicuaNumIntervalo_Dois_Cinco() {
        int esperado = 2 ;
        int resultado = Bloco3.obterPrimeiraCapicuaNumIntervalo(2,5);
        assertEquals(esperado, resultado);
    }

    @Test
    void assegurarPrimeiraCapicuaNumIntervalo_Dez_Vinte() {
        int esperado = 11 ;
        int resultado = Bloco3.obterPrimeiraCapicuaNumIntervalo(10,20);
        assertEquals(esperado, resultado);
    }

    @Test
    void assegurarPrimeiraCapicuaNumIntervalo_Doze_Vinte() {
        int esperado = -1 ;
        int resultado = Bloco3.obterPrimeiraCapicuaNumIntervalo(12,20);
        assertEquals(esperado, resultado);
    }

    @Test
    void assegurarPrimeiraCapicuaNumIntervalo_Cem_CentoEDez() {
        int esperado = 101 ;
        int resultado = Bloco3.obterPrimeiraCapicuaNumIntervalo(100,110);
        assertEquals(esperado, resultado);
    }

    @Test
    void assegurarPrimeiraCapicuaNumIntervalo_Mil_MilEDez() {
        int esperado = 1001 ;
        int resultado = Bloco3.obterPrimeiraCapicuaNumIntervalo(1000,1010);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_Nove() {
        int esperado = 9;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,9);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Treze_VinteEUm() {
        int esperado = -1;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(13,21);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_CentoEDois_CentoEDez() {
        int esperado = -1;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(102,110);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_Cinquenta() {
        int esperado = 44;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,50);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_Cem() {
        int esperado = 99;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,100);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_Quinhentos() {
        int esperado = 494;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,500);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_Mil() {
        int esperado = 999;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,1000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMaiorCapicuaNumIntervalo_Zero_DezMil() {
        int esperado = 9999;
        int resultado = Bloco3.obterMaiorCapicuaNumIntervalo(0,10000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Zero_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(0,0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Zero_Um() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(0,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Zero_Dez() {
        int esperado = 10;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(0,10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Zero_Vinte() {
        int esperado = 11;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(0,20);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Zero_Cinquenta() {
        int esperado = 14;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(0,50);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Zero_Cem() {
        int esperado = 19;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(0,100);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Doze_VinteEUm() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(12,21);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Cem_Duzentos() {
        int esperado = 10;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(100,200);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_Cem_Mil() {
        int esperado = 90;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(100,1000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroCapicuasNumIntervalo_CentoEDois_CentoEDez() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroCapicuasNumIntervalo(102,110);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Zero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(0,0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Zero_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(0,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Dois_Duzentos() {
        int esperado = 153;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(2,200);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Duzentos_Quatrocentos() {
        int esperado = 370;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(200,400);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_TrescentosESetentaEUm_Quatrocentos() {
        int esperado = 371;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(371,400);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Quatrocentos_Quinhentos() {
        int esperado = 407;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(400,500);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Quinhentos_Mil() {
        int esperado = -1;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(500,1000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarPrimeiroAmstrongNumIntervalo_Dez_Cem() {
        int esperado = -1;
        int resultado = Bloco3.obterPrimeiroAmstrongNumIntervalo(10,100);
        assertEquals(esperado, resultado);
    }


    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Zero_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(0,0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Zero_Um() {
        int esperado = 2;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(0,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Zero_Duzentos() {
        int esperado = 3;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(0,200);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Zero_Quatrocentos() {
        int esperado = 5;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(0,400);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Zero_Quinhentos() {
        int esperado = 6;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(0,500);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Zero_Mil() {
        int esperado = 6;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(0,1000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Dois_Cem() {
        int esperado = 0;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(2,100);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarQuantidadeDeNumerosAmstrongNumIntervalo_Quinhentos_Mil() {
        int esperado = 0;
        int resultado = Bloco3.obterQuantidadeDeNumerosAmstrongNumIntervalo(500,1000);
        assertEquals(esperado, resultado);
    }


}