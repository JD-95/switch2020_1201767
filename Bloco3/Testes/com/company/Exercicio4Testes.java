package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio4Testes {

    @Test
    void verificarNumeroDeMultiplosDe3IgualAUmTeste1() {
        int expected = 1;
        int result = Bloco3.obterNumeroDeMultiplosDe3(0,0);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDe3IgualAUmTeste2() {
        int expected = 1;
        int result = Bloco3.obterNumeroDeMultiplosDe3(3,3);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDe3IgualADoisTeste1() {
        int expected = 2;
        int result = Bloco3.obterNumeroDeMultiplosDe3(0,3);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDe3IgualADoisTeste2() {
        int expected = 2;
        int result = Bloco3.obterNumeroDeMultiplosDe3(0,5);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDe3IgualATres() {
        int expected = 3;
        int result = Bloco3.obterNumeroDeMultiplosDe3(0,6);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDe3IgualAZero() {
        int expected = 0;
        int result = Bloco3.obterNumeroDeMultiplosDe3(1,2);
        assertEquals(expected, result);
    }

    /***************************************************************************/

    @Test
    void verificarNumeroDeMultiplosDeZeroIgualAZeroTeste1() {
        int expected = 0;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,1,0);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeZeroIgualAZeroTeste2() {
        int expected = 0;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(-1,1,0);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmIgualAUm() {
        int expected = 1;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(1,1,1);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmIgualADois() {
        int expected = 2;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,1,1);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmIgualATres() {
        int expected = 3;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,2,1);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmIgualACentoEUm() {
        int expected = 101;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,100,1);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeTresIgualAUm() {
        int expected = 1;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,2,3);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeTresIgualADois() {
        int expected = 2;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,3,3);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeTresIgualATresTeste1() {
        int expected = 3;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(0,6,3);
        assertEquals(expected, result);
    }

    @Test
    void verificarNumeroDeMultiplosDeTresIgualATresTeste2() {
        int expected = 3;
        int result = Bloco3.obterNumeroDeMultiplosNumIntervalo(1,11,3);
        assertEquals(expected, result);
    }

    /***************************************************************************/

    @Test
    void verificarSeZeroMultiploDeZero() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(0,0);
        assertFalse(resultado);
    }

    @Test
    void verificarSeUmMultiploDeZero() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(1,0);
        assertFalse(resultado);
    }

    @Test
    void verificarSeZeroMultiploDeUm() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(0,1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNegUmMultiploDeUm() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(-1,1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeUmMultiploDeNegUm() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(1,-1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeZeroMultiploDeQuinze() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(0,15);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCincoMultiploDeUm() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(5,1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeUmMultiploDeCinco() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(1,5);
        assertFalse(resultado);
    }

    @Test
    void verificarSeDezUmMultiploDeCinco() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(10,5);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCincoUmMultiploDeDez() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(5,10);
        assertFalse(resultado);
    }

    @Test
    void verificarSeNeg10MultiploDeCinco() {
        boolean resultado = Bloco3.verificarSeXMultiploDeY(-10,5);
        assertTrue(resultado);
    }

    /***************************************************************************/

    @Test
    void verificarNumeroDeMultiplosDeZeroEZeroNumIntervalo_Zero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,0,0,0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmEZeroNumIntervalo_Zero_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,0,1,0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmEUmIntervalo_Zero_Zero() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,0,1,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeZeroEUmIntervalo_Zero_Um() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,1,0,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeZeroEUmIntervalo_Zero_Dois() {
        int esperado = 3;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,2,0,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmEDoisIntervalo_Zero_Dois() {
        int esperado = 5;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,2,1,2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmEDoisIntervalo_Zero_Cinco() {
        int esperado = 9;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(0,5,1,2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeMultiplosDeUmEDoisIntervalo_NegCinco_Cinco() {
        int esperado = 16;
        int resultado = Bloco3.obterNumeroDeMultiplosDeDoisNumerosNumIntervalo(-5,5,1,2);
        assertEquals(esperado,resultado);
    }

    /***************************************************************************/

    @Test
    void verificarSomaDosMultiplosDeZeroEZeroNumerosNumIntervalo_Zero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(0,0,0,0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeUmEUmNumerosNumIntervalo_Zero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(0,0,1,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeZeroEZeroNumerosNumIntervalo_Um_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(1,1,0,0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeUmEUmNumerosNumIntervalo_Um_Um() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(1,1,1,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeZeroEUmNumerosNumIntervalo_Um_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(1,1,0,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeZeroEUmNumerosNumIntervalo_Zero_Dois() {
        int esperado = 3;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(0,2,0,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeZeroEUmNumerosNumIntervalo_Zero_Tres() {
        int esperado = 6;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(0,3,0,1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeUmEDoisNumerosNumIntervalo_Zero_Tres() {
        int esperado = 8;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(0,3,1,2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosMultiplosDeUmEDoisNumerosNumIntervalo_Zero_Cinco() {
        int esperado = 21;
        int resultado = Bloco3.obterSomaDosMultiplosDeDoisNumerosNumIntervalo(0,5,1,2);
        assertEquals(esperado,resultado);
    }
}