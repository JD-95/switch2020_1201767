package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio18Testes {

    @Test
    void verificarSomaPonderadaNumeroCC_NegativaTeste1() {
        int esperado = -1;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("1234567", 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_NegativaTeste2() {
        int esperado = -1;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("12345678", -1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_NegativaTeste3() {
        int esperado = -1;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("12345678", 10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste1() {
        int esperado = 45;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("11111111", 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste2() {
        int esperado = 46;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("11111111", 2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste3() {
        int esperado = 9;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("10000000", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste4() {
        int esperado = 8;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("01000000", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste5() {
        int esperado = 7;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("00100000", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste6() {
        int esperado = 6;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("00010000", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste7() {
        int esperado = 5;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("00001000", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste8() {
        int esperado = 4;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("00000100", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste9() {
        int esperado = 3;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("00000010", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaPonderadaNumeroCC_PossiveisTeste10() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaPonderadaNumeroCC("00000001", 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste1() {
        boolean resultado = Bloco3.verificarSeCCValido("10000000", 2);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste2() {
        boolean resultado = Bloco3.verificarSeCCValido("01000000", 3);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste3() {
        boolean resultado = Bloco3.verificarSeCCValido("00100000", 4);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste4() {
        boolean resultado = Bloco3.verificarSeCCValido("00010000", 5);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste5() {
        boolean resultado = Bloco3.verificarSeCCValido("00001000", 6);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste6() {
        boolean resultado = Bloco3.verificarSeCCValido("00000100", 7);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste7() {
        boolean resultado = Bloco3.verificarSeCCValido("00000010", 8);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste8() {
        boolean resultado = Bloco3.verificarSeCCValido("00000001", 9);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste9() {
        boolean resultado = Bloco3.verificarSeCCValido("00002000", 1);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_VerdadeiroTeste10() {
        boolean resultado = Bloco3.verificarSeCCValido("00000031", 0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeCCValido_FalsoTeste1() {
        boolean resultado = Bloco3.verificarSeCCValido("10000000", 1);
        assertFalse(resultado);
    }

    @Test
    void verificarSeCCValido_FalsoTeste2() {
        boolean resultado = Bloco3.verificarSeCCValido("10000000", 3);
        assertFalse(resultado);
    }
}