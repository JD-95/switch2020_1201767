package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio13Testes {

    @Test
    void verificarClassificacaoDoProduto_InvalidoTeste1() {
        String esperado = "Código Inválido";
        String resultado = Bloco3.obterClassificacaoDoProduto(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_InvalidoTeste2() {
        String esperado = "Código Inválido";
        String resultado = Bloco3.obterClassificacaoDoProduto(-1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_InvalidoTeste3() {
        String esperado = "Código Inválido";
        String resultado = Bloco3.obterClassificacaoDoProduto(16);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_AlimentoNaoPerecivel() {
        String esperado = "Alimento não perecível";
        String resultado = Bloco3.obterClassificacaoDoProduto(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_AlimentoPerecivelTeste1() {
        String esperado = "Alimento perecível";
        String resultado = Bloco3.obterClassificacaoDoProduto(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_AlimentoPerecivelTeste2() {
        String esperado = "Alimento perecível";
        String resultado = Bloco3.obterClassificacaoDoProduto(3);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_AlimentoPerecivelTeste3() {
        String esperado = "Alimento perecível";
        String resultado = Bloco3.obterClassificacaoDoProduto(4);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_VestuarioTeste1() {
        String esperado = "Vestuário";
        String resultado = Bloco3.obterClassificacaoDoProduto(5);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_VestuarioTeste2() {
        String esperado = "Vestuário";
        String resultado = Bloco3.obterClassificacaoDoProduto(6);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_HigienePessoal() {
        String esperado = "Higiene pessoal";
        String resultado = Bloco3.obterClassificacaoDoProduto(7);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_LimpezaUtensiliosDomesticosTeste1() {
        String esperado = "Limpeza e utensílios domésticos";
        String resultado = Bloco3.obterClassificacaoDoProduto(8);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_LimpezaUtensiliosDomesticosTeste2() {
        String esperado = "Limpeza e utensílios domésticos";
        String resultado = Bloco3.obterClassificacaoDoProduto(11);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoProduto_LimpezaUtensiliosDomesticosTeste3() {
        String esperado = "Limpeza e utensílios domésticos";
        String resultado = Bloco3.obterClassificacaoDoProduto(15);
        assertEquals(esperado, resultado);
    }






}