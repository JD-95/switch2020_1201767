package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio16Testes {

    @Test
    void verificarSalarioLiquido_Zero() {
        double esperado = -1;
        double resultado = Bloco3.obterSalarioLiquido(0);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_Negativo() {
        double esperado = -1;
        double resultado = Bloco3.obterSalarioLiquido(-1);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaDezTeste1() {
        double esperado = 0.90;
        double resultado = Bloco3.obterSalarioLiquido(1);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaDezTeste2() {
        double esperado = 450;
        double resultado = Bloco3.obterSalarioLiquido(500);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaDezTeste3() {
        double esperado = 225.45;
        double resultado = Bloco3.obterSalarioLiquido(250.50);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaQuinzeTeste1() {
        double esperado = 425.85;
        double resultado = Bloco3.obterSalarioLiquido(501);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaQuinzeTeste2() {
        double esperado = 850;
        double resultado = Bloco3.obterSalarioLiquido(1000);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaQuinzeTeste3() {
        double esperado = 637.92;
        double resultado = Bloco3.obterSalarioLiquido(750.50);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaVinteTeste1() {
        double esperado = 800.80;
        double resultado = Bloco3.obterSalarioLiquido(1001);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaVinteTeste2() {
        double esperado = 1600;
        double resultado = Bloco3.obterSalarioLiquido(2000);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarSalarioLiquido_TaxaVinteTeste3() {
        double esperado = 1200.40;
        double resultado = Bloco3.obterSalarioLiquido(1500.50);
        assertEquals(esperado, resultado, 0.01);
    }
}