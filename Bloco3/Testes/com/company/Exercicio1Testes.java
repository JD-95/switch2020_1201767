package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio1Testes {

    @Test
    void verificarResExercicio1IgualAZeroTeste1() {
        int expected = 0;
        int result = Bloco3.obterFatorial(-1);
        assertEquals(expected, result);
    }

    @Test
    void verificarResExercicio1IgualAZeroTeste2() {
        int expected = 0;
        int result = Bloco3.obterFatorial(-2);
        assertEquals(expected, result);
    }

    @Test
    void verificarResExercicio1IgualAUmTeste1() {
        int expected = 1;
        int result = Bloco3.obterFatorial(0);
        assertEquals(expected, result);
    }

    @Test
    void verificarResExercicio1IgualAUmTeste2() {
        int expected = 1;
        int result = Bloco3.obterFatorial(1);
        assertEquals(expected, result);
    }



    @Test
    void verificarResExercicio1IgualADois() {
        int expected = 2;
        int result = Bloco3.obterFatorial(2);
        assertEquals(expected, result);
    }

    @Test
    void verificarResExercicio1IgualASeis() {
        int expected = 6;
        int result = Bloco3.obterFatorial(3);
        assertEquals(expected, result);
    }

    @Test
    void verificarResExercicio1IgualAVinteEQuatro() {
        int expected = 24;
        int result = Bloco3.obterFatorial(4);
        assertEquals(expected, result);
    }

    @Test
    void verificarResExercicio1IgualACentoEVinte() {
        int expected = 120;
        int result = Bloco3.obterFatorial(5);
        assertEquals(expected, result);
    }
}