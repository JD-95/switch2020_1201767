package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio19Testes {

    @Test
    void verificarSequenciaAlteradaTeste1() {
        String esperado = "10";
        String resultado = Bloco3.obterSequenciaAlterada("01");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste2() {
        String esperado = "10";
        String resultado = Bloco3.obterSequenciaAlterada("10");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste3() {
        String esperado = "102";
        String resultado = Bloco3.obterSequenciaAlterada("012");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste4() {
        String esperado = "1302";
        String resultado = Bloco3.obterSequenciaAlterada("0123");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste5() {
        String esperado = "13024";
        String resultado = Bloco3.obterSequenciaAlterada("01234");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste6() {
        String esperado = "135024";
        String resultado = Bloco3.obterSequenciaAlterada("012345");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste7() {
        String esperado = "1350246";
        String resultado = Bloco3.obterSequenciaAlterada("0123456");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste8() {
        String esperado = "13570246";
        String resultado = Bloco3.obterSequenciaAlterada("01234567");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste9() {
        String esperado = "135702468";
        String resultado = Bloco3.obterSequenciaAlterada("012345678");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste10() {
        String esperado = "1357902468";
        String resultado = Bloco3.obterSequenciaAlterada("0123456789");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste11() {
        String esperado = "13579024680";
        String resultado = Bloco3.obterSequenciaAlterada("01234567890");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste12() {
        String esperado = "135791024680";
        String resultado = Bloco3.obterSequenciaAlterada("012345678901");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste13() {
        String esperado = "0";
        String resultado = Bloco3.obterSequenciaAlterada("0");
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSequenciaAlteradaTeste14() {
        String esperado = "1";
        String resultado = Bloco3.obterSequenciaAlterada("1");
        assertEquals(esperado, resultado);
    }
}