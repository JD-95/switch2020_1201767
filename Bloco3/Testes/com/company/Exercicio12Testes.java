package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio12Testes {

    @Test
    void verificarRaizesEquacaoSegundoGrau_NaoESegundoGrau() {
        String esperado = "Não é equação do segundo grau";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(0,1,1);
        assertEquals(esperado, resultado);
    }


    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizDuplaTeste1() {
        String esperado = "A equação tem uma raiz dupla";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,2,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizDuplaTeste2() {
        String esperado = "A equação tem uma raiz dupla";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(-1,-2,-1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizDuplaTeste3() {
        String esperado = "A equação tem uma raiz dupla";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(2.25,3,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesReaisTeste1() {
        String esperado = "A equação tem duas raízes reais";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,3,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesReaisTeste2() {
        String esperado = "A equação tem duas raízes reais";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,3,2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesReaisTeste3() {
        String esperado = "A equação tem duas raízes reais";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1.5,4,2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesReaisTeste4() {
        String esperado = "A equação tem duas raízes reais";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,1,0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesReaisTeste5() {
        String esperado = "A equação tem duas raízes reais";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,-5,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesImaginariasTeste1() {
        String esperado = "A equação tem raízes imaginárias";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,1,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesImaginariasTeste2() {
        String esperado = "A equação tem raízes imaginárias";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,2,1.5);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesImaginariasTeste3() {
        String esperado = "A equação tem raízes imaginárias";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(3,2,1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRaizesEquacaoSegundoGrau_RaizesImaginariasTeste4() {
        String esperado = "A equação tem raízes imaginárias";
        String resultado = Bloco3.obterRaizesEquacaoSegundoGrau(1,0,1);
        assertEquals(esperado, resultado);
    }



}