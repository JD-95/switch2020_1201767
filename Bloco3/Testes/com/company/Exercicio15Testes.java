package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio15Testes {

    @Test
    void verificarNotaQualitativa_InvalidaTeste1() {
        String esperado = "Nota inválida";
        String resultado = Bloco3.obterNotaQualitativa(-1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_InvalidaTeste2() {
        String esperado = "Nota inválida";
        String resultado = Bloco3.obterNotaQualitativa(21);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_MauTeste1() {
        String esperado = "Mau";
        String resultado = Bloco3.obterNotaQualitativa(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_MauTeste2() {
        String esperado = "Mau";
        String resultado = Bloco3.obterNotaQualitativa(4);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_MediocreTeste1() {
        String esperado = "Medíocre";
        String resultado = Bloco3.obterNotaQualitativa(5);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_MediocreTeste2() {
        String esperado = "Medíocre";
        String resultado = Bloco3.obterNotaQualitativa(9);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_SuficienteTeste1() {
        String esperado = "Suficiente";
        String resultado = Bloco3.obterNotaQualitativa(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_SuficienteTeste2() {
        String esperado = "Suficiente";
        String resultado = Bloco3.obterNotaQualitativa(13);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_BomTeste1() {
        String esperado = "Bom";
        String resultado = Bloco3.obterNotaQualitativa(14);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_BomTeste2() {
        String esperado = "Bom";
        String resultado = Bloco3.obterNotaQualitativa(17);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_MuitoBomTeste1() {
        String esperado = "Muito Bom";
        String resultado = Bloco3.obterNotaQualitativa(18);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNotaQualitativa_MuitoBomTeste2() {
        String esperado = "Muito Bom";
        String resultado = Bloco3.obterNotaQualitativa(20);
        assertEquals(esperado,resultado);
    }
}