package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio14Testes {

    @Test
    void verificarCambioEmEuros_InvalidoTeste1() {
        double esperado = -1;
        double resultado =  Bloco3.obterCambioEmEuros(1,"Real");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_InvalidoTeste2() {
        double esperado = -1;
        double resultado =  Bloco3.obterCambioEmEuros(1,"Dólar");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_DolarTeste1() {
        double esperado = 1.534;
        double resultado =  Bloco3.obterCambioEmEuros(1,"D");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_DolarTeste2() {
        double esperado = 0.767;
        double resultado =  Bloco3.obterCambioEmEuros(0.5,"D");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_LibraTeste1() {
        double esperado = 0.774;
        double resultado =  Bloco3.obterCambioEmEuros(1,"L");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_LibraTeste2() {
        double esperado = 0.387;
        double resultado =  Bloco3.obterCambioEmEuros(0.5,"L");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_IeneTeste1() {
        double esperado = 161.480;
        double resultado =  Bloco3.obterCambioEmEuros(1,"I");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_IeneTeste2() {
        double esperado = 80.74;
        double resultado =  Bloco3.obterCambioEmEuros(0.5,"I");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_CoroaSuecaTeste1() {
        double esperado = 9.593;
        double resultado =  Bloco3.obterCambioEmEuros(1,"CS");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_CoroaSuecaTeste2() {
        double esperado = 4.797;
        double resultado =  Bloco3.obterCambioEmEuros(0.5,"CS");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_FrancoSuicoTeste1() {
        double esperado = 1.601;
        double resultado =  Bloco3.obterCambioEmEuros(1,"FS");
        assertEquals(esperado,resultado,0.001);
    }

    @Test
    void verificarCambioEmEuros_FrancoSuicoTeste2() {
        double esperado = 0.801;
        double resultado =  Bloco3.obterCambioEmEuros(0.5,"FS");
        assertEquals(esperado,resultado,0.001);
    }


}