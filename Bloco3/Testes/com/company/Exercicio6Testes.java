package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio6Testes {


    @Test
    void verificarNumeroDeDigitos_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeDigitos(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeDigitos(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_Nove() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeDigitos(9);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_Dez() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeDigitos(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_NoventaENove() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeDigitos(99);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_Cem() {
        int esperado = 3;
        int resultado = Bloco3.obterNumeroDeDigitos(100);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_QuatroDigitos() {
        int esperado = 4;
        int resultado = Bloco3.obterNumeroDeDigitos(2657);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeDigitos_MaiorDigitosPossiveis() {
        int esperado = 10;
        int resultado = Bloco3.obterNumeroDeDigitos(1000000000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoZero() {
        long esperado = -1;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoUm() {
        long esperado = -1;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoNove() {
        long esperado = -1;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(9);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoDez() {
        long esperado = 1;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoCem() {
        long esperado = 10;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(100);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoMil() {
        long esperado = 100;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(1000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoMilDuzentosEVinteETres() {
        long esperado = 123;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(1234);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoNoventaECincoMilETrescentosEVinteESete() {
        long esperado = 9532;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(95327);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarRetirarUltimoAlgarismoAoMaiorPossivel() {
        long esperado = 100000000;
        long resultado = Bloco3.retirarUltimoAlgarismoAoNumero(1000000000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_Dois() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_Dez() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_Vinte() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(20);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_CentoEVinteETres() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(123);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_CentoEVinteEQuatro() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(124);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_MaiorNumeroPares() {
        int esperado = 10;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(2000000000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosPares_MaiorNumeroSemPares() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeAlgarismosPares(1111111111);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoZero() {
        int esperado = 0;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoUm() {
        int esperado = 1;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoNove() {
        int esperado = 9;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(9);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoDez() {
        int esperado = 0;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoOitentaENove() {
        int esperado = 9;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(89);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoCentoEDois() {
        int esperado = 2;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(102);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarUltimoAlgarismoDoCentoEVinteETresMilESeiscentosECinquentaEQuatro() {
        int esperado = 4;
        int resultado = Bloco3.obterUltimoAlgarismoDoNumero(123654);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_Dois() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_Nove() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(9);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_Dez() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_TrintaEUm() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(31);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_CentoETrintaECinco() {
        int esperado = 3;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(135);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_MaiorNumeroSemImpares() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(2000000000);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeAlgarismosImpares_MaiorNumeroImpares() {
        int esperado = 10;
        int resultado = Bloco3.obterNumeroDeAlgarismosImpares(1111111111);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismos(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosAlgarismos(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_Dois() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaDosAlgarismos(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_Dez() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosAlgarismos(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_Doze() {
        int esperado = 3;
        int resultado = Bloco3.obterSomaDosAlgarismos(12);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_CentoEVinteETres() {
        int esperado = 6;
        int resultado = Bloco3.obterSomaDosAlgarismos(123);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_QuatrocentosECinquentaESeis() {
        int esperado = 15;
        int resultado = Bloco3.obterSomaDosAlgarismos(456);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_MilDuzentosETrintaEQuatro() {
        int esperado = 10;
        int resultado = Bloco3.obterSomaDosAlgarismos(1234);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismos_Maximo() {
        int esperado = 81;
        int resultado = Bloco3.obterSomaDosAlgarismos(999999999);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Dois() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Quatro() {
        int esperado = 4;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(4);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_Dez() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_SesentaEOito() {
        int esperado = 14;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(68);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_DuzentosEQuarentaESeis() {
        int esperado = 12;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(246);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosPares_DoisMilQuatrocentosESesentaEOito() {
        int esperado = 20;
        int resultado = Bloco3.obterSomaDosAlgarismosPares(2468);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Dois() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_Tres() {
        int esperado = 3;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(3);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_CentoETrintaECinco() {
        int esperado = 9;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(135);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_DuzentosEQuarentaESeis() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(246);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_MilTrescentosECinquentaESete() {
        int esperado = 16;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(1357);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarSomaDosAlgarismosImpares_TrezeMilEQuinhentosESetentaENove() {
        int esperado = 25;
        int resultado = Bloco3.obterSomaDosAlgarismosImpares(13579);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarMediaDosAlgarismos_Zero() {
        double esperado = 0;
        double resultado = Bloco3.obterMediaDosAlgarismos(0);
        assertEquals(esperado,resultado, 0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_Um() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosAlgarismos(1);
        assertEquals(esperado,resultado, 0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_Dois() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismos(2);
        assertEquals(esperado,resultado, 0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_Dez() {
        double esperado = 0.5;
        double resultado = Bloco3.obterMediaDosAlgarismos(10);
        assertEquals(esperado,resultado, 0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_Doze() {
        double esperado = 1.5;
        double resultado = Bloco3.obterMediaDosAlgarismos(12);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_Cem() {
        double esperado = 0.33;
        double resultado = Bloco3.obterMediaDosAlgarismos(100);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_CentoEOnze() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosAlgarismos(111);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_CentoEVinteETres() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismos(123);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_Mil() {
        double esperado = 0.25;
        double resultado = Bloco3.obterMediaDosAlgarismos(1000);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismos_MilEQuinhentosEDezaseis() {
        double esperado = 3.25;
        double resultado = Bloco3.obterMediaDosAlgarismos(1516);
        assertEquals(esperado,resultado,0.01);
    }


    @Test
    void verificarMediaDosAlgarismosPares_Zero() {
        double esperado = 0;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(0);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_Um() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(1);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_Dois() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(2);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_Dez() {
        double esperado = 0;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(10);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_Doze() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(12);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_Vinte() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(20);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_VinteEUm() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(21);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_VinteEDois() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(22);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_CentoESeis() {
        double esperado = 3;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(106);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_DuzentosEQuarentaESeis() {
        double esperado = 4;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(246);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_CentoEQuarentaEOito() {
        double esperado = 6;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(148);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_DezMilESeis() {
        double esperado = 1.5;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(10006);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosPares_MilTrezentosECinquentaESete() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosAlgarismosPares(1357);
        assertEquals(esperado,resultado,0.01);
    }


    @Test
    void verificarMediaDosAlgarismosImpares_Zero() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(0);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_Um() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(1);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_Dois() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(2);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_Tres() {
        double esperado = 3;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(3);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_Dez() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(10);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_Treze() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(13);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_CentoETres() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(103);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_CentoETrintaESete() {
        double esperado = 3.66;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(137);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarMediaDosAlgarismosImpares_NoveMilEQuinhentosEVinteESete() {
        double esperado = 7;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(9527);
        assertEquals(esperado,resultado,0.01);
    }


    @Test
    void verificarMediaDosAlgarismosImpares_DoisMil() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosAlgarismosImpares(2000);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void verificarNumeroInvertido_Zero() {
        long esperado = 0;
        long resultado = Bloco3.obterNumeroInvertido(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_Um() {
        long esperado = 1;
        long resultado = Bloco3.obterNumeroInvertido(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_Dez() {
        long esperado = 01;
        long resultado = Bloco3.obterNumeroInvertido(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_Doze() {
        long esperado = 21;
        long resultado = Bloco3.obterNumeroInvertido(12);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_CentoEVinteETres() {
        long esperado = 321;
        long resultado = Bloco3.obterNumeroInvertido(123);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_MilEDuzentosETrintaEQuatro() {
        long esperado = 4321;
        long resultado = Bloco3.obterNumeroInvertido(1234);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_DozeMilETrescentosEQuarentaECinco() {
        long esperado = 54321;
        long resultado = Bloco3.obterNumeroInvertido(12345);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarNumeroInvertido_CentoEDois() {
        long esperado = 201;
        long resultado = Bloco3.obterNumeroInvertido(102);
        assertEquals(esperado,resultado);
    }


}