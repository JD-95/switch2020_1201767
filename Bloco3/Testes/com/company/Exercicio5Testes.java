package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio5Testes {

    /***********************************************************/
    @Test
    void verificarSomaDosParesNumIntervaloNegUm_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(-1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloNegDois_Zero() {
        int esperado = -2;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(-2, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloNegTres_NegUm() {
        int esperado = -2;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(-3, -1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloNegCinco_NegUm() {
        int esperado = -6;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(-5, -1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloZero_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(0, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloZero_Dois() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(0, 2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloZero_Tres() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(0, 3);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloZero_Quatro() {
        int esperado = 6;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(0, 4);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosParesNumIntervaloUm_Onze() {
        int esperado = 30;
        int resultado = Bloco3.obterSomaDosParesNumIntervalo(1, 11);
        assertEquals(esperado, resultado);
    }

    /***********************************************************/

    @Test
    void verificarSeZeroEPar() {
        boolean resultado = Bloco3.verificarSeEPar(0);
        assertTrue(resultado);
    }

    @Test
    void verificarSeNegUmEPar() {
        boolean resultado = Bloco3.verificarSeEPar(-1);
        assertFalse(resultado);
    }

    @Test
    void verificarSeNegDoisEPar() {
        boolean resultado = Bloco3.verificarSeEPar(-2);
        assertTrue(resultado);
    }

    @Test
    void verificarSeUmEPar() {
        boolean resultado = Bloco3.verificarSeEPar(1);
        assertFalse(resultado);
    }

    @Test
    void verificarSeDoisEPar() {
        boolean resultado = Bloco3.verificarSeEPar(2);
        assertTrue(resultado);
    }

    /***********************************************************/

    @Test
    void verificarNumeroDeParesNumIntervaloZero_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(0, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloNegUm_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(-1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloNegDois_Zero() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(-2, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloNegTres_Zero() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(-3, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloNegDez_Zero() {
        int esperado = 6;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(-10, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloZero_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(0, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloZero_Dois() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(0, 2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloZero_Tres() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(0, 3);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeParesNumIntervaloZero_Dez() {
        int esperado = 6;
        int resultado = Bloco3.obterNumeroDeParesNumIntervalo(0, 10);
        assertEquals(esperado, resultado);
    }

    /***********************************************************/

    @Test
    void obterSomaDosImparesNumIntervaloZero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(0, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloNegUm_Zero() {
        int esperado = -1;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(-1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloNegDois_Zero() {
        int esperado = -1;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(-2, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloNegTres_Zero() {
        int esperado = -4;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(-3, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloNegDez_Zero() {
        int esperado = -25;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(-10, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloZero_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(0, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloZero_Dois() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(0, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloZero_Tres() {
        int esperado = 4;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(0, 3);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosImparesNumIntervaloZero_Dez() {
        int esperado = 25;
        int resultado = Bloco3.obterSomaDosImparesNumIntervalo(0, 10);
        assertEquals(esperado, resultado);
    }

    /***********************************************************/

    @Test
    void obterNumeroDeImparesNumIntervaloZero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(0, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloUm_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegUm_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(-1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegDois_Zero() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(-2, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegTres_Zero() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(-3, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegDez_Zero() {
        int esperado = 5;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(-10, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegZero_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(0, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegZero_Dois() {
        int esperado = 1;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(0, 2);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegZero_Tres() {
        int esperado = 2;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(0, 3);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterNumeroDeImparesNumIntervaloNegZero_Dez() {
        int esperado = 5;
        int resultado = Bloco3.obterNumeroDeImparesNumIntervalo(0, 10);
        assertEquals(esperado, resultado);
    }

    /***********************************************************/

    @Test
    void obterSomaDosMultiplosDeZeroNumIntervaloZero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(0, 0, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeZeroNumIntervaloZero_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(0, 1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeUmNumIntervaloUm_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(1, 1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeDoisNumIntervaloQuatro_Quatro() {
        int esperado = 4;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(4, 4, 2);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeUmNumIntervaloZero_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(0, 1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeUmNumIntervaloZero_Dois() {
        int esperado = 3;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(0, 2, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeUmNumIntervaloZero_Tres() {
        int esperado = 6;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(0, 3, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void obterSomaDosMultiplosDeDoisNumIntervaloZero_Tres() {
        int esperado = 2;
        int resultado = Bloco3.obterSomaDosMultiplosNumIntervalo(0, 3, 2);
        assertEquals(esperado, resultado);
    }

    /***********************************************************/

    @Test
    void verificarProdutoDosMultiplosDeZeroNumIntervaloZero_Zero() {
        int esperado = 0;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(0, 0, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarProdutoDosMultiplosDeZeroNumIntervaloZero_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(0, 1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarProdutoDosMultiplosDeUmNumIntervaloZero_Um() {
        int esperado = 0;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(0, 1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarProdutoDosMultiplosDeUmNumIntervaloZero_Dois() {
        int esperado = 0;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(0, 2, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarProdutoDosMultiplosDeUmNumIntervaloUm_Um() {
        int esperado = 1;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(1, 1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarProdutoDosMultiplosDeUmNumIntervaloUm_Dois() {
        int esperado = 2;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(1, 2, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarProdutoDosMultiplosDeUmNumIntervaloUm_Tres() {
        int esperado = 6;
        int resultado = Bloco3.obterProdutoDosMultiplosNumIntervalo(1, 3, 1);
        assertEquals(esperado, resultado);
    }

    /***********************************************************/

    @Test
    void verificarMediaDosMultiplosDeZeroNumIntervaloZero_Zero() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(0, 0, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeZeroNumIntervaloZero_Um() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(0, 1, 0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeDezNumIntervaloUm_Cinco() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(1, 5, 10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloZero_Zero() {
        double esperado = 0;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(0, 0, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloZero_Um() {
        double esperado = 0.5;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(0, 1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloZero_Dois() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(0, 2, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloZero_Tres() {
        double esperado = 1.5;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(0, 3, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloUm_Um() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(1, 1, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloUm_Dois() {
        double esperado = 1.5;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(1, 2, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloUm_Tres() {
        double esperado = 2;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(1, 3, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloNegTres_Tres() {
        double esperado = 0;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(-3, 3, 1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarMediaDosMultiplosDeUmNumIntervaloNegQuatro_Tres() {
        double esperado = -0.5;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(-4, 3, 1);
        assertEquals(esperado, resultado, 0.01);
    }

    @Test
    void verificarMediaDosMultiplosDeDoisNumIntervaloNegSeis_Quatro() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosMultiplosNumIntervalo(-6, 4, 2);
        assertEquals(esperado, resultado, 0.01);
    }

    /***********************************************************/

    @Test
    void obterMediaDosMultiplosDeZero_ZeroNumIntervaloZero_Zero() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,0,0,0);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeUm_ZeroNumIntervaloZero_Zero() {
        double esperado = 0.0;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,0,1,0);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeUm_UmNumIntervaloZero_Zero() {
        double esperado = 0.0;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,0,1,1);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeZero_UmNumIntervaloZero_Um() {
        double esperado = 0.5;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,1,0,1);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeZero_UmNumIntervaloZero_Dois() {
        double esperado = 1;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,2,0,1);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeZero_UmNumIntervaloZero_Tres() {
        double esperado = 1.5;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,3,0,1);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeUm_DoisNumIntervaloZero_Cinco() {
        double esperado = 2.33;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(0,5,1,2);
        assertEquals(esperado,resultado,0.01);
    }

    @Test
    void obterMediaDosMultiplosDeSeis_DezNumIntervaloUm_Cinco() {
        double esperado = -1;
        double resultado = Bloco3.obterMediaDosMultiplosDeDoisNumerosNumIntervalo(1,5,6,10);
        assertEquals(esperado,resultado,0.01);
    }


}