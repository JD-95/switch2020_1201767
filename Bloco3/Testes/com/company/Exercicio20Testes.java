package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio20Testes {

    @Test
    void verificarSomaDosDivisoresDeUm() {
        int esperado = 0 ;
        int resultado = Bloco3.obterSomaDosDivisores(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeDois() {
        int esperado = 1 ;
        int resultado = Bloco3.obterSomaDosDivisores(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeTres() {
        int esperado = 1 ;
        int resultado = Bloco3.obterSomaDosDivisores(3);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeQuatro() {
        int esperado = 3 ;
        int resultado = Bloco3.obterSomaDosDivisores(4);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeCinco() {
        int esperado = 1 ;
        int resultado = Bloco3.obterSomaDosDivisores(5);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeSeis() {
        int esperado = 6 ;
        int resultado = Bloco3.obterSomaDosDivisores(6);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeSete() {
        int esperado = 1 ;
        int resultado = Bloco3.obterSomaDosDivisores(7);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeOito() {
        int esperado = 7 ;
        int resultado = Bloco3.obterSomaDosDivisores(8);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeNove() {
        int esperado = 4 ;
        int resultado = Bloco3.obterSomaDosDivisores(9);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSomaDosDivisoresDeDez() {
        int esperado = 8 ;
        int resultado = Bloco3.obterSomaDosDivisores(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarClassificacaoDoZero() {
        String esperado = "Número inválido" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(0);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoUm() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(1);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoDois() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(2);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoTres() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(3);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoQuatro() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(4);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoCinco() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(5);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoSeis() {
        String esperado = "Perfeitos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(6);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoSete() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(7);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoOito() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(8);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoNove() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(9);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoDez() {
        String esperado = "Reduzidos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(10);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoVinteEOito() {
        String esperado = "Perfeitos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(28);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoQuatrocentosENoventaESeis() {
        String esperado = "Perfeitos" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(496);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoDoze() {
        String esperado = "Abundantes" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(12);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoDezoito() {
        String esperado = "Abundantes" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(18);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoVinte() {
        String esperado = "Abundantes" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(20);
        assertEquals(esperado,resultado);
    }

    @Test
    void verificarClassificacaoDoVinteEQuatro() {
        String esperado = "Abundantes" ;
        String resultado = Bloco3.obterClassificacaoDoNumero(24);
        assertEquals(esperado,resultado);
    }
}