package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio11Testes {

    @Test
    void verificarNumeroDeSomasPossiveisParaObterUm() {
        String esperado = "Existe 1 maneira diferente de obter o número 1 : 0+1 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterDois() {
        String esperado = "Existem 2 maneiras diferentes de obter o número 2 : 0+2 1+1 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(2);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterTres() {
        String esperado = "Existem 2 maneiras diferentes de obter o número 3 : 0+3 1+2 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(3);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterQuatro() {
        String esperado = "Existem 3 maneiras diferentes de obter o número 4 : 0+4 1+3 2+2 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(4);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterCinco() {
        String esperado = "Existem 3 maneiras diferentes de obter o número 5 : 0+5 1+4 2+3 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(5);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterSeis() {
        String esperado = "Existem 4 maneiras diferentes de obter o número 6 : 0+6 1+5 2+4 3+3 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(6);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterOito() {
        String esperado = "Existem 5 maneiras diferentes de obter o número 8 : 0+8 1+7 2+6 3+5 4+4 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(8);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterDez() {
        String esperado = "Existem 6 maneiras diferentes de obter o número 10 : 0+10 1+9 2+8 3+7 4+6 5+5 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterQuinze() {
        String esperado = "Existem 3 maneiras diferentes de obter o número 15 : 5+10 6+9 7+8 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(15);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterVinte() {
        String esperado = "Existe 1 maneira diferente de obter o número 20 : 10+10 ";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(20);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterZero() {
        String esperado = "Número fora do intervalo";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarNumeroDeSomasPossiveisParaObterVinteEUm() {
        String esperado = "Número fora do intervalo";
        String resultado = Bloco3.obterNumeroDeSomasPossiveisParaObterUmNumero(21);
        assertEquals(esperado, resultado);
    }

}