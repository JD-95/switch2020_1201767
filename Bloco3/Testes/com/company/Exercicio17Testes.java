package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio17Testes {

    @Test
    void verificarCategoriaRacaCao_InvalidoTeste1() {
        String esperado = "Peso inválido";
        String resultado = Bloco3.obterCategoriaRacaCao(0);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_InvalidoTeste2() {
        String esperado = "Peso inválido";
        String resultado = Bloco3.obterCategoriaRacaCao(-1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_PequenaTeste1() {
        String esperado = "Pequena";
        String resultado = Bloco3.obterCategoriaRacaCao(1);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_PequenaTeste2() {
        String esperado = "Pequena";
        String resultado = Bloco3.obterCategoriaRacaCao(10);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_PequenaTeste3() {
        String esperado = "Pequena";
        String resultado = Bloco3.obterCategoriaRacaCao(5.438);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_MediaTeste1() {
        String esperado = "Média";
        String resultado = Bloco3.obterCategoriaRacaCao(11);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_MediaTeste2() {
        String esperado = "Média";
        String resultado = Bloco3.obterCategoriaRacaCao(25);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_MediaTeste3() {
        String esperado = "Média";
        String resultado = Bloco3.obterCategoriaRacaCao(19.852);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_GrandeTeste1() {
        String esperado = "Grande";
        String resultado = Bloco3.obterCategoriaRacaCao(26);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_GrandeTeste2() {
        String esperado = "Grande";
        String resultado = Bloco3.obterCategoriaRacaCao(45);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_GrandeTeste3() {
        String esperado = "Grande";
        String resultado = Bloco3.obterCategoriaRacaCao(32.549);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_GiganteTeste1() {
        String esperado = "Gigante";
        String resultado = Bloco3.obterCategoriaRacaCao(46);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_GiganteTeste2() {
        String esperado = "Gigante";
        String resultado = Bloco3.obterCategoriaRacaCao(60);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarCategoriaRacaCao_GiganteTeste3() {
        String esperado = "Gigante";
        String resultado = Bloco3.obterCategoriaRacaCao(52.468);
        assertEquals(esperado, resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Pequena_Sim() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Pequena", 100);
        assertTrue(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Pequena_NaoTeste1() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Pequena", 99);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Pequena_NaoTeste2() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Pequena", 101);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Media_Sim() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Média", 250);
        assertTrue(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Media_NaoTeste1() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Média", 249);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Media_NaoTeste2() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Média", 251);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Grande_Sim() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Grande", 300);
        assertTrue(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Grande_NaoTeste1() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Grande", 299);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Grande_NaoTeste2() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Grande", 301);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Gigante_Sim() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Gigante", 500);
        assertTrue(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Gigante_NaoTeste1() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Gigante", 499);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Gigante_NaoTeste2() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Gigante", 501);
        assertFalse(resultado);
    }

    @Test
    void verificarSeRacaoAdequada_Invalido() {
        boolean resultado = Bloco3.verificarSeRacaoAdequada("Peso inválido", 100);
        assertFalse(resultado);
    }
}